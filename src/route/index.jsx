// dashbaord
import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import Default from '../components/dashboard/default'
import UserEdit from "../components/users/userEdit"
import StudentView from "../components/studentManagement"
import Report from "../components/report"
import FaqForm from '../components/faqForm'
import Menus from '../components/manageMenus'
import KnowledgebaseComponent from "../components/knowledgebase"
import Chat from "../components/application/chat-app"
import KbCategory from "../components/kbCategory";
import Responses from '../components/response';
import Model from '../components/model';
import Programs from '../components/managePrograms';
import InsidePrograms from '../components/insideManagePrograms';
import FranchiseSchools from '../components/franchiseSchools';
import UploadStudent from '../components/uploadStudentData';
import ManageDirectAdmission from '../components/manageDirectAdmission';
import IndicatorsStatistics from '../components/indicators&statistics';
import QualificationsDetails from '../components/qualificationsDetails';
import AppointmentByProgCode from '../components/appointmentByProgCode';
import SecondScreenIngrates from '../components/secondScreenIngrates';


export const routes = [
  { path: "/dashboard/:layout/", Component: Default },
  { path: "/app/kbcategory/:layout/", Component: KbCategory, type: 'Agent'},
  { path: "/app/faqform/:layout/", Component: FaqForm, type: 'Agent'},
  { path: "/app/users/userEdit/:layout", Component: UserEdit, type:'Agent' },
  { path: "/app/responses/:layout", Component: Responses, type:'Agent' },
  { path: "/app/models/:layout", Component: Model, type:'Agent' },
  { path: "/app/students/:layout", Component: StudentView },
  { path: "/app/reports/:layout", Component: Report },
  { path: "/app/knowledgebase/:layout", Component: KnowledgebaseComponent },
  { path: "/app/chat-app/:layout", Component: Chat },
  { path: "/app/menus/programs/:layout", Component: Programs,type: 'Agent' },
  { path: "/app/menus/:layout/", Component: Menus, type: 'Agent'},
  { path: "/app/upload/:layout", Component: UploadStudent,type: 'Agent' },
  { path: "/app/menus/inside-Programs/:layout", Component: InsidePrograms,type: 'Agent' },
  { path: "/app/menus/manage-direct-admission/:layout", Component: ManageDirectAdmission,type: 'Agent' },
  { path: "/app/menus/franchise-schools/:layout", Component: FranchiseSchools,type: 'Agent' },
  { path: "/app/menus/indicators&statistics/:layout", Component: IndicatorsStatistics,type: 'Agent' },
  { path: "/app/menus/qualificationsDetails/:layout", Component: QualificationsDetails,type: 'Agent' },
  { path: "/app/menus/appointmentByProgCode/:layout", Component: AppointmentByProgCode,type: 'Agent' },
  { path: "/app/menus/secondScreenIngrates/:layout", Component: SecondScreenIngrates,type: 'Agent' }
]