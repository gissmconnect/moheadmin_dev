import React from 'react'
import { useIdleTimer } from 'react-idle-timer'
import { useHistory } from 'react-router'
import Swal from "sweetalert2";

const SESSION_IDEL_MINUTES = 5;

const AutoLagoutTimer = (props: any) => {
    const { ComposedClass } = props
    const history = useHistory()

    const handleOnIdle = (event: any) => {
        // SHOW YOUR MODAL HERE AND LAGOUT
        console.log('user is idle', event)
        console.log('last active', getLastActiveTime())
        Swal.fire({
            icon: "info",
            title: "Session Timeout",
            text: "This session has ended due to inactivity.",
            showDenyButton: false,
            showCancelButton: false,
            confirmButtonText: "Login",
            denyButtonText: `Don't save`,
            allowOutsideClick: false
          }).then(async (result) => {
            if (result.isConfirmed) {
                history.push(`${process.env.PUBLIC_URL}/login`)
            } else if (result.isDenied) {
              Swal.fire("Changes are not saved", "", "info");
            }
          });
    }

    const {getLastActiveTime } = useIdleTimer({
        timeout: 1000 * 60 * SESSION_IDEL_MINUTES,
        onIdle: handleOnIdle,
        debounce: 500,
    })

    return <ComposedClass />
}

export default AutoLagoutTimer;