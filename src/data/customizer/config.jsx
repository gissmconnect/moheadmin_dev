export class ConfigDB {
	static data = {
		settings: {
			layout_type: 'ltr',
			sidebar: {
				type: 'compact-wrapper',
			},
			sidebar_setting: 'default-sidebar',
		},
		color: {
			// primary_color: '#7366ff',
			primary_color: '#ed222b',
			// secondary_color: '#f73164',
			secondary_color: '#7366ff',
			mix_background_layout: 'light-only',
		},
		router_animation: 'fadeIn'
	}
}
export default ConfigDB;




