import { Home, Airplay, Box, FolderPlus, Command, Cloud,Upload, Download, FileText, Server, BarChart, Users, UserPlus, Layers, ShoppingBag, List, Mail, MessageCircle, GitPullRequest, Monitor, Heart, Clock, Zap, CheckSquare, Calendar, Image, Film, HelpCircle, Radio, Map, Edit, Sunrise, Package } from 'react-feather'
export const MENUITEMS = [

    {
        menutitle: "General",
        menucontent: "Dashboards,Widgets",
        Items: [
            { path: `${process.env.PUBLIC_URL}/dashboard`, icon: Home, title: 'Dashboard', active: false, type: 'link' },
            { path: `${process.env.PUBLIC_URL}/app/upload`, icon: Upload, title: 'Upload Student Data', type: 'link', role: 'Agent' },
            // { path: `${process.env.PUBLIC_URL}/app/programs`, icon: Command, title: 'Programs', type: 'link', role: 'Agent' },
            // { path: `${process.env.PUBLIC_URL}/app/menus/inside-Programs`, icon: UserPlus, type: 'link', title: 'Inside Programs', role: 'Agent' },
            { path: `${process.env.PUBLIC_URL}/app/chat-app`, icon: MessageCircle, title: 'Chat', type: 'link', role: 'Admin' },
            { path: `${process.env.PUBLIC_URL}/app/knowledgebase`, icon: Sunrise, type: 'link', active: false, title: 'Knowledgebase' },
            { path: `${process.env.PUBLIC_URL}/app/users/userEdit`, icon: UserPlus, type: 'link', title: 'User Management', role: 'Agent' },
            { path: `${process.env.PUBLIC_URL}/app/Students`, icon: Users, type: 'link', title: 'Students Management' },
            { path: `${process.env.PUBLIC_URL}/app/kbcategory`, icon: Cloud, title: 'KBCategory Management', type: 'link', role: 'Agent' },
            { path: `${process.env.PUBLIC_URL}/app/reports`, icon: Edit, title: 'Reports', type: 'link' },
            { path: `${process.env.PUBLIC_URL}/app/menus`, icon: Layers, title: 'Manage Menus', type: 'link', role: 'Agent' },
            { path: `${process.env.PUBLIC_URL}/app/responses`, icon: FolderPlus, title: 'Responses', type: 'link',role: 'Agent' },
            { path: `${process.env.PUBLIC_URL}/app/faqform`, icon: FileText, title: 'Faq Forms', type: 'link',role: 'Agent' },
            { path: `${process.env.PUBLIC_URL}/app/models`, icon: Server, title: 'Models', type: 'link',role: 'Agent' },
           
        ]
    },
]