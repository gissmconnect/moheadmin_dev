import { SERVER_URL, CHATBOT_EDIT } from "../config";
import { toast } from "react-toastify";

export const getFaq = async () => {
  // const token = localStorage.getItem("user_id");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/api/faqData/all`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        // Authorization: `Bearer ${token}`,
      },
    });
    if (response.status === 200) {
      let result = await response.json();
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getRes = async (faqName) => {
  const data = { faqName };
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/api/faqData?faq=${faqName}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${token}`
        },
      }
    );
    // console.log(response, "response--------")
    if (response.status === 200) {
      let result = await response.json();
      // toast.success('Updated Successfully')
      // console.log(result, "result--------")
      return result;
    }
    // toast.error('Something Went Wrong!');
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const uploadFile = async (file, pdf) => {
  const token = localStorage.getItem("token");

  if (pdf === "pdf") {
    var formData = new FormData();
    formData.append("file", file);
    var url = `${SERVER_URL}/upload/attachement`;
  } else if (pdf === "video") {
    var formData = new FormData();
    formData.append("video", file);
    var url = `${SERVER_URL}/upload/attachement/video`;
  }
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    });
    if (response.status === 200) {
      let result = await response.json();
      toast.success("Uploaded Successfully");
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};

export const uploadFileNew = async (file, pdf) => {
  // console.log(file, pdf, "file, pdf----")
  const token = localStorage.getItem("token");
  if (pdf === true) {
    var formData = new FormData();
    formData.append("file", file);
    var url = `${SERVER_URL}/upload/attachement`;
  } else if (pdf === false) {
    var formData = new FormData();
    formData.append("video", file);
    var url = `${SERVER_URL}/upload/attachement/video`;
  }
  try {
    const response = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    });
    if (response.status === 200) {
      let result = await response.json();
      toast.success("Uploaded Successfully");
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};

export const AddResponse = async (datas) => {
  const data = datas;
  try {
    const response = await fetch(`${CHATBOT_EDIT}/api/faqData/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(data),
    });
    if (response.status === 200) {
      let result = await response.json();
      toast.success(result.message);
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const AddUpdate = async (datas, id) => {
  const data = datas;
  try {
    const response = await fetch(`${CHATBOT_EDIT}/api/faqData/add?id=${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(data),
    });
    if (response.status === 200) {
      let result = await response.json();
      toast.success(result.message);
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const deleteFaq = async (faqName) => {
  // const token = localStorage.getItem("user_id");
  const data = { faqName };
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/api/faqData/delete?faq=${faqName}`,
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${token}`
        },
      }
    );
    // console.log(response);
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result);
      if (result) {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};
