import { SERVER_URL } from '../config';
import { toast } from 'react-toastify';
import { ResultSetPager } from 'stanza/helpers/RSM';

export const uploadExl = async (file, offer) => {
    var formData = new FormData();
    formData.append("file", file);
    const token = localStorage.getItem("token");
    if (offer === "1") {
        var url = `${SERVER_URL}/student/import`;
    } else if (offer === "2") {
        var url = `${SERVER_URL}/student/import/offer`;
    } else if (offer === "3") {
        var url = `${SERVER_URL}/student/import/offer1`;
    } else if (offer === "4") {
        var url = `${SERVER_URL}/student/registration`;
    } else if (offer === "5") {
        var url = `${SERVER_URL}/student/registration1`;
    } else if (offer === "6") {
        var url = `${SERVER_URL}/student/cutoff`;
    }
    console.log(url)
    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`
            },
            body: formData
        })
        console.log(response)
        if (response.status === 200) {
            let result = await response.json();
            console.log(result)
            if (result.status === "OK") {
                toast.success('Uploaded Successfully')

            } else if (result.success === true ) {
                toast.success('Uploaded Successfully') 
            } else if (result.status === "NOK") {
                toast.error('Something went wrong!')

            }

            return result;
        } else if (response.status === 504) {
            let result = "timeout"
            toast.info('Processing in background!')
            return result;
        }
        toast.error('Something Went Wrong!');
        return []
    } catch (error) {
        console.log(error)
        toast.error('Something Went Wrong!');
    }
}