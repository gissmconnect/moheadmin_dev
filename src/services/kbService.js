import { SERVER_URL } from '../config';
import { toast } from 'react-toastify';

export const getKbCategory = async () => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/category`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result.success === true) {
                return result.data;
            }
        }
        return []
    } catch (error) {
        console.log(error)

    }
}

export const addCategory = async (type) => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/category`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({ type })
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result && result.success === true) {
                toast.success('Added Successfully')
            } else {
                toast.error('Internal Server Error!');
            }
        }
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}
export const search = async (search, sort, category) => {
   
    const token = localStorage.getItem("token");
const data={search, sort, category}
    try {
        const response = await fetch(`${SERVER_URL}/knowledge/search`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
        })
        if (response.status === 200) {
            let result = await response.json();
            console.log(result)
            if (result.success === true) {
                toast.success('Done')
                return result.data;
               
            } 
        }
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}
export const updateCategory = async (type, id) => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/category/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({ type })
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result && result.success === true) {
                toast.success('Updated Successfully')
            } else {
                toast.error('Internal Server Error!');
            }
        }
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}

export const deleteCategory = async (id) => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/category/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result && result.success === true) {
                toast.success('Deleted Successfully')
            }
            else {
                toast.error('Internal Server Error!');
            }
        }
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}


export const getKB = async () => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/knowledge/getAll`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            if(result.success === true){
                return result.data;
            }
        }
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}

export const postKBData = async (category, kb_title, kb_body, kb_keywords, kb_published) => {
    const token = localStorage.getItem("token");
    const data = {
        category, kb_title, kb_body, kb_keywords, kb_published
    }
    try {
        const response = await fetch(`${SERVER_URL}/knowledge/insert_kb`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
        })
        if (response.status === 200) {
            let result = await response.json();
            toast.success("Added Successfully")
            return result;
        }
        toast.error('Internal Server Error!');
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}

export const updateData = async (id, category, kb_title, kb_body, kb_keywords, kb_published) => {
    const token = localStorage.getItem("token");
    const data ={
        category, kb_title, kb_body, kb_keywords, kb_published
    }
    try {
        const response = await fetch(`${SERVER_URL}/knowledge/update_kb/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result && result.success === true) {
                toast.success('Updated Successfully')
                return result
            } else {
                toast.error('Internal Server Error!');
            }
        }
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}