import { SERVER_URL } from '../config'


export const getStudents = async () => {
    // const token = localStorage.getItem("user_id");
    try {
        const response = await fetch(`${SERVER_URL}/user/getStudent`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Bearer ${token}`
            },
            // body: JSON.stringify(item)
        })
        if (response.status === 200) {
            
            let result = await response.json();
            
            return result.data;
        }
       
        return []
    } catch (error) {
        console.log(error)
       
    }
}