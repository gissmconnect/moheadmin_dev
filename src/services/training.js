import React from 'react'
import { CHATBOT_EDIT } from "../config"
import { toast } from 'react-toastify';


export const train = async () => {
    // toast.info('Training Started');
    // const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${CHATBOT_EDIT}/api/faqData/train`, {
            method: 'POST',
            headers: {
                // 'Authorization': `Bearer ${token}`
            },
        })
        if (response.status === 200) {
            let result = await response.json();
            console.log(result, "resu")
            localStorage.setItem('train', result);
            toast.success('Training Completed')

            return result;
        } else {
            console.log("hhdhd")
        }
    } catch (error) {
        console.log(error)
        toast.error('Something Went Wrong!');
    }
}