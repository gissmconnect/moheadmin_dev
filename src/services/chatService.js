import axios from 'axios';
import { RASA_SERVER_URL, SERVER_URL } from '../config';

axios.interceptors.response.use(
    function (response) {
      console.log('interceptor response', response);
      return response;
    },
    function (error) {
      console.log('interceptor response error', error);
      return Promise.reject(error);
    },
  );
  
  axios.interceptors.request.use(
    function (response) {
      console.log('interceptor request', response);
      return response;
    },
    function (error) {
      console.log('interceptor request error', error);
      return Promise.reject(error);
    },
  );

// export const registerChatUser = (username) => {
//     return axios.post(`${SERVER_URL}/chat/userRegister`, {username})
// }

export const getQueueChatUsers = () => {
    return axios.get(`${SERVER_URL}/chat/getQueue`)
}

export const userMapping = (user, agent) => {
    return axios.post(`${SERVER_URL}/chatagent/mapping`, {user, agent})
}

export const getTransferedQueue = (currentagent) => {
    return axios.post(`${SERVER_URL}/chatagent/transfered_queue`, {currentagent})
}

export const sendTransferRequest = (nextagent, currentagent, transferuser) => {
    return axios.post(`${SERVER_URL}/chatagent/transfer_request`, {nextagent, currentagent, transferuser})
}

export const getAcativeAgents = () => {
    return axios.get(`${SERVER_URL}/chatagent/getAgent`)
}

export const getTransferredChatLogs = (user) => {
    return axios.post(`${SERVER_URL}/chatagent/chat_transfer`, {user})
}

// export const getWebMessages = (data) => {
//     return axios.post(`https://mohe.omantel.om/rasaapp/api/dialog/getWebMessage`, data)
// }

export const getWebMessages = (data) => {
    return axios.post(`${RASA_SERVER_URL}/dialog/getWebMessage`, data)
}
