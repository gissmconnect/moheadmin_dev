import { SERVER_URL } from '../config'
import { toast } from 'react-toastify';


export const getReports = async () => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/dialog/getAllMessages`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            return result.data;
        }
        return []
    } catch (error) {
        console.log(error)
    }
}

export const getPdf = async (limit) => {
    const token = localStorage.getItem("token");
    console.log(limit, "limit")
    try {
        const response = await fetch(`${SERVER_URL}/dialog/pdfReport?limit=${limit}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            return result.link;
        }
        return []
    } catch (error) {
        console.log(error)
    }
}

export const getExcel = async (limit) => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/dialog/xlReport?limit=${limit}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            return result.link;
        }
        return []
    } catch (error) {
        console.log(error)
    }
}