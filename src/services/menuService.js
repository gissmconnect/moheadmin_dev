import { CHATBOT_EDIT, SERVER_URL } from "../config";
import { toast } from "react-toastify";

export const getMainMenu = async () => {
  // const token = localStorage.getItem("user_id");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/main_menu`, {
      method: "GET",
    });
    if (response.status === 200) {
      let result = await response.json();
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getSubMenu = async (item) => {
  // const token = localStorage.getItem("user_id");
  // console.log(item, "item");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/sub_menu/${item}`, {
      method: "GET",
    });
    // console.log(response);
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result);
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getCollege = async (item) => {
  // const token = localStorage.getItem("user_id");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/college_details${item}`, {
      method: "GET",
    });
    if (response.status === 200) {
      let result = await response.json();
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addCollege = async (data) => {
  var newData = data[1];
  // console.log(data[1], "data[1]")
  try {
    const response = await fetch(`${CHATBOT_EDIT}/college_add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const updateCollege = async (data) => {
  // console.log(JSON.stringify(data[0]),"data-------")
  var newData = data;
  // console.log(newData, "newData----")
  try {
    const response = await fetch(`${CHATBOT_EDIT}/college_update`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    // console.log(response, "response-------");
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Updated Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const deleteCollege = async (id) => {
  try {
    const response = await fetch(`${CHATBOT_EDIT}/college_delete?id=${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    });
    // console.log(response, "response-------");
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Delete Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addSubmenus = async (title, id, uniqueTitle) => {
  // console.log(title, id, uniqueTitle);
  const data1 = {
    title: title,
    payload: `/choose_option{\"option\":\"${uniqueTitle}\"}`,
  };
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/sub_menu_update/v2?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data1),
      }
    );
    // console.log(response, "response--");
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result, "result--");
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addSubmenusIndicatorsStatistics = async (title, id) => {
  // console.log(title, id, uniqueTitle);
  const data1 = {
    title: title,
    payload: `/choose_statistic{\"student_type\":\"${title}\"}`,
  };
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/sub_menu_update/v2?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data1),
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        // toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addSubmenusFranchiseSchools = async (title, id) => {
  const data1 = {
    title: title,
    payload: `/choose_schools_details{\"area\":\"${title}\"}`,
  };
  // console.log(data1, id, "uniqueTitle--");
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/sub_menu_update/v2?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data1),
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result, "result--")
      if (result.status === "done") {
        // toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const updateSubmenus = async (
  title,
  oldTitle,
  id,
  uniqueTitle,
  utterance,
  intent,
  slot
) => {
  let data1 = {};
  if (utterance && !title && !oldTitle) {
    data1 = {
      utterance: utterance,
    };
    try {
      const response = await fetch(
        `${CHATBOT_EDIT}/sub_menu_update/v2?id=${id}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data1),
        }
      );
      if (response.status === 200) {
        let result = await response.json();
        if (result.status === "done") {
          toast.success("Updated Successfully");
        }
        return result;
      }
      return [];
    } catch (error) {
      console.log(error);
    }
  } else {
    data1 = {
      title: title,
      payload: `/${intent}{\"${slot}\":\"${uniqueTitle}\"}`,
    };
    try {
      const response = await fetch(
        `${CHATBOT_EDIT}/sub_menu_update/v2?id=${id}&title=${oldTitle}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(data1),
        }
      );
      if (response.status === 200) {
        let result = await response.json();
        if (result.status === "done") {
          toast.success("Updated Successfully");
        }
        return result;
      }
      return [];
    } catch (error) {
      console.log(error);
    }
  }

  // const data1 = {
  //   title: title,
  //   payload: `/choose_option{\"option\":\"${uniqueTitle}\"}`,
  // };
  console.log(data1, "data1---");
  // try {
  //   const response = await fetch(
  //     `${CHATBOT_EDIT}/sub_menu_update/v2?id=${id}&title=${oldTitle}`,
  //     {
  //       method: "POST",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //       body: JSON.stringify(data1),
  //     }
  //   );
  //   console.log(response);
  //   if (response.status === 200) {
  //     let result = await response.json();
  //     if (result.status === "done") {
  //       toast.success("Updated Successfully");
  //     }
  //     return result;
  //   }
  //   return [];
  // } catch (error) {
  //   console.log(error);
  // }
};

export const addMainmenus = async (title, id, uniqueTitle) => {
  // console.log(title, id, uniqueTitle);
  const data1 = {
    title: title,
    payload: `/choose_option{\"option\":\"${uniqueTitle}\"}`,
  };
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/main_menu_update/v2?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data1),
      }
    );
    // console.log(response, "response--");
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result, "result--");
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const updateMainmenus = async (title, oldTitle, id, uniqueTitle) => {
  const data1 = {
    title: title,
    payload: `/choose_option{\"option\":\"${uniqueTitle}\"}`,
  };
  // console.log(data1);
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/main_menu_update/v2?id=${id}&title=${oldTitle}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data1),
      }
    );
    // console.log(response);
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Updated Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const deleteMainmenus = async (title, id, uniqueTitle) => {
  if (title === "خروج") {
    var data1 = {
      title: title,
      payload: `/restart`,
    };
  } else if (title === "القائمة الرئيسية") {
    var data1 = {
      title: title,
      payload: `/main_menu`,
    };
  } else {
    var data1 = {
      title: title,
      payload: `/choose_option{\"option\":\"${uniqueTitle}\"}`,
    };
  }
  // console.log(data1, "data1");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/main_menu_delete?id=${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data1),
    });
    // console.log(response);
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result);
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};

export const deleteSubmenus = async (title, id, uniqueTitle, intent, slot) => {
  if (title === "خروج") {
    var data1 = {
      title: title,
      payload: `/restart`,
    };
  } else if (title === "القائمة الرئيسية") {
    var data1 = {
      title: title,
      payload: `/main_menu`,
    };
  } else {
    var data1 = {
      title: title,
      payload: `/${intent}{\"${slot}\":\"${uniqueTitle}\"}`,
    };
  }

  // console.log(data1, "data1");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/sub_menu_delete?id=${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data1),
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};

export const deleteFranchiseSubmenus = async (title, id, uniqueTitle) => {
  const data1 = {
    title: title,
    payload: `/choose_schools_details{\"area\":\"${uniqueTitle}\"}`,
  };
  // console.log(data1, id, "data1-----");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/sub_menu_delete?id=${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data1),
    });
    console.log(response);
    if (response.status === 200) {
      let result = await response.json();
      console.log(result);
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};

export const deleteSubmenusUtterance = async (id, utterance) => {
  let data = { utterance: utterance };
  try {
    const response = await fetch(`${CHATBOT_EDIT}/sub_menu_delete?id=${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    // console.log(response);
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result);
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};

export const deleteStructure = async (id) => {
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/sub_menu_deleteall?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};

export const AddMenus = async (
  title,
  subName,
  uniqueTitle,
  unkTitle,
  utterance,
  responseType,
  fileData,
  keywordValue
) => {
  var datas = {};
  if (responseType === "Sub_Menu") {
    datas = {
      category: title,
      type: "sub menu",
      unique_title: uniqueTitle,
      data: [
        {
          title: subName,
          payload: `/choose_option{\"option\":\"${unkTitle}\"}`,
        },
      ],
      utterance: utterance
        ? utterance
        : "Please select from the following options",
    };
  } else if (responseType === "Text_Message_only") {
    datas = {
      category: title,
      type: "sub menu",
      unique_title: uniqueTitle,
      data: [],
      utterance: keywordValue,
    };
  } else if (responseType === "Video/Pdf_Message") {
    datas = {
      category: title,
      type: "sub menu",
      unique_title: uniqueTitle,
      data: [],
      utterance: fileData,
    };
  }
  try {
    const response = await fetch(`${CHATBOT_EDIT}/sub_menu_add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(datas),
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getFranchiseSchools = async () => {
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/school_detailsFranchise%20Schools`,
      {
        method: "GET",
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      // console.log(result, "result")
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const updateFranchiseSchools = async (data) => {
  // console.log(JSON.stringify(data[0]),"data-------")
  var newData = data;
  try {
    const response = await fetch(`${CHATBOT_EDIT}/school_updates`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    // console.log(response, "response-------");
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addFranchiseSchools = async (data) => {
  // console.log(JSON.stringify(data[0]),"data-------")
  var newData = data[1];
  try {
    const response = await fetch(`${CHATBOT_EDIT}/school_add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    // console.log(response, "response-------");
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const deletePrefecture = async (_id) => {
  try {
    const response = await fetch(`${CHATBOT_EDIT}/school_delete?id=${_id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      // body: JSON.stringify(newData),
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getStatistic = async (item) => {
  // const token = localStorage.getItem("user_id");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/statistic details${item}`, {
      method: "GET",
    });
    if (response.status === 200) {
      let result = await response.json();
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addStatistic = async (data) => {
  var newData = data[1];
  console.log(newData, "newData--");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/statistic_add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const updateStatistic = async (data) => {
  var newData = data;
  try {
    const response = await fetch(`${CHATBOT_EDIT}/statistic_update`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    // console.log(response, "response-------");
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Updated Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const deleteStatistic = async (_id) => {
  try {
    const response = await fetch(`${CHATBOT_EDIT}/statistic_delete?id=${_id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getQualificationsDetails = async (item) => {
  // const token = localStorage.getItem("user_id");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/majors_details${item}`, {
      method: "GET",
    });
    if (response.status === 200) {
      let result = await response.json();
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addCompatibleType = async (data) => {
  var newData = data[1];
  console.log(newData, "newData--");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/majors_add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const deleteCompatible = async (_id) => {
  try {
    const response = await fetch(`${CHATBOT_EDIT}/majors_delete?id=${_id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const updateCompatibleType = async (data) => {
  var newData = data;
  try {
    const response = await fetch(`${CHATBOT_EDIT}/majors_update`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newData),
    });
    // console.log(response, "response-------");
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Updated Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addMainmenusButton = async (data, id) => {
  const data1 = data;
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/main_menu_update/v2?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data1),
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addSubmenusButton = async (data, id) => {
  // console.log(title, id, uniqueTitle);
  const data1 = data;
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/sub_menu_update/v2?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data1),
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const uploadFileExcle = async (file) => {
  const token = localStorage.getItem("token");
  var formData = new FormData();
  formData.append("file", file);
  try {
    const response = await fetch(`${SERVER_URL}/upload/attachement/excel`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    });
    console.log(response, "response---");
    if (response.status === 200) {
      let result = await response.json();
      console.log(result, "result---");
      if (result) {
        toast.success("Excel Uploaded Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const submitExcleInside = async (data) => {
  const category = "داخل السلطنة";
  const unique_title = "Inside Sultanate";
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/addcolleges_by_excel?excel_url=${data}&category=${category}&unique_title=${unique_title}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const submitExcleOutside = async (data) => {
  const category = "خارج السلطنة";
  const unique_title = "Outside Sultanate";
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/addcolleges_by_excel?excel_url=${data}&category=${category}&unique_title=${unique_title}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      if (result.status === "done") {
        toast.success("Added Successfully");
      }
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};
