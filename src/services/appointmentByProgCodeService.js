import { SERVER_URL, CHATBOT_EDIT } from "../config";
import { toast } from "react-toastify";

export const getAppointmentByProgCodeList = async (item) => {
  // const token = localStorage.getItem("user_id");
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/appointmentbycode_details${item}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          // Authorization: `Bearer ${token}`,
        },
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const addList = async (datas) => {
  const data = datas;
  try {
    const response = await fetch(`${CHATBOT_EDIT}/appointmentbycode_add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(data),
    });
    if (response.status === 200) {
      let result = await response.json();
      toast.success(result.message);
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const UpdateList = async (datas, id) => {
  const data = datas;
  try {
    const response = await fetch(`${CHATBOT_EDIT}/appointmentbycode_update`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        // 'Authorization': `Bearer ${token}`
      },
      body: JSON.stringify(data),
    });
    if (response.status === 200) {
      // let result = await response.json();
      // console.log(result, "result")
      toast.success("Successfully updated");
      return [];
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const deleteAppointmentByProgCode = async (id) => {
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/appointmentbycode_delete?id=${id}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${token}`
        },
      }
    );
    console.log(response);
    if (response.status === 200) {
      let result = await response.json();
      console.log(result);
      if (result) {
        toast.success("Deleted Successfully");
      }
      return result;
    }
    toast.error("Something Went Wrong!");
    return [];
  } catch (error) {
    console.log(error);
    toast.error("Something Went Wrong!");
  }
};
