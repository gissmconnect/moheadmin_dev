import { CHATBOT_EDIT } from "../config";
import { toast } from "react-toastify";

export const getModel = async () => {
  // const token = localStorage.getItem("user_id");
  try {
    const response = await fetch(`${CHATBOT_EDIT}/api/faqData/modelList`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        // Authorization: `Bearer ${token}`,
      },
    });
    if (response.status === 200) {
      let result = await response.json();
      return result;
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getActive = async (model) => {
  try {
    const response = await fetch(
      `${CHATBOT_EDIT}/api/faqData/trained/model?model=${model}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          // 'Authorization': `Bearer ${token}`
        },
        // body: JSON.stringify(data),
      }
    );
    if (response.status === 200) {
      let result = await response.json();
      toast.success("Model Activated Successfully");
      return result;
    } else if (response.status === 400) {
      let result = await response.json();
      toast.error("Model Not Activated");
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};
