import { RASA_SERVER_URL } from "../config";
import { toast } from "react-toastify";

export const getDashboardTotalCount = async (type) => {
  const token = localStorage.getItem("token");
  try {
    const response = await fetch(`${RASA_SERVER_URL}/dialog/mixreport?type=${type}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    if (response.status === 200) {
      let result = await response.json();
      if (result.success === true) {
        return result;
      }
    }
    return [];
  } catch (error) {
    console.log(error);
  }
};

export const getDashboardTotalReport = async () => {
    const token = localStorage.getItem("token");
    try {
      const response = await fetch(`${RASA_SERVER_URL}/dialog/report`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        let result = await response.json();
        if (result.success === true) {
          return result;
        }
      }
      return [];
    } catch (error) {
      console.log(error);
    }
  };

  export const getVisitors = async (date) => {
    console.log(date)
    const token = localStorage.getItem("token");
    try {
      const response = await fetch(`${RASA_SERVER_URL}/dialog/numberOfUser?date=${date}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      console.log(response)
      if (response.status === 200) {
        let result = await response.json();
        // if (result.success === true) {
          return result;
        // }
      }
      return [];
    } catch (error) {
      console.log(error);
    }
  };

  export const getDayWiseTotalReport = async (selectedDateRange) => {
    console.log('selectedDateRange-----------', selectedDateRange)
    const token = localStorage.getItem("token");
    const date = "8/22/2021"
    
    try {
      const response = await fetch(`${RASA_SERVER_URL}/dialog/daywise?from=${selectedDateRange}`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        let result = await response.json();
        if (result.length > 0) {
          return result;
        }
      }
      return [];
    } catch (error) {
      console.log(error);
    }
  };
