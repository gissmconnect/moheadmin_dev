import { SERVER_URL } from '../config'
import { toast } from 'react-toastify';


export const getUsers = async () => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/user/getAll`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            return result;
        }
        return []
    } catch (error) {
        console.log(error)
    }
}

export const AddUser = async (nickname,username, email, password, role) => {
    const data = {
        nickname,username, email, password, role
    }
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/user/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result && result.message === "Account created successfully.") {
                toast.success('Created Successfully');
            }
            return result;
        }
        toast.error('Internal Server Error!');
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}
export const updateUser = async (nickname,username, email, role, _id) => {
    const data = {
        nickname,username, email, role
    }
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/user/update/${_id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(data)
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result && result.success === true) {
                toast.success('Updated Successfully');
            }
            return result;
        }
        toast.error('Internal Server Error!');
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}

export const deleteUser = async (id) => {
    const token = localStorage.getItem("token");
    try {
        const response = await fetch(`${SERVER_URL}/user/delete/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
        if (response.status === 200) {
            let result = await response.json();
            if (result && result.success === true) {
                toast.success('Deleted Successfully');
            }
            return result;
        }
        toast.error('Internal Server Error!');
        return []
    } catch (error) {
        console.log(error)
        toast.error('Internal Server Error!');
    }
}