import { SERVER_URL } from "../config";
import { handleResponse } from "./fack.backend";
import { toast } from "react-toastify";

export const UserLogin = async (email, password) => {
  const data = {
    email,
    password,
  };
  console.log(data, "data----------------")
  try {
    await fetch(`${SERVER_URL}/agent/login`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then(handleResponse)
      .then((user) => {
        console.log(JSON.stringify(user.data), "data----------------")
        localStorage.setItem("USERSRSR", JSON.stringify(user.data));
        localStorage.setItem("token", user.token);
        localStorage.setItem("role", user.data.role);
        localStorage.setItem("user_id", user.data._id);
        localStorage.setItem("email", user.data.email);
        localStorage.setItem("username", user.data.username);
        if (user.data.role === "Agent") {
          window.location.href = `${process.env.PUBLIC_URL}/app/chat-app/`;
        } else {
          window.location.href = `${process.env.PUBLIC_URL}/dashboard/`;
        }
        // window.location.href = `${process.env.PUBLIC_URL}/app/chat-app/`
        return user;
      });
  } catch (error) {
    // toast.error('Email or Password incorrect!');
    console.log(error);
  }
};
