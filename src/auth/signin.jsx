import React, { useState, useEffect } from "react";
// import man from '../assets/images/dashboard/profile.jpg';
import {
  Container,
  Row,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Button,
  NavItem,
  NavLink,
  Nav,
  TabContent,
  TabPane,
} from "reactstrap";
import { ToastContainer } from "react-toastify";
import { withRouter } from "react-router-dom";
import { toast } from "react-toastify";
import { UserLogin } from "services/userService";
import CustomerChatBoard from "../components/CustomerChatBoard";

const Logins = (props) => {
  // const {loginWithRedirect} = useAuth0()
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState("jwt");
  const [togglePassword, setTogglePassword] = useState(false);
  // const [rememberPassword, setRememberPassword] = useState(false);

  // function handleChechbox(e) {
  //   setRememberPassword(!rememberPassword);
  //   // const { name, value } = e.target;
  //   console.log("eeeeee check", e.target.type);
  //   // console.log("eeeeee check", e.target.checked);
  //   // console.log("eeeeee check inputs remember", rememberPassword);
  //   if (e.target.type === "checkbox") {
  //     setEmail(...email, email);
  //     setPassword(...password, password);
  //   } else {
  //     setEmail("");
  //     setPassword("");
  //   }
  //   console.log(email, password, "email password")
  // }

  const loginWithJwt = async (email, password) => {
    let errorList = [];
    if (!email) {
      toast.error("Email address could not be empty");
      errorList.push("Email address could not be empty");
    } else if (!password) {
      toast.error("Password could not be empty");
      errorList.push("Password could not be empty");
    }
    if (errorList.length === 0) {
      try {
        await UserLogin(email, password);
      } catch (error) {
        console.log(error);
      }
    }
  };

  return (
    <Container fluid={true} className="p-0">
      <Row>
        <Col xs="12">
          <div className="login-card">
            <div>
              <div>
                <a className="logo" href="index.html">
                  <img
                    className="img-fluid for-light"
                    src={require("../assets/images/logo/login.png")}
                    alt=""
                  />
                  <img
                    className="img-fluid for-dark"
                    src={require("../assets/images/logo/logo_dark.png")}
                    alt=""
                  />
                </a>
              </div>
              <div className="login-main login-tab">
                {/* <Nav className="border-tab flex-column" tabs>
                  <NavItem>
                    <NavLink className={selected === 'firebase' ? 'active' : ''} onClick={() => setSelected('firebase')}>
                      <img src={require("../assets/images/firebase.svg")} alt="" />
                      <span>{FIREBASE}</span>
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink className={selected === 'jwt' ? 'active' : ''} onClick={() => setSelected('jwt')}>
                    <img src={require("../assets/images/jwt.svg")} alt="" />
                    <span>{JWT}</span>
                    </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink className={selected === 'auth0' ? 'active' : ''} onClick={() => setSelected('auth0')}>
                    <img src={require("../assets/images/auth0.svg")} alt="" />
                    <span>{AUTH0}</span>
                    </NavLink>
                  </NavItem>
                </Nav> */}
                <TabContent activeTab={selected} className="content-login">
                  <TabPane className="fade show" tabId="jwt">
                    <Form className="theme-form">
                      <h4>Sign In</h4>
                      <p>{"Enter your email & password to login"}</p>
                      <FormGroup>
                        <Label className="col-form-label">EmailAddress</Label>
                        <Input
                          className="form-control"
                          type="email"
                          required=""
                          onChange={(e) => setEmail(e.target.value)}
                          defaultValue={email}
                        />
                      </FormGroup>
                      <FormGroup>
                        <Label className="col-form-label">Password</Label>
                        <Input
                          className="form-control"
                          type={togglePassword ? "text" : "password"}
                          onChange={(e) => setPassword(e.target.value)}
                          defaultValue={password}
                          required=""
                        />
                        <div
                          className="show-hide"
                          onClick={() => setTogglePassword(!togglePassword)}
                        >
                          <span className={togglePassword ? "" : "show"}></span>
                        </div>
                      </FormGroup>
                      <div className="form-group mb-0">
                        {/* <div className="checkbox ml-3">
                          <Input
                            id="checkbox1"
                            type="checkbox"
                            // checked={rememberPassword}
                            // onChange={(event) => handleChechbox(event)}
                          />
                          <Label className="text-muted" for="checkbox1">
                            RememberPassword
                          </Label>
                        </div> */}
                        {/* <a className="link" href="#javascript">{ForgotPassword}</a> */}
                        {/* {selected === "firebase" ?
                        <Button color="primary" className="btn-block" disabled={loading ? loading : loading} onClick={(e) => loginAuth(e)}>{loading ? "LOADING..." : SignIn }</Button>
                        : */}
                        <br></br>
                        <Button
                          color="primary"
                          className="btn-block"
                          onClick={() => loginWithJwt(email, password)}
                        >
                          Sign In
                        </Button>
                        {/* } */}
                      </div>
                      {/* <h6 className="text-muted mt-4 or">{"Or Sign in with"}</h6>
                      <div className="social mt-4">
                        <div className="btn-showcase">
                          <Button color="light" onClick={facebookAuth} >
                            <Facebook className="txt-fb" />
                          </Button>
                          <Button color="light" onClick={googleAuth} >
                            <i className="icon-social-google txt-googleplus"></i>
                          </Button>
                          <Button color="light" onClick={twitterAuth} >
                            <Twitter className="txt-twitter" />
                          </Button>
                          <Button color="light" onClick={githubAuth} >
                            <GitHub />
                          </Button>
                        </div>
                      </div>
                      <p className="mt-4 mb-0">{"Don't have account?"}<a className="ml-2" href="#javascript">{CreateAccount}</a></p> */}
                    </Form>
                  </TabPane>
                  {/* <TabPane  className="fade show" tabId="auth0">
                    <div className="auth-content">
                        <img src={require("../assets/images/auth-img.svg")} alt="" />
                        <h4>{"Welcome to login with Auth0"}</h4>
                        <p>{"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy"}</p>
                        <Button color="info" onClick={loginWithRedirect}>{AUTH0}</Button> 
                    </div>
                  </TabPane> */}
                </TabContent>
              </div>
            </div>
          </div>
        </Col>
      </Row>
      <CustomerChatBoard />
      <ToastContainer />
    </Container>
  );
};

export default withRouter(Logins);
