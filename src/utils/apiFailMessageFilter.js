export const extractArrayMessage = (message) => {
  const keys = Object.keys(message);
  let msgStr = '';
  if (keys.length) {
    keys.map((key) => {
      msgStr += key === 'non_field_errors' ? message[key] : `${key}: ${message[key]} \n`;
    });
  }
  return msgStr;
};

export const ApiFailMessageFilter = (error = {}) => {
  const message = {};
  console.log('ApiFailMessageFilter', error, error.response);

  if (error.code === 'ECONNABORTED') {
    message.networkError = true;
  }

  const { response } = error;
  if (response) {
    const { data = {}, config = {} } = response;
    message.status = response.status || data.http_status_code;

    if (data.extra_info) {
      message.extra_info = data.extra_info;
    }

    if (data.message) {
      message.message = typeof data.message === 'object' ? extractArrayMessage(data.message) : data.message;
    }

    message.url = config.url;
    message.component_on_error = data.component_on_error;
    message.cash_error_code = data.cash_error_code;
  }

  if (!message.message) {
    message.message = 'Something Went Wrong';
  }

  return message;
};
