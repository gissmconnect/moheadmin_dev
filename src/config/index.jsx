// // Production
// export const SERVER_URL = "https://mohe.omantel.om/moheapp/api";
// export const RASA_SERVER_URL = "https://mohe.omantel.om/rasaapp/api"
// export const CHATBOT_URL = "https://mohe.omantel.om/chatbot/#/";
// export const CHATBOT_BACKEND = "http://86.107.197.194:3882/api";

// Production
export const SERVER_URL = "https://chatbot.heac.gov.om/moheapp/api";
export const RASA_SERVER_URL = "https://chatbot.heac.gov.om/rasaapp/api"
// export const CHATBOT_URL = "https://mohe.omantel.om/chatbot/#/";
// export const CHATBOT_BACKEND = "http://86.107.197.194:3882/api";
export const CHATBOT_EDIT = "https://chatbot.heac.gov.om/rasadynamic";

// Dev
// export const SERVER_URL = "http://2.56.215.239:3010/api";
// export const SERVER_URL = "https://moheapp.moheoman.live/api";
// export const RASA_SERVER_URL = "http://93.115.22.132:3033/api";
// export const RASA_SERVER_URL = "https://rasaapp.moheoman.live/api";
// export const CHATBOT_URL = "http://omannews.tk:7233/";
// export const CHATBOT_BACKEND = "http://86.107.197.194:3882/api";


// export const CHATBOT_EDIT = "http://86.107.197.194:8000";
// export const CHATBOT_EDIT = "https://rasadynamic.moheoman.live/";

