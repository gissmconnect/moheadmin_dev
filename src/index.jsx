import React, { Fragment, useState, useEffect } from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import { firebase_app, auth0 } from "./data/config";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
// import { Auth0Provider } from '@auth0/auth0-react'
import store from "./store";
import App from "./components/app";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { routes } from "./route";
import ConfigDB from "./data/customizer/config";
import AutoLogout from "./route/autoLogout";
// import { configureFakeBackend, authHeader, handleResponse } from './services/fack.backend'

// Signin page
import Signin from "./auth/signin";

import Callback from "./auth/callback";
import { classes } from "./data/layouts";

const Root = (props) => {
  const role = localStorage.getItem("role");
  const [anim, setAnim] = useState("");
  const animation =
    localStorage.getItem("animation") ||
    ConfigDB.data.router_animation ||
    "fade";
  const abortController = new AbortController();
  const [currentUser, setCurrentUser] = useState(false);
  const [authenticated, setAuthenticated] = useState(false);
  const jwt_token = localStorage.getItem("token");
  const defaultLayoutObj = classes.find(
    (item) => Object.values(item).pop(1) === "compact-wrapper"
  );
  const layout =
    localStorage.getItem("layout") || Object.keys(defaultLayoutObj).pop();

  useEffect(() => {
    setAnim(animation);
    firebase_app.auth().onAuthStateChanged(setCurrentUser);
    setAuthenticated(JSON.parse(localStorage.getItem("authenticated")));
    console.ignoredYellowBox = ["Warning: Each", "Warning: Failed"];
    console.disableYellowBox = true;
    return function cleanup() {
      abortController.abort();
    };
  }, []);

  return (
    <Fragment>
      <Provider store={store}>
        <BrowserRouter basename={`/`}>
          <Switch>
            <Route
              path={`${process.env.PUBLIC_URL}/login`}
              component={Signin}
            />
            <Route
              path={`${process.env.PUBLIC_URL}/callback`}
              render={() => <Callback />}
            />
            {currentUser !== null || authenticated || jwt_token ? (
              <App>
                <Route
                  exact
                  path={`${process.env.PUBLIC_URL}/`}
                  render={() => {
                    return role === "Agent" ? (
                      <Redirect
                        to={`${process.env.PUBLIC_URL}/app/chat-app/${layout}`}
                      />
                    ) : (
                      <Redirect
                        to={`${process.env.PUBLIC_URL}/dashboard/${layout}`}
                      />
                    );
                  }}
                />
                <TransitionGroup>
                  {routes.map(({ path, Component, type }) => {
                    if (role !== type) {
                      return (
                        <Route
                          key={path}
                          exact
                          path={`${process.env.PUBLIC_URL}${path}`}
                        >
                          {({ match }) => (
                            <CSSTransition
                              in={match != null}
                              timeout={100}
                              classNames={anim}
                              unmountOnExit
                            >
                              <AutoLogout ComposedClass={Component} />
                              {/* <Component /> */}
                              {/* </AutoLogout> */}
                            </CSSTransition>
                          )}
                        </Route>
                      );
                    }
                  })}
                </TransitionGroup>
              </App>
            ) : (
              <Redirect to={`${process.env.PUBLIC_URL}/login`} />
            )}
          </Switch>
        </BrowserRouter>
      </Provider>
      {/* </Auth0Provider> */}
    </Fragment>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));
serviceWorker.unregister();
