import React from "react";
import { Media } from "reactstrap";

const ChatAgentAvatar = (props) => {
  return <Media
            className={`rounded-circle user-image chat-user ${props.className}`}
            src={require('assets/images/agent-avatar.svg')}
            alt=""
        />
};

export default ChatAgentAvatar;
