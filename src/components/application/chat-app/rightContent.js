import React, { useEffect, useState } from 'react';
import { Col, Nav, NavItem, NavLink, TabContent } from 'reactstrap';
import PendingUsers from './pendingUsers';
import ActiveAgents from './activeAgents';
import TransferedUsers from './transferedUsers';

const RightSideContent = (props) => {
  const [menuToggle, setMenuToggle] = useState(false);
  const [activeTab, setActiveTab] = useState('2');
  const [pendingLength,setPendingLength] = useState('')
  const [transferLength,setTransferLength] = useState('')


  return (
    <Col className={`pl-0 chat-menu ${menuToggle ? 'show' : ''}`}>
      <Nav tabs className="nav  border-tab nav-primary">
        <NavItem id="myTab" role="tablist">
          <NavLink
            tag="a"
            href="#javascript"
            className={activeTab === '1' ? 'active' : ''}
            onClick={() => setActiveTab('1')}>
            TRANSFERED
          </NavLink>
        </NavItem>
        <NavItem id="myTab" role="tablist">
          <NavLink
            tag="a"
            href="#javascript"
            className={activeTab === '2' ? 'active' : ''}
            onClick={() => setActiveTab('2')}>
            PENDING
          {pendingLength > 0 && <span className="badge badge-pill badge-secondary" style={{marginTop: -15 }}>{pendingLength}</span>}
          </NavLink>
        </NavItem>
        
        <NavItem id="myTab" role="tablist">
          <NavLink
            tag="a"
            href="#javascript"
            className={activeTab === '3' ? 'active' : ''}
            onClick={() => setActiveTab('3')}>
            ActiveAgent
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={activeTab}>
        <TransferedUsers currentUser={props.currentUser} client={props.client} transferLength={transferLength} setTransferLength={setTransferLength} />
        <PendingUsers currentUser={props.currentUser} client={props.client} pendingLength= {pendingLength} setPendingLength={setPendingLength} />
        <ActiveAgents />
      </TabContent>
    </Col>
  );
};

export default RightSideContent;
