import React, { useRef, useEffect } from 'react';
import { Media, Spinner } from 'reactstrap';
import start_conversion from 'assets/images/start-conversion.jpg';
import MessagesItem from './messagesItem';
import { useChatData } from '../chatDataContext';

const MessagesList = (props) => {
  const { messages = [] } = props;
  const { fetchingChatLogs, selectedUser={} } = useChatData();

  const isFetchingChatLogs = fetchingChatLogs === selectedUser.jid;

  const messagesLength = messages.length;
  const listEndRef = useRef();

  useEffect(() => {
    scrollToBottom();
  }, [messagesLength]);

  const scrollToBottom = () => {
    console.log('MessagesList scrollToBottom', listEndRef);
    if (listEndRef.current) {
      listEndRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  };

  console.log('ChatUsers messagesList Render', { messages });

  if(isFetchingChatLogs){
    return <div style={{flex: 1, marginTop: '30%'}} className="d-flex justify-content-center align-items-center flex-column">
      <Spinner type="grow" color="primary" className="mb-1"/>
      <div className="text-center">Fetching Previous Messages...</div>
    </div>
  }

  return (
    <div className="chat-history chat-msg-box custom-scrollbar">
      <ul>
        {messages.length ? (
          messages.map((message) => <MessagesItem key={message.id} {...props} message={message} />)
        ) : (
          <div>
            <Media className="img-fluid" src={start_conversion} alt="start conversion " />
          </div>
        )}
        <div
          style={{ float: 'left', clear: 'both' }}
          ref={(el) => {
            listEndRef.current = el;
          }}></div>
      </ul>
    </div>
  );
};

export default MessagesList;
