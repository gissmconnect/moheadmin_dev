import moment from 'moment';
import React, { useEffect } from 'react';
import { Media } from 'reactstrap';
import { MESSAGE_MARKER } from '.';
import { useChatData } from '../chatDataContext';

export const renderCheck = (marker = {}, notSent) => {
  let check = '';
  const type = marker.type;
  if (notSent) return <i className="fa fa-clock-o"></i>;
  if (type === MESSAGE_MARKER.MARKABLE) {
    check = <i className="fa fa-check"></i>;
  } else if (type === MESSAGE_MARKER.RECEIVED) {
    check = (
      <>
        <i className="fa fa-check"></i>
        <i className="fa fa-check"></i>
      </>
    );
  } else if (type === MESSAGE_MARKER.DISPLAYED) {
    check = (
      <span style={{ color: 'green' }}>
        <i className="fa fa-check"></i>
        <i className="fa fa-check"></i>
      </span>
    );
  }
  return check;
};

const MessagesItem = (props) => {
  const { message = {} } = props;
  const { client } = useChatData();

  useEffect(() => {
    client.markDisplayed(message);
  }, []);

  // console.log('ChatUsers messagesItem Render', { message });

  return (
    <li className="clearfix">
      <div className={`message my-message ${!message.incoming ? '' : 'float-right'}`}>
        <Media
          src={require(message.incoming ? 'assets/images/user-avatar.svg' : 'assets/images/agent-avatar.svg')}
          className={`rounded-circle ${!message.incoming ? 'float-left' : 'float-right'} chat-user-img img-30`}
          alt=""
        />
        <div style={{ textAlign: message.incoming ? 'right' : 'left' }}>{message.body}</div>
        <span className="float-right">
          {!message.incoming ? renderCheck(message.marker, message.notSent) : null}
        </span>
        {message.timestamp ? <div className="message-data text-right">
          <span className="message-data-time mr-2">
            {moment(message.timestamp).format("h:mm a")}
          </span>
        </div> : null}
      </div>
    </li>
  );
};

export default MessagesItem;
