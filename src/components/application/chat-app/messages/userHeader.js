import React, { useState } from 'react';
import ChatUserAvatar from '../userAvatar';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Media, Spinner } from 'reactstrap';
import { useChatData } from '../chatDataContext';
import { toast } from 'react-toastify';
import { sendTransferRequest } from 'services/chatService';
import { ApiFailMessageFilter } from 'utils/apiFailMessageFilter';
import { getUsername } from 'utils/filters';
import ChatAgentAvatar from '../agentAvatar';

const ActiveChatHeader = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [agentListOpen, setAgentListOpen] = useState(false);
  const [selectedAgent, setSelectedAgent] = useState('');
  const [mapping, setMapping] = useState(false);
  const [endChatOpen, setEndChatOpen] = useState(false);

  const { selectedUser = {}, currentUser={}, activeAgents=[], endChat } = useChatData();

  const openModal = () => {
    setIsOpen(!isOpen);
  };

  const toggleAgentListModal = () => {
    setAgentListOpen(!agentListOpen);
    setIsOpen(false);
    setSelectedAgent('');
  };

  const toggleEndChat = () => {
    setEndChatOpen(!endChatOpen);
  };

  const transferUser = (to) => {
    setMapping(to);
    sendTransferRequest(selectedAgent, currentUser.jid, selectedUser.jid)
      .then((res) => {
        const {data={}} = res;
        console.log('ChatUser Transfer chat to other agent res', res);
        setMapping(false);
        toggleAgentListModal();
        toast.success(data.message || "Transfer Request Sent.")
      })
      .catch((err) => {
        const error = ApiFailMessageFilter(err) || {};
        console.log('ChatUser Transfer chat to other agent error', {error, err});
        setMapping(false);
        toast.error(error.message || "Something Went Wrong")
        openModal();
      });
  };

  const endChatWithUser = () => {
    toggleEndChat();
    endChat(selectedUser.jid);
  }

  const renderItem = (name, jid) => (
    <li
      className="clearfix d-flex mb-2 align-items-center"
      onClick={() => setSelectedAgent(jid)}
      style={{ cursor: 'pointer' }}>
      <div className="clearfix d-flex align-items-center col-8">
        <ChatAgentAvatar className="agent-avatar" />
        <div className="about">
          <div className="name ml-2" style={{ lineHeight: '44px' }}>
            {name}
          </div>
        </div>
      </div>
      {selectedAgent === jid ? (
        <div style={{ color: 'green' }}>
          <i className="fa fa-check"></i>
        </div>
      ) : null}
    </li>
  );

  return (
    <>
      <div className="chat-header clearfix">
        <ChatUserAvatar />
        <div className="about mr-4">
          <div className="name">{selectedUser.username}</div>
          {!selectedUser.typing ? (
            <div className="status digits">{selectedUser.online ? 'online' : 'offline'}</div>
          ) : null}
          {selectedUser.typing ? <div className="typing">typing...</div> : null}
        </div>
        <Button onClick={toggleAgentListModal} size="sm" outline color="primary">
          Transfer
        </Button>
        <Button onClick={toggleEndChat} size="sm" className="ml-2" outline color="danger">
          End Chat
        </Button>
      </div>
      <Modal isOpen={agentListOpen} toggle={toggleAgentListModal}>
        <ModalHeader toggle={toggleAgentListModal}>Transfer Chat</ModalHeader>
        <ModalBody>
          <ul className="list digits custom-scrollbar">
            {activeAgents.map(item => renderItem(getUsername(item), item))}
            {!activeAgents.length ? (
              <div className="text-center my-4">No have any active agent.</div>
            ) : null}
          </ul>
          <Modal isOpen={isOpen} toggle={openModal}>
            <ModalHeader toggle={openModal}>Transfer Your Chat</ModalHeader>
            <ModalBody>Do you want to transfer this chat to another Agent</ModalBody>
            <ModalFooter>
              <Button disabled={mapping} color="secondary" onClick={openModal}>
                Cancel
              </Button>
              <Button onClick={transferUser} disabled={mapping} color="primary">
                {mapping ? <Spinner className="ml-1" size="sm" color="primary" /> : 'Confirm'}
              </Button>
            </ModalFooter>
          </Modal>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" disabled={!selectedAgent} onClick={openModal}>
            Transfer
          </Button>
          <Button color="secondary" onClick={toggleAgentListModal}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={endChatOpen} toggle={toggleEndChat}>
        <ModalHeader toggle={toggleEndChat}>End Chat</ModalHeader>
        <ModalBody>
          <div>Are your sure to end this chat?</div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={endChatWithUser}>
            End Chat
          </Button>
          <Button color="secondary" onClick={toggleEndChat}>
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default ActiveChatHeader;
