import React, { useEffect, useState } from 'react';
import { TabPane, Spinner } from 'reactstrap';
import { useRef } from 'react';
import { getAcativeAgents } from 'services/chatService';
import { useChatData } from '../chatDataContext';
import ChatAgentAvatar from '../agentAvatar';
import { getUsername } from 'utils/filters';
import { xmppDomain } from '..';

const ActiveAgents = (props) => {
  const { currentUser, setActiveAgents } = useChatData();
  const [pendingLoading, setAgentLoading] = useState(false);
  const [intervalApiCall, setIntervalApiCall] = useState(false);

  const agentData = useRef([]);
  const activeAgents = agentData.current;

  useEffect(() => {
    const interval = setInterval(() => setIntervalApiCall(Math.random), 30000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (intervalApiCall) {
      getAgents();
    } else {
      getAgents(true);
    }
  }, [intervalApiCall]);

  const getAgents = (loading) => {
    setAgentLoading(loading);
    getAcativeAgents()
      .then((res) => {
        const { data = {} } = res;
        const { users = [] } = data;
        const index = users.indexOf(getUsername(currentUser.jid));
        if(index >= 0){
          users.splice(index, 1);
        }
        agentData.current = users;
        const filteredData = users.map(u => u+xmppDomain);
        setActiveAgents(filteredData);
        setAgentLoading(false);
        console.log('ChatDataProvider ActiveAgents getAgents res', res);
      })
      .catch((error) => {
        console.log('ChatDataProvider ActiveAgents getAgents error', error);
        setAgentLoading(false);
      });
  };

  return (
    <TabPane tabId="3">
      <div className="people-list">
        <ul className="list digits custom-scrollbar">
          {pendingLoading && !activeAgents.length ? (
            <div className="d-flex justify-content-center mt-5 pt-5">
              <Spinner type="grow" color="primary" />{' '}
            </div>
          ) : null}
          {activeAgents.map((member, index) => {
            return (
              <li key={index} className="clearfix" style={{ cursor: 'pointer' }}>
                <ChatAgentAvatar />
                <div className="about">
                  <div className="name" style={{ lineHeight: '44px' }}>
                    {member}
                  </div>
                </div>
              </li>
            );
          })}
          {!activeAgents.length && !pendingLoading ? (
            <div className="text-center mt-5">No have any active agent.</div>
          ) : null}
        </ul>
      </div>
    </TabPane>
  );
};

export default ActiveAgents;
