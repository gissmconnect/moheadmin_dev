import React from "react";
import { Media } from "reactstrap";

const ChatUserAvatar = () => {
  return <Media
            className="rounded-circle user-image chat-user"
            src={require('../../../assets/images/user-avatar.svg')}
            alt=""
        />
};

export default ChatUserAvatar;
