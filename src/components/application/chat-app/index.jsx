import React, { useState, useEffect } from 'react';
import Breadcrumb from 'layout/breadcrumb';
import { Container, Row, Col, Card, CardBody, Input, Button } from 'reactstrap';
import ActiveUsers from './activeUsers';
import RightSideContent from './rightContent';
import Messages from './messages';
import { ChatDataProvider, useChatData } from './chatDataContext';

// export const xmppDomain = '@mohe-ejabberd'
export const xmppDomain = '@omannews.tk'


const Chat = (props) => {
  var name = localStorage.getItem("username");
  // var name = email.substring(0, email.lastIndexOf("@"));
  var  rolee = localStorage.getItem("role")
  const [agentname, setAgentname] = useState(name);
  const [role, setRole] = useState(rolee);
  const { selectedUser, connect, connected } = useChatData();
  useEffect(() => {
    onConnect()
    // setRole(rolee)
  }, [])

  const onConnect = () => {

    connect(`${agentname}@omannews.tk`, agentname)
  }

  return (
    <>
      <Breadcrumb parent="Apps" title="Chat App" />
      {/* Ony For testing should remove */}
      {role === "Agent" ? "" : <div className="col-4 mb-4">
        <Input
          type="text"
          className="form-control input-txt-bx mb-2"
          placeholder="Agent name"
          value={agentname}
          // onKeyPress={(e) => handleMessagePress(e)}
          onChange={(e) => setAgentname(e.target.value)}
        />
        <Button onClick={onConnect}>Connect</Button>
      </div>}
      <Container fluid={true}>
        <Row>
          {connected ? <><ActiveUsers />
            <Col className="call-chat-body">
              <Card>
                <CardBody className="p-0">
                  <Row className="chat-box">
                    <Messages selectedUser={selectedUser} />
                    <RightSideContent />
                  </Row>
                </CardBody>
              </Card>
            </Col></> : null}
        </Row>
      </Container>
    </>
  );
};

export default (props) => {
  return (
    <ChatDataProvider>
      <Chat {...props} />
    </ChatDataProvider>
  );
};
