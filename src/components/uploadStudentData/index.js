import React, { useState, Fragment } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col, Card, CardHeader, Input, CardBody, FormGroup, Button, Nav, NavItem, NavLink, Label, Dropdown, DropdownMenu, DropdownItem, DropdownToggle, TabContent, TabPane } from 'reactstrap'
import studentmaster from '../../../src/assets/files/StudentMaster.xlsx'
import { Link } from "react-router-dom";
import studentoffer from '../../../src/assets/files/StudentOffer1.xlsx'
import studentoffer1 from '../../../src/assets/files/StudentOffer2.xlsx'
import regoffer from '../../../src/assets/files/RegistrationMaster1.xlsx'
import regoffer1 from '../../../src/assets/files/RegistrationMaster2.xlsx'
import cutOff from '../../../src/assets/files/Cutoffdetails.xlsx'
import { uploadExl } from '../../services/uploadService'
import { toast } from 'react-toastify';
import CircularProgress from "@material-ui/core/CircularProgress";

const UploadStudent = (props) => {

    const [VerticleTab, setVerticleTab] = useState('1');
    const [file, setFile] = useState();
    const [fileName, setFileName] = useState("");
    const [loader, setloader] = useState(false);
    const [button, setbutton] = useState(false);

    const handleFile = (event) => {
        setFileName(event.target.files[0].name)
        setFile(event.target.files[0])
    }

    const handleClose = () => {
        setFileName("")
        setFile();
    }

    const handleUpload = async () => {
         if (!file) {
            toast.error("Select file");
        } else {
        setloader(true)
            try {
                let res = await uploadExl(file,VerticleTab);
                if (res === "timeout") {
        setbutton(true)
                    setTimeout(() => {
                        setbutton(false)
                    }, 150000);
                }
                handleClose();
            } catch (error) {
                console.log(error);
            }
            setloader(false)
        }
    };

    return (
        <Fragment>
            <Breadcrumb parent="Apps" title="Upload Student Data" />
            <Container fluid={true}>
                <Col sm="12" xl="6 xl-100 box-col-12">
                    <Card>

                        <CardBody>
                            <Row>
                                <Col sm="3" xs="12">
                                    <Nav className="nav flex-column nav-pills navStyleNew">
                                        <NavItem>
                                            <NavLink href="#javascript" className={VerticleTab === '1' ? 'active' : ''}
                                                onClick={() => {
                                                    setVerticleTab('1');
                                                    setFileName('');
                                                    setFile();
                                                }}>
                                                Student Master</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#javascript" className={VerticleTab === '2' ? 'active' : ''}
                                                onClick={() => {
                                                    setVerticleTab('2');
                                                    setFileName('');
                                                    setFile();
                                                }}>Student Offer 1</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#javascript" className={VerticleTab === '3' ? 'active' : ''}
                                                onClick={() => {
                                                    setVerticleTab('3');
                                                    setFileName('');
                                                    setFile();
                                                }}>Student Offer 2</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#javascript" className={VerticleTab === '4' ? 'active' : ''}
                                                onClick={() => {
                                                    setVerticleTab('4');
                                                    setFileName('');
                                                    setFile();
                                                }}>Registration Offer 1</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#javascript" className={VerticleTab === '5' ? 'active' : ''}
                                                onClick={() => {
                                                    setVerticleTab('5');
                                                    setFileName('');
                                                    setFile();
                                                }}>Registration Offer 2</NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink href="#javascript" className={VerticleTab === '6' ? 'active' : ''}
                                                onClick={() => {
                                                    setVerticleTab('6');
                                                    setFileName('');
                                                    setFile();
                                                }}>CutOff Details</NavLink>
                                        </NavItem>
                                    </Nav>
                                </Col>
                                <Col sm="9" xs="12">
                                    <TabContent activeTab={VerticleTab}>
                                        <TabPane className="fade show" tabId="1">
                                            <Row>
                                                <Col>
                                                    <div style={{ width: '65%' }}>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 30 }}>
                                                            <h4>Student Master</h4>

                                                        </div>
                                                        {loader ? <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 100 }}><CircularProgress /></div>
                                                            : <>
                                                                <div className="custom-file">
                                                                    <Input className="custom-file-input" onChange={handleFile} id="validatedCustomFile" type="file" accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required="" />
                                                                    {fileName === "" ?
                                                                        <Label className="custom-file-label" htmlFor="validatedCustomFile">Choose file...</Label>
                                                                        : <Label className="custom-file-label" htmlFor="validatedCustomFile">{fileName}</Label>}
                                                                </div>
                                                                <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >**Only .XLSX files allowed.
                                                                </p>
                                                                <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 30 }}>
                                                                    <Button color="primary" className="mr-1" >
                                                                        <Link
                                                                            to={studentmaster}
                                                                            target="_blank"
                                                                            style={{ color: "#fff" }}
                                                                            download
                                                                        >
                                                                            Download Sample
                                                                        </Link>
                                                                    </Button>

                                                                    <Button color="primary" disabled={loader ? true : button ? true : false} className="mr-1" onClick={handleUpload}  >Upload</Button>

                                                                    <Button color="light" type="reset" onClick={handleClose} >Cancel</Button>

                                                                </div>
                                                                {button && <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >*Uploading file in background. Please wait before uploading new file.
                                                                </p>}
                                                            </>}
                                                    </div>
                                                </Col>
                                            </Row>

                                        </TabPane>
                                        <TabPane tabId="2">
                                            <Row>
                                                <Col>
                                                    <div style={{ width: '65%' }}>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 30 }}>
                                                            <h4>Student Offer 1</h4>
                                                        </div>
                                                        {loader ? <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 100 }}><CircularProgress /></div>
                                                            : <>
                                                                <div className="custom-file">
                                                                    <Input className="custom-file-input" onChange={handleFile} id="validatedCustomFile" type="file" accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required="" />
                                                                    {fileName === "" ?
                                                                        <Label className="custom-file-label" htmlFor="validatedCustomFile">Choose file...</Label>
                                                                        : <Label className="custom-file-label" htmlFor="validatedCustomFile">{fileName}</Label>}
                                                                </div>
                                                                <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >**Only .XLSX files allowed.
                                                                </p>
                                                                <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 30 }}>
                                                                    <Button color="primary" className="mr-1" >
                                                                        <Link
                                                                            to={studentoffer}
                                                                            target="_blank"
                                                                            style={{ color: "#fff" }}
                                                                            download
                                                                        >
                                                                            Download Sample
                                                                        </Link>
                                                                    </Button>

                                                                    <Button color="primary" disabled={loader ? true : button ? true : false} className="mr-1" onClick={handleUpload}  >Upload</Button>

                                                                    <Button color="light" type="reset"  >Cancel</Button>
                                                                </div>
                                                                {button && <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >*Uploading file in background. Please wait before uploading new file.
                                                                </p>}
                                                            </>}
                                                    </div>
                                                </Col>
                                            </Row>
                                        </TabPane>
                                        <TabPane tabId="3">
                                            <Row>
                                                <Col>
                                                    <div style={{ width: '65%' }}>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 30 }}>
                                                            <h4>Student Offer 2</h4>
                                                        </div>
                                                        {loader ? <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 100 }}><CircularProgress /></div>
                                                            : <>
                                                                <div className="custom-file">
                                                                    <Input className="custom-file-input" onChange={handleFile} id="validatedCustomFile" type="file" accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required="" />
                                                                    {fileName === "" ?
                                                                        <Label className="custom-file-label" htmlFor="validatedCustomFile">Choose file...</Label>
                                                                        : <Label className="custom-file-label" htmlFor="validatedCustomFile">{fileName}</Label>}
                                                                </div>
                                                                <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >**Only .XLSX files allowed.
                                                                </p>
                                                                <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 30 }}>
                                                                    <Button color="primary" className="mr-1" >
                                                                        <Link
                                                                            to={studentoffer1}
                                                                            target="_blank"
                                                                            style={{ color: "#fff" }}
                                                                            download
                                                                        >
                                                                            Download Sample
                                                                        </Link>
                                                                    </Button>

                                                                    <Button color="primary" disabled={loader ? true : button ? true : false} className="mr-1" onClick={handleUpload}  >Upload</Button>

                                                                    <Button color="light" type="reset"  >Cancel</Button>
                                                                </div>
                                                                {button && <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >*Uploading file in background. Please wait before uploading new file.
                                                                </p>}
                                                            </>}
                                                    </div>
                                                </Col>
                                            </Row>
                                        </TabPane>
                                        <TabPane tabId="4">
                                            <Row>
                                                <Col>
                                                    <div style={{ width: '65%' }}>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 30 }}>
                                                            <h4>Registration Offer 1</h4>
                                                        </div>
                                                        {loader ? <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 100 }}><CircularProgress /></div>
                                                            : <>
                                                                <div className="custom-file">
                                                                    <Input className="custom-file-input" onChange={handleFile} id="validatedCustomFile" type="file" accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required="" />
                                                                    {fileName === "" ?
                                                                        <Label className="custom-file-label" htmlFor="validatedCustomFile">Choose file...</Label>
                                                                        : <Label className="custom-file-label" htmlFor="validatedCustomFile">{fileName}</Label>}
                                                                </div>
                                                                <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >**Only .XLSX files allowed.
                                                                </p>
                                                                <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 30 }}>
                                                                    <Button color="primary" className="mr-1" >
                                                                        <Link
                                                                            to={regoffer}
                                                                            target="_blank"
                                                                            style={{ color: "#fff" }}
                                                                            download
                                                                        >
                                                                            Download Sample
                                                                        </Link>
                                                                    </Button>

                                                                    <Button color="primary" disabled={loader ? true : button ? true : false} className="mr-1" onClick={handleUpload}  >Upload</Button>

                                                                    <Button color="light" type="reset"  >Cancel</Button>
                                                                </div>
                                                                {button && <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >*Uploading file in background. Please wait before uploading new file.
                                                                </p>}
                                                            </>}
                                                    </div>
                                                </Col>
                                            </Row>
                                        </TabPane>
                                        <TabPane tabId="5">
                                            <Row>
                                                <Col>
                                                    <div style={{ width: '65%' }}>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 30 }}>
                                                            <h4>Registration Offer 2</h4>
                                                        </div>
                                                        {loader ? <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 100 }}><CircularProgress /></div>
                                                            : <>
                                                                <div className="custom-file">
                                                                    <Input className="custom-file-input" onChange={handleFile} id="validatedCustomFile" type="file" accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required="" />
                                                                    {fileName === "" ?
                                                                        <Label className="custom-file-label" htmlFor="validatedCustomFile">Choose file...</Label>
                                                                        : <Label className="custom-file-label" htmlFor="validatedCustomFile">{fileName}</Label>}
                                                                </div>
                                                                <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >**Only .XLSX files allowed.
                                                                </p>
                                                                <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 30 }}>
                                                                    <Button color="primary" className="mr-1" >
                                                                        <Link
                                                                            to={regoffer1}
                                                                            target="_blank"
                                                                            style={{ color: "#fff" }}
                                                                            download
                                                                        >
                                                                            Download Sample
                                                                        </Link>
                                                                    </Button>

                                                                    <Button color="primary" disabled={loader ? true : button ? true : false} className="mr-1" onClick={handleUpload}  >Upload</Button>

                                                                    <Button color="light" type="reset"  >Cancel</Button>
                                                                </div>
                                                                {button && <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >*Uploading file in background. Please wait before uploading new file.
                                                                </p>}
                                                            </>}
                                                    </div>
                                                </Col>
                                            </Row>
                                        </TabPane>
                                        <TabPane tabId="6">
                                            <Row>
                                                <Col>
                                                    <div style={{ width: '65%' }}>
                                                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end', marginBottom: 30 }}>
                                                            <h4>Cuttoff Details</h4>
                                                        </div>
                                                        {loader ? <div style={{ display: 'flex', justifyContent: 'center', paddingTop: 100 }}><CircularProgress /></div>
                                                            : <>
                                                                <div className="custom-file">
                                                                    <Input className="custom-file-input" onChange={handleFile} id="validatedCustomFile" type="file" accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" required="" />
                                                                    {fileName === "" ?
                                                                        <Label className="custom-file-label" htmlFor="validatedCustomFile">Choose file...</Label>
                                                                        : <Label className="custom-file-label" htmlFor="validatedCustomFile">{fileName}</Label>}
                                                                </div>
                                                                <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >**Only .XLSX files allowed.
                                                                </p>
                                                                <div style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'flex-end', marginTop: 30 }}>
                                                                    <Button color="primary" className="mr-1" >
                                                                        <Link
                                                                            to={cutOff}
                                                                            target="_blank"
                                                                            style={{ color: "#fff" }}
                                                                            download
                                                                        >
                                                                            Download Sample
                                                                        </Link>
                                                                    </Button>

                                                                    <Button color="primary" disabled={loader ? true : button ? true : false} className="mr-1" onClick={handleUpload}  >Upload</Button>

                                                                    <Button color="light" type="reset"  >Cancel</Button>
                                                                </div>
                                                                {button && <p
                                                                    style={{
                                                                        color: "red",
                                                                        margin: "0 5px",
                                                                        width: "100%",
                                                                        fontSize: 10
                                                                    }}
                                                                >*Uploading file in background. Please wait before uploading new file.
                                                                </p>}
                                                            </>}
                                                    </div>
                                                </Col>
                                            </Row>
                                        </TabPane>

                                    </TabContent>
                                </Col>
                            </Row>
                        </CardBody>
                    </Card>
                </Col>
            </Container>
        </Fragment>
    );
}

export default UploadStudent;