import React, { Component } from 'react';
import IdleTimer from 'react-idle-timer';
import { connect } from 'react-redux';
import { userSignOut } from '../../actions/Auth';
import { sessionTimeoutActionCreater } from '../../reducers/SessionTimeout';

class IdleTrackWrapper extends Component {
    constructor(props) {
        super(props);
        this.idleTimer = null;
        this.onAction = this._onAction.bind(this);
        this.onActive = this._onActive.bind(this);
        this.onIdle = this._onIdle.bind(this);
    }

    render() {
        return (
            <>
                <IdleTimer
                    ref={(ref) => {
                        this.idleTimer = ref;
                    }}
                    element={document}
                    // onActive={this.onActive}
                    onIdle={this.onIdle}
                    // onAction={this.onAction}
                    debounce={250}
                    timeout={50000*60}
                />
                {this.props.children}
            </>
        );
    }
    _onAction(e) {
        console.log('user did something', e);
    }

    _onActive(e) {
        console.log('user is active', e);
        console.log('time remaining', this.idleTimer.getRemainingTime());
    }

    _onIdle(e) {
        this.props.openSessionTimeoutModal();
        localStorage.setItem('sessionTimeout', true);
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(userSignOut()),
        openSessionTimeoutModal: () =>
            dispatch(sessionTimeoutActionCreater.openTimeoutModal()),
    };
};

export default connect(null, mapDispatchToProps)(IdleTrackWrapper);
