import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import { toast } from "react-toastify";
import Breadcrumb from "../../layout/breadcrumb";
import { Container, Row, Col, Button } from "reactstrap";
import { AddBox, ArrowDownward, Add } from "@material-ui/icons";
// import MaterialTable, { MTableEditField } from "material-table";
import MaterialTable from '@material-table/core';
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import Cached from "@material-ui/icons/Cached";
import {
  getAppointmentByProgCodeList,
  addList,
  UpdateList,
  deleteAppointmentByProgCode,
} from "../../services/appointmentByProgCodeService";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import Tooltip from "@material-ui/core/Tooltip";
import { flatMap } from "lodash";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const SecondScreenIngrates = (props) => {
  const [open, setOpen] = useState();
  const history = useHistory();
  const [loader, setloader] = useState(false);
  const [menuList, setMenuList] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);
    handleAppointmentByProgCodeList();
  }, []);

  const handleAppointmentByProgCodeList = async () => {
    try {
      setloader(true);
      let res = await getAppointmentByProgCodeList("secondscreeningrates");
      setMenuList(res);
      setloader(false);
    } catch (error) {
      console.log(error);
    }
  };

  const handleRowDelete = async (oldData, resolve, reject) => {
    Swal.fire({
      icon: "info",
      title: "Do you want to Delete ?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Ok",
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deleteAppointmentByProgCode(oldData.id);
          if (res.status === "done") {
            handleAppointmentByProgCodeList();
            setloader(false);
          }
          setloader(false);
          resolve();
        } catch (error) {
          console.log(error);
          resolve();
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
        resolve();
      }
    });
    resolve();
  };

  const handleRowAdd = async (newData, resolve, reject) => {
    const code = newData.code;
    const details = newData.details;
    let errorList = [];
    if (!code) {
      Swal.fire({
        icon: "error",
        text: "Code could not be empty",
      });
      errorList.push("Code could not be empty");
    } else if (!details) {
      Swal.fire({
        icon: "error",
        text: "Details field could not be empty",
      });
      errorList.push("Details field could not be empty");
    }
    if (errorList.length <= 0) {
      const data = {
        category: "البحث عن المواعيد بواسطة رمز البرنامج",
        unique_title: "secondscreeningrates",
        code: code,
        details: details,
      };
      try {
        setloader(true);
        let res = await addList(data);
        if (res.status === "done") {
          handleAppointmentByProgCodeList();
          setloader(false);
        }
        resolve();
      } catch (error) {
        console.log(error);
        resolve();
      }
      setloader(false);
    }
  };

  const handleRowUpdate = async (newData, oldData, resolve, reject) => {
    setloader(true);
    const code = newData.code;
    const details = newData.details;
    const id = newData.id;
    let errorList = [];
    if (!code) {
      Swal.fire({
        icon: "error",
        text: "Code could not be empty",
      });
      errorList.push("Code could not be empty");
    } else if (!details) {
      Swal.fire({
        icon: "error",
        text: "Details field could not be empty",
      });
      errorList.push("Details field could not be empty");
    }
    if (errorList.length <= 0) {
      const data = {
        _id: id,
        category: "البحث عن المواعيد بواسطة رمز البرنامج",
        unique_title: "secondscreeningrates",
        code: code,
        details: details,
      };
      try {
        let res = await UpdateList(data);
        if (res) {
          handleAppointmentByProgCodeList();
          setloader(false);
        }
        resolve();
      } catch (error) {
        console.log(error);
        resolve();
      }
    }
    resolve();
  };

  const handleRefreshPage = async () => {
    history.push(`${process.env.PUBLIC_URL}/app/menus/Dubai`);
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="Second screen ingrates" />
      <Container fluid={true}>
        <div className="edit-profile edit-profile-style-update">
        <div
            style={{
              cursor: "pointer",
              position: "relative",
              zIndex: 2,
              marginBottom: 10,
              marginTop: -15,
            }}
          >
            <Button onClick={handleRefreshPage}>Main Menu</Button>
          </div>
          <Row>
            <Col md="12">
              <>
                <MaterialTable
                  icons={tableIcons}
                  isLoading={false}
                  columns={[
                    {
                      title: "Code",
                      field: "code",
                    },
                    {
                      title: "Details",
                      field: "details",
                    },
                    {
                      title: "Category",
                      field: "category",
                      hidden: true,
                    },
                    {
                      title: "Unique Title",
                      field: "unique_title",
                      hidden: true,
                    },
                    {
                      title: "Id",
                      field: "id",
                      hidden: true,
                    },
                  ]}
                  data={
                    menuList &&
                    menuList.map((item) => {
                      return {
                        code: item.code,
                        details: item.details,
                        category: item.category,
                        unique_title: item.unique_title,
                        id: item._id.$oid,
                      };
                    })
                  }
                  title=""
                  options={{
                    search: false,
                    actionsColumnIndex: -1,
                    addRowPosition: "first",
                    pageSize: 50,
                    pageSizeOptions: [50, 100, 200, 400],
                  }}
                  editable={{
                    onRowAdd: (newData) =>
                      new Promise((resolve, reject) => {
                        handleRowAdd(newData, resolve, reject);
                      }),

                    onRowUpdate: (newData, oldData) =>
                      new Promise((resolve, reject) => {
                        handleRowUpdate(newData, oldData, resolve, reject);
                      }),
                    onRowDelete: (oldData) =>
                      new Promise((resolve, reject) => {
                        handleRowDelete(oldData, resolve, reject);
                      }),
                  }}
                />
              </>
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
};

export default SecondScreenIngrates;
