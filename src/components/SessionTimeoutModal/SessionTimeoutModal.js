import React from "react";
import { useDispatch } from "react-redux";
import { Button, Modal, ModalBody, ModalFooter } from "reactstrap";
import { userSignOut } from "../../actions/Auth";
import "./styles.scss";
import { sessionTimeoutActionCreater } from "../../reducers/SessionTimeout";

export default function SessionTimeoutModal(props) {
  const { isopen } = props;
  const dispatch = useDispatch();

  const logout = async () => {
    dispatch(sessionTimeoutActionCreater.closeTimeoutModal());
    dispatch(userSignOut());
  };
  return (
    <div>
      <Modal isOpen={isopen} centered className="modal-small ">
        <ModalBody>
          <h2>Session Timeout</h2>
          <p>This session has ended due to inactivity. </p>
          <ModalFooter>
            <Button color="secondary" variant="contained" onClick={logout}>
              Log In Again
            </Button>
          </ModalFooter>
        </ModalBody>
      </Modal>
    </div>
  );
}
