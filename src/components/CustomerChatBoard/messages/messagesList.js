import React, {useEffect, useRef} from 'react';
import { Button } from 'reactstrap';
import MessageItem from './messageItem';

const MessagesList = (props) => {
  const { messages = [], activeOptions=[] } = props;
  const messagesLength = messages.length;
  const listEndRef = useRef();

  useEffect(() => {
    if(props.scrollToBottom){
      scrollToBottom();
    }
  }, [props.scrollToBottom])

  const scrollToBottom = () => {
    console.log('MessagesList scrollToBottom', listEndRef)
    if(listEndRef.current){
      listEndRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }

  const renderOptions = () => {
    return <div className="mx-4"> 
      {activeOptions.map(o => (
        <Button outline style={{fontSize: 10}} color="danger" size="sm" className="m-1 text-left p-2" onClick={() => props.onSelectOption(o.value, o.body)}>
          {o.body}
        </Button>
      ))}
    </div>
  }

  return (
    <div className="outgoing" style={{}}>
      <div className="row">
        <div className="outgoing_msg" style={{ width: '100%' }}>
          {messages.map(message => <MessageItem key={message.id} {...props} message={message}/>)}
          {renderOptions()}
          <div style={{ float:"left", clear: "both" }}
             ref={(el) => { listEndRef.current = el; }}>
        </div>
        </div>
      </div>
    </div>
  );
};

export default MessagesList;
