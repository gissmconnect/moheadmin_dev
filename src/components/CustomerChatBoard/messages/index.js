import React, { useState, useEffect, useRef, useCallback } from 'react';
import { ChevronsRight } from 'react-feather';
import { getJid } from 'utils/filters';
import { v4 as uuidv4 } from 'uuid';
import { CHATSTATES, isDataBody } from '..';
import MessagesList from './messagesList';
import { MESSAGE_MARKER } from '../../application/chat-app/messages';
import _ from 'lodash';
import ChatBot from './chatBot';

const USER_MESSAGES_LOCALSTORAGE = 'user_chat_messages_'

const Messages = (props) => {
  const [messageText, setMessageText] = useState('');
  const [update, setUpdate] = useState(false);

  const { client = {}, connected, currentUser = {}, targetUser } = props;

  const typingTimerRef = useRef();
  const messagesRef = useRef({});
  const messages = Object.values(messagesRef.current);

  const localStorageKey = `${USER_MESSAGES_LOCALSTORAGE}${targetUser}`;

  useEffect(() => {
    if (!update) return;
    localStorage.setItem(localStorageKey, JSON.stringify(messagesRef.current))
  }, [update]);

  useEffect(() => {
    const allPreviousMessages = localStorage.getItem(localStorageKey);
    messagesRef.current = JSON.parse(allPreviousMessages || "{}") || {};
    console.log('ChatUser allPreviousMessages', allPreviousMessages, JSON.parse(allPreviousMessages || "{}"))
    setUpdate(null);
  }, [])

  useEffect(() => {
    if (!connected) return;
    client.on('message', onMessage);
    client.on('message:sent', onSentMessage);
    client.on("marker:received", onReceipt);
    client.on("marker:displayed", onReceipt);
  }, [connected]);

  const onReceipt = (d) => {
    console.log('ChatUser Messages marker', {messages, d, currentUser})
  if(getJid(d.from) !== currentUser.jid){
    const marker = d.marker || {};
    const messages = messagesRef.current;
    const message = messages[marker.id];
    messages[marker.id] = {...message, marker};
    setUpdate(Math.random());
}};

  const onMessage = (msg = {}, notSent) => {
    console.log('ChatUser messages onMessage', { msg });
    const jid = getJid(msg.from);
    const currentJid = getJid(currentUser.jid);
    if (!msg.body) return;
    if(isDataBody(msg.body)) {
      props.handleDataMessage(msg.body);
      return;
    }


    const messages = messagesRef.current;
    messages[msg.id] = { ...msg, timestamp: new Date(), incoming: currentJid !== jid, notSent };
    client.markDisplayed(msg);
    setUpdate(Math.random());
  };

  const onSentMessage = (msg={}) => {
    onMessage({...msg, from: currentUser?.jid})
  }

  const sendMessage = (e) => {
    e.preventDefault();
    if (!messageText.trim()) return;
    const message = {
        to: targetUser,
        body: messageText,
        marker: {type: MESSAGE_MARKER.MARKABLE},
        id: uuidv4(),
    }
    client.sendMessage(message);
    onMessage({...message, from: currentUser.jid}, true)
    setMessageText('');
    clearTimeout(typingTimerRef.current);
    sendChatStateDebounce(CHATSTATES.PAUSED, targetUser);
  };

  const handleMessage = (e) => {
    const value = e.target.value;
    setMessageText(value);
    clearTimeout(typingTimerRef.current);
    if(value.length === 1){
      sendChatState(CHATSTATES.COMPOSING, targetUser);
    } else {
      sendChatStateDebounce(CHATSTATES.COMPOSING, targetUser);
    }
    setTypingTimeout();
  };

  const setTypingTimeout = () => {
    typingTimerRef.current = setTimeout(() => {
      sendChatStateDebounce(CHATSTATES.PAUSED, targetUser);
    }, 5000);
  };

  const sendChatStateDebounce = useCallback(
    _.debounce((state, to) => sendChatState(state, to), 500),
    [],
  );

  const sendChatState = (state, to) => {
    client.sendMessage({
      chatState: state,
      to,
      type: 'chat',
    });
  };

  return <ChatBot/>

  return (
    <div>
      <div
        className="activeChatContent"
        style={{
          background: '#fff',
          maxHeight: 350,
          minHeight: 150,
          overflowY: 'scroll',
          overflowX: 'hidden',
        }}>
        <MessagesList
            messages={messages}
            {...props}
        />
      </div>
      <div className="OnlineCustomerFooter" style={{ padding: '5px 0px' }}>
        <div className="d-flex flex-wrap">
          <form className="d-flex" onSubmit={sendMessage}>
            <div className="" style={{ paddingRight: 0, flex: 1 }}>
              {props.agentChanged ? <div className="">{props.agentChanged}</div> : null}
              <input
                type="text"
                className="form-control"
                placeholder="Enter Message"
                value={messageText}
                onChange={handleMessage}
              />
            </div>
            <div className="col-md-1" style={{ paddingLeft: 2 }}>
              <button type="submit" className="btn btn-primary" style={{ padding: '3px 6px' }}>
                <ChevronsRight />
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Messages;
