import moment from 'moment';
import React from 'react';
import { renderCheck } from '../../application/chat-app/messages/messagesItem';

const MessageItem = (props) => {
  const { message = {} } = props;

  const style = {
    textAlign: 'right',
    background: '#f4f0f0',
    padding: '5px 10px',
    border: 'transparent',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    margin: '10px auto',
    fontSize: 12,
    position: 'relative',
    width: '80%',
  };

  if (!message.incoming) {
    style.textAlign = 'left';
    style.background = '#f3f2fc';
  }

  return <div>{message.incoming ? (
    <p style={style}>
      {message.body}
      <div className="message-data text-right">
          <span className="message-data-time">
            {moment(message.timestamp).format("h:mm a")}
          </span>
        </div>
    </p>
  ) : (
    <p style={style}>
      {message.body}
      <span className="float-right pb-1 d-flex">
        <div className="message-data text-right">
          <span className="message-data-time mr-2">
            {moment(message.timestamp).format("h:mm a")}
          </span>
        </div>
        <span style={{marginTop: 5}}>
    {renderCheck(message.marker, message.notSent)}
    </span>
  </span>
    </p>
  )}
  
  </div>;
};

export default MessageItem;
