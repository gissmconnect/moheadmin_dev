import React, { useEffect, useState } from 'react';
import { ChevronsRight } from 'react-feather';
import MessagesList from './messagesList';
import _ from 'lodash';
import { getWebMessages } from "services/chatService";
import { v4 as uuidv4 } from 'uuid';

const optionSplitText = "\n";
const defaultOptions = [
    {body: 'خروج', value: 'exit'},
    {body: 'السابق', value: '0'},
]

const ChatBot = (props) => {
  const [messageText, setMessageText] = useState('hi');
  const [showPrevious, setShowPrevious] = useState(false);
  const [activeOptions, setActiveOptions] = useState([]);
  const [messages, setMessages] = useState({});
  const [activeOptionsString, setActiveOptionsString] = useState('');
  const [scrollToBottom, setScrollToBottom] = useState(false);

  useEffect(() => {
    sendWebMessage();
  }, [])

  const addMessage = (body, incoming) => {
    const id = uuidv4();
    const allMessages = messages;
    allMessages[id] = {body: body || messageText, incoming, id};
    setMessages(allMessages);
    setMessageText('');
  }

  const sendWebMessage = (val, body) => {
    const data = {
      auth: "PdfgdfJFDGTfrfgdsf",
      sender: "9876543210",
      message: val || messageText
  }
    if(activeOptionsString){
        addMessage(activeOptionsString, true);
        setActiveOptionsString('');
    }

    addMessage(body || messageText, false);
    setActiveOptions([]);
    
    getWebMessages(data).then(res => {
      console.log('getWebMessages res', res);
      const {data={}} = res;
      const message = data.message || '';
      const splitArray = message.split(optionSplitText);
      let options = [];

      splitArray.map(body => {
          const value = body.replace(/[^0-9]/g, '');
          if(!value) return;
          options.push({value, body});
      });

      if(val && val !== defaultOptions[0].value){
          options.push(defaultOptions[1])
      }
      if(val !== defaultOptions[0].value){
          options.push(defaultOptions[0])
      }
      if(options.length){
        setActiveOptions(options);
      } else if(splitArray.length){
        addMessage(message, true);
      }
      setActiveOptionsString(message);
      setScrollToBottom(Math.random());
      console.log('getWebMessages splitArray', {splitArray, number: splitArray[0].replace(/[^0-9]/g, ''), options});
    }).catch(error => {
      console.log('getWebMessages error', error);
    })
  }

  const sendMessage = (e) => {
    e.preventDefault();
    if (!messageText.trim()) return;

    sendWebMessage();
  };

  const handleMessage = (e) => {
    const value = e.target.value;
    setMessageText(value);
  };

  return (
    <div>
      <div
        className="activeChatContent"
        style={{
          background: '#fff',
          maxHeight: 350,
          overflowY: 'hidden',
          overflowX: 'hidden',
        }}>
        <MessagesList
            messages={Object.values(messages)}
            activeOptions={activeOptions}
            onSelectOption={sendWebMessage}
            scrollToBottom={scrollToBottom}
            {...props}
        />
      </div>
      <div className="OnlineCustomerFooter" style={{ padding: '5px 0px' }}>
        <div className="d-flex flex-wrap">
          <form className="d-flex" onSubmit={sendMessage}>
            <div className="" style={{ paddingRight: 0, flex: 1 }}>
              {props.agentChanged ? <div className="">{props.agentChanged}</div> : null}
              <input
                type="text"
                className="form-control"
                placeholder="Enter Message"
                value={messageText}
                onChange={handleMessage}
              />
            </div>
            <div className="col-md-1" style={{ paddingLeft: 2 }}>
              <button type="submit" className="btn btn-primary" style={{ padding: '3px 6px' }}>
                <ChevronsRight />
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default ChatBot;
