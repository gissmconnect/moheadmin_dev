import React from "react";

const WaitingConfirmation = () => {
  return (
    <div
        className="activeChatContent"
        style={{
            background: "#fff",
            maxHeight: 350,
            overflowY: "scroll",
            overflowX: "hidden",
            border: "solid 1px #ccc",
        }}
        >
        <div className="incoming" style={{ textAlign: "left" }}>
            <div className="incoming_msg">
            <p
                style={{
                background: "#f4f0f0",
                padding: "5px 10px",
                border: "transparent",
                borderTopRightRadius: 10,
                borderBottomRightRadius: 10,
                borderBottomLeftRadius: 10,
                margin: "10px auto",
                fontSize: 12,
                position: "relative",
                width: "80%",
                }}
            >
                Waiting for agent confirmation ...
            </p>
            </div>
        </div>
    </div>
  );
};

export default WaitingConfirmation;
