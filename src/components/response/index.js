import React, { useEffect, Fragment, useState } from 'react';
import Breadcrumb from '../../layout/breadcrumb'
import { Container, Row, Col } from 'reactstrap'
import Tabs from '@material-ui/core/Tabs';
import { alpha, makeStyles } from "@material-ui/core/styles";
import Tab from '@material-ui/core/Tab';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Accordionnew from "./accordin"
import { getResponses, addResponse } from '../../services/responses';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Autocomplete from '@material-ui/lab/Autocomplete';
import DetailedAccordion from "./newAccodin";

function TabPanel(props) {
    const { children, value, valu, index, handleEdit, ...other } = props;

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'space-between',
            }}
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <>
                    <Box
                        style={{ textAlign: 'left' }}
                        p={3}>
                        <Typography>{children}</Typography>
                    </Box>
                    <Box
                        style={{
                            display: 'flex',
                            flexDirection: 'row',
                        }}
                        p={3}>
                        {children && <> <Typography style={{ marginRight: 15 }}><EditOutlinedIcon onClick={() => handleEdit()} /></Typography>
                            {/* <Typography><DeleteOutlineOutlinedIcon /></Typography> */}
                        </>}
                    </Box>
                </>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: "100%"
    },
    table: {
        width: 1100,
        minHeight: 100,
        margin: "20px auto",
        height: 500,
        borderWidth: 2,
        borderColor: "#e4e4e4",
        backgroundColor: "#fff",
        borderRadius: 20,
        overflow: "hidden",
        boxShadow: "0px 2px 4px -1px #00000033, 0px 4px 5px 0px #00000024, 0px 1px 10px 0px #0000001f"
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),

    },
    searchIcon: {
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
    },
    inputInput: {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
    },
    searchPanel: {
        borderBottom: "solid 1px rgb(236, 239, 247)",
        paddingTop: 10,
        paddingBottom: 10
    },
    tab: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        height: 390,

    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
        width: 300,
        alignItems: 'flex-start',

    },
    accordin: {
        display: "flex",
        // flexWrap: "wrap",
        flexDirection: 'row'
    },
    scroll1: {
        overflowY: 'scroll',
        overflowX: 'hidden',
        height: 390,
        display: 'grid'
    },
    details: {
        alignItems: 'center',
    },
    button: {
        display: 'flex',
        padding: 8,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    divider: {
        border: 'none',
        height: 1,
        margin: 0,
        flexShrink: 0,
        backgroundColor: '#a9a9a9'
    },
    searchOption: {
        padding: 6,
        width: 250
    },
    searchRes: {
        marginLeft: 20,
        padding: 6,
        width: 750
    },
    head: {
        flex: 1,
        padding: '5px 37px',
        justifyContent: 'flex-start',
        alignContent: 'flex-start',
        display: 'flex'
    },
    tabhead: {
        flex: 1
    },
    column: {
        flexBasis: '33.33%',
    },
    headerPart: {
        display: "flex",
        justifyContent: "space-between",
        background: "#fff",
        padding: "5px 0",
        alignItems: "center",
    }
}));
const Responses = () => {
    const classes = useStyles();
    const [value, setValue] = useState();
    const [responses, setResponses] = useState({})
    const [response, setResponse] = useState('');
    const [responseData, setResponseData] = useState("")
    const [responseName, setResponseName] = useState("")
    const [responseSearch, setResponseSearch] = useState()
    const [edit, setEdit] = useState(false);
    const [hide, setHide] = useState(false);
    const [addButton, setAddButton] = useState(false);

    const handleTab = (event, newValue) => {
        setValue(newValue);

    }

    useEffect(() => {
        getResponsesList();

    }, []);


    const getResponsesList = async () => {
        try {
            let res = await getResponses();
            setResponses(res.data)
        } catch (error) {
            console.log(error);
        }
    };
    const handleSubmit = async () => {

        try {
            let result = await addResponse(responseName, response)
            setEdit(false)
            getResponsesList();
            setResponseData(response)
        }
        catch (error) {
            console.log(error)
        }
    }
    const handleEdit = async () => {
        setEdit(true)
        setResponse(responseData)
    }
    const handleCancel = async () => {
        setEdit(false)
        setResponse('')
    }

    const handleSearch = async (e, v) => {
        if (v) {
            setResponseSearch(v[0])
            setResponseData(v[1][0].text)
        } else {
            setResponseSearch()
            setResponseData()
        }

    }

    var sectionStyle = {
        color: "#000",
    };


    return (
        <Fragment>
            <Breadcrumb parent="Apps" title="Responses" />
            <Container fluid={true}>
                <p
                    style={{
                        color: "red",
                        margin: "0 0",
                        width: "100%",
                        textAlign: "center",
                    }}
                >*** Please start training after editing responses
                </p>
                <div className="edit-profile">
                    <Row>
                        <Col md="12">
                            <div style={{ overflow: "hidden" }}>

                                <div className={classes.table}>
                                    <div className={classes.searchPanel}>
                                        <div className={classes.search}>
                                            <Autocomplete
                                                id="tags-standard"
                                                options={Object.entries(responses)}
                                                onChange={(e, v) => handleSearch(e, v)}
                                                getOptionLabel={(option) => option[0]}
                                                renderOption={option => <div style={{ flexDirection: 'row', display: 'flex' }}>
                                                    <div className={classes.searchOption}>{option[0]}</div>
                                                    <div className={classes.searchRes}>{option[1][0].text}</div></div>}
                                                filterSelectedOptions
                                                renderInput={(params) => (
                                                    <TextField
                                                        {...params}
                                                        variant="standard"
                                                        label="Search Responses"
                                                    />
                                                )}
                                            />
                                            {/* <InputBase
              placeholder="Search Responses"
              fullWidth
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            /> */}
                                        </div>
                                    </div>

                                    <div className={classes.accordin}>
                                        <div style={{ width: 300 }} ><DetailedAccordion /></div>
                                        {/* <div style={{ width: 300, border:2,borderColor:"#fff" }} >
                                            <div className={classes.column}>
                                                <Typography className={classes.heading}>Responses</Typography>
                                            </div>
                                        </div> */}
                                        <div>
                                            <div style={{ width: 800 }}>
                                                {!hide && <div style={{ display: 'flex', flexDirection: 'row', padding: 10, minHeight: 48, justifyContent: ' space-between' }}>
                                                    <Typography className={classes.heading} >Response variants for  {responseName}</Typography>
                                                    {/* {addButton && <AddIcon onClick={() => {
                                                        setEdit(!edit)
                                                        setResponse('')
                                                    }
                                                    } />} */}
                                                </div>}</div>
                                            {!hide && <div className={classes.divider}></div>}
                                        </div>

                                    </div>


                                    <div className={classes.tab}>
                                        <Tabs
                                            orientation="vertical"
                                            indicatorColor='#fff'
                                            value={value}
                                            onChange={handleTab}
                                            // aria-label="Vertical tabs example"
                                            className={classes.tabs}
                                        >
                                            {responseSearch ? <Tab label={responseSearch} ></Tab> :
                                                <div className={classes.scroll1}>
                                                    <>
                                                        {Object.entries(responses).map(([key, valu], i) => {
                                                            return (

                                                                <Tab value={i} label={key} className="tabViewStyleNew" style={sectionStyle} onClick={() => {
                                                                    setValue(i)
                                                                    setResponseName(key)
                                                                    setResponseData(valu[0].text)
                                                                    setAddButton(true)
                                                                }} />

                                                            )
                                                        })}
                                                    </>
                                                </div>}
                                        </Tabs>
                                        <div style={{ width: 800 }} >
                                            {!hide && <>
                                                {edit === false ? <TabPanel value={value} index={value} handleEdit={handleEdit}>
                                                    {responseData}
                                                </TabPanel> : <div className={classes.details}>
                                                    <TextField
                                                        id="outlined-multiline-static"
                                                        multiline
                                                        fullWidth
                                                        rows={4}
                                                        color='primary'
                                                        variant="outlined"
                                                        placeholder='Create response variation...'
                                                        style={{ padding: 15 }}
                                                        value={response}
                                                        onChange={(event) => setResponse(event.target.value)}
                                                    />
                                                    <div className={classes.divider}></div>
                                                    <div className={classes.button}>
                                                        <Button size="small" onClick={() => handleCancel()} >Cancel</Button>
                                                        <Button size="small" color="primary" onClick={() => handleSubmit()}>Save</Button>
                                                    </div>
                                                </div>}
                                            </>
                                            }
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
        </Fragment>
    )
}

export default Responses