import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionActions from '@material-ui/core/AccordionActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import { addResponse } from '../../services/responses';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),

  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
  icon: {
    verticalAlign: 'bottom',
    height: 20,
    width: 20,
  },
  details: {
    alignItems: 'center',
  },
  column: {
      justifyContent:'space-between',
    display:'flex',
    flexDirection: 'row',
    width:260
  },
  helper: {
    borderLeft: `2px solid ${theme.palette.divider}`,
    padding: theme.spacing(1, 2),
  },
  link: {
    color: theme.palette.primary.main,
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
}));

export default function Accordionnew(props) {
  const { responseData, responseName,getResponsesList,setResponseData,setEdit } = props;
  const [response, setResponse] = useState('');
  const [expanded, setExpanded] = useState(false);
  const classes = useStyles();

  const handleSubmit = async () => {

    try {
     let result = await addResponse(responseName,response)
     console.log(result);
     getResponsesList();
     setExpanded(false)
     setResponseData(response)
    }
    catch (error) {
      console.log(error)
    }
  }
  const handleCancel = async () => {
setExpanded(!expanded)
  }


  return (
    <div className={classes.root}>
      <Accordion
      >
        <AccordionSummary
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          <div className={classes.column}>
            <Typography className={classes.heading}>Response variants for  {responseName}</Typography>
            <AddIcon onClick={() => setEdit(true)} />
          </div>

        </AccordionSummary>
        {/* <AccordionDetails className={classes.details}>
          <TextField
            id="outlined-multiline-static"
            multiline
            fullWidth
            rows={4}
            color='primary'
            variant="outlined"
            placeholder='Create response variation...'
            value={response}
            onChange={(event) => setResponse(event.target.value)}
          />
        </AccordionDetails>
        <Divider />
        <AccordionActions>
          <Button size="small" onClick={() => handleCancel()}>Cancel</Button>
          <Button size="small" color="primary" onClick={() => handleSubmit()}>
            Save
          </Button>
        </AccordionActions> */}
      </Accordion>
    </div>
  );
}
