export const dataNew = [
  {
    category: null,
    type: "sub menu",
    college_name: null,
    college_type: null,
    program_type: null,
    utterance: "",
    country: "",
    streams_available: [
      {
        stream_name: null,
        program_list: [
          {
            program_code: null,
            details: null
          }
        ]
      }
    ]
  }
]
