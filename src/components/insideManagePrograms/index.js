import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import { AddBox, ArrowDownward } from "@material-ui/icons";
// import MaterialTable, { MTableEditField } from "material-table";
import MaterialTable from "@material-table/core";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import { toast } from "react-toastify";
import { data } from "./data";
import { dataNew } from "./dataNew";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Streams from "./streams";
import StreamsAdd from "./streamsAdd";
import NewModal from "./modal";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import {
  getCollege,
  addCollege,
  updateCollege,
  getSubMenu,
  deleteCollege,
} from "../../services/menuService";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";

const Programs = (props) => {
  const history = useHistory();
  const [dataA, setDataA] = useState(dataNew);
  const [dataE, setDataE] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [addcountry, setAddCountry] = useState("");
  const [addStream, setAddStream] = useState("");
  const [edit, setEdit] = useState(false);
  const [add, setAdd] = useState(false);
  const [addC, setAddC] = useState(false);
  const [addProgram, setAddProgram] = useState("");
  const [details, setDetails] = useState("");
  const [addCollegeType, setAddCollegeType] = useState("");
  const [addProgramType, setAddProgramType] = useState("");
  const [newStream, setNewStream] = useState("");

  const [edit1, setEdit1] = useState(false);
  const [add1, setAdd1] = useState(false);
  const [saveData, setSaveData] = useState(false);
  const [submenuData, setSubmenuData] = useState("");
  const [collegeTypeData, setCollegeTypeData] = useState("");
  const [pagetitle, setPagetitle] = useState("");
  const [id, setId] = useState("");

  const [dataView, setDataView] = useState("");

  useEffect(() => {
    getCollegeList();
    getFranchiseSchoolsData();
  }, []);

  const getCollegeList = async () => {
    try {
      let res = await getCollege("Inside Sultanate");
      // console.log(res);
      setDataE(res);
    } catch (error) {
      console.log(error);
    }
  };

  const handleUploadFile = () => {
    setOpenModal(true);
  };

  const handleAddS = () => {
    setEdit(false);
    setAdd(!add);
    if (add === false) {
      setAddStream("");
    }
  };

  const handleAddCountry = () => {
    setAddC(!addC);
    if (add === false) {
      setAddCountry("");
      setAddCollegeType("");
      setAddProgramType("");
    }
  };

  const handleAddCountryData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objC = {
      category: "داخل السلطنة",
      type: "sub menu",
      college_name: addcountry,
      college_type: addCollegeType,
      program_type: addProgramType,
      unique_title: "Inside Sultanate",
      utterance: "",
      country: "",
      streams_available: [],
    };
    obj = { ...objC };
    // obj = { ...obj, streams_available: [...obj.streams_available, objC] };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    setAddC(!addC);
    setAddCountry("");
    setAddCollegeType("");
    setAddProgramType("");
    toast.success(
      "College added successfully. Proceed to stream and programs section"
    );
    setDataView(formData);
    // console.log(formData, "formData----");
  };

  const handleAddStreamData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objS = {
        stream_name: addStream,
        program_list: [],
      },
      obj = { ...obj, streams_available: [...obj.streams_available, objS] };
    formData[index] = obj;
    setDataA(formData);
    setAddStream("");
    toast.success("Stream added successfully. Proceed to programs section");
    // console.log(formData, "formData----");
  };

  const handleAddProgramData = (indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS];
    // console.log(obj, "obj---");
    var objS = {
        program_code: addProgram,
        details: details,
      },
      obj = { ...obj, program_list: [...obj.program_list, objS] };
    formData[index].streams_available[indexS] = obj;
    setDataA(formData);
    setAddProgram("");
    setDetails("");
    toast.success("Programs added successfully. Click Submit to add on list");
    // console.log(formData, "program_code---");
  };

  const handleEditCountryName = () => {
    setAdd(false);
    setEdit(!edit);
  };

  const handleEditCountry = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, college_name: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditCountry-----------");
  };

  const handleEditCollegeType = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, college_type: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramType-----------");
  };

  const handleEditProgramType = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, program_type: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramType-----------");
  };

  const handleEditStreamData = (e, stream_option, indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS];
    obj = { ...obj, stream_name: e.target.value };
    formData[index].streams_available[indexS] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditStreamData-----------");
  };

  const handleEditProgramData = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, program_code: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramData-----------");
  };

  const handleEditProgramDetails = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, details: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramDetails-----------");
  };

  const handleAddCollege = async () => {
    Swal.fire({
      title: "Do you want to submit ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await addCollege(dataA);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been added successfully.",
              "success"
            );
            window.location.reload(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleEditCountryNameList = (item) => {
    // console.log(item, "e, index--------");
    setAdd1(false);
    setEdit1(!edit1);
  };

  const handleEditCountry1 = (e, index, item) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, college_name: e.target.value };
    formData[index] = obj;
    setDataE(formData);
    // console.log(formData, "handleEditCountry1-----------");
  };

  useEffect(() => {
    // console.log(id, "id-------");
  }, [id]);

  // const handleupdateID = (index) => {
  //   console.log(index, "index----")
  //   var formData = [...dataE];
  //   var obj = dataE[index];
  //   obj = { ...obj, _id: id };
  //   formData[index] = obj;
  //   setDataE(formData);
  //   console.log(formData, "handleupdateID-----------");
  // };

  const handleEditCollegeType1 = (e, index) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, college_type: e.target.value };
    formData[index] = obj;
    setDataE(formData);
    // console.log(formData, "handleEditCollegeType1-----------");
  };

  const handleEditProgramType1 = (e, index) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, program_type: e.target.value };
    formData[index] = obj;
    setDataE(formData);
    // console.log(formData, "handleEditProgramType1-----------");
  };

  const handleEditStreamData1 = (e, stream_option, indexS, index) => {
    var formData = [...dataE];
    var obj = dataE[index].streams_available[indexS];
    obj = { ...obj, stream_name: e.target.value };
    formData[index].streams_available[indexS] = obj;
    setDataE(formData);
  };

  const handleEditProgramData1 = (e, indexS, indexP, index) => {
    var formData = [...dataE];
    var obj = dataE[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, program_code: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setDataE(formData);
    // console.log(formData);
  };

  const handleEditProgramDetails1 = (e, indexS, indexP, index) => {
    var formData = [...dataE];
    var obj = dataE[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, details: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setDataE(formData);
    // console.log(formData);
  };

  const handleSaveCollege = async (index, item) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, _id: item._id.$oid };
    formData[index] = obj;
    setDataE(formData);
    setSaveData(true);
    toast.success("all data saved. Click submit to update");
    // console.log(formData, "save-----");
  };

  const handleUpdateCollege = async (item) => {
    Swal.fire({
      title: "Do you want to update ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await updateCollege(item);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been updated successfully.",
              "success"
            );
            window.location.reload(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  // const handleAddStreamData = (index) => {
  //   var formData = [...dataE];
  //   var obj = dataE[index];
  //   var objS = {
  //       stream_name: addStream,
  //       program_list: [],
  //     },
  //     obj = { ...obj, streams_available: [...obj.streams_available, objS] };
  //   formData[index] = obj;
  //   setDataE(formData);
  //   setAddStream("");
  // };

  // const handleEditCountry = (e, index) => {
  //   var formData = [...dataE];
  //   var obj = dataE[index];
  //   obj = { ...obj, college_name: e.target.value };
  //   formData[index] = obj;
  //   setDataE(formData);
  // };
  // const handleDeleteCountry = (index) => {
  //   var formData = [...dataE];
  //   var array = dataE;
  //   array.splice(index, 1);
  //   formData = array;
  //   setDataE(formData);
  // };

  const handleDeleteStream = (index, indexS) => {
    Swal.fire({
      title: "Do you want to Remove Stream ?",
      text: "Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...dataE];
        var array = dataE[index].streams_available;
        array.splice(indexS, 1);
        formData[index].streams_available = array;
        setDataE(formData);
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleDeleteProgram = (indexS, indexP, index) => {
    Swal.fire({
      title: "Do you want to Remove Program ?",
      text: "Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...dataE];
        var array = dataE[index].streams_available[indexS].program_list;
        array.splice(indexP, 1);
        formData[index].streams_available[indexS].program_list = array;
        setDataE(formData);
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleNewStreamData = (index) => {
    var formData = [...dataE];
    var obj = dataE[index];
    var objS = {
        stream_name: newStream,
        program_list: [],
      },
      obj = { ...obj, streams_available: [...obj.streams_available, objS] };
    formData[index] = obj;
    setDataE(formData);
    setNewStream("");
    toast.success("Stream saved. Proceed to program section");
    // console.log(formData, "formData----stream");
  };

  const handleNewProgramData = (indexS, index) => {
    var formData = [...dataE];
    var obj = dataE[index].streams_available[indexS];
    // console.log(obj, "obj---");
    var objS = {
        program_code: addProgram,
        details: details,
      },
      obj = { ...obj, program_list: [...obj.program_list, objS] };
    formData[index].streams_available[indexS] = obj;
    setDataE(formData);
    toast.success("Program saved. Click submit to update");
    // console.log(formData, "program_code---");
  };

  const handleDeleteInsideCollege = async (item) => {
    let _id = item?._id?.$oid;
    // console.log(item?._id?.$oid, "item?._id?.$oid");
    Swal.fire({
      title: "Do you want to Delete College ?",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Delete`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deleteCollege(_id);
          if (res) {
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  // const handleEditProgramData = (e, indexS, indexP, index) => {
  //   var formData = [...dataE];
  //   var obj = dataE[index].streams_available[indexS].program_code[indexP];
  //   obj = { ...obj, program_code: e.target.value };
  //   formData[index].streams_available[indexS].program_code[indexP] = obj;
  //   setDataE(formData);
  //   console.log(formData);
  // };

  const getFranchiseSchoolsData = async () => {
    try {
      let res = await getSubMenu("Inside Sultanate");
      if (res) {
        setSubmenuData(res);
        setPagetitle(res.category);
      }
    } catch (error) {
      console.log(error);
    }
  };

  var updatedData =
    submenuData?.data &&
    submenuData.data.map((item, index) => {
      return item.title;
    });
  const programTypeList = [...new Set(updatedData)];

  const getCollegeData = async () => {
    try {
      let res = await getSubMenu(addProgramType);
      if (res) {
        setCollegeTypeData(res);
      }
    } catch (error) {
      console.log(error);
    }
  };

  var collegeData =
    collegeTypeData?.data &&
    collegeTypeData.data.map((item, index) => {
      return item.title;
    });
  const collegeTypeList = [...new Set(collegeData)];

  useEffect(() => {
    getCollegeData();
  }, [addProgramType]);

  const handleRefreshPage = async () => {
    history.push(`${process.env.PUBLIC_URL}/app/menus/Dubai`);
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title={pagetitle && pagetitle} />
      <Container fluid={true}>
        <div className="edit-profile">
          <div
            style={{
              cursor: "pointer",
              position: "relative",
              zIndex: 2,
              marginBottom: 10,
              marginTop: -15,
            }}
          >
            <Button onClick={handleRefreshPage}>Main Menu</Button>
          </div>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <FormGroup
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Tooltip title="Upload Excel">
                      <IconButton>
                        <CloudUploadIcon onClick={handleUploadFile} />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Add College">
                      <IconButton>
                        {!addC ? (
                          <Add onClick={handleAddCountry} />
                        ) : (
                          <Clear onClick={handleAddCountry} />
                        )}
                      </IconButton>
                    </Tooltip>
                  </FormGroup>

                  {/* {addC && (
                    <div className="row">
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Country
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Add College"
                            value={addcountry}
                            onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div className="col-md-5">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Program Type
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Program Type"
                            value={addProgramType}
                            onChange={(e) => setAddProgramType(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddCountryData()}
                          style={{ height: 40, marginTop: 10 }}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )} */}
                  {addC && (
                    <div className="row">
                      <div className="col-md-12">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add College
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Add College"
                            value={addcountry}
                            onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div className="col-md-4">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Program Type
                          </Label>
                          <Input
                            type="select"
                            name="select"
                            style={{ height: 38, padding: "0px 10px" }}
                            className="form-control digits"
                            value={addProgramType}
                            onChange={(e) => {
                              setAddProgramType(e.target.value);
                            }}
                          >
                            <option value={"{}"}>
                              Please select program type
                            </option>
                            {programTypeList &&
                              programTypeList.map((item, index) => {
                                return (
                                  <option key={index} value={item}>
                                    {item}
                                  </option>
                                );
                              })}
                          </Input>
                        </FormGroup>
                      </div>
                      <div className="col-md-4">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add College Type
                          </Label>
                          <Input
                            type="select"
                            name="select"
                            style={{ height: 38, padding: "0px 10px" }}
                            className="form-control digits"
                            value={addCollegeType}
                            onChange={(e) => {
                              setAddCollegeType(e.target.value);
                            }}
                          >
                            <option value={"{}"}>
                              Please select College Type
                            </option>
                            {collegeTypeList &&
                              collegeTypeList.map((item, index) => {
                                return (
                                  <option key={index} value={item}>
                                    {item}
                                  </option>
                                );
                              })}
                          </Input>
                        </FormGroup>
                      </div>
                      <div className="col-md-3">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Unique Title
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Unique Title"
                            value="Inside Sultanate"
                            readOnly={true}
                          />
                        </FormGroup>
                      </div>
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddCountryData()}
                          style={{ height: 40, marginTop: 10 }}
                          disabled={
                            addProgramType && addCollegeType ? false : true
                          }
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )}
                  {dataA &&
                    dataA.map((item, index) => {
                      return (
                        index != 0 && (
                          <div
                            className="accordinContainer accordinContainerStyle"
                            key={index}
                          >
                            <Accordion style={{ marginTop: 10 }}>
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1c-content"
                                id="panel1c-header"
                              >
                                College : {item.college_name}
                              </AccordionSummary>
                              <AccordionDetails
                                style={{ flexDirection: "column" }}
                              >
                                <div className="AccordionSummaryHeading">
                                  <div className="AccordionSummaryHeadingActionButtons">
                                    {!edit ? (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Edit Country and College type
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                    {/* <Tooltip title="Edit College and details">
                                      <IconButton>
                                        {!edit ? (
                                          <Edit
                                            onClick={handleEditCountryName}
                                          />
                                        ) : (
                                          <Check
                                            onClick={handleEditCountryName}
                                          />
                                        )}
                                      </IconButton>
                                    </Tooltip> */}
                                  </div>
                                </div>
                                <div>
                                  <div className="row">
                                    {edit && (
                                      <div className="col-md-12">
                                        <FormGroup>
                                          <Label className="col-form-label pt-0">
                                            Edit college
                                          </Label>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Edit college"
                                            value={item.college_name}
                                            onChange={(e) =>
                                              handleEditCountry(e, index)
                                            }
                                          />
                                        </FormGroup>
                                      </div>
                                    )}
                                    <div className="col-md-4">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          {edit
                                            ? "Edit college Type"
                                            : "Add college Type"}
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="College Type"
                                          value={item.college_type}
                                          onChange={(e) =>
                                            handleEditCollegeType(e, index)
                                          }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div className="col-md-4">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Program Type
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Enter Program Type"
                                          value={item.program_type}
                                          onChange={(e) =>
                                            handleEditProgramType(e, index)
                                          }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div className="col-md-4">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Unique Title
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Unique Title"
                                          value="Inside Sultanate"
                                          // onChange={(e) =>
                                          //   handleEditCollegeType(e, index)
                                          // }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div
                                      className="col-md-12"
                                      style={{
                                        display: "flex",
                                        justifyContent: "flex-end",
                                      }}
                                    >
                                      {add ? (
                                        item.program_type !=
                                          "برامج لذوي الإعاقة" && (
                                          <Button
                                            onClick={handleAddS}
                                            style={{
                                              height: 40,
                                              marginTop: 10,
                                              marginBottom: 10,
                                            }}
                                          >
                                            Remove Stream
                                          </Button>
                                        )
                                      ) : (
                                        <Button
                                          onClick={handleAddS}
                                          style={{
                                            height: 40,
                                            marginTop: 10,
                                            marginBottom: 10,
                                          }}
                                        >
                                          {/* {addProgramType === "برامج لذوي الإعاقة"} */}
                                          {item.program_type ===
                                          "برامج لذوي الإعاقة"
                                            ? "Continue"
                                            : "Add Stream"}
                                        </Button>
                                      )}
                                    </div>
                                  </div>
                                </div>
                                {add && (
                                  <FormGroup>
                                    {item.program_type !=
                                      "برامج لذوي الإعاقة" && (
                                      <Label className="col-form-label pt-0">
                                        Add Stream
                                      </Label>
                                    )}
                                    <div
                                      style={{
                                        display: "flex",
                                        justifyContent: "flex-end",
                                      }}
                                    >
                                      {item.program_type !=
                                        "برامج لذوي الإعاقة" && (
                                        <Input
                                          className="form-control"
                                          type="text"
                                          placeholder="Enter Stream"
                                          value={addStream}
                                          onChange={(e) =>
                                            setAddStream(e.target.value)
                                          }
                                        />
                                      )}
                                      <Button
                                        onClick={() =>
                                          handleAddStreamData(index)
                                        }
                                        style={{ height: 38 }}
                                      >
                                        {item.program_type !=
                                        "برامج لذوي الإعاقة"
                                          ? "Add"
                                          : "Add Program"}
                                      </Button>
                                      {/* <Tooltip title="Save">
                                        <IconButton>
                                          <Check
                                            onClick={() =>
                                              handleAddStreamData(index)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip> */}
                                    </div>
                                  </FormGroup>
                                )}
                                {item.streams_available.map((itemS, indexS) => {
                                  return (
                                    <div className="accordinContainer">
                                      <StreamsAdd
                                        dataView={item.program_type}
                                        itemS={itemS}
                                        indexS={indexS}
                                        index={index}
                                        handleEditProgramDetails={
                                          handleEditProgramDetails
                                        }
                                        handleEditStreamData={
                                          handleEditStreamData
                                        }
                                        handleDeleteProgram={
                                          handleDeleteProgram
                                        }
                                        handleAddProgramData={
                                          handleAddProgramData
                                        }
                                        handleEditProgramData={
                                          handleEditProgramData
                                        }
                                        setAddProgram={setAddProgram}
                                        addProgram={addProgram}
                                        handleAddS={handleAddS}
                                        setDetails={setDetails}
                                        details={details}
                                      />
                                      {/* <Tooltip title="Delete Stream">
                                      <IconButton>
                                        <DeleteOutline
                                          onClick={() =>
                                            handleDeleteStream(index, indexS)
                                          }
                                        />
                                      </IconButton>
                                    </Tooltip> */}
                                    </div>
                                  );
                                })}
                                <div className="row insideSubmitButton">
                                  <Button onClick={handleAddCollege}>
                                    Submit
                                  </Button>
                                </div>
                              </AccordionDetails>
                            </Accordion>
                            {/* <Tooltip title="Delete Country">
                            <IconButton>
                              <DeleteOutline
                                onClick={() => handleDeleteCountry(index)}
                              />
                            </IconButton>
                          </Tooltip> */}
                          </div>
                        )
                      );
                    })}
                  {/* sbksakcbksabcasbcbsakcbksakcksakcbksackakscbksabkcbsckaskcbksacksabkc */}
                  {dataE &&
                    dataE.map((item, index) => {
                      return (
                        <div className="accordinContainer">
                          <Accordion key={index} style={{ marginTop: 10 }}>
                            <AccordionSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1c-content"
                              id="panel1c-header"
                            >
                              College : {item.college_name}
                            </AccordionSummary>
                            <AccordionDetails
                              style={{ flexDirection: "column" }}
                            >
                              <div className="AccordionSummaryHeading">
                                {/* <div> College : {item.college_name}</div> */}
                                <div className="AccordionSummaryHeadingActionButtons">
                                  <Tooltip title="Edit College and details">
                                    {!edit1 ? (
                                      <Button
                                        onClick={() =>
                                          handleEditCountryNameList(item)
                                        }
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Edit
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={() =>
                                          handleEditCountryNameList(item)
                                        }
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                  </Tooltip>
                                  <Tooltip title="Add Stream">
                                    {!add1 ? (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Add Stream
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Remove
                                      </Button>
                                    )}
                                  </Tooltip>
                                </div>
                              </div>
                              <div>
                                {edit1 && (
                                  <FormGroup>
                                    <Label className="col-form-label pt-0">
                                      Edit College
                                    </Label>
                                    <Input
                                      className="form-control"
                                      type="text"
                                      placeholder="Edit College"
                                      value={item.college_name}
                                      onChange={(e) =>
                                        handleEditCountry1(e, index, item)
                                      }
                                    />
                                  </FormGroup>
                                )}
                                <div className="row">
                                  <div className="col-md-4">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        College Type
                                      </Label>
                                      <Input
                                        readOnly={edit1 ? false : true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Enter College Type"
                                        value={item.college_type}
                                        onChange={(e) =>
                                          handleEditCollegeType1(e, index)
                                        }
                                      />
                                    </FormGroup>
                                  </div>
                                  <div className="col-md-4">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Program Type
                                      </Label>
                                      <Input
                                        readOnly={edit1 ? false : true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Enter Program Type"
                                        value={item.program_type}
                                        onChange={(e) =>
                                          handleEditProgramType1(e, index)
                                        }
                                      />
                                    </FormGroup>
                                  </div>
                                  <div className="col-md-4">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Unique Title
                                      </Label>
                                      <Input
                                        readOnly={true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Unique Title"
                                        value="Inside Sultanate"
                                        // onChange={(e) =>
                                        //   handleEditProgramType1(e, index)
                                        // }
                                      />
                                    </FormGroup>
                                  </div>
                                </div>
                              </div>
                              {add && (
                                <FormGroup>
                                  <Label className="col-form-label pt-0">
                                    Add Stream
                                  </Label>
                                  <div style={{ display: "flex" }}>
                                    <Input
                                      className="form-control"
                                      type="text"
                                      placeholder="Enter Stream"
                                      value={newStream}
                                      onChange={(e) =>
                                        setNewStream(e.target.value)
                                      }
                                    />
                                    <Button
                                      onClick={() => handleNewStreamData(index)}
                                      style={{ height: 38 }}
                                    >
                                      Add
                                    </Button>
                                  </div>
                                </FormGroup>
                              )}
                              {item.streams_available.map((itemS, indexS) => {
                                return (
                                  <div className="accordinContainer">
                                    <Streams
                                      dataView={item.program_type}
                                      itemS={itemS}
                                      indexS={indexS}
                                      index={index}
                                      handleEditStreamData1={
                                        handleEditStreamData1
                                      }
                                      handleDeleteProgram={handleDeleteProgram}
                                      handleEditProgramData1={
                                        handleEditProgramData1
                                      }
                                      handleEditProgramDetails1={
                                        handleEditProgramDetails1
                                      }
                                      setAddProgram={setAddProgram}
                                      addProgram={addProgram}
                                      handleAddS={handleAddS}
                                      setDetails={setDetails}
                                      details={details}
                                      handleNewProgramData={
                                        handleNewProgramData
                                      }
                                    />
                                    <Tooltip title="Delete Stream">
                                      <IconButton>
                                        <DeleteOutline
                                          onClick={() =>
                                            handleDeleteStream(index, indexS)
                                          }
                                        />
                                      </IconButton>
                                    </Tooltip>
                                  </div>
                                );
                              })}
                              <div style={{ display: "flex" }}>
                                <Button
                                  onClick={() => handleSaveCollege(index, item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Save
                                </Button>
                                <Button
                                  disabled={saveData ? false : true}
                                  onClick={() => handleUpdateCollege(item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Submit
                                </Button>
                              </div>
                            </AccordionDetails>
                          </Accordion>
                          <div style={{ marginLeft: 10 }}>
                            <Tooltip title="Delete College">
                              <IconButton>
                                <DeleteOutline
                                  onClick={() =>
                                    handleDeleteInsideCollege(item)
                                  }
                                />
                              </IconButton>
                            </Tooltip>
                          </div>
                        </div>
                      );
                    })}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
      <NewModal
        open={openModal}
        setOpen={setOpenModal}
        getCollegeData={getCollegeData}
      />
    </Fragment>
  );
};

export default Programs;

// import React, {
//   Fragment,
//   useEffect,
//   useState,
//   Component,
//   forwardRef,
// } from "react";
// import Breadcrumb from "../../layout/breadcrumb";
// import {
//   Container,
//   Row,
//   Col,
//   Card,
//   CardHeader,
//   CardBody,
//   CardFooter,
//   Media,
//   Form,
//   FormGroup,
//   Label,
//   Input,
//   Button,
// } from "reactstrap";
// import { AddBox, ArrowDownward } from "@material-ui/icons";
// import MaterialTable, { MTableEditField } from "material-table";
// import Check from "@material-ui/icons/Check";
// import ChevronLeft from "@material-ui/icons/ChevronLeft";
// import ChevronRight from "@material-ui/icons/ChevronRight";
// import Clear from "@material-ui/icons/Clear";
// import DeleteOutline from "@material-ui/icons/DeleteOutline";
// import Edit from "@material-ui/icons/Edit";
// import Add from "@material-ui/icons/Add";
// import FilterList from "@material-ui/icons/FilterList";
// import FirstPage from "@material-ui/icons/FirstPage";
// import LastPage from "@material-ui/icons/LastPage";
// import Remove from "@material-ui/icons/Remove";
// import SaveAlt from "@material-ui/icons/SaveAlt";
// import Search from "@material-ui/icons/Search";
// import ViewColumn from "@material-ui/icons/ViewColumn";
// import { toast } from "react-toastify";
// import { data } from "./data";
// import { dataNew } from "./dataNew";
// import Accordion from "@material-ui/core/Accordion";
// import AccordionDetails from "@material-ui/core/AccordionDetails";
// import AccordionSummary from "@material-ui/core/AccordionSummary";
// import AccordionActions from "@material-ui/core/AccordionActions";
// import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
// import Streams from "./streams";
// import StreamsAdd from "./streamsAdd";
// import IconButton from "@material-ui/core/IconButton";
// import Tooltip from "@material-ui/core/Tooltip";
// import { getCollege, addCollege } from "../../services/menuService";

// const Programs = (props) => {
//   const [dataA, setDataA] = useState(dataNew);
//   const [dataE, setDataE] = useState([]);
//   const [addcountry, setAddCountry] = useState("");
//   const [addStream, setAddStream] = useState("");
//   const [edit, setEdit] = useState(false);
//   const [add, setAdd] = useState(false);
//   const [addC, setAddC] = useState(false);
//   const [addProgram, setAddProgram] = useState("");
//   const [details, setDetails] = useState("");
//   const [addCollegeType, setAddCollegeType] = useState("");
//   const [addProgramType, setAddProgramType] = useState("");

//   useEffect(() => {
//     getCollegeList();
//   }, []);

//   const getCollegeList = async () => {
//     try {
//       let res = await getCollege("Inside Sultanate");
//       console.log(res);
//       setDataE(res);
//     } catch (error) {
//       console.log(error);
//     }
//   };

//   const handleEditCountryName = () => {
//     setAdd(false);
//     setEdit(!edit);
//   };
//   const handleAddCountry = () => {
//     setAddC(!addC);
//     if (add === false) {
//       setAddCountry("");
//       setAddCollegeType("");
//       setAddProgramType("");
//     }
//   };
//   const handleAddS = () => {
//     setEdit(false);
//     setAdd(!add);
//     if (add === false) {
//       setAddStream("");
//     }
//   };

//   const handleAddCountryData = (index) => {
//     var formData = [...dataA];
//     var obj = dataA[index];
//     var objC = {
//       category: "Inside Sultanate",
//       type: "sub menu",
//       college_name: addcountry,
//       college_type: addCollegeType,
//       program_type: addProgramType,
//       utterance: "",
//       country: "",
//       streams_available: [],
//     };
//     obj = { ...objC };
//     // obj = { ...obj, streams_available: [...obj.streams_available, objC] };
//     formData[dataA?.length && dataA.length] = obj;
//     setDataA(formData);
//     setAddC(!addC);
//     setAddCountry("");
//     setAddCollegeType("");
//     setAddProgramType("");
//     toast.success("College added successfully. Proceed to stream and programs section");
//     console.log(formData, "formData----");
//   };

//   const handleAddStreamData = (index) => {
//     var formData = [...dataA];
//     var obj = dataA[index];
//     var objS = {
//         stream_name: addStream,
//         program_list: [],
//       },
//       obj = { ...obj, streams_available: [...obj.streams_available, objS] };
//     formData[index] = obj;
//     setDataA(formData);
//     setAddStream("");
//     toast.success("Stream added successfully. Proceed to programs section");
//     console.log(formData, "formData----");
//   };

//   const handleAddProgramData = (indexS, index) => {
//     var formData = [...dataA];
//     var obj = dataA[index].streams_available[indexS];
//     console.log(obj, "obj---");
//     var objS = {
//         program_code: addProgram,
//         details: details,
//       },
//       obj = { ...obj, program_list: [...obj.program_list, objS] };
//     formData[index].streams_available[indexS] = obj;
//     setDataA(formData);
//     setAddProgram("");
//     setDetails("");
//     toast.success("Programs added successfully. Click Submit to add on list");
//     console.log(formData, "program_code---");
//   };

//   const handleAddCollege = async () => {
//     try {
//       let res = await addCollege(dataA);
//       console.log(res, "res-----------");
//       // setResponses(res)
//     } catch (error) {
//       console.log(error);
//     }
//     // setOpen(true);
//     // seteditrowData(rowData);
//   };

//   // const handleAddStreamData = (index) => {
//   //   var formData = [...dataE];
//   //   var obj = dataE[index];
//   //   var objS = {
//   //       stream_name: addStream,
//   //       program_list: [],
//   //     },
//   //     obj = { ...obj, streams_available: [...obj.streams_available, objS] };
//   //   formData[index] = obj;
//   //   setDataE(formData);
//   //   setAddStream("");
//   // };

//   const handleEditCountry = (e, index) => {
//     var formData = [...dataE];
//     var obj = dataE[index];
//     obj = { ...obj, college_name: e.target.value };
//     formData[index] = obj;
//     setDataE(formData);
//   };
//   const handleDeleteCountry = (index) => {
//     var formData = [...dataE];
//     var array = dataE;
//     array.splice(index, 1);
//     formData = array;
//     setDataE(formData);
//   };

//   const handleDeleteStream = (index, indexS) => {
//     var formData = [...dataE];
//     var array = dataE[index].streams_available;
//     array.splice(indexS, 1);
//     formData[index].streams_available = array;
//     setDataE(formData);
//   };
//   const handleEditStreamData = (e, stream_option, indexS, index) => {
//     var formData = [...dataE];
//     var obj = dataE[index].streams_available[indexS];
//     obj = { ...obj, stream_name: e.target.value };
//     formData[index].streams_available[indexS] = obj;
//     setDataE(formData);
//   };
//   const handleDeleteProgram = (indexS, indexP, index) => {
//     var formData = [...dataE];
//     var array = dataE[index].streams_available[indexS].program_code;
//     array.splice(indexP, 1);
//     formData[index].streams_available[indexS].program_code = array;
//     setDataE(formData);
//   };

//   const handleEditProgramData = (e, indexS, indexP, index) => {
//     var formData = [...dataE];
//     var obj = dataE[index].streams_available[indexS].program_code[indexP];
//     obj = { ...obj, program_code: e.target.value };
//     formData[index].streams_available[indexS].program_code[indexP] = obj;
//     setDataE(formData);
//     console.log(formData);
//   };

//   return (
//     <Fragment>
//       <Breadcrumb parent="Apps" title="Inside Sultanate" />
//       <Container fluid={true}>
//         <div className="edit-profile">
//           <Row>
//             <Col md="12">
//               <Card>
//                 <CardBody>
//                   <FormGroup
//                     style={{ display: "flex", justifyContent: "flex-end" }}
//                   >
//                     <Tooltip title="Add College">
//                       <IconButton>
//                         {!addC ? (
//                           <Add onClick={handleAddCountry} />
//                         ) : (
//                           <Clear onClick={handleAddCountry} />
//                         )}
//                       </IconButton>
//                     </Tooltip>
//                   </FormGroup>

//                   {addC && (
//                     <div className="row">
//                       <div className="col-md-12">
//                         <FormGroup>
//                           <Label className="col-form-label pt-0">
//                             Add College
//                           </Label>
//                           <Input
//                             className="form-control"
//                             type="text"
//                             placeholder="Add College"
//                             value={addcountry}
//                             onChange={(e) => setAddCountry(e.target.value)}
//                           />
//                         </FormGroup>
//                       </div>
//                       <div className="col-md-5">
//                         <FormGroup>
//                           <Label className="col-form-label pt-0">
//                             Add College Type
//                           </Label>
//                           <Input
//                             className="form-control"
//                             type="text"
//                             placeholder="College Type"
//                             value={addCollegeType}
//                             onChange={(e) => setAddCollegeType(e.target.value)}
//                           />
//                         </FormGroup>
//                       </div>
//                       <div className="col-md-6">
//                         <FormGroup>
//                           <Label className="col-form-label pt-0">
//                             Add Program Type
//                           </Label>
//                           <Input
//                             className="form-control"
//                             type="text"
//                             placeholder="Program Type"
//                             value={addProgramType}
//                             onChange={(e) => setAddProgramType(e.target.value)}
//                           />
//                         </FormGroup>
//                       </div>
//                       <div
//                         className="col-md-1"
//                         style={{ display: "flex", alignItems: "center" }}
//                       >
//                         <Button
//                           onClick={() => handleAddCountryData()}
//                           style={{ height: 40, marginTop: 10 }}
//                         >
//                           Add
//                         </Button>
//                       </div>
//                     </div>
//                   )}
//                   {dataA &&
//                     dataA.map((item, index) => {
//                       return (
//                         index != 0 && (
//                           <div className="accordinContainer accordinContainerStyle" key={index}>
//                             <Accordion style={{ marginTop: 10 }}>
//                               <AccordionSummary
//                                 expandIcon={<ExpandMoreIcon />}
//                                 aria-controls="panel1c-content"
//                                 id="panel1c-header"
//                               >
//                                 College : {item.college_name}
//                               </AccordionSummary>
//                               <AccordionDetails
//                                 style={{ flexDirection: "column" }}
//                               >
//                                 <div className="AccordionSummaryHeading">
//                                   {/* <div> College : {item.college_name}</div> */}
//                                   <div className="AccordionSummaryHeadingActionButtons">
//                                     {/* <Tooltip title="Edit College and details">
//                                     <IconButton>
//                                       {!edit ? (
//                                         <Edit onClick={handleEditCountryName} />
//                                       ) : (
//                                         <Clear
//                                           onClick={handleEditCountryName}
//                                         />
//                                       )}
//                                     </IconButton>
//                                   </Tooltip> */}
//                                   </div>
//                                 </div>
//                                 <div>
//                                   {/* {edit && (
//                                   <FormGroup>
//                                     <Label className="col-form-label pt-0">
//                                       Edit College
//                                     </Label>
//                                     <Input
//                                       className="form-control"
//                                       type="text"
//                                       placeholder="Edit College"
//                                       value={item.college_name}
//                                       onChange={(e) =>
//                                         handleEditCountry(e, index)
//                                       }
//                                     />
//                                   </FormGroup>
//                                 )} */}
//                                   <div className="row">
//                                     <div className="col-md-6">
//                                       <FormGroup>
//                                         <Label className="col-form-label pt-0">
//                                           College Type
//                                         </Label>
//                                         <Input
//                                           readOnly={edit ? false : true}
//                                           className="form-control"
//                                           type="text"
//                                           placeholder="Enter College Type"
//                                           value={item.college_type}
//                                           onChange={(e) =>
//                                             handleEditCountry(
//                                               e,
//                                               item.country_option,
//                                               index
//                                             )
//                                           }
//                                         />
//                                       </FormGroup>
//                                     </div>
//                                     <div className="col-md-6">
//                                       <FormGroup>
//                                         <Label className="col-form-label pt-0">
//                                           Program Type
//                                         </Label>
//                                         <Input
//                                           readOnly={edit ? false : true}
//                                           className="form-control"
//                                           type="text"
//                                           placeholder="Enter Program Type"
//                                           value={item.program_type}
//                                           onChange={(e) =>
//                                             handleEditCountry(
//                                               e,
//                                               item.country_option,
//                                               index
//                                             )
//                                           }
//                                         />
//                                       </FormGroup>
//                                     </div>
//                                     <div
//                                       className="col-md-12"
//                                       style={{
//                                         display: "flex",
//                                         justifyContent: "flex-end",
//                                       }}
//                                     >
//                                       {add ? (
//                                         <Button
//                                           onClick={handleAddS}
//                                           style={{
//                                             height: 40,
//                                             marginTop: 10,
//                                             marginBottom: 10,
//                                           }}
//                                         >
//                                           Remove Stream
//                                         </Button>
//                                       ) : (
//                                         <Button
//                                           onClick={handleAddS}
//                                           style={{
//                                             height: 40,
//                                             marginTop: 10,
//                                             marginBottom: 10,
//                                           }}
//                                         >
//                                           Add Stream
//                                         </Button>
//                                       )}
//                                       {/* <Tooltip title="Add Stream">
//                                         <IconButton>
//                                           {!add ? (
//                                             <Add onClick={handleAddS} />
//                                           ) : (
//                                             <Clear onClick={handleAddS} />
//                                           )}
//                                         </IconButton>
//                                       </Tooltip> */}
//                                     </div>
//                                   </div>
//                                 </div>
//                                 {add && (
//                                   <FormGroup>
//                                     <Label className="col-form-label pt-0">
//                                       Add Stream
//                                     </Label>
//                                     <div style={{ display: "flex" }}>
//                                       <Input
//                                         className="form-control"
//                                         type="text"
//                                         placeholder="Enter Stream"
//                                         value={addStream}
//                                         onChange={(e) =>
//                                           setAddStream(e.target.value)
//                                         }
//                                       />
//                                       <Button
//                                         onClick={() =>
//                                           handleAddStreamData(index)
//                                         }
//                                         style={{ height: 38 }}
//                                       >
//                                         Add
//                                       </Button>
//                                       {/* <Tooltip title="Save">
//                                         <IconButton>
//                                           <Check
//                                             onClick={() =>
//                                               handleAddStreamData(index)
//                                             }
//                                           />
//                                         </IconButton>
//                                       </Tooltip> */}
//                                     </div>
//                                   </FormGroup>
//                                 )}
//                                 {item.streams_available.map((itemS, indexS) => {
//                                   return (
//                                     <div className="accordinContainer">
//                                       <StreamsAdd
//                                         itemS={itemS}
//                                         indexS={indexS}
//                                         index={index}
//                                         handleEditStreamData={
//                                           handleEditStreamData
//                                         }
//                                         handleDeleteProgram={
//                                           handleDeleteProgram
//                                         }
//                                         handleAddProgramData={
//                                           handleAddProgramData
//                                         }
//                                         handleEditProgramData={
//                                           handleEditProgramData
//                                         }
//                                         setAddProgram={setAddProgram}
//                                         addProgram={addProgram}
//                                         handleAddS={handleAddS}
//                                         setDetails={setDetails}
//                                         details={details}
//                                       />
//                                       {/* <Tooltip title="Delete Stream">
//                                       <IconButton>
//                                         <DeleteOutline
//                                           onClick={() =>
//                                             handleDeleteStream(index, indexS)
//                                           }
//                                         />
//                                       </IconButton>
//                                     </Tooltip> */}
//                                     </div>
//                                   );
//                                 })}
//                                 <div className="row insideSubmitButton">
//                                   <Button onClick={handleAddCollege}>
//                                     Submit
//                                   </Button>
//                                 </div>
//                               </AccordionDetails>
//                             </Accordion>
//                             {/* <Tooltip title="Delete Country">
//                             <IconButton>
//                               <DeleteOutline
//                                 onClick={() => handleDeleteCountry(index)}
//                               />
//                             </IconButton>
//                           </Tooltip> */}
//                           </div>
//                         )
//                       );
//                     })}
//                   {/* sbksakcbksabcasbcbsakcbksakcksakcbksackakscbksabkcbsckaskcbksacksabkc */}
//                   {dataE &&
//                     dataE.map((item, index) => {
//                       return (
//                         <div className="accordinContainer">
//                           <Accordion key={index} style={{ marginTop: 10 }}>
//                             <AccordionSummary
//                               expandIcon={<ExpandMoreIcon />}
//                               aria-controls="panel1c-content"
//                               id="panel1c-header"
//                             >
//                               College : {item.college_name}
//                             </AccordionSummary>
//                             <AccordionDetails
//                               style={{ flexDirection: "column" }}
//                             >
//                               <div className="AccordionSummaryHeading">
//                                 {/* <div> College : {item.college_name}</div> */}
//                                 <div className="AccordionSummaryHeadingActionButtons">
//                                   {/* <Tooltip title="Edit College and details">
//                                     <IconButton>
//                                       {!edit ? (
//                                         <Edit onClick={handleEditCountryName} />
//                                       ) : (
//                                         <Clear
//                                           onClick={handleEditCountryName}
//                                         />
//                                       )}
//                                     </IconButton>
//                                   </Tooltip> */}
//                                   {/* <Tooltip title="Add Stream">
//                                     <IconButton>
//                                       {!add ? (
//                                         <Add onClick={handleAddS} />
//                                       ) : (
//                                         <Clear onClick={handleAddS} />
//                                       )}
//                                     </IconButton>
//                                   </Tooltip> */}
//                                 </div>
//                               </div>
//                               <div>
//                                 {/* {edit && (
//                                   <FormGroup>
//                                     <Label className="col-form-label pt-0">
//                                       Edit College
//                                     </Label>
//                                     <Input
//                                       className="form-control"
//                                       type="text"
//                                       placeholder="Edit College"
//                                       value={item.college_name}
//                                       onChange={(e) =>
//                                         handleEditCountry(e, index)
//                                       }
//                                     />
//                                   </FormGroup>
//                                 )} */}
//                                 <div className="row">
//                                   <div className="col-md-6">
//                                     <FormGroup>
//                                       <Label className="col-form-label pt-0">
//                                         College Type
//                                       </Label>
//                                       <Input
//                                         readOnly={edit ? false : true}
//                                         className="form-control"
//                                         type="text"
//                                         placeholder="Enter College Type"
//                                         value={item.college_type}
//                                         onChange={(e) =>
//                                           handleEditCountry(
//                                             e,
//                                             item.country_option,
//                                             index
//                                           )
//                                         }
//                                       />
//                                     </FormGroup>
//                                   </div>
//                                   <div className="col-md-6">
//                                     <FormGroup>
//                                       <Label className="col-form-label pt-0">
//                                         Program Type
//                                       </Label>
//                                       <Input
//                                         readOnly={edit ? false : true}
//                                         className="form-control"
//                                         type="text"
//                                         placeholder="Enter Program Type"
//                                         value={item.program_type}
//                                         onChange={(e) =>
//                                           handleEditCountry(
//                                             e,
//                                             item.country_option,
//                                             index
//                                           )
//                                         }
//                                       />
//                                     </FormGroup>
//                                   </div>
//                                 </div>
//                               </div>
//                               {add && (
//                                 <FormGroup>
//                                   <Label className="col-form-label pt-0">
//                                     Add Stream
//                                   </Label>
//                                   <div style={{ display: "flex" }}>
//                                     <Input
//                                       className="form-control"
//                                       type="text"
//                                       placeholder="Enter Stream"
//                                       value={addStream}
//                                       onChange={(e) =>
//                                         setAddStream(e.target.value)
//                                       }
//                                     />
//                                     <Tooltip title="Save">
//                                       <IconButton>
//                                         <Check
//                                           onClick={() =>
//                                             handleAddStreamData(index)
//                                           }
//                                         />
//                                       </IconButton>
//                                     </Tooltip>
//                                   </div>
//                                 </FormGroup>
//                               )}
//                               {item.streams_available.map((itemS, indexS) => {
//                                 return (
//                                   <div className="accordinContainer">
//                                     <Streams
//                                       itemS={itemS}
//                                       indexS={indexS}
//                                       index={index}
//                                       handleEditStreamData={
//                                         handleEditStreamData
//                                       }
//                                       handleDeleteProgram={handleDeleteProgram}
//                                       handleAddProgramData={
//                                         handleAddProgramData
//                                       }
//                                       handleEditProgramData={
//                                         handleEditProgramData
//                                       }
//                                       setAddProgram={setAddProgram}
//                                       addProgram={addProgram}
//                                       handleAddS={handleAddS}
//                                       setDetails={setDetails}
//                                       details={details}
//                                     />
//                                     {/* <Tooltip title="Delete Stream">
//                                       <IconButton>
//                                         <DeleteOutline
//                                           onClick={() =>
//                                             handleDeleteStream(index, indexS)
//                                           }
//                                         />
//                                       </IconButton>
//                                     </Tooltip> */}
//                                   </div>
//                                 );
//                               })}
//                             </AccordionDetails>
//                           </Accordion>
//                           {/* <Tooltip title="Delete Country">
//                             <IconButton>
//                               <DeleteOutline
//                                 onClick={() => handleDeleteCountry(index)}
//                               />
//                             </IconButton>
//                           </Tooltip> */}
//                         </div>
//                       );
//                     })}
//                 </CardBody>
//               </Card>
//             </Col>
//           </Row>
//         </div>
//       </Container>
//     </Fragment>
//   );
// };

// export default Programs;

// // import React, {
// //   Fragment,
// //   useEffect,
// //   useState,
// //   Component,
// //   forwardRef,
// // } from "react";
// // import Breadcrumb from "../../layout/breadcrumb";
// // import {
// //   Container,
// //   Row,
// //   Col,
// //   Card,
// //   CardHeader,
// //   CardBody,
// //   CardFooter,
// //   Media,
// //   Form,
// //   FormGroup,
// //   Label,
// //   Input,
// //   Button,
// // } from "reactstrap";
// // import { AddBox, ArrowDownward } from "@material-ui/icons";
// // import MaterialTable, { MTableEditField } from "material-table";
// // import Check from "@material-ui/icons/Check";
// // import ChevronLeft from "@material-ui/icons/ChevronLeft";
// // import ChevronRight from "@material-ui/icons/ChevronRight";
// // import Clear from "@material-ui/icons/Clear";
// // import DeleteOutline from "@material-ui/icons/DeleteOutline";
// // import Edit from "@material-ui/icons/Edit";
// // import Add from "@material-ui/icons/Add";
// // import FilterList from "@material-ui/icons/FilterList";
// // import FirstPage from "@material-ui/icons/FirstPage";
// // import LastPage from "@material-ui/icons/LastPage";
// // import Remove from "@material-ui/icons/Remove";
// // import SaveAlt from "@material-ui/icons/SaveAlt";
// // import Search from "@material-ui/icons/Search";
// // import ViewColumn from "@material-ui/icons/ViewColumn";
// // import { toast } from "react-toastify";
// // import { data } from "./data";
// // import { dataNew } from "./dataNew";
// // import Accordion from "@material-ui/core/Accordion";
// // import AccordionDetails from "@material-ui/core/AccordionDetails";
// // import AccordionSummary from "@material-ui/core/AccordionSummary";
// // import AccordionActions from "@material-ui/core/AccordionActions";
// // import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
// // import Streams from "./streams";
// // import IconButton from "@material-ui/core/IconButton";
// // import Tooltip from "@material-ui/core/Tooltip";
// // import { getCollege, addCollege } from "../../services/menuService";

// // const Programs = (props) => {
// //   // const [dataA, setDataA] = useState(data);
// //   const [dataE, setDataE] = useState([]);
// //   const [addcountry, setAddCountry] = useState("");
// //   const [addStream, setAddStream] = useState("");
// //   const [edit, setEdit] = useState(false);
// //   const [add, setAdd] = useState(false);
// //   const [addC, setAddC] = useState(false);
// //   const [addProgram, setAddProgram] = useState("");
// //   const [details, setDetails] = useState("");
// //   const [addCollegeType, setAddCollegeType] = useState("");
// //   const [addProgramType, setAddProgramType] = useState("");

// //   const [dataA, setDataA] = useState(dataNew);
// //   const [edit1, setEdit1] = useState(false);
// //   const [add1, setAdd1] = useState(false);
// //   const [addC1, setAddC1] = useState(false);
// //   const [addCollegeType1, setAddCollegeType1] = useState("");
// //   const [addProgramType1, setAddProgramType1] = useState("");
// //   const [addcountry1, setAddCountry1] = useState("");
// //   const [addStream1, setAddStream1] = useState("");
// //   const [fieldsProgram, setFieldsProgram] = useState([{ value: null }]);
// //   const [fieldsStream, setFieldsStream] = useState([{ value: null }]);
// //   const [addProgram1, setAddProgram1] = useState("");
// //   const [details1, setDetails1] = useState("");

// //   useEffect(() => {
// //     getCollegeList();
// //   }, []);

// //   const getCollegeList = async () => {
// //     try {
// //       let res = await getCollege("Inside Sultanate");
// //       console.log(res);
// //       setDataE(res);
// //     } catch (error) {
// //       console.log(error);
// //     }
// //   };

// //   const handleEditCountryName = () => {
// //     setAdd(false);
// //     setEdit(!edit);
// //   };
// //   const handleAddCountry = () => {
// //     setAddC(!addC);
// //   };
// //   const handleAddS = () => {
// //     setEdit(false);
// //     setAdd(!add);
// //   };

// //   const handleAddCountry1 = () => {
// //     setAddC1(!addC1);
// //   };
// //   const handleAddS1 = () => {
// //     setEdit1(false);
// //     setAdd1(!add1);
// //   };

// //   const handleChangeStream = (i, event) => {
// //     const values = [...fieldsStream];
// //     values[i].value = event.target.value;
// //     setFieldsStream(values);
// //   };

// //   const handleAddStream = () => {
// //     const values = [...fieldsStream];
// //     values.push({ value: null });
// //     setFieldsStream(values);
// //   };

// //   const handleRemoveStream = (i) => {
// //     const values = [...fieldsStream];
// //     values.splice(i, 1);
// //     setFieldsStream(values);
// //   };

// //   const handleChangeProgram = (i, event) => {
// //     const values = [...fieldsProgram];
// //     values[i].value = event.target.value;
// //     setFieldsStream(values);
// //   };

// //   const handleAddProgram = () => {
// //     const values = [...fieldsProgram];
// //     values.push({ value: null });
// //     setFieldsProgram(values);
// //   };

// //   const handleRemoveProgram = (i) => {
// //     const values = [...fieldsProgram];
// //     values.splice(i, 1);
// //     setFieldsProgram(values);
// //   };

// //   const handleAddCountryData1 = (index) => {
// //     console.log(dataA, "dataA----");
// //     var formData = [...dataA];
// //     var obj = dataA[index];
// //     var objC = {
// //       category: "Inside Sultanate",
// //       type: "sub menu",
// //       college_name: addcountry1,
// //       college_type: addCollegeType1,
// //       program_type: addProgramType1,
// //       utterance: "",
// //       country: "",
// //       streams_available: [],
// //     };
// //     obj = { ...objC };
// //     // obj = { ...obj, streams_available: [...obj.streams_available, objC] };
// //     formData[0] = obj;
// //     setDataA(formData);
// //     console.log(formData, "formData1----");
// //   };

// //   const handleAddStreamData1 = (index) => {
// //     console.log(dataA[0].streams_available, addStream1, "dataA------------");
// //     var formData = [...dataA];
// //     var obj = dataA[index];
// //     var objS = {
// //         stream_name: addStream1,
// //         program_list: [],
// //       },
// //       obj = { ...obj, streams_available: [...obj.streams_available, objS] };
// //     formData[index] = obj;
// //     setDataA(formData);
// //     // setAddStream("");
// //     console.log(formData, "formData----");
// //   };

// //   const handleAddProgramData1 = (idx, index) => {
// //     var formData = { ...dataA };
// //     console.log(idx, index, "field, index, e-------");
// //     var obj =
// //       dataA[idx]?.streams_available[index] &&
// //       dataA[idx].streams_available[index];
// //     console.log(obj, "obj---");
// //     var objS = {
// //         program_code: addProgram1,
// //         details: details1,
// //       },
// //       obj = { ...obj, program_list: [...obj.program_list, objS] };
// //     formData[index].streams_available[index] = obj;
// //     setDataA(formData);
// //     console.log(formData, "program_code---");
// //   };

// //   const handleAddCollege1 = async () => {
// //     try {
// //       let res = await addCollege(dataA);
// //       console.log(res, "res-----------");
// //       if(res.status === "done"){
// //         getCollegeList();
// //       }
// //     } catch (error) {
// //       console.log(error);
// //     }
// //   };

// //   const handleAddCountryData = (index) => {
// //     //   var formData = [...dataE];
// //     //   var obj = dataE[index];
// //     //   var objC = {
// //     //     category: "Inside Sultanate",
// //     //     type: "sub menu",
// //     //     college_name: addcountry,
// //     //     college_type: addCollegeType,
// //     //     program_type: addProgramType,
// //     //     utterance: "",
// //     //     country: "",
// //     //     streams_available: [],
// //     //   };
// //     //   obj = { ...objC };
// //     //   // obj = { ...obj, streams_available: [...obj.streams_available, objC] };
// //     //   formData[dataE?.length && dataE.length] = obj;
// //     //   setDataE(formData);
// //     //   console.log(formData, "formData----");
// //   };

// //   const handleAddStreamData = (index) => {
// //     //   var formData = [...dataE];
// //     //   var obj = dataE[index];
// //     //   var objS = {
// //     //       stream_name: addStream,
// //     //       program_list: [],
// //     //     },
// //     //     obj = { ...obj, streams_available: [...obj.streams_available, objS] };
// //     //   formData[index] = obj;
// //     //   setDataE(formData);
// //     //   setAddStream("");
// //     //   console.log(formData, "formData----");
// //   };

// //   const handleAddProgramData = (indexS, index) => {
// //     //   var formData = [...dataE];
// //     //   var obj = dataE[index].streams_available[indexS];
// //     //   console.log(obj, "obj---");
// //     //   var objS = {
// //     //       program_code: addProgram,
// //     //       details: details,
// //     //     },
// //     //     obj = { ...obj, program_list: [...obj.program_list, objS] };
// //     //   formData[index].streams_available[indexS] = obj;
// //     //   setDataE(formData);
// //     //   console.log(formData, "program_code---");
// //   };

// //   const handleAddCollege = async () => {
// //     try {
// //       let res = await addCollege(dataE);
// //       console.log(res, "res-----------");
// //       // setResponses(res)
// //     } catch (error) {
// //       console.log(error);
// //     }
// //     // setOpen(true);
// //     // seteditrowData(rowData);
// //   };

// //   // const handleAddStreamData = (index) => {
// //   //   var formData = [...dataE];
// //   //   var obj = dataE[index];
// //   //   var objS = {
// //   //       stream_name: addStream,
// //   //       program_list: [],
// //   //     },
// //   //     obj = { ...obj, streams_available: [...obj.streams_available, objS] };
// //   //   formData[index] = obj;
// //   //   setDataE(formData);
// //   //   setAddStream("");
// //   // };

// //   const handleEditCountry = (e, index) => {
// //     var formData = [...dataE];
// //     var obj = dataE[index];
// //     obj = { ...obj, college_name: e.target.value };
// //     formData[index] = obj;
// //     setDataE(formData);
// //   };
// //   const handleDeleteCountry = (index) => {
// //     var formData = [...dataE];
// //     var array = dataE;
// //     array.splice(index, 1);
// //     formData = array;
// //     setDataE(formData);
// //   };

// //   const handleDeleteStream = (index, indexS) => {
// //     var formData = [...dataE];
// //     var array = dataE[index].streams_available;
// //     array.splice(indexS, 1);
// //     formData[index].streams_available = array;
// //     setDataE(formData);
// //   };
// //   const handleEditStreamData = (e, stream_option, indexS, index) => {
// //     var formData = [...dataE];
// //     var obj = dataE[index].streams_available[indexS];
// //     obj = { ...obj, stream_name: e.target.value };
// //     formData[index].streams_available[indexS] = obj;
// //     setDataE(formData);
// //   };
// //   const handleDeleteProgram = (indexS, indexP, index) => {
// //     var formData = [...dataE];
// //     var array = dataE[index].streams_available[indexS].program_code;
// //     array.splice(indexP, 1);
// //     formData[index].streams_available[indexS].program_code = array;
// //     setDataE(formData);
// //   };

// //   const handleEditProgramData = (e, indexS, indexP, index) => {
// //     var formData = [...dataE];
// //     var obj = dataE[index].streams_available[indexS].program_code[indexP];
// //     obj = { ...obj, program_code: e.target.value };
// //     formData[index].streams_available[indexS].program_code[indexP] = obj;
// //     setDataE(formData);
// //     console.log(formData);
// //   };

// //   return (
// //     <Fragment>
// //       <Breadcrumb parent="Apps" title="Inside Sultanate" />
// //       <Container fluid={true}>
// //         <div className="edit-profile">
// //           <Row>
// //             <Col md="12">
// //               <Card>
// //                 <CardBody>
// //                   {/* <FormGroup
// //                     style={{ display: "flex", justifyContent: "flex-end" }}
// //                   >
// //                     <Tooltip title="Add College">
// //                       <IconButton>
// //                         {!addC ? (
// //                           <Add onClick={handleAddCountry} />
// //                         ) : (
// //                           <Clear onClick={handleAddCountry} />
// //                         )}
// //                       </IconButton>
// //                     </Tooltip>
// //                   </FormGroup> */}
// //                   <div className="row">
// //                     <div className="col-md-12">
// //                       Add Collage :
// //                       <Tooltip title="Add">
// //                         <IconButton>
// //                           <Add onClick={handleAddCountry1} />
// //                         </IconButton>
// //                       </Tooltip>
// //                     </div>
// //                     {addC1 && (
// //                       <div className="col-md-12">
// //                         <div className="row">
// //                           <div className="col-md-12">
// //                             <FormGroup>
// //                               <Label className="col-form-label pt-0">
// //                                 Add College
// //                               </Label>
// //                               <div style={{ display: "flex" }}>
// //                                 <Input
// //                                   className="form-control"
// //                                   type="text"
// //                                   placeholder="Add College"
// //                                   value={addcountry1}
// //                                   onChange={(e) =>
// //                                     setAddCountry1(e.target.value)
// //                                   }
// //                                 />
// //                               </div>
// //                             </FormGroup>
// //                           </div>
// //                         </div>
// //                         <div className="row">
// //                           <div className="col-md-6">
// //                             <FormGroup>
// //                               <Label className="col-form-label pt-0">
// //                                 Add College Type
// //                               </Label>
// //                               <Input
// //                                 className="form-control"
// //                                 type="text"
// //                                 placeholder="College Type"
// //                                 value={addCollegeType1}
// //                                 onChange={(e) =>
// //                                   setAddCollegeType1(e.target.value)
// //                                 }
// //                               />
// //                             </FormGroup>
// //                           </div>
// //                           <div className="col-md-5">
// //                             <FormGroup>
// //                               <Label className="col-form-label pt-0">
// //                                 Add Program Type
// //                               </Label>
// //                               <Input
// //                                 className="form-control"
// //                                 type="text"
// //                                 placeholder="Program Type"
// //                                 value={addProgramType1}
// //                                 onChange={(e) =>
// //                                   setAddProgramType1(e.target.value)
// //                                 }
// //                               />
// //                             </FormGroup>
// //                           </div>
// //                           <div className="col-md-1" style={{ display: "flex" }}>
// //                             <Tooltip title="Save">
// //                               <IconButton>
// //                                 <Check
// //                                   onClick={() => handleAddCountryData1()}
// //                                 />
// //                               </IconButton>
// //                             </Tooltip>
// //                           </div>
// //                         </div>
// //                         <div className="row">
// //                           <div className="col-md-12">
// //                             Add Stream :
// //                             <Tooltip title="Add">
// //                               <IconButton>
// //                                 <Add onClick={() => handleAddStream()} />
// //                               </IconButton>
// //                             </Tooltip>
// //                             {fieldsStream.map((field, idx) => {
// //                               return (
// //                                 <div
// //                                   key={`${field}-${idx}`}
// //                                   style={{
// //                                     borderBottom: "solid 1px #ccc",
// //                                     marginTop: 10,
// //                                     marginBottom: 10,
// //                                     paddingBottom: 10,
// //                                   }}
// //                                 >
// //                                   <div className="row">
// //                                     <div className="col-md-11">
// //                                       <FormGroup>
// //                                         <Label
// //                                           className="col-form-label pt-0"
// //                                           style={{ marginTop: 10 }}
// //                                         >
// //                                           Add Stream
// //                                         </Label>
// //                                         <Input
// //                                           className="form-control"
// //                                           type="text"
// //                                           placeholder="College Type"
// //                                           value={addStream1}
// //                                           // onChange={(e) => handleChangeStream(idx, e)}
// //                                           onChange={(e) =>
// //                                             setAddStream1(e.target.value)
// //                                           }
// //                                         />
// //                                       </FormGroup>
// //                                     </div>
// //                                     <div
// //                                       className="col-md-1"
// //                                       style={{ display: "flex" }}
// //                                     >
// //                                       <Tooltip title="Save">
// //                                         <IconButton>
// //                                           <Check
// //                                             onClick={() =>
// //                                               handleAddStreamData1(idx)
// //                                             }
// //                                           />
// //                                         </IconButton>
// //                                       </Tooltip>
// //                                       {/* <Tooltip title="Remove">
// //                                         <IconButton>
// //                                           <DeleteOutline
// //                                             onClick={() =>
// //                                               handleRemoveStream(idx)
// //                                             }
// //                                           />
// //                                         </IconButton>
// //                                       </Tooltip> */}
// //                                     </div>
// //                                   </div>
// //                                   <div className="row">
// //                                     <div className="col-md-12">
// //                                       Add Program :
// //                                       <Tooltip title="Add">
// //                                         <IconButton>
// //                                           <Add
// //                                             onClick={() => handleAddProgram()}
// //                                           />
// //                                         </IconButton>
// //                                       </Tooltip>
// //                                       {fieldsProgram.map((field, index) => {
// //                                         return (
// //                                           <div
// //                                             key={`${field}-${index}`}
// //                                             style={{ marginTop: 10 }}
// //                                           >
// //                                             <div className="row">
// //                                               <div className="col-md-4">
// //                                                 <FormGroup>
// //                                                   <Label className="col-form-label pt-0">
// //                                                     Add Program Code
// //                                                   </Label>
// //                                                   <Input
// //                                                     className="form-control"
// //                                                     type="text"
// //                                                     placeholder="College Type"
// //                                                     value={addProgram1}
// //                                                     // onChange={(e) => handleChangeProgram(index, e)}
// //                                                     onChange={(e) =>
// //                                                       setAddProgram1(
// //                                                         e.target.value
// //                                                       )
// //                                                     }
// //                                                   />
// //                                                 </FormGroup>
// //                                               </div>
// //                                               <div className="col-md-7">
// //                                                 <FormGroup>
// //                                                   <Label className="col-form-label pt-0">
// //                                                     Add Details
// //                                                   </Label>
// //                                                   <Input
// //                                                     className="form-control"
// //                                                     type="text"
// //                                                     placeholder="Program Type"
// //                                                     value={details1}
// //                                                     onChange={(e) =>
// //                                                       setDetails1(
// //                                                         e.target.value
// //                                                       )
// //                                                     }
// //                                                   />
// //                                                 </FormGroup>
// //                                               </div>
// //                                               <div
// //                                                 className="col-md-1"
// //                                                 style={{ display: "flex" }}
// //                                               >
// //                                                 <Tooltip title="Save">
// //                                                   <IconButton>
// //                                                     <Check
// //                                                       onClick={() =>
// //                                                         handleAddProgramData1(
// //                                                           idx,
// //                                                           index
// //                                                         )
// //                                                       }
// //                                                     />
// //                                                   </IconButton>
// //                                                 </Tooltip>
// //                                                 {/* <Tooltip title="Remove">
// //                                                   <IconButton>
// //                                                     <DeleteOutline
// //                                                       onClick={() =>
// //                                                         handleRemoveProgram(idx)
// //                                                       }
// //                                                     />
// //                                                   </IconButton>
// //                                                 </Tooltip> */}
// //                                               </div>
// //                                             </div>
// //                                           </div>
// //                                         );
// //                                       })}
// //                                     </div>
// //                                   </div>
// //                                 </div>
// //                               );
// //                             })}
// //                           </div>
// //                           <div
// //                             className="row insideSubmitButton"
// //                             style={{ width: "100%", marginBottom: 20 }}
// //                           >
// //                             <Button onClick={handleAddCollege1}>Submit</Button>
// //                           </div>
// //                         </div>

// //                         {/* <div className="row">
// //                           <div className="col-md-12">
// //                             <Button>Add Program</Button>
// //                           </div>
// //                           <div className="col-md-4">
// //                             <FormGroup>
// //                               <Label className="col-form-label pt-0">
// //                                 Add Program Code
// //                               </Label>
// //                               <Input
// //                                 className="form-control"
// //                                 type="text"
// //                                 placeholder="College Type"
// //                                 value={addCollegeType}
// //                                 onChange={(e) =>
// //                                   setAddCollegeType(e.target.value)
// //                                 }
// //                               />
// //                             </FormGroup>
// //                           </div>
// //                           <div className="col-md-7">
// //                             <FormGroup>
// //                               <Label className="col-form-label pt-0">
// //                                 Add Details
// //                               </Label>
// //                               <Input
// //                                 className="form-control"
// //                                 type="text"
// //                                 placeholder="Program Type"
// //                                 value={addProgramType}
// //                                 onChange={(e) =>
// //                                   setAddProgramType(e.target.value)
// //                                 }
// //                               />
// //                             </FormGroup>
// //                           </div>
// //                           <div className="col-md-1" style={{ display: "flex" }}>
// //                             <Tooltip title="Save">
// //                               <IconButton>
// //                                 <Check onClick={() => handleAddCountryData()} />
// //                               </IconButton>
// //                             </Tooltip>
// //                           </div>
// //                         </div> */}
// //                       </div>
// //                     )}
// //                   </div>
// //                   {addC && (
// //                     <div className="row">
// //                       <div className="col-md-12">
// //                         <FormGroup>
// //                           <Label className="col-form-label pt-0">
// //                             Add College
// //                           </Label>
// //                           <div style={{ display: "flex" }}>
// //                             <Input
// //                               className="form-control"
// //                               type="text"
// //                               placeholder="Add College"
// //                               value={addcountry}
// //                               onChange={(e) => setAddCountry(e.target.value)}
// //                             />
// //                             <Tooltip title="Save">
// //                               <IconButton>
// //                                 <Check onClick={() => handleAddCountryData()} />
// //                               </IconButton>
// //                             </Tooltip>
// //                           </div>
// //                         </FormGroup>
// //                       </div>
// //                       <div className="col-md-6">
// //                         <FormGroup>
// //                           <Label className="col-form-label pt-0">
// //                             Add College Type
// //                           </Label>
// //                           <Input
// //                             className="form-control"
// //                             type="text"
// //                             placeholder="College Type"
// //                             value={addCollegeType}
// //                             onChange={(e) => setAddCollegeType(e.target.value)}
// //                           />
// //                         </FormGroup>
// //                       </div>
// //                       <div className="col-md-6">
// //                         <FormGroup>
// //                           <Label className="col-form-label pt-0">
// //                             Add Program Type
// //                           </Label>
// //                           <Input
// //                             className="form-control"
// //                             type="text"
// //                             placeholder="Program Type"
// //                             value={addProgramType}
// //                             onChange={(e) => setAddProgramType(e.target.value)}
// //                           />
// //                         </FormGroup>
// //                       </div>
// //                     </div>
// //                   )}
// //                   {dataE &&
// //                     dataE.map((item, index) => {
// //                       return (
// //                         <div className="accordinContainer">
// //                           <Accordion key={index} style={{ marginTop: 10 }}>
// //                             <AccordionSummary
// //                               expandIcon={<ExpandMoreIcon />}
// //                               aria-controls="panel1c-content"
// //                               id="panel1c-header"
// //                             >
// //                               College : {item.college_name}
// //                             </AccordionSummary>
// //                             <AccordionDetails
// //                               style={{ flexDirection: "column" }}
// //                             >
// //                               <div className="AccordionSummaryHeading">
// //                                 <div className="AccordionSummaryHeadingActionButtons">
// //                                   <Tooltip title="Edit College and details">
// //                                     <IconButton>
// //                                       {!edit ? (
// //                                         <Edit onClick={handleEditCountryName} />
// //                                       ) : (
// //                                         <Clear
// //                                           onClick={handleEditCountryName}
// //                                         />
// //                                       )}
// //                                     </IconButton>
// //                                   </Tooltip>
// //                                   <Tooltip title="Add Stream">
// //                                     <IconButton>
// //                                       {!add ? (
// //                                         <Add onClick={handleAddS} />
// //                                       ) : (
// //                                         <Clear onClick={handleAddS} />
// //                                       )}
// //                                     </IconButton>
// //                                   </Tooltip>
// //                                 </div>
// //                               </div>
// //                               <div>
// //                                 {edit && (
// //                                   <FormGroup>
// //                                     <Label className="col-form-label pt-0">
// //                                       Edit College
// //                                     </Label>
// //                                     <Input
// //                                       className="form-control"
// //                                       type="text"
// //                                       placeholder="Edit College"
// //                                       value={item.college_name}
// //                                       onChange={(e) =>
// //                                         handleEditCountry(e, index)
// //                                       }
// //                                     />
// //                                   </FormGroup>
// //                                 )}
// //                                 <div className="row">
// //                                   <div className="col-md-6">
// //                                     <FormGroup>
// //                                       <Label className="col-form-label pt-0">
// //                                         College Type
// //                                       </Label>
// //                                       <Input
// //                                         readOnly={edit ? false : true}
// //                                         className="form-control"
// //                                         type="text"
// //                                         placeholder="Enter College Type"
// //                                         value={item.college_type}
// //                                         onChange={(e) =>
// //                                           handleEditCountry(
// //                                             e,
// //                                             item.country_option,
// //                                             index
// //                                           )
// //                                         }
// //                                       />
// //                                     </FormGroup>
// //                                   </div>
// //                                   <div className="col-md-6">
// //                                     <FormGroup>
// //                                       <Label className="col-form-label pt-0">
// //                                         Program Type
// //                                       </Label>
// //                                       <Input
// //                                         readOnly={edit ? false : true}
// //                                         className="form-control"
// //                                         type="text"
// //                                         placeholder="Enter Program Type"
// //                                         value={item.program_type}
// //                                         onChange={(e) =>
// //                                           handleEditCountry(
// //                                             e,
// //                                             item.country_option,
// //                                             index
// //                                           )
// //                                         }
// //                                       />
// //                                     </FormGroup>
// //                                   </div>
// //                                 </div>
// //                               </div>
// //                               {add && (
// //                                 <FormGroup>
// //                                   <Label className="col-form-label pt-0">
// //                                     Add Stream
// //                                   </Label>
// //                                   <div style={{ display: "flex" }}>
// //                                     <Input
// //                                       className="form-control"
// //                                       type="text"
// //                                       placeholder="Enter Stream"
// //                                       value={addStream}
// //                                       onChange={(e) =>
// //                                         setAddStream(e.target.value)
// //                                       }
// //                                     />
// //                                     <Tooltip title="Save">
// //                                       <IconButton>
// //                                         <Check
// //                                           onClick={() =>
// //                                             handleAddStreamData(index)
// //                                           }
// //                                         />
// //                                       </IconButton>
// //                                     </Tooltip>
// //                                   </div>
// //                                 </FormGroup>
// //                               )}
// //                               {item.streams_available.map((itemS, indexS) => {
// //                                 return (
// //                                   <div className="accordinContainer">
// //                                     <Streams
// //                                       itemS={itemS}
// //                                       indexS={indexS}
// //                                       index={index}
// //                                       handleEditStreamData={
// //                                         handleEditStreamData
// //                                       }
// //                                       handleDeleteProgram={handleDeleteProgram}
// //                                       handleAddProgramData={
// //                                         handleAddProgramData
// //                                       }
// //                                       handleEditProgramData={
// //                                         handleEditProgramData
// //                                       }
// //                                       setAddProgram={setAddProgram}
// //                                       addProgram={addProgram}
// //                                       handleAddS={handleAddS}
// //                                       setDetails={setDetails}
// //                                       details={details}
// //                                     />
// //                                     {/* <Tooltip title="Delete Stream">
// //                                       <IconButton>
// //                                         <DeleteOutline
// //                                           onClick={() =>
// //                                             handleDeleteStream(index, indexS)
// //                                           }
// //                                         />
// //                                       </IconButton>
// //                                     </Tooltip> */}
// //                                   </div>
// //                                 );
// //                               })}
// //                             </AccordionDetails>
// //                           </Accordion>
// //                           {/* <Tooltip title="Delete Country">
// //                             <IconButton>
// //                               <DeleteOutline
// //                                 onClick={() => handleDeleteCountry(index)}
// //                               />
// //                             </IconButton>
// //                           </Tooltip> */}
// //                         </div>
// //                       );
// //                     })}
// //                   <div className="row insideSubmitButton">
// //                     <Button onClick={handleAddCollege}>Submit</Button>
// //                   </div>
// //                 </CardBody>
// //               </Card>
// //             </Col>
// //           </Row>
// //         </div>
// //       </Container>
// //     </Fragment>
// //   );
// // };

// // export default Programs;
