import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionActions from '@material-ui/core/AccordionActions';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Chip from '@material-ui/core/Chip';
import { Row, Col, Button, FormGroup, Label, Input, } from 'reactstrap'
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import Edit from '@material-ui/icons/Edit';
import Add from '@material-ui/icons/Add';


export default function Programs(props) {
    const { itemP, indexP } = props;
    const handleAdd = () => {

    }
    return (
        <>
            <Row>
                <Col md="12" style={{ marginTop: 10 }}>
                    <Accordion key={indexP} >
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel2c-content"
                            id="panel2c-header"
                        >
                            Program : {itemP.program_code}
                        </AccordionSummary>
                        <AccordionDetails style={{ flexDirection: 'column', background: '#f7faff' }} >
                            <div style={{ display: 'flex', justifyContent: 'end' }}>
                                <Tooltip title='Edit Program'>
                                    <IconButton onClick={handleAdd}>
                                        <Edit />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title='Add Program'>
                                    <IconButton onClick={handleAdd}>
                                        <Add />
                                    </IconButton>
                                </Tooltip>
                            </div>
                            {/* <FormGroup>
                                <Label className="col-form-label pt-0" >Edit Program</Label>
                                <Input className="form-control" type="text" placeholder="Enter Country"
                                    value={item.program_code}
                                />
                            </FormGroup> */}
                        </AccordionDetails>
                        {/* <AccordionActions>
                        <Button size="small">Cancel</Button>
                        <Button size="small" color="primary">
                            Save
                        </Button>
                    </AccordionActions> */}
                    </Accordion>
                </Col>
            </Row>
        </>
    );
}
