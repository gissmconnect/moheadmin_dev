import React, { useState, useEffect } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Chip from "@material-ui/core/Chip";
import { Row, Col, Button, FormGroup, Label, Input } from "reactstrap";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

export default function StreamsAdd(props) {
  const {
    itemS,
    indexS,
    index,
    handleEditStreamData,
    handleEditProgramData,
    handleEditProgramDetails,
    handleDeleteProgram,
    handleAddProgramData,
    setAddProgram,
    addProgram,
    setDetails,
    details,
    handleAddS,
  } = props;
  const [addS, setAddS] = useState(false);
  const [editS, setEditS] = useState(false);

  const handleEditStream = () => {
    setAddS(false);
    setEditS(!editS);
  };
  const handleAddP = () => {
    setEditS(false);
    setAddS(!addS);
    if (addS === false) {
      setAddProgram("");
      setDetails("");
    }
  };
  return (
    <>
      <Accordion key={indexS} style={{ marginTop: 10 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          Session : {itemS.session}
        </AccordionSummary>
        <AccordionDetails style={{ flexDirection: "column" }}>
          <FormGroup style={{ display: "block", textAlign: "end" }}></FormGroup>
          <div className="AccordionSummaryHeading">
            <div className="AccordionSummaryHeadingActionButtons">
              {!editS ? (
                <Button
                  onClick={handleEditStream}
                  style={{ height: 40, margin: 10 }}
                >
                  Edit Session and Statistic list
                </Button>
              ) : (
                <Button
                  onClick={handleEditStream}
                  style={{ height: 40, margin: 10 }}
                >
                  Save
                </Button>
              )}
              {addS ? (
                <Button
                  onClick={handleAddP}
                  style={{ height: 40, marginTop: 10, marginBottom: 10 }}
                >
                  Remove Statistic
                </Button>
              ) : (
                <Button
                  onClick={handleAddP}
                  style={{ height: 40, marginTop: 10, marginBottom: 10 }}
                >
                  Add Statistic
                </Button>
              )}
            </div>
          </div>
          {editS && (
            <FormGroup>
              <Label className="col-form-label pt-0">Edit Session</Label>
              <Input
                className="form-control"
                type="text"
                placeholder="Enter Session"
                value={itemS.session}
                onChange={(e) =>
                  handleEditStreamData(e, itemS.stream_option, indexS, index)
                }
              />
            </FormGroup>
          )}
          {addS && (
            <FormGroup>
              <div className="row" style={{ display: "flex" }}>
                <div className="col-md-12">
                  <Label className="col-form-label pt-0">
                    Add Statistic Category
                  </Label>
                </div>
                <div className="col-md-12">
                  <Input
                    className="form-control"
                    type="text"
                    placeholder="Enter Statistic Category"
                    value={addProgram}
                    onChange={(e) => setAddProgram(e.target.value)}
                  />
                </div>
                <div className="col-md-12">
                  <Label className="col-form-label pt-0 mt-2">
                    Add Statistic
                  </Label>
                  <Input
                    className="form-control"
                    type="textarea"
                    placeholder="Enter Statistic"
                    value={details}
                    onChange={(e) => setDetails(e.target.value)}
                    style={{ minHeight: 150, marginTop: 10 }}
                  />
                </div>
                <div className="col-md-1">
                  <div style={{ display: "flex" }}>
                    <Button
                      onClick={() => handleAddProgramData(indexS, index)}
                      style={{ height: 40, marginTop: 10, marginBottom: 10 }}
                    >
                      Add
                    </Button>
                    {/* <Tooltip title="Save">
                            <IconButton>
                              <Check
                                onClick={() =>
                                  handleAddProgramData(indexS, index)
                                }
                              />
                            </IconButton>
                          </Tooltip> */}
                  </div>
                </div>
              </div>
            </FormGroup>
          )}
          {itemS?.statistic_list &&
            itemS.statistic_list.map((itemP, indexP) => {
              return (
                <div className="row">
                  <div className="col-md-12">
                    <Label className="col-form-label pt-0">
                      Statistic Category
                    </Label>
                    <FormGroup
                      style={{ display: "flex", flexDirection: "row" }}
                    >
                      <Input
                        readOnly={editS ? false : true}
                        className="form-control"
                        type="text"
                        placeholder="Enter Statistic Category"
                        value={itemP.statistic_category}
                        onChange={(e) =>
                          handleEditProgramData(e, indexS, indexP, index)
                        }
                      />
                      {/* <Tooltip title="Delete Program">
                        <IconButton>
                          <DeleteOutline
                            onClick={() => handleDeleteProgram(indexS, indexP, index)}
                          />
                        </IconButton>
                      </Tooltip> */}
                    </FormGroup>
                  </div>
                  <div className="col-md-12">
                    <Label className="col-form-label pt-0">Statistic</Label>
                    <FormGroup
                      style={{ display: "flex", flexDirection: "row" }}
                    >
                      <Input
                        readOnly={editS ? false : true}
                        className="form-control"
                        type="textarea"
                        placeholder="Enter Statistic"
                        value={itemP.statistic}
                        onChange={(e) =>
                          handleEditProgramDetails(e, indexS, indexP, index)
                        }
                        style={{ minHeight: 150 }}
                      />
                      {/* <Tooltip title="Delete Program">
                        <IconButton>
                          <DeleteOutline
                            onClick={() =>
                              handleDeleteProgram(indexS, indexP, index)
                            }
                          />
                        </IconButton>
                      </Tooltip> */}
                    </FormGroup>
                  </div>
                </div>
              );
            })}
        </AccordionDetails>
      </Accordion>
    </>
  );
}
