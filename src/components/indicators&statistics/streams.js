import React, { useState, useEffect } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Chip from "@material-ui/core/Chip";
import { Row, Col, Button, FormGroup, Label, Input } from "reactstrap";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

export default function Streams(props) {
  const {
    itemS,
    indexS,
    index,
    handleEditStreamData1,
    handleEditProgramData1,
    handleEditProgramDetails1,
    handleDeleteProgram,
    handleNewProgramData,
    handleAddProgramData,
    setAddProgram,
    addProgram,
    setDetails,
    details,
  } = props;
  const [addS, setAddS] = useState(false);
  const [editS, setEditS] = useState(false);

  const handleEditStream = () => {
    setAddS(false);
    setEditS(!editS);
  };
  const handleAddP = () => {
    setEditS(false);
    setAddS(!addS);
  };

  return (
    <>
      <Accordion key={indexS} style={{ marginTop: 10 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          Session : {itemS.session}
        </AccordionSummary>
        <AccordionDetails style={{ flexDirection: "column" }}>
          <FormGroup style={{ display: "block", textAlign: "end" }}></FormGroup>

          <div className="AccordionSummaryHeading">
            <div className="AccordionSummaryHeadingActionButtons">
              <Tooltip title="Edit Session">
                {!editS ? (
                  <Button onClick={handleEditStream} style={{ height: 38 }}>
                    Edit
                  </Button>
                ) : (
                  <Button onClick={handleEditStream} style={{ height: 38 }}>
                    Save
                  </Button>
                )}
              </Tooltip>
              {addS ? (
                <Button
                  onClick={handleAddP}
                  style={{ height: 38, marginLeft: 10 }}
                >
                  Remove
                </Button>
              ) : (
                <Button
                  onClick={handleAddP}
                  style={{ height: 38, marginLeft: 10 }}
                >
                  Add Statistics
                </Button>
              )}
            </div>
          </div>
          {editS && (
            <FormGroup>
              <Label className="col-form-label pt-0">Edit Session</Label>
              <Input
                className="form-control"
                type="text"
                placeholder="Enter Session"
                value={itemS.session}
                onChange={(e) =>
                  handleEditStreamData1(e, itemS.stream_option, indexS, index)
                }
              />
            </FormGroup>
          )}
          {addS && (
            <FormGroup>
              <div className="row" style={{ display: "flex" }}>
                <div className="col-md-12">
                  <Label className="col-form-label pt-0">
                    Add Statistics Category
                  </Label>
                </div>
                <div className="col-md-12" style={{ display: "flex" }}>
                  <Input
                    className="form-control"
                    type="text"
                    placeholder="Enter Statistics Category"
                    value={addProgram}
                    onChange={(e) => setAddProgram(e.target.value)}
                  />
                  {/* <Tooltip title="Save">
                    <IconButton>
                      <Check
                        onClick={() => handleAddProgramData(indexS, index)}
                      />
                    </IconButton>
                  </Tooltip> */}
                </div>
                <div className="col-md-12">
                  <Label className="col-form-label pt-0">Add Statistics</Label>
                  <Input
                    className="form-control"
                    type="textarea"
                    placeholder="Enter Statistics"
                    value={details}
                    onChange={(e) => setDetails(e.target.value)}
                    style={{ minHeight: 150, marginTop: 10 }}
                  />
                </div>
                <div
                  className="col-md-12"
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    marginTop: 10,
                  }}
                >
                  <Button
                    onClick={() => handleNewProgramData(indexS, index)}
                    style={{ height: 38, marginLeft: 10 }}
                  >
                    Save Session
                  </Button>
                </div>
              </div>
            </FormGroup>
          )}
          {itemS?.statistic_list &&
            itemS.statistic_list.map((itemP, indexP) => {
              return (
                <div className="row">
                  <div className="col-md-12">
                    <Label className="col-form-label pt-0">
                      Statistic Category
                    </Label>
                    <FormGroup
                      style={{ display: "flex", flexDirection: "row" }}
                    >
                      <Input
                        readOnly={editS ? false : true}
                        className="form-control"
                        type="text"
                        placeholder="Enter Statistic Category"
                        value={itemP.statistic_category}
                        onChange={(e) =>
                          handleEditProgramData1(e, indexS, indexP, index)
                        }
                      />
                      <Tooltip title="Delete Statistic">
                        <IconButton>
                          <DeleteOutline
                            onClick={() =>
                              handleDeleteProgram(indexS, indexP, index)
                            }
                          />
                        </IconButton>
                      </Tooltip>
                    </FormGroup>
                  </div>
                  <div className="col-md-12">
                    <Label className="col-form-label pt-0">Statistic</Label>
                    <FormGroup
                      style={{ display: "flex", flexDirection: "row" }}
                    >
                      <Input
                        readOnly={editS ? false : true}
                        className="form-control"
                        type="textarea"
                        placeholder="Enter Statistic"
                        value={itemP.statistic}
                        onChange={(e) =>
                          handleEditProgramDetails1(e, indexS, indexP, index)
                        }
                        style={{ minHeight: 150 }}
                      />
                      {/* <Tooltip title="Delete Program">
                        <IconButton>
                          <DeleteOutline
                            onClick={() =>
                              handleDeleteProgram(indexS, indexP, index)
                            }
                          />
                        </IconButton>
                      </Tooltip> */}
                    </FormGroup>
                  </div>
                </div>
              );
            })}
        </AccordionDetails>
      </Accordion>
    </>
  );
}
