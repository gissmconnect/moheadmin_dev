import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Add from "@material-ui/icons/Add";
import { toast } from "react-toastify";
import { dataNew } from "./dataNew";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Streams from "./streams";
import StreamsAdd from "./streamsAdd";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import {
  getStatistic,
  addStatistic,
  updateStatistic,
  addSubmenusIndicatorsStatistics,
  getSubMenu,
  deleteStatistic,
} from "../../services/menuService";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";

const IndicatorsStatistics = (props) => {
  const history = useHistory();
  const [dataA, setDataA] = useState(dataNew);
  const [dataE, setDataE] = useState([]);
  const [addcountry, setAddCountry] = useState("");
  const [addStream, setAddStream] = useState("");
  const [edit, setEdit] = useState(false);
  const [add, setAdd] = useState(false);
  const [addC, setAddC] = useState(false);
  const [addProgram, setAddProgram] = useState("");
  const [details, setDetails] = useState("");
  const [newStream, setNewStream] = useState("");

  const [edit1, setEdit1] = useState(false);
  const [add1, setAdd1] = useState(false);
  const [saveData, setSaveData] = useState(false);
  const [unique_title, setUnique_title] = useState("");
  const [button_title, setButton_title] = useState("");
  const [pagetitle, setPagetitle] = useState("");

  useEffect(() => {
    getCollegeList();
    getStatisticDataList();
  }, []);

  const getCollegeList = async () => {
    try {
      let res = await getStatistic("indicators and statistics");
      setDataE(res);
    } catch (error) {
      console.log(error);
    }
  };

  const handleAddS = () => {
    setEdit(false);
    setAdd(!add);
    if (add === false) {
      setAddStream("");
    }
  };

  const handleAddCountry = () => {
    setAddC(!addC);
    if (add === false) {
      setAddCountry("");
    }
  };

  const handleAddCountryData = (index) => {
    setUnique_title(addcountry);
    var formData = [...dataA];
    var obj = dataA[index];
    var objC = {
      category: "مؤشرات واحصاءات",
      unique_title: "indicators and statistics",
      type: "sub menu",
      student_type: addcountry,
      session_list: [],
    };
    obj = { ...objC };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    setAddC(!addC);
    setAddCountry("");
    toast.success(
      "Student type added successfully. Proceed to Session section"
    );
    // console.log(formData, "formData----");
  };

  const handleAddStreamData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objS = {
        session: addStream,
        statistic_list: [],
      },
      obj = { ...obj, session_list: [...obj.session_list, objS] };
    formData[index] = obj;
    setDataA(formData);
    setAddStream("");
    toast.success("Session added successfully. Proceed to Statistic section");
    // console.log(formData, "formData----");
  };

  const handleAddProgramData = (indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].session_list[indexS];
    // console.log(obj, "obj---");
    var objS = {
        statistic_category: addProgram,
        statistic: details,
      },
      obj = { ...obj, statistic_list: [...obj.statistic_list, objS] };
    formData[index].session_list[indexS] = obj;
    setDataA(formData);
    setAddProgram("");
    setDetails("");
    toast.success("Statistic added successfully. Click Submit to add on list");
    // console.log(formData, "program_code---");
  };

  const handleEditCountryName = () => {
    setAdd(false);
    setEdit(!edit);
  };

  const handleEditCountry = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, student_type: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditCountry-----------");
  };

  const handleEditStreamData = (e, stream_option, indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].session_list[indexS];
    obj = { ...obj, session: e.target.value };
    formData[index].session_list[indexS] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditStreamData-----------");
  };

  const handleEditProgramData = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].session_list[indexS].statistic_list[indexP];
    obj = { ...obj, statistic_category: e.target.value };
    formData[index].session_list[indexS].statistic_list[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramData-----------");
  };

  const handleEditProgramDetails = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].session_list[indexS].statistic_list[indexP];
    obj = { ...obj, statistic: e.target.value };
    formData[index].session_list[indexS].statistic_list[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramDetails-----------");
  };

  const handleAddCollege = async () => {
    Swal.fire({
      title: "Do you want to submit ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await addStatistic(dataA);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been added successfully.",
              "success"
            );
            window.location.reload(false);
            addButton();
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const addButton = async () => {
    // console.log(unique_title, button_title, "unique_title")
    try {
      let res = await addSubmenusIndicatorsStatistics(
        unique_title,
        button_title
      );
      if (res) {
        setUnique_title("");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getStatisticDataList = async () => {
    try {
      let res = await getSubMenu("indicators and statistics");
      if (res) {
        setButton_title(res?._id);
        setPagetitle(res.category);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleEditCountryNameList = (item) => {
    // console.log(item, "e, index--------");
    setAdd1(false);
    setEdit1(!edit1);
  };

  const handleEditCountry1 = (e, index, item) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, student_type: e.target.value };
    formData[index] = obj;
    setDataE(formData);
    // console.log(formData, "handleEditCountry1-----------");
  };

  // const handleupdateID = (index) => {
  //   console.log(index, "index----")
  //   var formData = [...dataE];
  //   var obj = dataE[index];
  //   obj = { ...obj, _id: id };
  //   formData[index] = obj;
  //   setDataE(formData);
  //   console.log(formData, "handleupdateID-----------");
  // };

  const handleEditStreamData1 = (e, stream_option, indexS, index) => {
    var formData = [...dataE];
    var obj = dataE[index].session_list[indexS];
    obj = { ...obj, session: e.target.value };
    formData[index].session_list[indexS] = obj;
    setDataE(formData);
  };

  const handleEditProgramData1 = (e, indexS, indexP, index) => {
    var formData = [...dataE];
    var obj = dataE[index].session_list[indexS].statistic_list[indexP];
    obj = { ...obj, statistic_category: e.target.value };
    formData[index].session_list[indexS].statistic_list[indexP] = obj;
    setDataE(formData);
    // console.log(formData);
  };

  const handleEditProgramDetails1 = (e, indexS, indexP, index) => {
    var formData = [...dataE];
    var obj = dataE[index].session_list[indexS].statistic_list[indexP];
    obj = { ...obj, statistic: e.target.value };
    formData[index].session_list[indexS].statistic_list[indexP] = obj;
    setDataE(formData);
    // console.log(formData);
  };

  const handleSaveCollege = async (index, item) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, _id: item._id.$oid };
    formData[index] = obj;
    setDataE(formData);
    setSaveData(true);
    toast.success("all data saved. Click submit to update");
    // console.log(formData, "save-----");
  };

  const handleUpdateCollege = async (item) => {
    Swal.fire({
      title: "Do you want to update ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await updateStatistic(item);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been updated successfully.",
              "success"
            );
            window.location.reload(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleNewStreamData = (index) => {
    var formData = [...dataE];
    var obj = dataE[index];
    var objS = {
        session: newStream,
        statistic_list: [],
      },
      obj = { ...obj, session_list: [...obj.session_list, objS] };
    formData[index] = obj;
    setDataE(formData);
    setNewStream("");
    toast.success("Stream saved. Proceed to program section");
    // console.log(formData, "formData----stream");
  };

  const handleNewProgramData = (indexS, index) => {
    var formData = [...dataE];
    var obj = dataE[index].session_list[indexS];
    // console.log(obj, "obj---");
    var objS = {
        statistic_category: addProgram,
        statistic: details,
      },
      obj = { ...obj, statistic_list: [...obj.statistic_list, objS] };
    formData[index].session_list[indexS] = obj;
    setDataE(formData);
    toast.success("Program saved. Click submit to update");
    // console.log(formData, "program_code---");
  };

  // const handleAddStreamData = (index) => {
  //   var formData = [...dataE];
  //   var obj = dataE[index];
  //   var objS = {
  //       stream_name: addStream,
  //       program_list: [],
  //     },
  //     obj = { ...obj, streams_available: [...obj.streams_available, objS] };
  //   formData[index] = obj;
  //   setDataE(formData);
  //   setAddStream("");
  // };

  // const handleEditCountry = (e, index) => {
  //   var formData = [...dataE];
  //   var obj = dataE[index];
  //   obj = { ...obj, college_name: e.target.value };
  //   formData[index] = obj;
  //   setDataE(formData);
  // };
  
  const handleDeleteCountry = (index) => {
    var formData = [...dataE];
    var array = dataE;
    array.splice(index, 1);
    formData = array;
    setDataE(formData);
  };

  const handleDeleteStream = (index, indexS) => {
    Swal.fire({
      title: "Do you want to Remove Session  ?",
      text:"Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...dataE];
        var array = dataE[index].session_list;
        array.splice(indexS, 1);
        formData[index].session_list = array;
        setDataE(formData);
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  // const handleEditStreamData = (e, stream_option, indexS, index) => {
  //   var formData = [...dataE];
  //   var obj = dataE[index].streams_available[indexS];
  //   obj = { ...obj, stream_name: e.target.value };
  //   formData[index].streams_available[indexS] = obj;
  //   setDataE(formData);
  // };

  const handleDeleteProgram = (indexS, indexP, index) => {
    Swal.fire({
      title: "Do you want to Remove Statistic ?",
      text:"Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...dataE];
        var array = dataE[index].session_list[indexS].statistic_list;
        array.splice(indexP, 1);
        formData[index].session_list[indexS].statistic_list = array;
        setDataE(formData);
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  // const handleEditProgramData = (e, indexS, indexP, index) => {
  //   var formData = [...dataE];
  //   var obj = dataE[index].streams_available[indexS].program_code[indexP];
  //   obj = { ...obj, program_code: e.target.value };
  //   formData[index].streams_available[indexS].program_code[indexP] = obj;
  //   setDataE(formData);
  //   console.log(formData);
  // };

  const handleDeleteStatic = async (item) => {
    let _id = item?._id?.$oid;
    Swal.fire({
      title: "Do you want to Delete Student type ?",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Delete`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deleteStatistic(_id);
          if (res) {
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleRefreshPage = async () => {
    history.push(`${process.env.PUBLIC_URL}/app/menus/Dubai`);
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title={pagetitle && pagetitle} />
      <Container fluid={true}>
        <div className="edit-profile">
          <div
            style={{
              cursor: "pointer",
              position: "relative",
              zIndex: 2,
              marginBottom: 10,
              marginTop: -15,
            }}
          >
            <Button onClick={handleRefreshPage}>Main Menu</Button>
          </div>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <FormGroup
                    style={{ display: "flex", justifyContent: "flex-end" }}
                  >
                    <Tooltip title="Add Country">
                      <IconButton>
                        {!addC ? (
                          <Add onClick={handleAddCountry} />
                        ) : (
                          <Clear onClick={handleAddCountry} />
                        )}
                      </IconButton>
                    </Tooltip>
                  </FormGroup>
                  {addC && (
                    <div className="row">
                      <div className="col-md-11">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Student Type
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Student Type"
                            value={addcountry}
                            onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddCountryData()}
                          style={{ height: 40, marginTop: 10 }}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )}
                  {dataA &&
                    dataA.map((item, index) => {
                      return (
                        index != 0 && (
                          <div
                            className="accordinContainer accordinContainerStyle"
                            key={index}
                          >
                            <Accordion style={{ marginTop: 10 }}>
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1c-content"
                                id="panel1c-header"
                              >
                                Student Type : {item.student_type}
                              </AccordionSummary>
                              <AccordionDetails
                                style={{ flexDirection: "column" }}
                              >
                                <div className="AccordionSummaryHeading">
                                  <div className="AccordionSummaryHeadingActionButtons">
                                    {!edit ? (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Edit Student Type
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                  </div>
                                </div>
                                <div>
                                  <div className="row">
                                    {edit && (
                                      <div className="col-md-6">
                                        <FormGroup>
                                          <Label className="col-form-label pt-0">
                                            Edit Student Type
                                          </Label>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Edit Student Type"
                                            value={item.student_type}
                                            onChange={(e) =>
                                              handleEditCountry(e, index)
                                            }
                                          />
                                        </FormGroup>
                                      </div>
                                    )}
                                    <div className="col-md-6">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Unique Title
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Unique Title"
                                          value="indicators and statistics"
                                          // onChange={(e) =>
                                          //   handleEditCollegeType(e, index)
                                          // }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div
                                      className="col-md-12"
                                      style={{
                                        display: "flex",
                                        justifyContent: "flex-end",
                                      }}
                                    >
                                      {add ? (
                                        <Button
                                          onClick={handleAddS}
                                          style={{
                                            height: 40,
                                            marginTop: 10,
                                            marginBottom: 10,
                                          }}
                                        >
                                          Remove Session
                                        </Button>
                                      ) : (
                                        <Button
                                          onClick={handleAddS}
                                          style={{
                                            height: 40,
                                            marginTop: 10,
                                            marginBottom: 10,
                                          }}
                                        >
                                          Add Session
                                        </Button>
                                      )}
                                    </div>
                                  </div>
                                </div>
                                {add && (
                                  <FormGroup>
                                    <Label className="col-form-label pt-0">
                                      Add Session
                                    </Label>
                                    <div style={{ display: "flex" }}>
                                      <Input
                                        className="form-control"
                                        type="text"
                                        placeholder="Add Session"
                                        value={addStream}
                                        onChange={(e) =>
                                          setAddStream(e.target.value)
                                        }
                                      />
                                      <Button
                                        onClick={() =>
                                          handleAddStreamData(index)
                                        }
                                        style={{ height: 38 }}
                                      >
                                        Add
                                      </Button>
                                      {/* <Tooltip title="Save">
                                          <IconButton>
                                            <Check
                                              onClick={() =>
                                                handleAddStreamData(index)
                                              }
                                            />
                                          </IconButton>
                                        </Tooltip> */}
                                    </div>
                                  </FormGroup>
                                )}
                                {item.session_list.map((itemS, indexS) => {
                                  return (
                                    <div className="accordinContainer">
                                      <StreamsAdd
                                        itemS={itemS}
                                        indexS={indexS}
                                        index={index}
                                        handleEditProgramDetails={
                                          handleEditProgramDetails
                                        }
                                        handleEditStreamData={
                                          handleEditStreamData
                                        }
                                        // handleDeleteProgram={
                                        //   handleDeleteProgram
                                        // }
                                        handleAddProgramData={
                                          handleAddProgramData
                                        }
                                        handleEditProgramData={
                                          handleEditProgramData
                                        }
                                        setAddProgram={setAddProgram}
                                        addProgram={addProgram}
                                        handleAddS={handleAddS}
                                        setDetails={setDetails}
                                        details={details}
                                      />
                                      {/* <Tooltip title="Delete Stream">
                                        <IconButton>
                                          <DeleteOutline
                                            onClick={() =>
                                              handleDeleteStream(index, indexS)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip> */}
                                    </div>
                                  );
                                })}
                                <div className="row insideSubmitButton">
                                  <Button onClick={handleAddCollege}>
                                    Submit
                                  </Button>
                                </div>
                              </AccordionDetails>
                            </Accordion>
                            {/* <Tooltip title="Delete Country">
                              <IconButton>
                                <DeleteOutline
                                  onClick={() => handleDeleteCountry(index)}
                                />
                              </IconButton>
                            </Tooltip> */}
                          </div>
                        )
                      );
                    })}
                  {/* sbksakcbksabcasbcbsakcbksakcksakcbksackakscbksabkcbsckaskcbksacksabkc */}
                  {dataE &&
                    dataE.map((item, index) => {
                      return (
                        <div className="accordinContainer">
                          <Accordion key={index} style={{ marginTop: 10 }}>
                            <AccordionSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1c-content"
                              id="panel1c-header"
                            >
                              Student Type : {item.student_type}
                            </AccordionSummary>
                            <AccordionDetails
                              style={{ flexDirection: "column" }}
                            >
                              <div className="AccordionSummaryHeading">
                                {/* <div> College : {item.college_name}</div> */}
                                <div className="AccordionSummaryHeadingActionButtons">
                                  <Tooltip title="Edit College and details">
                                    {!edit1 ? (
                                      <Button
                                        onClick={() =>
                                          handleEditCountryNameList(item)
                                        }
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Edit
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={() =>
                                          handleEditCountryNameList(item)
                                        }
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                  </Tooltip>
                                  <Tooltip title="Add Stream">
                                    {!add1 ? (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Add Session
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Remove
                                      </Button>
                                    )}
                                  </Tooltip>
                                </div>
                              </div>
                              <div>
                                <div className="row">
                                  <div
                                    className={edit1 ? "col-md-6" : "col-md-0"}
                                  >
                                    {edit1 && (
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Edit Student Type
                                        </Label>
                                        <Input
                                          className="form-control"
                                          type="text"
                                          placeholder="Edit Student Type"
                                          value={item.student_type}
                                          onChange={(e) =>
                                            handleEditCountry1(e, index, item)
                                          }
                                        />
                                      </FormGroup>
                                    )}
                                  </div>
                                  <div
                                    className={edit1 ? "col-md-6" : "col-md-12"}
                                  >
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Unique Title
                                      </Label>
                                      <Input
                                        readOnly={true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Unique Title"
                                        value={item.unique_title}
                                        // onChange={(e) =>
                                        //   handleEditCollegeType1(e, index)
                                        // }
                                      />
                                    </FormGroup>
                                  </div>
                                </div>
                              </div>
                              {add && (
                                <FormGroup>
                                  <Label className="col-form-label pt-0">
                                    Add Session
                                  </Label>
                                  <div style={{ display: "flex" }}>
                                    <Input
                                      className="form-control"
                                      type="text"
                                      placeholder="Enter Session"
                                      value={newStream}
                                      onChange={(e) =>
                                        setNewStream(e.target.value)
                                      }
                                    />
                                    {/* <Tooltip title="Save">
                                      <IconButton>
                                        <Check
                                          onClick={() =>
                                            handleAddStreamData(index)
                                          }
                                        />
                                      </IconButton>
                                    </Tooltip> */}
                                    <Button
                                      onClick={() => handleNewStreamData(index)}
                                      style={{ height: 38 }}
                                    >
                                      Add
                                    </Button>
                                  </div>
                                </FormGroup>
                              )}
                              {item?.session_list &&
                                item.session_list.map((itemS, indexS) => {
                                  return (
                                    <div className="accordinContainer">
                                      <Streams
                                        itemS={itemS}
                                        indexS={indexS}
                                        index={index}
                                        handleEditStreamData1={
                                          handleEditStreamData1
                                        }
                                        handleDeleteProgram={
                                          handleDeleteProgram
                                        }
                                        handleEditProgramData1={
                                          handleEditProgramData1
                                        }
                                        handleEditProgramDetails1={
                                          handleEditProgramDetails1
                                        }
                                        setAddProgram={setAddProgram}
                                        addProgram={addProgram}
                                        handleAddS={handleAddS}
                                        setDetails={setDetails}
                                        details={details}
                                        handleNewProgramData={
                                          handleNewProgramData
                                        }
                                      />
                                      <Tooltip title="Delete Session">
                                        <IconButton>
                                          <DeleteOutline
                                            onClick={() =>
                                              handleDeleteStream(index, indexS)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip>
                                    </div>
                                  );
                                })}
                              <div style={{ display: "flex" }}>
                                <Button
                                  onClick={() => handleSaveCollege(index, item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Save
                                </Button>
                                <Button
                                  disabled={saveData ? false : true}
                                  onClick={() => handleUpdateCollege(item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Submit
                                </Button>
                              </div>
                            </AccordionDetails>
                          </Accordion>
                          <div style={{ marginLeft: 10 }}>
                            <Tooltip title="Delete Student Type">
                              <IconButton>
                                <DeleteOutline
                                  onClick={() => handleDeleteStatic(item)}
                                />
                              </IconButton>
                            </Tooltip>
                          </div>
                        </div>
                      );
                    })}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
};

export default IndicatorsStatistics;
