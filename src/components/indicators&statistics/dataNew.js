export const dataNew = [
  {
    category: null,
    unique_title: null,
    type: "sub menu",
    student_type: null,
    session_list: [
      {
        session: null,
        statistic_list: [
          {
            statistic_category: null,
            statistic: null,
          },
        ],
      },
    ],
  },
];
