import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import { AddBox, ArrowDownward } from "@material-ui/icons";
// import MaterialTable, { MTableEditField } from "material-table";
import MaterialTable from '@material-table/core';
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import { toast } from "react-toastify";
import { getModel, getActive } from "../../services/modelService";
import Swal from 'sweetalert2'; 

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  // Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const Model = (props) => {
  const [open, setOpen] = useState();
  const [modelList, setModelList] = useState([]);
  const [loader, setloader] = useState(false);
  // const [trains, setTrains] = useState(localStorage.getItem('train'));
  
  useEffect(() => {
    getModelList();
  }, []);

  const handleActive = async (event, rowData) => {
    Swal.fire({
      icon: 'info',
      title: 'Do you want to activate ?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: 'Activate',
      denyButtonText: `Don't save`,
    }).then(async(result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        setloader(true);
        try {
          let res = await getActive(rowData.model);
          console.log(res);
        } catch (error) {
          console.log(error);
        }
        setloader(false);
      } else if (result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })
  };

  const getModelList = async () => {
    try {
      let res = await getModel();
    //   console.log(res.data);
      setModelList(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="Models" />
      <Container fluid={true}>
        <div className="edit-profile">
          <Row>
            <Col md="12">
              <MaterialTable
                isLoading={loader}
                icons={tableIcons}
                columns={[
                  {
                    title: "Model Name",
                    field: "model",
                    headerStyle: {
                      zIndex: 0,
                    },
                    // render: (rowData) => {
                    //     return (
                    //         <p
                    //             style={{
                    //                 color: "blue",
                    //                 textDecoration: "underline",
                    //                 cursor: "pointer",
                    //                 fontSize: 15
                    //             }}
                    //             onClick={() => handleDialog(rowData)}
                    //         >
                    //             {rowData.intent}
                    //         </p>
                    //     );
                    // },
                  },
                ]}
                data={modelList.reverse().map((item) => {
                  return {
                    model: item,
                  };
                })}
                title=""
                options={{
                  actionsColumnIndex: -1,
                  addRowPosition: "first",
                  pageSize: 10,
                  pageSizeOptions: [10.5, 100, 200],
                }}
                actions={[
                  {
                    icon: tableIcons.Edit,
                    onClick: (event, rowData) => {
                      handleActive(event, rowData);
                    },

                    tooltip: "Make model active",
                  },
                ]}
              />
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
};

export default Model;
