import React, { useEffect, Fragment, useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col,
  Card,
  Badge,
  CardHeader,
  CardBody,
  CardFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { alpha, makeStyles } from "@material-ui/core/styles";
import { addMainmenus } from "../../services/menuService";
import { X } from "react-feather";
import { uploadFile } from "../../services/faqService";
import Dropzone from "react-dropzone-uploader";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
  },
  table: {
    width: 1100,
    minHeight: 100,
    margin: "20px auto",
    height: 500,
    borderWidth: 2,
    borderColor: "#e4e4e4",
    backgroundColor: "#fff",
    borderRadius: 20,
    overflow: "hidden",
    boxShadow:
      "0px 2px 4px -1px #00000033, 0px 4px 5px 0px #00000024, 0px 1px 10px 0px #0000001f",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchPanel: {
    borderBottom: "solid 1px rgb(236, 239, 247)",
    paddingTop: 10,
    paddingBottom: 10,
  },
}));

const SubMenuModal = (props) => {
  const {
    open,
    setOpen,
    unique,
    subMenuUttr,
    setSubMenuUttr,
    uniqueTitleUttr,
    setUniqueTitleUttr,
    handleSubmit,
  } = props;

  const classes = useStyles();
  const [intent, setIntent] = useState("choose_option");
  const [slot, setSlot] = useState("option");

  const handleClose = () => {
    setOpen(false);
    setSubMenuUttr("");
    setUniqueTitleUttr("");
  };

  return (
    <Modal isOpen={open} centered>
      <ModalHeader>Add Submenu : {unique && unique}</ModalHeader>
      <ModalBody>
        <Row>
          <Col md="12">
            <Card>
              <Form className="form theme-form">
                <CardBody className="CardBodyMenus">
                  <Row>
                    <Col>
                      <FormGroup className="m-b-20">
                        <Label>Sub Menu</Label>
                        <Input
                          className="form-control"
                          id="validationCustom01"
                          type="text"
                          value={subMenuUttr}
                          onChange={(e) => setSubMenuUttr(e.target.value)}
                          placeholder="Please enter sub menu"
                        />
                      </FormGroup>
                      <FormGroup className="m-b-20">
                        <Label>Unique Title</Label>
                        <Input
                          className="form-control"
                          id="validationCustom01"
                          type="text"
                          value={uniqueTitleUttr}
                          onChange={(e) => setUniqueTitleUttr(e.target.value)}
                          placeholder="Please enter unique title"
                        />
                      </FormGroup>
                      <FormGroup className="m-b-20">
                        <Label>Intent</Label>
                        <Input
                          className="form-control"
                          id="validationCustom01"
                          type="text"
                          value={intent}
                          onChange={(e) => setIntent(e.target.value)}
                          readOnly={true}
                        />
                      </FormGroup>
                      <FormGroup className="m-b-20">
                        <Label>Slot</Label>
                        <Input
                          className="form-control"
                          id="validationCustom01"
                          type="text"
                          value={slot}
                          onChange={(e) => setSlot(e.target.value)}
                          readOnly={true}
                        />
                      </FormGroup>
                      <div className="CardFooterMenus">
                        <Button
                          color="primary"
                          type="button"
                          className="mr-1"
                          onClick={handleSubmit}
                          disabled={subMenuUttr && setUniqueTitleUttr ? false : true}
                        >
                          Submit
                        </Button>
                        <Button
                          color="light"
                          type="button"
                          onClick={handleClose}
                        >
                          Cancel
                        </Button>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
              </Form>
            </Card>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};
export default SubMenuModal;
