import React, { useEffect, Fragment, useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col,
  Card,
  Badge,
  CardHeader,
  CardBody,
  CardFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { alpha, makeStyles } from "@material-ui/core/styles";
import { X } from "react-feather";
import { uploadFile } from "../../services/faqService";
import Dropzone from "react-dropzone-uploader";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
  },
  table: {
    width: 1100,
    minHeight: 100,
    margin: "20px auto",
    height: 500,
    borderWidth: 2,
    borderColor: "#e4e4e4",
    backgroundColor: "#fff",
    borderRadius: 20,
    overflow: "hidden",
    boxShadow:
      "0px 2px 4px -1px #00000033, 0px 4px 5px 0px #00000024, 0px 1px 10px 0px #0000001f",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchPanel: {
    borderBottom: "solid 1px rgb(236, 239, 247)",
    paddingTop: 10,
    paddingBottom: 10,
  },
}));

const MenuModal = (props) => {
  const { open, setOpen, handleSubmitButton, buttonValue, setButtonValue } = props;

  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
    setButtonValue("")
  };

  const handlebutton = (e) => {
    setButtonValue(e.target.value);
  };

  return (
    <Modal isOpen={open} centered>
      <ModalHeader>Exit / Main menu button</ModalHeader>
      <ModalBody>
        <Row>
          <Col md="12">
            <Card>
              <Form className="form theme-form">
                <CardBody className="CardBodyMenus">
                  <Row>
                    <Col>
                      <>
                        <FormGroup>
                          <Label>Select Button</Label>
                          <Input
                            type="select"
                            name="select"
                            className="form-control digits"
                            value={buttonValue}
                            onChange={(e) => handlebutton(e)}
                          >
                            <option value="">Select</option>
                            <option value="Main Menu">Main Menu</option>
                            <option value="Exit">Exit</option>
                          </Input>
                        </FormGroup>
                        <div className="CardFooterMenus">
                          <Button
                            color="primary"
                            type="button"
                            className="mr-1"
                            onClick={handleSubmitButton}
                            disabled={buttonValue ? false : true}
                          >
                            Submit
                          </Button>
                          <Button
                            color="light"
                            type="button"
                            onClick={handleClose}
                          >
                            Cancel
                          </Button>
                        </div>
                      </>
                    </Col>
                  </Row>
                </CardBody>
              </Form>
            </Card>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};
export default MenuModal;
