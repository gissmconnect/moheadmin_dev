import React, { useEffect, Fragment, useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col,
  Card,
  Badge,
  CardHeader,
  CardBody,
  CardFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { alpha, makeStyles } from "@material-ui/core/styles";
import { AddMenus, getSubMenu } from "../../services/menuService";
import { X } from "react-feather";
import { uploadFile } from "../../services/faqService";
import Dropzone from "react-dropzone-uploader";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
  },
  table: {
    width: 1100,
    minHeight: 100,
    margin: "20px auto",
    height: 500,
    borderWidth: 2,
    borderColor: "#e4e4e4",
    backgroundColor: "#fff",
    borderRadius: 20,
    overflow: "hidden",
    boxShadow:
      "0px 2px 4px -1px #00000033, 0px 4px 5px 0px #00000024, 0px 1px 10px 0px #0000001f",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchPanel: {
    borderBottom: "solid 1px rgb(236, 239, 247)",
    paddingTop: 10,
    paddingBottom: 10,
  },
}));

const MenuModal = (props) => {
  const { open, setOpen, handleSubmit, utteranceView, fileData, setFileData } = props;

  const classes = useStyles();
  // const [lastClicked, setLastClicked] = useState(false);
  const [fileName, setFileName] = useState();
  const [upload, setUpload] = useState(false);
  const [buttonStatus, setButtonStatus] = useState(true);
  const [uploadedFileName, setUploadedFileName] = useState("");
  const [pdf, setPdf] = useState(false);
  const [message, setMessage] = useState("");
  const [responsePdfVideo, setResponsePdfVideo] = useState("");

  const handleClose = () => {
    setOpen(false);
    setMessage("");
    setFileName();
    setUploadedFileName("");
    setUpload(false);
    setFileData([])
  };

  const handlePdfVideo = (e) => {
    setResponsePdfVideo(e.target.value);
  };

  const getUploadParams = ({ meta }) => {
    return { url: "https://httpbin.org/post" };
  };

  const handleChangeStatus = ({ meta, file }, status) => {
    if (status === "done") {
      setFileName(file);
      setUpload(true);
      setButtonStatus(!buttonStatus);
    } else if (status === "removed") {
      setFileName();
      setUpload(false);
    }
  };

  const handleUpload = async () => {
    // console.log(fileName, pdf, "fileName, pdf");
    try {
      let res = await uploadFile(fileName, responsePdfVideo);
      let fileArray = [...uploadedFileName];
      // fileArray.push(res.url);
      // setUploadedFileName(fileArray);
      setUploadedFileName(res.url);
      setFileName();
      // console.log(res, "res---");
    } catch (error) {
      console.log(error);
    }
  };

  const handleFileSave = () => {
    let data = `${message}\n ${uploadedFileName}\n`;
    let keyArray = [...fileData];
    keyArray.push(data);
    let text = "";
    for (let member in keyArray) {
      text += keyArray[member];
    }
    setFileData(text);
    setMessage("");
    setUploadedFileName("");
    setFileName();
    // console.log(text, fileData, "data");
  };

  return (
    <Modal isOpen={open} centered>
      <ModalHeader>Add Submenu : {utteranceView?.unique_title}</ModalHeader>
      <ModalBody>
        <Row>
          <Col md="12">
            <Card>
              <Form className="form theme-form">
                <CardBody className="CardBodyMenus">
                  <Row>
                    <Col>
                      <>
                        <FormGroup className="m-b-20">
                          <Label>Previous details</Label>
                          <Input
                            className="form-control"
                            id="validationCustom01"
                            type="textarea"
                            value={utteranceView?.utterance}
                            placeholder="Previous details"
                            readOnly={true}
                            // onChange={(e) => setMessage(e.target.value)}
                          />
                        </FormGroup>
                        <FormGroup className="m-b-20">
                          <Label>Header Message</Label>
                          <Input
                            className="form-control"
                            id="validationCustom01"
                            type="textarea"
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                            placeholder="Please enter message"
                          />
                        </FormGroup>
                        <FormGroup>
                          <Label>Select file type Video/Pdf</Label>
                          <Input
                            type="select"
                            name="select"
                            className="form-control digits"
                            value={responsePdfVideo}
                            onChange={(e) => handlePdfVideo(e)}
                          >
                            <option value="">Select</option>
                            <option value="video">Video</option>
                            <option value="pdf">Pdf</option>
                          </Input>
                        </FormGroup>

                        <FormGroup className="m-b-20 file_Message">
                          {responsePdfVideo && (
                            <div className="m-0 dz-message needsclick file_Message_Dropzone">
                              <Dropzone
                                getUploadParams={getUploadParams}
                                onChangeStatus={handleChangeStatus}
                                maxFiles={1}
                                multiple={false}
                                accept={".mp4, .pdf"}
                                inputContent="Drop files or click to upload."
                                styles={{
                                  // dropzone: { width: "100%", height: 25 },
                                  dropzoneActive: { borderColor: "green" },
                                }}
                              />
                              <Button
                                color="primary"
                                type="button"
                                className="mr-1"
                                disabled={upload ? false : true}
                                onClick={handleUpload}
                              >
                                Upload
                              </Button>
                            </div>
                          )}
                          <div className="file_Message_SaveButton">
                            <Button
                              color="primary"
                              type="button"
                              className="mr-1"
                              disabled={
                                message || (upload && uploadedFileName)
                                  ? false
                                  : true
                              }
                              onClick={handleFileSave}
                            >
                              Save
                            </Button>
                          </div>
                        </FormGroup>
                        <FormGroup>
                          <Input
                            className="form-control"
                            id="validationCustom01"
                            type="textarea"
                            value={fileData}
                            placeholder="Show input details"
                            readOnly={true}
                            // onChange={(e) => setMessage(e.target.value)}
                          />
                        </FormGroup>
                        <div className="before_submit_style">
                          * Please save file before submit
                        </div>
                        <div className="CardFooterMenus">
                          <Button
                            color="primary"
                            type="button"
                            className="mr-1"
                            onClick={handleSubmit}
                            disabled={fileData?.length != 0 ? false : true}
                          >
                            Submit
                          </Button>
                          <Button
                            color="light"
                            type="button"
                            onClick={handleClose}
                          >
                            Cancel
                          </Button>
                        </div>
                      </>
                    </Col>
                  </Row>
                </CardBody>
              </Form>
            </Card>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};
export default MenuModal;
