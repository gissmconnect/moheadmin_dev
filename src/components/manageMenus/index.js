import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import { toast } from "react-toastify";
import Breadcrumb from "../../layout/breadcrumb";
import { Container, Row, Col, Button } from "reactstrap";
import { filePlus } from "react-feather";
import { AddBox, ArrowDownward, Add } from "@material-ui/icons";
// import MaterialTable, { MTableEditField } from "material-table";
import MaterialTable from "@material-table/core";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import Cached from "@material-ui/icons/Cached";
import {
  getMainMenu,
  getSubMenu,
  addSubmenus,
  updateSubmenus,
  addMainmenus,
  updateMainmenus,
  deleteMainmenus,
  deleteSubmenus,
  AddMenus,
  deleteSubmenusUtterance,
  deleteStructure,
  addMainmenusButton,
  addSubmenusButton,
} from "../../services/menuService";
import { useHistory } from "react-router-dom";
import MenuModal from "./menuModal";
import SubMenuModal from "./subMenuModal";
import ButtonExitMainMenu from "./buttonExitMainMenu";
import SubMenu from "./subMenu";
import Swal from "sweetalert2";
import Tooltip from "@material-ui/core/Tooltip";
import { flatMap } from "lodash";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const Menus = (props) => {
  const [open, setOpen] = useState();
  const history = useHistory();
  const [editrowData, seteditrowData] = useState([]);
  const [menuList, setMenuList] = useState([]);
  const [loader, setloader] = useState(false);
  const [title, setTitle] = useState("Main Menu");
  const [type, setType] = useState("main menu");
  const [id, setId] = useState("");
  const [data, setData] = useState({});
  const [subName, setSubName] = useState("");
  const [intent, setIntent] = useState("choose_option");
  const [slot, setSlot] = useState("option");
  const [uniqueTitle, setUniqueTitle] = useState("");
  const [unique, setUnique] = useState("");
  const [unkTitle, setUnkTitle] = useState("");
  const [utterance, setUtterance] = useState("");
  const [responseType, setResponseType] = useState("");
  const [fileData, setFileData] = useState([]);
  const [keywordValue, setKeywordValue] = useState("");
  const [keywords, setKeywords] = useState([]);
  const [utteranceView, setUtteranceView] = useState([]);

  const [openSubMenuModal, setOpenSubMenuModal] = useState(false);
  const [editrowDatas, setEditrowDatas] = useState([]);
  const [fileSunMenuData, setFileSunMenuData] = useState([]);
  const [uniqueTitleMenu, setUniqueTitleMenu] = useState([]);
  const [openSubMenu, setOpenSubMenu] = useState(false);
  const [subMenuUttr, setSubMenuUttr] = useState("");
  const [uniqueTitleUttr, setUniqueTitleUttr] = useState("");
  const [datass, setDatass] = useState([]);

  const [openButton, setOpenButton] = useState(false);
  const [buttonValue, setButtonValue] = useState("");

  useEffect(() => {
    getMainMenuList();
  }, []);

  const handleSubmit = async () => {
    try {
      setloader(true);
      let res = await AddMenus(
        editrowData.title,
        subName,
        uniqueTitle,
        unkTitle,
        utterance,
        responseType,
        fileData,
        keywordValue
      );
      if (res.status === "done") {
        setOpen(false);
        setKeywords([]);
        setKeywordValue("");
        setFileData([]);
        setUtterance("");
        setUnkTitle("");
        setloader(false);
        if (type === "main menu") {
          getMainMenuList();
          setSubName("");
          setKeywords([]);
          setKeywordValue("");
          setFileData([]);
          setUtterance("");
          setUnkTitle("");
        } else if (type === "sub menu") {
          try {
            setloader(true);
            let res = await getSubMenu(uniqueTitle);
            if (res?.data) {
              setSubName("");
              setData(res);
              setTitle(editrowData.title);
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setKeywords([]);
              setKeywordValue("");
              setFileData([]);
              setUtterance("");
              setUnkTitle("");
              setUtteranceView(res);
            }
            setloader(true);
          } catch (error) {
            console.log(error);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleDialog = async (rowData) => {
    setUnique(rowData.uniqueTitle);
    if (
      rowData.uniqueTitle === "Outside Sultanate" ||
      rowData.uniqueTitle === "Inside Sultanate" ||
      rowData.uniqueTitle === "Direct Admission Programs" ||
      rowData.uniqueTitle === "Franchise Schools" ||
      rowData.uniqueTitle === "indicators and statistics" ||
      rowData.uniqueTitle ===
        "Qualifications corresponding to the required specialization" ||
      rowData.uniqueTitle === "appointmentbyprogcode"
    ) {
      if (rowData.uniqueTitle === "Inside Sultanate") {
        history.push(
          `${process.env.PUBLIC_URL}/app/menus/inside-Programs/Dubai`
        );
      } else if (rowData.uniqueTitle === "Outside Sultanate") {
        history.push(`${process.env.PUBLIC_URL}/app/menus/programs/Dubai`);
      } else if (rowData.uniqueTitle === "Direct Admission Programs") {
        history.push(
          `${process.env.PUBLIC_URL}/app/menus/manage-direct-admission/Dubai`
        );
      } else if (rowData.uniqueTitle === "Franchise Schools") {
        history.push(
          `${process.env.PUBLIC_URL}/app/menus/franchise-schools/Dubai`
        );
      } else if (rowData.uniqueTitle === "indicators and statistics") {
        history.push(
          `${process.env.PUBLIC_URL}/app/menus/indicators&statistics/Dubai`
        );
      } else if (
        rowData.uniqueTitle ===
        "Qualifications corresponding to the required specialization"
      ) {
        history.push(
          `${process.env.PUBLIC_URL}/app/menus/qualificationsDetails/Dubai`
        );
      } else if (rowData.uniqueTitle === "appointmentbyprogcode") {
        history.push(
          `${process.env.PUBLIC_URL}/app/menus/appointmentByProgCode/Dubai`
        );
      }
    } else {
      try {
        setloader(true);
        let res = await getSubMenu(rowData.uniqueTitle);
        if (res.data) {
          window.scrollTo(0, 0);
          setData(res);
          setTitle(rowData.title);
          setMenuList(res.data);
          setType(res.type);
          setId(res._id);
          setUtteranceView(res);
        } else if (res) {
          setUtteranceView(res);
        }
        setloader(false);
      } catch (error) {
        console.log(error);
      }
    }
  };

  const getMainMenuList = async () => {
    try {
      setloader(true);
      let res = await getMainMenu();
      window.scrollTo(0, 0);
      setData(res);
      setMenuList(res.data);
      setType(res.type);
      setId(res._id);
      setTitle("Main Menu");
      setloader(false);
      setUtteranceView(res);
    } catch (error) {
      console.log(error);
    }
  };

  const handleRowAdd = async (newData, resolve, reject) => {
    const name = newData.title;
    const uniqueT = newData.uniqueTitle;
    if (type === "main menu") {
      let errorList = [];
      if (!name) {
        Swal.fire({
          icon: "error",
          text: "Name field could not be empty",
        });
        errorList.push("Name field could not be empty");
      } else if (!uniqueT) {
        Swal.fire({
          icon: "error",
          text: "unique Title field could not be empty",
        });
        errorList.push("unique Title field could not be empty");
      }
      if (errorList.length <= 0) {
        try {
          setloader(true);
          let res = await addMainmenus(name, id, uniqueT);
          if (res.status === "done") {
            try {
              let res = await getMainMenu();
              if (res?.data) {
                setData(res);
                setMenuList(res.data);
                setType(res.type);
                setId(res._id);
                setloader(false);
              }
            } catch (error) {
              console.log(error);
            }
            setloader(false);
          }
          resolve();
        } catch (error) {
          console.log(error);
          resolve();
        }
        setloader(false);
      }
    } else if (type === "sub menu") {
      try {
        setloader(true);
        let res = await addSubmenus(
          name || subMenuUttr,
          id,
          uniqueT || uniqueTitleUttr
        );
        if (res.status === "done") {
          setloader(false);
          try {
            let res = await getSubMenu(unique);
            if (res?.data) {
              setData(res);
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setOpenSubMenu(false);
              setSubMenuUttr("");
              setUniqueTitleUttr("");
              setUtteranceView(res);
            }
          } catch (error) {
            console.log(error);
            resolve();
          }
        }
      } catch (error) {
        console.log(error);
        resolve();
      }
      setloader(false);
      resolve();
    }
  };

  const handleRowUpdate = async (newData, oldData, resolve, reject) => {
    if (type === "main menu") {
      setloader(true);
      try {
        let res = await updateMainmenus(
          newData.title,
          oldData.title,
          id,
          oldData.uniqueTitle
        );
        if (res.status === "done") {
          try {
            let res = await getMainMenu();
            if (res?.data) {
              setData(res);
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setloader(false);
            }
          } catch (error) {
            console.log(error);
            resolve();
          }
          setloader(false);
        }
        resolve();
      } catch (error) {
        console.log(error);
        resolve();
      }
    } else if (type === "sub menu") {
      try {
        setloader(true);
        let res = await updateSubmenus(
          newData.title,
          oldData.title,
          id,
          newData.uniqueTitle,
          newData.utterance || fileSunMenuData,
          newData.intent,
          newData.slot
        );
        if (res.status === "done") {
          try {
            let res = await getSubMenu(unique || oldData.title);
            if (res) {
              if (res?.data) {
                setData(res);
                setMenuList(res.data);
                setType(res.type);
                setId(res._id);
                setUtteranceView(res);
                setloader(false);
              } else if (res) {
                setUtteranceView(res);
              }
            }
          } catch (error) {
            console.log(error);
            resolve();
          }
        }
        setloader(false);
        resolve();
      } catch (error) {
        console.log(error);
        resolve();
      }
    }
    setloader(true);
  };

  const handleUpdateSubMenu = async () => {
    try {
      setloader(true);
      let result = await updateSubmenus(
        "",
        "",
        utteranceView._id,
        utteranceView.unique_title,
        fileSunMenuData
      );
      if (result.status === "done") {
        setFileSunMenuData([]);
        setOpenSubMenuModal(false);
        setloader(false);
        try {
          let res = await getSubMenu(utteranceView.unique_title);
          if (res) {
            if (res?.data) {
              setData(res);
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setUtteranceView(res);
              setOpenSubMenuModal(false);
              setloader(false);
            } else if (res) {
              setUtteranceView(res);
            }
          }
        } catch (error) {
          setloader(false);
          console.log(error);
        }
      }
      setloader(false);
    } catch (error) {
      console.log(error);
    }
  };

  const handleRowDelete = async (oldData, resolve, reject) => {
    if (type === "main menu") {
      setloader(true);
      try {
        let res = await deleteMainmenus(oldData.title, id, oldData.uniqueTitle);
        if (res.status === "done") {
          try {
            let res = await getMainMenu();
            if (res?.data) {
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setloader(false);
            }
          } catch (error) {
            console.log(error);
            resolve();
          }
        }
        setloader(false);
        resolve();
      } catch (error) {
        console.log(error);
        resolve();
      }
    } else if (type === "sub menu") {
      // console.log(oldData, "oldData----")
      try {
        setloader(true);
        let res = await deleteSubmenus(
          oldData.title,
          id,
          oldData.uniqueTitle,
          oldData.intent,
          oldData.slot
        );
        if (res.status === "done") {
          try {
            let res = await getSubMenu(unique);
            if (res?.data) {
              setData(res);
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setloader(false);
            }
          } catch (error) {
            console.log(error);
            resolve();
          }
        }
        setloader(false);
        resolve();
      } catch (error) {
        console.log(error);
        resolve();
      }
    }
  };

  const handleMenuDialog = async (rowData) => {
    try {
      setloader(true);
      let res = await getSubMenu(rowData.uniqueTitle);
      if (res?.data === undefined || res?.utterance === undefined) {
        setOpen(true);
        seteditrowData(rowData);
      } else {
        Swal.fire({
          icon: "info",
          // title: 'Oops...',
          text: "Button already added please click on submenu to custimize button/messages",
        });
      }
    } catch (error) {
      console.log(error);
    }
    setloader(false);
  };

  const handleSubMenuDialog = async (event, rowData) => {
    // console.log(event, rowData, "rowData--");
    setOpenSubMenuModal(true);
    // setEditrowDatas(rowData);
  };

  function containsSpecialChars(str) {
    const specialChars = `_/`;
    const result = specialChars.split("").some((specialChar) => {
      if (str.includes(specialChar)) {
        return true;
      }
      return false;
    });
    return result;
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    setUniqueTitleMenu([...uniqueTitleMenu, unique ? unique : "main menu"]);
  }, [unique]);

  const handleDeleteUtterance = async (oldData, resolve, reject) => {
    try {
      setloader(true);
      let res = await deleteSubmenusUtterance(oldData._id, oldData.utterance);
      if (res.status === "done") {
        try {
          let res = await getSubMenu(unique);
          if (res?.data) {
            setData(res);
            setMenuList(res.data);
            setType(res.type);
            setId(res._id);
            setloader(false);
            setUtteranceView(res);
          } else {
            setUtteranceView(res);
          }
        } catch (error) {
          console.log(error);
          resolve();
        }
      }
      resolve();
    } catch (error) {
      console.log(error);
      resolve();
    }
    setloader(false);
  };

  const handleClickOpen = (rowData) => {
    setOpenSubMenu(true);
  };

  useEffect(() => {
    setDatass([
      {
        utterance: utteranceView?.utterance,
        unique_title: utteranceView?.unique_title,
        _id: utteranceView?._id,
      },
    ]);
  }, [utteranceView]);

  const handleBackMenu = async () => {
    if (uniqueTitleMenu[uniqueTitleMenu.length - 1] === "main menu") {
      try {
        let res = await getMainMenu();
        setData(res);
        setMenuList(res.data);
        setType(res.type);
        setId(res._id);
        setTitle("Main Menu");
        setUniqueTitleMenu(["main menu"]);
        setUtteranceView(res);
      } catch (error) {
        console.log(error);
      }
    } else {
      try {
        let res = await getSubMenu(uniqueTitleMenu[uniqueTitleMenu.length - 1]);
        if (res.data) {
          setData(res);
          setTitle(res.category);
          setMenuList(res.data);
          setType(res.type);
          setId(res._id);
          setUtteranceView(res);
          uniqueTitleMenu.pop();
        } else if (res) {
          setUtteranceView(res);
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleRefreshPage = async () => {
    try {
      let res = await getMainMenu();
      setData(res);
      setMenuList(res.data);
      setType(res.type);
      setId(res._id);
      setTitle("Main Menu");
      setUniqueTitleMenu(["main menu"]);
      setUtteranceView(res);
    } catch (error) {
      console.log(error);
    }
    setUniqueTitleMenu(["main menu"]);
  };

  const deleteStructureData = async () => {
    Swal.fire({
      icon: "info",
      title: "Do you want to Delete ?",
      text: `It will delete Complete Structure from ${
        unique || uniqueTitle || "main menu"
      }`,
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Ok",
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          setloader(true);
          let res = await deleteStructure(id);
          if (res.status === "done") {
            try {
              let res = await getSubMenu(
                uniqueTitleMenu[uniqueTitleMenu.length - 2]
              );
              if (res?.data) {
                setData(res);
                setMenuList(res.data);
                setType(res.type);
                setId(res._id);
                setloader(false);
                setUtteranceView(res);
              } else {
                setUtteranceView(res);
              }
            } catch (error) {
              console.log(error);
            }
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const addExitMainMenuButton = async () => {
    setOpenButton(true);
  };

  const handleSubmitButton = async () => {
    if (buttonValue === "Main Menu") {
      var dataButton = { title: "القائمة الرئيسية", payload: "/main_menu" };
    } else if (buttonValue === "Exit") {
      var dataButton = { title: "خروج", payload: "/restart" };
    }
    if (type === "main menu") {
      try {
        setloader(true);
        let res = await addMainmenusButton(dataButton, id);
        if (res.status === "done") {
          try {
            let res = await getMainMenu();
            if (res?.data) {
              setData(res);
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setloader(false);
              setOpenButton(false);
              setButtonValue("");
            }
          } catch (error) {
            console.log(error);
          }
          setloader(false);
        }
      } catch (error) {
        console.log(error);
      }
      setloader(false);
    } else if (type === "sub menu") {
      try {
        setloader(true);
        let res = await addSubmenusButton(dataButton, id);
        if (res.status === "done") {
          setloader(false);
          try {
            let res = await getSubMenu(unique);
            if (res?.data) {
              setData(res);
              setMenuList(res.data);
              setType(res.type);
              setId(res._id);
              setOpenSubMenu(false);
              setSubMenuUttr("");
              setUniqueTitleUttr("");
              setUtteranceView(res);
              setOpenButton(false);
              setButtonValue("");
            }
          } catch (error) {
            console.log(error);
          }
        }
      } catch (error) {
        console.log(error);
      }
      setloader(false);
    }
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="Menus" />
      <Container fluid={true}>
        <div className="edit-profile edit-profile-style-update">
          <Row>
            <Col md="12">
              <div
                style={{
                  position: "absolute",
                  zIndex: 2,
                  top: 15,
                  left: 30,
                  textTransform: "uppercase",
                  fontSize: 12,
                  letterSpacing: 1,
                  padding: "6px 14px 6px 0px",
                  width: "calc(100% - 90px)",
                }}
              >
                {uniqueTitleMenu.length != 1 && (
                  <>
                    <Button onClick={handleBackMenu}>Back</Button>
                    <Button
                      onClick={handleRefreshPage}
                      style={{ marginLeft: "10px" }}
                    >
                      Main Menu
                    </Button>
                  </>
                )}
                <Tooltip title="Delete Structure">
                  <DeleteOutline
                    onClick={deleteStructureData}
                    style={{
                      cursor: "pointer",
                      position: "absolute",
                      right: 20,
                      color: "#757575",
                    }}
                  >
                    Dalete
                  </DeleteOutline>
                </Tooltip>
                <Tooltip title="Add Exit Button/Main menu Button">
                  <Add
                    onClick={addExitMainMenuButton}
                    style={{
                      cursor: "pointer",
                      position: "absolute",
                      right: 60,
                      color: "#757575",
                    }}
                  >
                    Dalete
                  </Add>
                </Tooltip>
              </div>
              <MaterialTable
                icons={tableIcons}
                isLoading={false}
                columns={[
                  {
                    title: "Message Heading",
                    field: "utterance",
                  },
                  {
                    title: "Unique Title",
                    field: "unique_title",
                    hidden: true,
                  },
                  {
                    title: "ID",
                    field: "_id",
                    hidden: true,
                  },
                ]}
                data={
                  datass &&
                  datass.map((item) => {
                    return {
                      utterance: item.utterance,
                      unique_title: item.unique_title,
                      _id: item._id,
                    };
                  })
                }
                title=""
                options={{
                  actionsColumnIndex: -1,
                  addRowPosition: "first",
                  pageSize: 1,
                  pageSizeOptions: [1],
                  search: false,
                }}
                editable={{
                  // onRowAdd: (newData) =>
                  //   new Promise((resolve, reject) => {
                  //     handleRowAdd(newData, resolve, reject);
                  //   }),

                  // onRowUpdate: (newData, oldData) => handleSubMenuDialog(newData, oldData)

                  onRowDelete: (oldData) =>
                    new Promise((resolve, reject) => {
                      handleDeleteUtterance(oldData, resolve, reject);
                    }),
                }}
                actions={[
                  {
                    icon: tableIcons.Edit,
                    onClick: (event, rowData) => {
                      handleSubMenuDialog(event, rowData);
                    },
                    tooltip: "Edit",
                  },
                  utteranceView?.utterance?.length <= 0 && {
                    icon: tableIcons.Add,
                    onClick: (event, rowData) => {
                      handleClickOpen(rowData);
                    },
                    isFreeAction: true,
                    tooltip: "Add",
                  },
                ]}
              />
              {menuList?.length != 0 && (
                <>
                  {/* <div
                    style={{
                      position: "absolute",
                      zIndex: 2,
                      top: 15,
                      left: 30,
                      textTransform: "uppercase",
                      fontSize: 12,
                      letterSpacing: 1,
                      padding: "6px 14px 6px 0px",
                      width: "calc(100% - 90px)",
                    }}
                  >
                    {uniqueTitleMenu.length != 1 && (
                      <>
                        <Button onClick={handleBackMenu}>Back</Button>
                        <Button
                          onClick={handleRefreshPage}
                          style={{ marginLeft: "10px" }}
                        >
                          Main Menu
                        </Button>
                      </>
                    )}
                    <Tooltip title="Delete Structure">
                      <DeleteOutline
                        onClick={deleteStructureData}
                        style={{
                          cursor: "pointer",
                          position: "absolute",
                          right: 20,
                          color: "#757575",
                        }}
                      >
                        Dalete
                      </DeleteOutline>
                    </Tooltip>
                    <Tooltip title="Add Exit Button/Main menu Button">
                      <Add
                        onClick={addExitMainMenuButton}
                        style={{
                          cursor: "pointer",
                          position: "absolute",
                          right: 60,
                          color: "#757575",
                        }}
                      >
                        Dalete
                      </Add>
                    </Tooltip>
                  </div> */}
                  <MaterialTable
                    style={{ marginTop: 20 }}
                    icons={tableIcons}
                    isLoading={false}
                    columns={[
                      {
                        title: title,
                        field: "title",
                        headerStyle: {
                          zIndex: 0,
                        },
                        render: (rowData) => {
                          return (
                            <p
                              style={{
                                color: "blue",
                                textDecoration: "underline",
                                cursor: "pointer",
                                fontSize: 15,
                              }}
                              onClick={() => handleDialog(rowData)}
                            >
                              {rowData.title}
                            </p>
                          );
                        },
                      },
                      {
                        title: "Intent",
                        field: "intent",
                        editable: "never",
                      },
                      {
                        title: "Slot",
                        field: "slot",
                        editable: "never",
                      },
                      {
                        title: "Unique Title",
                        field: "uniqueTitle",
                      },
                      {
                        title: "Add Submenu",
                        field: "",
                        headerStyle: {
                          zIndex: 0,
                        },
                        render: (rowData) => {
                          return (
                            <Add onClick={() => handleMenuDialog(rowData)} />
                          );
                        },
                      },
                      {
                        title: "Payload",
                        field: "payload",
                        hidden: true,
                      },
                    ]}
                    data={
                      menuList &&
                      menuList.map((item) => {
                        return {
                          title: item.title,
                          // intent: item.payload.substring(1, 14),
                          intent:
                            containsSpecialChars(item.payload) &&
                            item.payload.split(/[{]/)[0].substring(1),
                          slot: containsSpecialChars(
                            item.payload.replace(/.*\{|\}/gi, "")
                          )
                            ? null
                            : item.payload
                                .replace(/.*\{|\}/gi, "")
                                .split(":")[0]
                                .slice(1, -1),
                          uniqueTitle: containsSpecialChars(
                            item.payload.replace(/.*\{|\}/gi, "")
                          )
                            ? null
                            : item.payload
                                .replace(/.*\{|\}/gi, "")
                                .split(":")[1]
                                .slice(1, -1),
                          payload: item.payload,
                        };
                      })
                    }
                    title=""
                    options={{
                      search: false,
                      actionsColumnIndex: -1,
                      addRowPosition: "first",
                      pageSize: 10,
                      pageSizeOptions: [20, 40, 50, 100],
                    }}
                    editable={{
                      onRowAdd: (newData) =>
                        new Promise((resolve, reject) => {
                          handleRowAdd(newData, resolve, reject);
                        }),

                      onRowUpdate: (newData, oldData) =>
                        new Promise((resolve, reject) => {
                          handleRowUpdate(newData, oldData, resolve, reject);
                        }),
                      onRowDelete: (oldData) =>
                        new Promise((resolve, reject) => {
                          handleRowDelete(oldData, resolve, reject);
                        }),
                    }}
                  />
                </>
              )}
            </Col>
          </Row>
        </div>
      </Container>
      <MenuModal
        open={open}
        setOpen={setOpen}
        setloader={setloader}
        editrowData={editrowData}
        handleSubmit={handleSubmit}
        subName={subName}
        setSubName={setSubName}
        intent={intent}
        setIntent={setIntent}
        slot={slot}
        setSlot={setSlot}
        uniqueTitle={uniqueTitle}
        setUniqueTitle={setUniqueTitle}
        unkTitle={unkTitle}
        setUnkTitle={setUnkTitle}
        utterance={utterance}
        setUtterance={setUtterance}
        responseType={responseType}
        setResponseType={setResponseType}
        fileData={fileData}
        setFileData={setFileData}
        keywordValue={keywordValue}
        setKeywordValue={setKeywordValue}
        keywords={keywords}
        setKeywords={setKeywords}
      />
      <SubMenuModal
        open={openSubMenuModal}
        setOpen={setOpenSubMenuModal}
        editrowData={editrowDatas}
        utteranceView={utteranceView}
        fileData={fileSunMenuData}
        setFileData={setFileSunMenuData}
        handleSubmit={handleUpdateSubMenu}
      />
      <SubMenu
        open={openSubMenu}
        setOpen={setOpenSubMenu}
        unique={unique}
        id={id}
        subMenuUttr={subMenuUttr}
        setSubMenuUttr={setSubMenuUttr}
        uniqueTitleUttr={uniqueTitleUttr}
        setUniqueTitleUttr={setUniqueTitleUttr}
        handleSubmit={handleRowAdd}
      />
      <ButtonExitMainMenu
        open={openButton}
        setOpen={setOpenButton}
        buttonValue={buttonValue}
        setButtonValue={setButtonValue}
        handleSubmitButton={handleSubmitButton}
      />
    </Fragment>
  );
};

export default Menus;
