import React, { useEffect, Fragment, useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col,
  Card,
  Badge,
  CardHeader,
  CardBody,
  CardFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { alpha, makeStyles } from "@material-ui/core/styles";
import { AddMenus, getSubMenu } from "../../services/menuService";
import { X } from "react-feather";
import { uploadFile } from "../../services/faqService";
import Dropzone from "react-dropzone-uploader";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
  },
  table: {
    width: 1100,
    minHeight: 100,
    margin: "20px auto",
    height: 500,
    borderWidth: 2,
    borderColor: "#e4e4e4",
    backgroundColor: "#fff",
    borderRadius: 20,
    overflow: "hidden",
    boxShadow:
      "0px 2px 4px -1px #00000033, 0px 4px 5px 0px #00000024, 0px 1px 10px 0px #0000001f",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchPanel: {
    borderBottom: "solid 1px rgb(236, 239, 247)",
    paddingTop: 10,
    paddingBottom: 10,
  },
}));

const MenuModal = (props) => {
  const {
    open,
    setOpen,
    editrowData,
    handleSubmit,
    subName,
    setSubName,
    setUniqueTitle,
    uniqueTitle,
    slot,
    setSlot,
    intent,
    setIntent,
    unkTitle,
    setUnkTitle,
    utterance,
    setUtterance,
    responseType,
    setResponseType,
    fileData,
    setFileData,
    keywordValue,
    setKeywordValue,
    keywords,
    setKeywords,
  } = props;
  const classes = useStyles();
  const [lastClicked, setLastClicked] = useState(false);
  const [fileName, setFileName] = useState();
  const [upload, setUpload] = useState(false);
  const [buttonStatus, setButtonStatus] = useState(true);
  const [uploadedFileName, setUploadedFileName] = useState("");
  const [pdf, setPdf] = useState(false);
  const [message, setMessage] = useState("");
  const [responsePdfVideo, setResponsePdfVideo] = useState("");

  const handleClose = () => {
    setOpen(false);
    setKeywords([]);
    setKeywordValue("");
    setFileData([]);
    setUtterance("");
    setUnkTitle("");
  };
  const handleType = (e) => {
    setResponseType(e.target.value);
    if (e.target.value === "Video/Pdf_Message") {
      setLastClicked(true);
      setResponseType(e.target.value);
      setPdf(true);
      setSubName("");
      setUnkTitle("");
      setMessage("");
    } else {
      setLastClicked(false);
      setSubName("");
      setUnkTitle("");
      setMessage("");
    }
  };

  const handlePdfVideo = (e) => {
    setResponsePdfVideo(e.target.value);
  };

  useEffect(() => {
    if (editrowData && editrowData?.uniqueTitle) {
      setUniqueTitle(editrowData.uniqueTitle);
    }
  }, [editrowData]);

  const handleRemoveKeyword = (index) => {
    const keyword = [...keywords];
    keyword.splice(index, 1);
    setKeywords(keyword);
  };

  const getUploadParams = ({ meta }) => {
    return { url: "https://httpbin.org/post" };
  };

  const handleChangeStatus = ({ meta, file }, status) => {
    if (status === "done") {
      setFileName(file);
      setUpload(true);
      setButtonStatus(!buttonStatus);
    } else if (status === "removed") {
      setFileName();
      setUpload(false);
    }
  };

  const handleUpload = async () => {
    // console.log(fileName, pdf, "fileName, pdf");
    try {
      let res = await uploadFile(fileName, responsePdfVideo);
      let fileArray = [...uploadedFileName];
      // fileArray.push(res.url);
      // setUploadedFileName(fileArray);
      setUploadedFileName(res.url);
      setFileName();
      // console.log(res, "res---");
    } catch (error) {
      console.log(error);
    }
  };

  // const handleFileSave = () => {
  //   console.log(uploadedFileName, message, "message");
  //   let data = {message ,uploadedFileName };
  //   let keyArray = [...fileData];
  //   keyArray.push(data);
  //   setFileData(keyArray);
  //   setMessage("");
  //   setUploadedFileName("");
  //   setFileName();
  //   console.log(keyArray, "keyArray");
  //   console.log(typeof(keyArray), "typeof--")
  // };

  const handleFileSave = () => {
    let data = `${message}\n ${uploadedFileName}\n`;
    let keyArray = [...fileData];
    keyArray.push(data);
    let text = "";
    for (let member in keyArray) {
      text += keyArray[member];
    }
    setFileData(text);
    setMessage("");
    setUploadedFileName("");
    setFileName();
    // console.log(text, fileData, "data");
  };

  return (
    <Modal isOpen={open} centered>
      <ModalHeader>Add Submenu : {editrowData.title}</ModalHeader>
      <ModalBody>
        <Row>
          <Col md="12">
            <Card>
              <Form className="form theme-form">
                <CardBody className="CardBodyMenus">
                  <Row>
                    <Col>
                      <FormGroup>
                        <Label>Select Button/Message</Label>
                        <Input
                          type="select"
                          name="select"
                          className="form-control digits"
                          value={responseType}
                          onChange={(e) => handleType(e)}
                        >
                          <option value="">Select</option>
                          <option value="Sub_Menu">Button (Sub Menu)</option>
                          <option value="Text_Message_only">
                            Text Message only
                          </option>
                          <option value="Video/Pdf_Message">
                            Video/Pdf Message
                          </option>
                        </Input>
                      </FormGroup>
                      {responseType === "Sub_Menu" && (
                        <>
                          <FormGroup>
                            <Label>Submenu Name</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setSubName(e.target.value)}
                              placeholder="Enter submenu Name"
                              value={subName}
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label>Unique Title</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setUnkTitle(e.target.value)}
                              placeholder="Enter unique Title"
                              value={unkTitle}
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label>Header Message</Label>
                            <Input
                              className="form-control"
                              type="textarea"
                              onChange={(e) => setUtterance(e.target.value)}
                              placeholder="Enter utterance"
                              value={utterance}
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label>Intent</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setIntent(e.target.value)}
                              placeholder="Intent"
                              value={intent}
                              readOnly
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label>Slot</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setSlot(e.target.value)}
                              placeholder="Slot"
                              value={slot}
                              readOnly
                            />
                          </FormGroup>
                          <div className="CardFooterMenus">
                            <Button
                              color="primary"
                              type="button"
                              className="mr-1"
                              onClick={handleSubmit}
                            >
                              Submit
                            </Button>
                            <Button
                              color="light"
                              type="button"
                              onClick={handleClose}
                            >
                              Cancel
                            </Button>
                          </div>
                        </>
                      )}
                      {responseType === "Text_Message_only" && (
                        <>
                          <FormGroup>
                            <Label>Submenu Name</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setSubName(e.target.value)}
                              placeholder="Enter submenu Name"
                              value={subName}
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label>Unique Title</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setUnkTitle(e.target.value)}
                              placeholder="Enter unique Title"
                              value={unkTitle}
                            />
                          </FormGroup>
                          <FormGroup className="m-b-20">
                            <Label>Header Message</Label>
                            <Input
                              className="form-control"
                              id="validationCustom01"
                              type="textarea"
                              value={keywordValue}
                              // onKeyPress={(e) => {
                              //   if (e.charCode === 13) {
                              //     e.preventDefault();
                              //     let keyArray = [...keywords];
                              //     keyArray.push(keywordValue);
                              //     setKeywords(keyArray);
                              //     setKeywordValue("");
                              //   }
                              // }}
                              onChange={(e) => setKeywordValue(e.target.value)}
                              placeholder="Please enter message"
                            />
                          </FormGroup>
                          <div
                            className="custom-scrollbar3 custom-scrollbarKeywordValue"
                            style={{
                              display: "flex",
                              flexWrap: "wrap",
                              overflowX: "auto",
                            }}
                          >
                            {keywords &&
                              keywords.map((item, index) => {
                                return (
                                  <Badge
                                    key={index}
                                    color="primary"
                                    style={{
                                      fontSize: 16,
                                      display: "flex",
                                      alignItems: "center",
                                      marginBottom: 6,
                                    }}
                                  >
                                    <textarea>{item}</textarea>
                                    <X
                                      size={"30px"}
                                      style={{
                                        width: "20px",
                                        height: "20px",
                                        marginRight: 0,
                                        position: "static",
                                      }}
                                      onClick={() => handleRemoveKeyword(index)}
                                    />
                                  </Badge>
                                );
                              })}
                          </div>
                          <div className="CardFooterMenus">
                            <Button
                              color="primary"
                              type="button"
                              className="mr-1"
                              onClick={handleSubmit}
                            >
                              Submit
                            </Button>
                            <Button
                              color="light"
                              type="button"
                              onClick={handleClose}
                            >
                              Cancel
                            </Button>
                          </div>
                        </>
                      )}
                      {responseType === "Video/Pdf_Message" && (
                        <>
                          <FormGroup>
                            <Label>Submenu Name</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setSubName(e.target.value)}
                              placeholder="Enter submenu Name"
                              value={subName}
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label>Unique Title</Label>
                            <Input
                              className="form-control"
                              type="text"
                              onChange={(e) => setUnkTitle(e.target.value)}
                              placeholder="Enter unique Title"
                              value={unkTitle}
                            />
                          </FormGroup>
                          <FormGroup className="m-b-20">
                            <Label>Header Message</Label>
                            <Input
                              className="form-control"
                              id="validationCustom01"
                              type="textarea"
                              value={message}
                              onChange={(e) => setMessage(e.target.value)}
                              placeholder="Please enter message"
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label>Select file type Video/Pdf</Label>
                            <Input
                              type="select"
                              name="select"
                              className="form-control digits"
                              value={responsePdfVideo}
                              onChange={(e) => handlePdfVideo(e)}
                            >
                              <option value="">Select</option>
                              <option value="video">Video</option>
                              <option value="pdf">Pdf</option>
                            </Input>
                          </FormGroup>
                          <FormGroup className="m-b-20 file_Message">
                            {responsePdfVideo && (
                              <div className="m-0 dz-message needsclick file_Message_Dropzone">
                                <Dropzone
                                  getUploadParams={getUploadParams}
                                  onChangeStatus={handleChangeStatus}
                                  maxFiles={1}
                                  multiple={false}
                                  accept={".mp4, .pdf"}
                                  inputContent="Drop files or click to upload."
                                  styles={{
                                    // dropzone: { width: "100%", height: 25 },
                                    dropzoneActive: { borderColor: "green" },
                                  }}
                                />
                                <Button
                                  color="primary"
                                  type="button"
                                  className="mr-1"
                                  disabled={upload ? false : true}
                                  onClick={handleUpload}
                                >
                                  Upload
                                </Button>
                              </div>
                            )}
                            <div className="file_Message_SaveButton">
                              <Button
                                color="primary"
                                type="button"
                                className="mr-1"
                                disabled={
                                  message || (upload && uploadedFileName)
                                    ? false
                                    : true
                                }
                                onClick={handleFileSave}
                              >
                                Save
                              </Button>
                            </div>
                          </FormGroup>
                          <FormGroup>
                            <Input
                              className="form-control"
                              id="validationCustom01"
                              type="textarea"
                              value={fileData}
                              placeholder="Show input details"
                              readOnly={true}
                              // onChange={(e) => setMessage(e.target.value)}
                            />
                          </FormGroup>
                          <div className="before_submit_style">
                            * Please save file before submit
                          </div>
                          <div className="CardFooterMenus">
                            <Button
                              color="primary"
                              type="button"
                              className="mr-1"
                              onClick={handleSubmit}
                            >
                              Submit
                            </Button>
                            <Button
                              color="light"
                              type="button"
                              onClick={handleClose}
                            >
                              Cancel
                            </Button>
                          </div>
                        </>
                      )}
                      {responseType.length <= 0 && (
                        <div>
                          <Button
                            color="light"
                            type="button"
                            onClick={handleClose}
                          >
                            Cancel
                          </Button>
                        </div>
                      )}
                    </Col>
                  </Row>
                </CardBody>
              </Form>
            </Card>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};
export default MenuModal;
