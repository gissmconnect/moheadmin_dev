import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Add from "@material-ui/icons/Add";
import { toast } from "react-toastify";
import { dataNew } from "./dataNew";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import parse from "html-react-parser";
import {
  getCollege,
  addCollege,
  updateCollege,
  getSubMenu,
  deleteCollege,
} from "../../services/menuService";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";

const Programs = (props) => {
  const history = useHistory();
  const [dataA, setDataA] = useState(dataNew);
  const [dataE, setDataE] = useState([]);
  const [addcountry, setAddCountry] = useState("");
  const [addStream, setAddStream] = useState("");
  const [edit, setEdit] = useState(false);
  const [add, setAdd] = useState(false);
  const [addC, setAddC] = useState(false);

  const [edit1, setEdit1] = useState(false);
  const [add1, setAdd1] = useState(false);
  const [saveData, setSaveData] = useState(false);
  const [addCollege1, setAddCollege1] = useState();
  const [submenuData, setSubmenuData] = useState("");
  const [pagetitle, setPagetitle] = useState("");

  useEffect(() => {
    getCollegeList();
    getFranchiseSchoolsData();
  }, []);

  const getCollegeList = async () => {
    try {
      let res = await getCollege("Outside Sultanate");
      setDataE(res);
    } catch (error) {
      console.log(error);
    }
  };

  const handleAddS = () => {
    setEdit(false);
    setAdd(!add);
    if (add === false) {
      setAddStream("");
    }
  };

  const handleAddCountry = () => {
    setAddC(!addC);
    if (add === false) {
      setAddCountry("");
    }
  };

  const handleAddCountryData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objC = {
      category: "خارج السلطنة",
      type: "sub menu",
      college_name: addCollege1,
      college_type: "",
      program_type: "برنامج القبول المباشر",
      utterance: "",
      country: addcountry,
      unique_title: "Outside Sultanate",
      streams_available: [],
    };
    obj = { ...objC };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    setAddC(!addC);
    setAddCountry("");
    toast.success(
      "College added successfully. Proceed to stream and programs section"
    );
    // console.log(formData, "formData----");
  };

  const handleEditCountryName = () => {
    setAdd(false);
    setEdit(!edit);
  };

  const handleEditCountry = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, country: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditCountry-----------");
  };

  const handleEditCollegeA = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, college_name: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditCollegeA-----------");
  };

  const handleAddCollege = () => {
    Swal.fire({
      title: "Do you want to save ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await addCollege(dataA);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been added successfully.",
              "success"
            );
            window.location.reload(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleDeleteCountry = (index) => {
    // var formData = [...dataE];
    // var array = dataE;
    // array.splice(index, 1);
    // formData = array;
    // setDataE(formData);
  };

  const handleDeleteDirectAdmission = async (item) => {
    let _id = item?._id?.$oid;
    Swal.fire({
      title: "Do you want to Delete Country ?",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Delete`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deleteCollege(_id);
          if (res) {
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  // const handleDeleteStream = (index, indexS) => {
  //   // var formData = [...dataE];
  //   // var array = dataE[index].streams_available;
  //   // array.splice(indexS, 1);
  //   // formData[index].streams_available = array;
  //   // setDataE(formData);
  // };

  // const handleDeleteProgram = (indexS, indexP, index) => {
  //   // var formData = [...dataE];
  //   // var array = dataE[index].streams_available[indexS].program_code;
  //   // array.splice(indexP, 1);
  //   // formData[index].streams_available[indexS].program_code = array;
  //   // setDataE(formData);
  // };

  const handleEditCountryNameList = (item) => {
    setAdd1(false);
    setEdit1(!edit1);
  };

  const handleEditCountry1 = (e, index, item) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, country: e.target.value };
    formData[index] = obj;
    setDataE(formData);
    // console.log(formData, "handleEditCountry1-----------");
  };

  const handleEditCollege1 = (e, index, item) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, college_name: e.target.value };
    formData[index] = obj;
    setDataE(formData);
    // console.log(formData, "handleEditCountry1-----------");
  };

  const handleSaveCollege = async (index, item) => {
    var formData = [...dataE];
    var obj = dataE[index];
    obj = { ...obj, _id: item._id.$oid };
    formData[index] = obj;
    setDataE(formData);
    // console.log(formData, "save-----");
    setSaveData(true);
  };

  const handleUpdateCollege = async (item) => {
    Swal.fire({
      title: "Do you want to update ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await updateCollege(item);
          if (res.status === "done") {
            Swal.fire(
              "Updated!",
              "Your list has been updated successfully.",
              "success"
            );
            window.location.reload(false);
            setAddC(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved");
      }
    });
  };

  const getFranchiseSchoolsData = async () => {
    try {
      let res = await getSubMenu("Direct Admission Programs");
      if (res) {
        setSubmenuData(res);
        setPagetitle(res.category);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleRefreshPage = async () => {
    history.push(`${process.env.PUBLIC_URL}/app/menus/Dubai`);
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title={pagetitle && pagetitle} />
      <Container fluid={true}>
        <div className="edit-profile">
          <div
            style={{
              cursor: "pointer",
              position: "relative",
              zIndex: 2,
              marginBottom: 10,
              marginTop: -15,
            }}
          >
            <Button onClick={handleRefreshPage}>Main Menu</Button>
          </div>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <div className="row">
                    <div
                      className="col-md-12"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        marginBottom: 20,
                        justifyContent: "flex-end",
                      }}
                    >
                      <Tooltip title="Add Country">
                        <IconButton>
                          {!addC ? (
                            <Add onClick={handleAddCountry} />
                          ) : (
                            <Clear onClick={handleAddCountry} />
                          )}
                        </IconButton>
                      </Tooltip>
                    </div>
                  </div>
                  {addC && (
                    <div className="row">
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Country
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Add Country"
                            value={addcountry}
                            onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Program Type
                          </Label>
                          <Input
                            type="text"
                            name="select"
                            style={{ height: 38, padding: "0px 10px" }}
                            className="form-control digits"
                            value={"برنامج القبول المباشر"}
                          />
                          {/* <Label className="col-form-label pt-0">
                            Add Program Type
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Program Type"
                            value={addProgramType}
                            onChange={(e) => setAddProgramType(e.target.value)}
                          /> */}
                        </FormGroup>
                      </div>
                      <div className="col-md-11">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add College
                          </Label>
                          <Input
                            className="form-control"
                            type="textarea"
                            placeholder="College"
                            value={addCollege1}
                            onChange={(e) => setAddCollege1(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddCountryData()}
                          style={{ height: 40, marginTop: 10 }}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )}
                  {dataA &&
                    dataA.map((item, index) => {
                      return (
                        index != 0 && (
                          <div
                            className="accordinContainer accordinContainerStyle"
                            key={index}
                          >
                            <Accordion style={{ marginTop: 10 }}>
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1c-content"
                                id="panel1c-header"
                              >
                                Country : {item.country}
                              </AccordionSummary>
                              <AccordionDetails
                                style={{ flexDirection: "column" }}
                              >
                                <div className="AccordionSummaryHeading">
                                  <div className="AccordionSummaryHeadingActionButtons">
                                    {!edit ? (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Edit Country and Program type
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                  </div>
                                </div>
                                <div>
                                  <div className="row">
                                    {edit && (
                                      <div className="col-md-6">
                                        <FormGroup>
                                          <Label className="col-form-label pt-0">
                                            Edit Country
                                          </Label>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Edit Country"
                                            value={item.country}
                                            onChange={(e) =>
                                              handleEditCountry(e, index)
                                            }
                                          />
                                        </FormGroup>
                                      </div>
                                    )}
                                    <div
                                      className={
                                        edit ? "col-md-6" : "col-md-12"
                                      }
                                    >
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Program Type
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Enter Program Type"
                                          value={item.program_type}
                                          // onChange={(e) =>
                                          //   handleEditProgramType(e, index)
                                          // }
                                        />
                                      </FormGroup>
                                    </div>
                                  </div>
                                </div>
                                {add && (
                                  <FormGroup>
                                    <Label className="col-form-label pt-0">
                                      Add College
                                    </Label>
                                    <div style={{ display: "flex" }}>
                                      <Input
                                        className="form-control"
                                        type="textarea"
                                        placeholder="Enter College"
                                        value={addCollege1}
                                        onChange={(e) =>
                                          setAddCollege1(e.target.value)
                                        }
                                      />
                                      <Button
                                        // onClick={() =>
                                        //   handleAddCollege1(index)
                                        // }
                                        style={{ height: 38 }}
                                      >
                                        Add
                                      </Button>
                                    </div>
                                  </FormGroup>
                                )}
                                <>
                                  {item.college_name && (
                                    <>
                                      <Input
                                        readOnly={edit ? false : true}
                                        className="form-control"
                                        type="textarea"
                                        placeholder="Enter College Name"
                                        value={item.college_name}
                                        onChange={(e) =>
                                          handleEditCollegeA(e, index)
                                        }
                                      />
                                    </>
                                  )}
                                </>
                                <div className="row insideSubmitButton">
                                  <Button onClick={handleAddCollege}>
                                    Submit
                                  </Button>
                                </div>
                              </AccordionDetails>
                            </Accordion>
                            {/* <Tooltip title="Delete Country">
                            <IconButton>
                              <DeleteOutline
                                onClick={() => handleDeleteCountry(index)}
                              />
                            </IconButton>
                          </Tooltip> */}
                          </div>
                        )
                      );
                    })}
                  {/* sbksakcbksabcasbcbsakcbksakcksakcbksackakscbksabkcbsckaskcbksacksabkc */}
                  {dataE &&
                    dataE.map((item, index) => {
                      return (
                        item.program_type === "برنامج القبول المباشر" && (
                          <div className="accordinContainer">
                            <Accordion key={index} style={{ marginTop: 10 }}>
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1c-content"
                                id="panel1c-header"
                              >
                                Country : {item.country}
                              </AccordionSummary>
                              <AccordionDetails
                                style={{ flexDirection: "column" }}
                              >
                                <div className="AccordionSummaryHeading">
                                  <div className="AccordionSummaryHeadingActionButtons">
                                    <Tooltip title="Edit Country and details">
                                      <IconButton>
                                        {edit1 ? (
                                          <Button
                                            onClick={() =>
                                              handleEditCountryNameList(item)
                                            }
                                            style={{ height: 38 }}
                                          >
                                            Save
                                          </Button>
                                        ) : (
                                          <Button
                                            onClick={() =>
                                              handleEditCountryNameList(item)
                                            }
                                            style={{ height: 38 }}
                                          >
                                            Edit
                                          </Button>
                                        )}
                                      </IconButton>
                                    </Tooltip>
                                  </div>
                                </div>
                                <div>
                                  <div className="row">
                                    {edit1 && (
                                      <div className="col-md-6">
                                        <FormGroup>
                                          <Label className="col-form-label pt-0">
                                            Edit Country
                                          </Label>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Edit Country"
                                            value={item.country}
                                            onChange={(e) =>
                                              handleEditCountry1(e, index, item)
                                            }
                                          />
                                        </FormGroup>
                                      </div>
                                    )}
                                    <div
                                      className={
                                        edit1 ? "col-md-6" : "col-md-12"
                                      }
                                    >
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Program Type
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Enter Program Type"
                                          value={item.program_type}
                                          // onChange={(e) =>
                                          //   handleEditProgramType1(e, index)
                                          // }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div className="col-md-12">
                                      <FormGroup>
                                        {item.college_name && edit1 ? (
                                          <>
                                            <Label className="col-form-label pt-0">
                                              Edit College
                                            </Label>
                                            <Input
                                              readOnly={edit1 ? false : true}
                                              className="form-control"
                                              type="textarea"
                                              placeholder="Enter College Name"
                                              value={item.college_name}
                                              onChange={(e) =>
                                                handleEditCollege1(e, index)
                                              }
                                            />
                                          </>
                                        ) : (
                                          item.college_name && (
                                            <>
                                              <Label className="col-form-label pt-0">
                                                College Name
                                              </Label>
                                              <p
                                                style={{
                                                  backgroundColor: "#e9ecef",
                                                  border: "1px solid #ced4da",
                                                  padding: 10,
                                                  borderRadius: 5,
                                                }}
                                              >
                                                {item?.college_name &&
                                                  item.college_name
                                                    .split("\n")
                                                    .map((item, idx) => {
                                                      return (
                                                        <React.Fragment
                                                          key={idx}
                                                        >
                                                          {parse(item)}
                                                          <br />
                                                        </React.Fragment>
                                                      );
                                                    })}
                                              </p>
                                            </>
                                          )
                                        )}
                                      </FormGroup>
                                    </div>
                                  </div>
                                </div>
                                <div style={{ display: "flex" }}>
                                  <Button
                                    onClick={() =>
                                      handleSaveCollege(index, item)
                                    }
                                    style={{
                                      width: 150,
                                      height: 40,
                                      margin: 10,
                                    }}
                                  >
                                    Save
                                  </Button>
                                  <Button
                                    disabled={saveData ? false : true}
                                    onClick={() => handleUpdateCollege(item)}
                                    style={{
                                      width: 150,
                                      height: 40,
                                      margin: 10,
                                    }}
                                  >
                                    Submit
                                  </Button>
                                </div>
                              </AccordionDetails>
                            </Accordion>
                            <div style={{ marginLeft: 10 }}>
                              <Tooltip title="Delete Country">
                                <IconButton>
                                  <DeleteOutline
                                    onClick={() =>
                                      handleDeleteDirectAdmission(item)
                                    }
                                  />
                                </IconButton>
                              </Tooltip>
                            </div>
                          </div>
                        )
                      );
                    })}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
};

export default Programs;
