export const dataNew = [
  {
    category: null,
    type: "sub menu",
    college_name: "",
    college_type: "",
    program_type: null,
    utterance: "",
    country: null,
    streams_available: [
      {
        stream_name: null,
        stream_option:"",
        program_list: [
          {
            program_code: null,
            details: null
          }
        ]
      }
    ]
  }
]
