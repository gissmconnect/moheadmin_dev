export const data = [
  {
    country: "الامارات العربية المتحدة",
    country_option: 1,
    streams_available: [
      {
        stream_name: " العلوم الطبيعية والفيزيائية ",
        stream_option: 1,
        program_code: [{ program_code: "SE900", program_option: 1 }],
      },
      {
        stream_name: "الإدارة والمعاملات التجارية",
        stream_option: 2,
        program_code: [{ program_code: "SE902", program_option: 1 }],
      },
      {
        stream_name: "التربية",
        stream_option: 3,
        program_code: [{ program_code: "SE901", program_option: 1 }],
      },
      {
        stream_name: "الزراعة والبيئة والعلوم المرتبطة به",
        stream_option: 4,
        program_code: [{ program_code: "SE905", program_option: 1 }],
      },
      {
        stream_name: "المجتمع والثقافة",
        stream_option: 5,
        program_code: [{ program_code: "SE903", program_option: 1 }],
      },
      {
        stream_name: "الهندسة والتقنيات ذات الصلة",
        stream_option: 6,
        program_code: [{ program_code: "SE904", program_option: 1 }],
      },
    ],
  },
  {
    country: "البحرين",
    country_option: 2,
    streams_available: [
      {
        stream_name: "الصحة",
        stream_option: 1,
        program_code: [{ program_code: "SE003", program_option: 1 }],
      },
    ],
  },
  {
    country: "المانيا",
    country_option: 3,
    streams_available: [
      {
        stream_name: "الهندسة والتقنيات ذات الصلة",
        stream_option: 1,
        program_code: [{ program_code: "SE508", program_option: 1 }],
      },
    ],
  },
  {
    country: "المملكة\xa0المتحدة",
    country_option: 4,
    streams_available: [
      {
        stream_name: "التربية",
        stream_option: 1,
        program_code: [
          { program_code: "ED007", program_option: 1 },
          { program_code: "ED009", program_option: 2 },
          { program_code: "ED006", program_option: 3 },
          { program_code: "ED008", program_option: 4 },
          { program_code: "ED010", program_option: 5 },
        ],
      },
      {
        stream_name: "الصحة",
        stream_option: 2,
        program_code: [
          { program_code: "SE007", program_option: 1 },
          { program_code: "SE009", program_option: 2 },
          { program_code: "SE012", program_option: 3 },
          { program_code: "SE416", program_option: 4 },
        ],
      },
    ],
  },
  {
    country: "الولايات المتحدة",
    country_option: 5,
    streams_available: [
      {
        stream_name: "الصحة",
        stream_option: 1,
        program_code: [{ program_code: "SE044", program_option: 1 }],
      },
    ],
  },
  {
    country: "جمهورية\xa0ايرلنده",
    country_option: 6,
    streams_available: [
      {
        stream_name: "الصحة",
        stream_option: 1,
        program_code: [
          { program_code: "SE017", program_option: 1 },
          { program_code: "SE008", program_option: 2 },
          { program_code: "SE021", program_option: 3 },
          { program_code: "SE005", program_option: 4 },
        ],
      },
    ],
  },
  {
    country: "دول مختلفة",
    country_option: 7,
    streams_available: [
      {
        stream_name: "غير محدد",
        stream_option: 1,
        program_code: [
          { program_code: "SE390", program_option: 1 },
          { program_code: "SE380", program_option: 2 },
        ],
      },
    ],
  },
  {
    country: "فرنسا",
    country_option: 8,
    streams_available: [
      {
        stream_name: "الهندسة والتقنيات ذات الصلة",
        stream_option: 1,
        program_code: [{ program_code: "SE524", program_option: 1 }],
      },
    ],
  },
];



// import React, {
//     Fragment,
//     useEffect,
//     useState,
//     Component,
//     forwardRef,
//   } from "react";
//   import Breadcrumb from "../../layout/breadcrumb";
//   import {
//     Container,
//     Row,
//     Col,
//     Card,
//     CardHeader,
//     CardBody,
//     CardFooter,
//     Media,
//     Form,
//     FormGroup,
//     Label,
//     Input,
//     Button,
//   } from "reactstrap";
//   import { AddBox, ArrowDownward } from "@material-ui/icons";
//   import MaterialTable, { MTableEditField } from "material-table";
//   import Check from "@material-ui/icons/Check";
//   import ChevronLeft from "@material-ui/icons/ChevronLeft";
//   import ChevronRight from "@material-ui/icons/ChevronRight";
//   import Clear from "@material-ui/icons/Clear";
//   import DeleteOutline from "@material-ui/icons/DeleteOutline";
//   import Edit from "@material-ui/icons/Edit";
//   import Add from "@material-ui/icons/Add";
//   import FilterList from "@material-ui/icons/FilterList";
//   import FirstPage from "@material-ui/icons/FirstPage";
//   import LastPage from "@material-ui/icons/LastPage";
//   import Remove from "@material-ui/icons/Remove";
//   import SaveAlt from "@material-ui/icons/SaveAlt";
//   import Search from "@material-ui/icons/Search";
//   import ViewColumn from "@material-ui/icons/ViewColumn";
//   import { toast } from "react-toastify";
//   import { data } from "./data";
//   import Accordion from "@material-ui/core/Accordion";
//   import AccordionDetails from "@material-ui/core/AccordionDetails";
//   import AccordionSummary from "@material-ui/core/AccordionSummary";
//   import AccordionActions from "@material-ui/core/AccordionActions";
//   import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
//   import Streams from "./streams";
//   import IconButton from "@material-ui/core/IconButton";
//   import Tooltip from "@material-ui/core/Tooltip";
//   import { getCollege, addCollege } from "../../services/menuService";
//   import { dataNew } from "./dataNew";


//   const Programs = (props) => {
//     // const [dataA, setDataA] = useState(data);
//     const [dataE, setDataE] = useState([]);
//     const [addcountry, setAddCountry] = useState("");
//     const [addStream, setAddStream] = useState("");
//     const [edit, setEdit] = useState(false);
//     const [add, setAdd] = useState(false);
//     const [addC, setAddC] = useState(false);
//     const [addProgram, setAddProgram] = useState("");
  

//     const [dataA, setDataA] = useState(dataNew);
//     const [edit1, setEdit1] = useState(false);
//     const [add1, setAdd1] = useState(false);
//     const [addC1, setAddC1] = useState(false);
//     const [addCollegeType1, setAddCollegeType1] = useState("");
//     const [addProgramType1, setAddProgramType1] = useState("");
//     const [addcountry1, setAddCountry1] = useState("");
//     const [addStream1, setAddStream1] = useState("");
//     const [fieldsProgram, setFieldsProgram] = useState([{ value: null }]);
//     const [fieldsStream, setFieldsStream] = useState([{ value: null }]);
//     const [addProgram1, setAddProgram1] = useState("");
//     const [details1, setDetails1] = useState("");





//     useEffect(() => {
//       getCollegeList();
//     }, []);
  
//     const getCollegeList = async () => {
//       try {
//         let res = await getCollege("Inside Sultanate");
//         console.log(res);
//         setDataE(res);
//       } catch (error) {
//         console.log(error);
//       }
//     };
  
//     const handleEditCountryName = () => {
//       setAdd(false);
//       setEdit(!edit);
//     };
//     const handleAddCountry = () => {
//       setAddC(!addC);
//     };
//     const handleAddS = () => {
//       setEdit(false);
//       setAdd(!add);
//     };

//     const handleAddCountry1 = () => {
//       setAddC1(!addC1);
//     };
//     const handleAddS1 = () => {
//       setEdit1(false);
//       setAdd1(!add1);
//     };

//     const handleAddStream = () => {
//       const values = [...fieldsStream];
//       values.push({ value: null });
//       setFieldsStream(values);
//     };
  
//     const handleRemoveStream = (i) => {
//       const values = [...fieldsStream];
//       values.splice(i, 1);
//       setFieldsStream(values);
//     };
  
//     const handleChangeProgram = (i, event) => {
//       const values = [...fieldsProgram];
//       values[i].value = event.target.value;
//       setFieldsStream(values);
//     };
  
//     const handleAddProgram = () => {
//       const values = [...fieldsProgram];
//       values.push({ value: null });
//       setFieldsProgram(values);
//     };
  
//     const handleRemoveProgram = (i) => {
//       const values = [...fieldsProgram];
//       values.splice(i, 1);
//       setFieldsProgram(values);
//     };


  
//     const handleAddCountryData1 = (index) => {
//       console.log(dataA, "dataA----");
//       var formData = [...dataA];
//       var obj = dataA[index];
//       var objC = {
//         category: "Outside Sultanate",
//         type: "sub menu",
//         college_name: "",
//         college_type: "",
//         program_type: addProgramType1,
//         utterance: "",
//         country: addcountry1,
//         streams_available: [],
//       };
//       obj = { ...objC };
//       // obj = { ...obj, streams_available: [...obj.streams_available, objC] };
//       formData[0] = obj;
//       setDataA(formData);
//       console.log(formData, "formData1----");
//     };
  
//     const handleAddStreamData1 = (index) => {
//       console.log(dataA[0].streams_available, addStream1, "dataA------------");
//       var formData = [...dataA];
//       var obj = dataA[index];
//       var objS = {
//           stream_name: addStream1,
//           program_list: [],
//         },
//         obj = { ...obj, streams_available: [...obj.streams_available, objS] };
//       formData[index] = obj;
//       setDataA(formData);
//       // setAddStream("");
//       console.log(formData, "formData----");
//     };
  
//     const handleAddProgramData1 = (idx, index) => {
//       var formData = { ...dataA };
//       console.log(idx, index, "field, index, e-------");
//       var obj =
//         dataA[idx]?.streams_available[index] &&
//         dataA[idx].streams_available[index];
//       console.log(obj, "obj---");
//       var objS = {
//           program_code: addProgram1,
//           details: details1,
//         },
//         obj = { ...obj, program_list: [...obj.program_list, objS] };
//       formData[index].streams_available[index] = obj;
//       setDataA(formData);
//       console.log(formData, "program_code---");
//     };
  
//     const handleAddCollege1 = async () => {
//       try {
//         let res = await addCollege(dataA);
//         console.log(res, "res-----------");
//         if(res.status === "done"){
//           getCollegeList();
//         }
//       } catch (error) {
//         console.log(error);
//       }
//     };












//     const handleAddCountryData = () => {
//       // var formData = [...dataE];
//       // var objC = {
//       //   country: addcountry,
//       //   country_option: dataE.length + 1,
//       //   streams_available: [],
//       // };
//     };
//     const handleEditCountry = (e, countryOption, index) => {
//       // var formData = [...dataE];
//       // var obj = dataE[index];
//       // obj = { ...obj, country: e.target.value };
//       // formData[index] = obj;
//       // setDataE(formData);
//     };
//     const handleDeleteCountry = (index) => {
//       // var formData = [...dataE];
//       // var array = dataE;
//       // array.splice(index, 1);
//       // formData = array;
//       // setDataE(formData);
//     };
  
//     const handleAddStreamData = (index) => {
//       // var formData = [...dataE];
//       // var obj = dataE[index];
//       // var objS = {
//       //     stream_name: addStream,
//       //     stream_option: obj.streams_available.length + 1,
//       //     program_code: [],
//       //   },
//       //   obj = { ...obj, streams_available: [...obj.streams_available, objS] };
//       // formData[index] = obj;
//       // setDataE(formData);
//     };
//     const handleDeleteStream = (index, indexS) => {
//       var formData = [...dataE];
//       var array = dataE[index].streams_available;
//       array.splice(indexS, 1);
//       formData[index].streams_available = array;
//       setDataE(formData);
//     };
//     const handleEditStreamData = (e, stream_option, indexS, index) => {
//       // var formData = [...dataE];
//       // var obj = dataE[index].streams_available[indexS];
//       // obj = { ...obj, stream_name: e.target.value };
//       // formData[index].streams_available[indexS] = obj;
//       // setDataE(formData);
//     };
//     const handleDeleteProgram = (indexS, indexP, index) => {
//       var formData = [...dataE];
//       var array = dataE[index].streams_available[indexS].program_code;
//       array.splice(indexP, 1);
//       formData[index].streams_available[indexS].program_code = array;
//       setDataE(formData);
//     };
//     const handleAddProgramData = (indexS, index) => {
//       // var formData = [...dataE];
//       // var obj = dataE[index].streams_available[indexS];
//       // var objS = {
//       //     program_code: addProgram,
//       //     program_option: obj.program_code.length + 1,
//       //   },
//       //   obj = { ...obj, program_code: [...obj.program_code, objS] };
//       // formData[index].streams_available[indexS] = obj;
//       // setDataE(formData);
//     };
//     const handleEditProgramData = (e, indexS, indexP, index) => {
//       var formData = [...dataE];
//       var obj = dataE[index].streams_available[indexS].program_code[indexP];
//       obj = { ...obj, program_code: e.target.value };
//       formData[index].streams_available[indexS].program_code[indexP] = obj;
//       setDataE(formData);
//       console.log(formData);
//     };
  
//     return (
//       <Fragment>
//         <Breadcrumb parent="Apps" title="Outside Sultanate" />
//         <Container fluid={true}>
//           <div className="edit-profile">
//             <Row>
//               <Col md="12">
//                 <Card>
//                   <CardBody>
//                     {/* <FormGroup
//                       style={{ display: "flex", justifyContent: "flex-end" }}
//                     >
//                       <Tooltip title="Add College">
//                         <IconButton>
//                           {!addC ? (
//                             <Add onClick={handleAddCountry} />
//                           ) : (
//                             <Clear onClick={handleAddCountry} />
//                           )}
//                         </IconButton>
//                       </Tooltip>
//                     </FormGroup> */}
//                     <div className="row">
//                     <div className="col-md-12">
//                       Add Collage :
//                       <Tooltip title="Add">
//                         <IconButton>
//                           <Add onClick={handleAddCountry1} />
//                         </IconButton>
//                       </Tooltip>
//                     </div>
//                     {addC1 && (
//                       <div className="col-md-12">
//                         <div className="row">
//                           <div className="col-md-4">
//                             <FormGroup>
//                               <Label className="col-form-label pt-0">
//                                 Add Country
//                               </Label>
//                               <div style={{ display: "flex" }}>
//                                 <Input
//                                   className="form-control"
//                                   type="text"
//                                   placeholder="Add Country"
//                                   value={addcountry1}
//                                   onChange={(e) =>
//                                     setAddCountry1(e.target.value)
//                                   }
//                                 />
//                               </div>
//                             </FormGroup>
//                           </div>
//                           <div className="col-md-7">
//                             <FormGroup>
//                               <Label className="col-form-label pt-0">
//                                 Add Program Type
//                               </Label>
//                               <Input
//                                 className="form-control"
//                                 type="text"
//                                 placeholder="Program Type"
//                                 value={addProgramType1}
//                                 onChange={(e) =>
//                                   setAddProgramType1(e.target.value)
//                                 }
//                               />
//                             </FormGroup>
//                           </div>
//                           <div className="col-md-1" style={{ display: "flex" }}>
//                             <Tooltip title="Save">
//                               <IconButton>
//                                 <Check
//                                   onClick={() => handleAddCountryData1()}
//                                 />
//                               </IconButton>
//                             </Tooltip>
//                           </div>
//                         </div>
//                         <div className="row">
//                           <div className="col-md-12">
//                             Add Stream :
//                             <Tooltip title="Add">
//                               <IconButton>
//                                 <Add onClick={() => handleAddStream()} />
//                               </IconButton>
//                             </Tooltip>
//                             {fieldsStream.map((field, idx) => {
//                               return (
//                                 <div
//                                   key={`${field}-${idx}`}
//                                   style={{
//                                     borderBottom: "solid 1px #ccc",
//                                     marginTop: 10,
//                                     marginBottom: 10,
//                                     paddingBottom: 10,
//                                   }}
//                                 >
//                                   <div className="row">
//                                     <div className="col-md-11">
//                                       <FormGroup>
//                                         <Label
//                                           className="col-form-label pt-0"
//                                           style={{ marginTop: 10 }}
//                                         >
//                                           Add Stream
//                                         </Label>
//                                         <Input
//                                           className="form-control"
//                                           type="text"
//                                           placeholder="College Type"
//                                           value={addStream1}
//                                           // onChange={(e) => handleChangeStream(idx, e)}
//                                           onChange={(e) =>
//                                             setAddStream1(e.target.value)
//                                           }
//                                         />
//                                       </FormGroup>
//                                     </div>
//                                     <div
//                                       className="col-md-1"
//                                       style={{ display: "flex" }}
//                                     >
//                                       <Tooltip title="Save">
//                                         <IconButton>
//                                           <Check
//                                             onClick={() =>
//                                               handleAddStreamData1(idx)
//                                             }
//                                           />
//                                         </IconButton>
//                                       </Tooltip>
//                                       {/* <Tooltip title="Remove">
//                                         <IconButton>
//                                           <DeleteOutline
//                                             onClick={() =>
//                                               handleRemoveStream(idx)
//                                             }
//                                           />
//                                         </IconButton>
//                                       </Tooltip> */}
//                                     </div>
//                                   </div>
//                                   <div className="row">
//                                     <div className="col-md-12">
//                                       Add Program :
//                                       <Tooltip title="Add">
//                                         <IconButton>
//                                           <Add
//                                             onClick={() => handleAddProgram()}
//                                           />
//                                         </IconButton>
//                                       </Tooltip>
//                                       {fieldsProgram.map((field, index) => {
//                                         return (
//                                           <div
//                                             key={`${field}-${index}`}
//                                             style={{ marginTop: 10 }}
//                                           >
//                                             <div className="row">
//                                               <div className="col-md-4">
//                                                 <FormGroup>
//                                                   <Label className="col-form-label pt-0">
//                                                     Add Program Code
//                                                   </Label>
//                                                   <Input
//                                                     className="form-control"
//                                                     type="text"
//                                                     placeholder="College Type"
//                                                     value={addProgram1}
//                                                     // onChange={(e) => handleChangeProgram(index, e)}
//                                                     onChange={(e) =>
//                                                       setAddProgram1(
//                                                         e.target.value
//                                                       )
//                                                     }
//                                                   />
//                                                 </FormGroup>
//                                               </div>
//                                               <div className="col-md-7">
//                                                 <FormGroup>
//                                                   <Label className="col-form-label pt-0">
//                                                     Add Details
//                                                   </Label>
//                                                   <Input
//                                                     className="form-control"
//                                                     type="text"
//                                                     placeholder="Program Type"
//                                                     value={details1}
//                                                     onChange={(e) =>
//                                                       setDetails1(
//                                                         e.target.value
//                                                       )
//                                                     }
//                                                   />
//                                                 </FormGroup>
//                                               </div>
//                                               <div
//                                                 className="col-md-1"
//                                                 style={{ display: "flex" }}
//                                               >
//                                                 <Tooltip title="Save">
//                                                   <IconButton>
//                                                     <Check
//                                                       onClick={() =>
//                                                         handleAddProgramData1(
//                                                           idx,
//                                                           index
//                                                         )
//                                                       }
//                                                     />
//                                                   </IconButton>
//                                                 </Tooltip>
//                                                 {/* <Tooltip title="Remove">
//                                                   <IconButton>
//                                                     <DeleteOutline
//                                                       onClick={() =>
//                                                         handleRemoveProgram(idx)
//                                                       }
//                                                     />
//                                                   </IconButton>
//                                                 </Tooltip> */}
//                                               </div>
//                                             </div>
//                                           </div>
//                                         );
//                                       })}
//                                     </div>
//                                   </div>
//                                 </div>
//                               );
//                             })}
//                           </div>
//                           <div className="col-md-12">
//                             {dataA &&
//                               dataA.map((item, index) => {
//                                 return (
//                                   <div className="row" key={index}>
//                                     <div className="col-md-6">
//                                       <FormGroup>
//                                         <Label className="col-form-label pt-0">
//                                           Added Country
//                                         </Label>
//                                         <Input
//                                           readOnly
//                                           className="form-control"
//                                           type="text"
//                                           placeholder="Program Type"
//                                           value={item.country}
//                                         />
//                                       </FormGroup>
//                                     </div>
//                                     <div className="col-md-6">
//                                       <FormGroup>
//                                         <Label className="col-form-label pt-0">
//                                           Added program Type
//                                         </Label>
//                                         <Input
//                                           readOnly
//                                           className="form-control"
//                                           type="text"
//                                           placeholder="Program Type"
//                                           value={item.program_type}
//                                         />
//                                       </FormGroup>
//                                     </div>
//                                     {item?.streams_available &&
//                                       item.streams_available.map(
//                                         (itemS, index) => {
//                                           return (
//                                             <>
//                                               <div
//                                                 className="col-md-12"
//                                                 key={index}
//                                               >
//                                                 <FormGroup>
//                                                   <Label className="col-form-label pt-0">
//                                                     Added Stream
//                                                   </Label>
//                                                   <Input
//                                                     readOnly
//                                                     className="form-control"
//                                                     type="text"
//                                                     placeholder="Program Type"
//                                                     value={itemS.stream_name}
//                                                   />
//                                                 </FormGroup>
//                                               </div>
//                                             </>
//                                           );
//                                         }
//                                       )}
//                                   </div>
//                                 );
//                               })}
//                           </div>
//                           <div
//                             className="row insideSubmitButton"
//                             style={{ width: "100%", marginBottom: 20 }}
//                           >
//                             <Button onClick={handleAddCollege1}>Submit</Button>
//                           </div>
//                         </div>
//                       </div>
//                     )}
//                   </div>
//                     {addC && (
//                       <FormGroup>
//                         <Label className="col-form-label pt-0">Add College</Label>
//                         <div style={{ display: "flex" }}>
//                           <Input
//                             className="form-control"
//                             type="text"
//                             placeholder="Add College"
//                             value={addcountry}
//                             onChange={(e) => setAddCountry(e.target.value)}
//                           />
//                           <Tooltip title="Save">
//                             <IconButton>
//                               <Check onClick={() => handleAddCountryData()} />
//                             </IconButton>
//                           </Tooltip>
//                         </div>
//                       </FormGroup>
//                     )}
//                     {dataE &&
//                       dataE.map((item, index) => {
//                         return (
//                           <div className="accordinContainer">
//                             <Accordion key={index} style={{ marginTop: 10 }}>
//                               <AccordionSummary
//                                 expandIcon={<ExpandMoreIcon />}
//                                 aria-controls="panel1c-content"
//                                 id="panel1c-header"
//                               >
//                                 College : {item.college_name}
//                               </AccordionSummary>
//                               <AccordionDetails
//                                 style={{ flexDirection: "column" }}
//                               >
//                                 <div className="AccordionSummaryHeading">
//                                   {/* <div> College : {item.college_name}</div> */}
//                                   <div className="AccordionSummaryHeadingActionButtons">
//                                     <Tooltip title="Edit College and details">
//                                       <IconButton>
//                                         {!edit ? (
//                                           <Edit onClick={handleEditCountryName} />
//                                         ) : (
//                                           <Clear onClick={handleEditCountryName} />
//                                         )}
//                                       </IconButton>
//                                     </Tooltip>
//                                     <Tooltip title="Add Stream">
//                                       <IconButton>
//                                         {!add ? (
//                                           <Add onClick={handleAddS} />
//                                         ) : (
//                                           <Clear onClick={handleAddS} />
//                                         )}
//                                       </IconButton>
//                                     </Tooltip>
//                                   </div>
//                                 </div>
//                                 <div>
//                                   {edit && (
//                                     <FormGroup>
//                                       <Label className="col-form-label pt-0">
//                                         Edit College
//                                       </Label>
//                                       <Input
//                                         className="form-control"
//                                         type="text"
//                                         placeholder="Edit College"
//                                         value={item.college_name}
//                                         onChange={(e) =>
//                                           handleEditCountry(
//                                             e,
//                                             item.country_option,
//                                             index
//                                           )
//                                         }
//                                       />
//                                     </FormGroup>
//                                   )}
//                                   <div className="row">
//                                     <div className="col-md-6">
//                                       <FormGroup>
//                                         <Label className="col-form-label pt-0">
//                                           College Type
//                                         </Label>
//                                         <Input
//                                           readOnly={edit ? false : true}
//                                           className="form-control"
//                                           type="text"
//                                           placeholder="Enter College Type"
//                                           value={item.college_type}
//                                           onChange={(e) =>
//                                             handleEditCountry(
//                                               e,
//                                               item.country_option,
//                                               index
//                                             )
//                                           }
//                                         />
//                                       </FormGroup>
//                                     </div>
//                                     <div className="col-md-6">
//                                       <FormGroup>
//                                         <Label className="col-form-label pt-0">
//                                           Program Type
//                                         </Label>
//                                         <Input
//                                           readOnly={edit ? false : true}
//                                           className="form-control"
//                                           type="text"
//                                           placeholder="Enter Program Type"
//                                           value={item.program_type}
//                                           onChange={(e) =>
//                                             handleEditCountry(
//                                               e,
//                                               item.country_option,
//                                               index
//                                             )
//                                           }
//                                         />
//                                       </FormGroup>
//                                     </div>
//                                   </div>
//                                 </div>
//                                 {add && (
//                                   <FormGroup>
//                                     <Label className="col-form-label pt-0">
//                                       Add Stream
//                                     </Label>
//                                     <div style={{ display: "flex" }}>
//                                       <Input
//                                         className="form-control"
//                                         type="text"
//                                         placeholder="Enter Stream"
//                                         value={addStream}
//                                         onChange={(e) =>
//                                           setAddStream(e.target.value)
//                                         }
//                                       />
//                                       <Tooltip title="Save">
//                                         <IconButton>
//                                           <Check
//                                             onClick={() =>
//                                               handleAddStreamData(index)
//                                             }
//                                           />
//                                         </IconButton>
//                                       </Tooltip>
//                                     </div>
//                                   </FormGroup>
//                                 )}
//                                 {item.streams_available.map((itemS, indexS) => {
//                                   return (
//                                     <div className="accordinContainer">
//                                       <Streams
//                                         itemS={itemS}
//                                         indexS={indexS}
//                                         index={index}
//                                         handleEditStreamData={
//                                           handleEditStreamData
//                                         }
//                                         handleDeleteProgram={handleDeleteProgram}
//                                         handleAddProgramData={
//                                           handleAddProgramData
//                                         }
//                                         handleEditProgramData={
//                                           handleEditProgramData
//                                         }
//                                         setAddProgram={setAddProgram}
//                                         addProgram={addProgram}
//                                         handleAddS={handleAddS}
//                                       />
//                                       {/* <Tooltip title="Delete Stream">
//                                         <IconButton>
//                                           <DeleteOutline
//                                             onClick={() =>
//                                               handleDeleteStream(index, indexS)
//                                             }
//                                           />
//                                         </IconButton>
//                                       </Tooltip> */}
//                                     </div>
//                                   );
//                                 })}
//                               </AccordionDetails>
//                             </Accordion>
//                             {/* <Tooltip title="Delete Country">
//                               <IconButton>
//                                 <DeleteOutline
//                                   onClick={() => handleDeleteCountry(index)}
//                                 />
//                               </IconButton>
//                             </Tooltip> */}
//                           </div>
//                         );
//                       })}
//                   </CardBody>
//                 </Card>
//               </Col>
//             </Row>
//           </div>
//         </Container>
//       </Fragment>
//     );
//   };
  
//   export default Programs;
  