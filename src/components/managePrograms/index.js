import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import Add from "@material-ui/icons/Add";
import { toast } from "react-toastify";
import { dataNew } from "./dataNew";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Streams from "./streams";
import StreamsAdd from "./streamsAdd";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import CloudDownloadIcon from "@material-ui/icons/CloudDownload";
import parse from "html-react-parser";
import NewModal from "./modal";
import {
  getCollege,
  addCollege,
  updateCollege,
  getSubMenu,
  deleteCollege,
} from "../../services/menuService";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";

const Programs = (props) => {
  const history = useHistory();
  const [dataA, setDataA] = useState(dataNew);
  const [dataE, setDataE] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [addcountry, setAddCountry] = useState("");
  const [addStream, setAddStream] = useState("");
  const [edit, setEdit] = useState(false);
  const [add, setAdd] = useState(false);
  const [addC, setAddC] = useState(false);
  const [addProgram, setAddProgram] = useState("");
  const [details, setDetails] = useState("");
  const [addCollegeType, setAddCollegeType] = useState("");
  const [addProgramType, setAddProgramType] = useState("");
  const [newStream, setNewStream] = useState("");

  const [edit1, setEdit1] = useState(false);
  const [add1, setAdd1] = useState(false);
  const [saveData, setSaveData] = useState(false);
  const [sorting, setSorting] = useState("");
  const [addCollege1, setAddCollege1] = useState();
  const [submenuData, setSubmenuData] = useState("");
  const [pagetitle, setPagetitle] = useState("");
  const [tableLoader, setTableLoader] = useState(false);

  useEffect(() => {
    getCollegeList();
    getFranchiseSchoolsData();
  }, []);

  const getCollegeList = async () => {
    try {
      let res = await getCollege("Outside Sultanate");
      setDataE(res);
    } catch (error) {
      console.log(error);
    }
  };

  const handleAddS = () => {
    setEdit(false);
    setAdd(!add);
    if (add === false) {
      setAddStream("");
    }
  };

  const handleAddCountry = () => {
    setAddC(!addC);
    if (add === false) {
      setAddCountry("");
      setAddCollegeType("");
      setAddProgramType("");
    }
  };

  const handleAddCountryData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objC = {
      category: "خارج السلطنة",
      type: "sub menu",
      college_name: addCollege1,
      college_type: "",
      unique_title: "Outside Sultanate",
      program_type: addProgramType,
      utterance: "",
      country: addcountry,
      streams_available: [],
    };
    obj = { ...objC };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    setAddC(!addC);
    setAddCountry("");
    setAddProgramType("");
    toast.success(
      "College added successfully. Proceed to stream and programs section"
    );
    // console.log(formData, "formData----");
  };

  const handleAddStreamData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objS = {
        stream_name: addStream,
        program_list: [],
      },
      obj = { ...obj, streams_available: [...obj.streams_available, objS] };
    formData[index] = obj;
    setDataA(formData);
    setAddStream("");
    toast.success("Stream added successfully. Proceed to programs section");
    // console.log(formData, "formData----");
  };

  const handleAddProgramData = (indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS];
    // console.log(obj, "obj---");
    var objS = {
        program_code: addProgram,
        details: details,
      },
      obj = { ...obj, program_list: [...obj.program_list, objS] };
    formData[index].streams_available[indexS] = obj;
    setDataA(formData);
    setAddProgram("");
    setDetails("");
    toast.success("Programs added successfully. Click Submit to add on list");
    // console.log(formData, "program_code---");
  };

  const handleEditCountryName = () => {
    setAdd(false);
    setEdit(!edit);
  };

  const handleEditCountry = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, country: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditCountry-----------");
  };

  const handleEditProgramType = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, program_type: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramType-----------");
  };

  const handleEditCollegeA = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, college_name: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramType-----------");
  };

  const handleEditStreamData = (e, stream_option, indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS];
    obj = { ...obj, stream_name: e.target.value };
    formData[index].streams_available[indexS] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditStreamData-----------");
  };

  const handleEditProgramData = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, program_code: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramData-----------");
  };

  const handleEditProgramDetails = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, details: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramDetails-----------");
  };

  const handleAddCollege = () => {
    Swal.fire({
      title: "Do you want to save ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await addCollege(dataA);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been added successfully.",
              "success"
            );
            window.location.reload(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  // const handleDeleteCountry = (index) => {
  //   var formData = [...dataE];
  //   var array = dataE;
  //   array.splice(index, 1);
  //   formData = array;
  //   setDataE(formData);
  // };

  const handleDeleteStream = (index, indexS) => {
    Swal.fire({
      title: "Do you want to Remove Stream ?",
      text: "Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...newData1];
        var array = newData1[index].streams_available;
        array.splice(indexS, 1);
        formData[index].streams_available = array;
        setNewData1(formData);
        // console.log(formData, "formData");
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleDeleteProgram = (indexS, indexP, index) => {
    Swal.fire({
      title: "Do you want to Remove Program ?",
      text: "Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...newData1];
        var array = newData1[index].streams_available[indexS].program_list;
        array.splice(indexP, 1);
        formData[index].streams_available[indexS].program_list = array;
        setNewData1(formData);
        // console.log(formData, "formData");
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  var updatedData =
    dataE &&
    dataE.map((item, index) => {
      return item.program_type;
    });
  const uniqueData = [...new Set(updatedData)];

  const [newData1, setNewData1] = useState([]);

  const newData =
    dataE &&
    dataE.filter((item) => item.program_type === sorting).map((item) => item);

  useEffect(() => {
    setNewData1(newData);
  }, [sorting]);

  const handleEditCountryNameList = (item) => {
    setAdd1(false);
    setEdit1(!edit1);
  };

  const handleEditCountry1 = (e, index, item) => {
    var formData = [...newData];
    var obj = newData[index];
    obj = { ...obj, country: e.target.value };
    formData[index] = obj;
    setNewData1(formData);
    // console.log(formData, "handleEditCountry1-----------");
  };

  const handleEditProgramType1 = (e, index) => {
    var formData = [...newData1];
    var obj = newData1[index];
    obj = { ...obj, program_type: e.target.value };
    formData[index] = obj;
    setNewData1(formData);
    // console.log(formData, "handleEditProgramType1-----------");
  };

  const handleEditCollege1 = (e, index, item) => {
    var formData = [...newData];
    var obj = newData[index];
    obj = { ...obj, college_name: e.target.value };
    formData[index] = obj;
    setNewData1(formData);
    // console.log(formData, "handleEditCountry1-----------");
  };

  const handleEditStreamData1 = (e, stream_option, indexS, index) => {
    var formData = [...newData1];
    var obj = newData1[index].streams_available[indexS];
    obj = { ...obj, stream_name: e.target.value };
    formData[index].streams_available[indexS] = obj;
    setNewData1(formData);
  };

  const handleEditProgramData1 = (e, indexS, indexP, index) => {
    var formData = [...newData1];
    var obj = newData1[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, program_code: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setNewData1(formData);
    // console.log(formData);
  };

  const handleEditProgramDetails1 = (e, indexS, indexP, index) => {
    var formData = [...newData1];
    var obj = newData1[index].streams_available[indexS].program_list[indexP];
    obj = { ...obj, details: e.target.value };
    formData[index].streams_available[indexS].program_list[indexP] = obj;
    setNewData1(formData);
    // console.log(formData);
  };

  const handleNewStreamData = (index) => {
    var formData = [...newData1];
    var obj = newData1[index];
    var objS = {
        stream_name: newStream,
        program_list: [],
      },
      obj = { ...obj, streams_available: [...obj.streams_available, objS] };
    formData[index] = obj;
    setNewData1(formData);
    setNewStream("");
    toast.success("Stream saved. Proceed to program section");
    // console.log(formData, "formData----stream");
  };

  const handleNewProgramData = (indexS, index) => {
    var formData = [...newData1];
    var obj = newData1[index].streams_available[indexS];
    // console.log(obj, "obj---");
    var objS = {
        program_code: addProgram,
        details: details,
      },
      obj = { ...obj, program_list: [...obj.program_list, objS] };
    formData[index].streams_available[indexS] = obj;
    setNewData1(formData);
    setAddProgram("");
    setDetails("");
    toast.success("Program saved. Click submit to update");
    // console.log(formData, "program_code---");
  };

  const handleSaveCollege = async (index, item) => {
    var formData = [...newData1];
    var obj = newData1[index];
    obj = { ...obj, _id: item._id.$oid };
    formData[index] = obj;
    setNewData1(formData);
    // console.log(formData, "save-----");
    setSaveData(true);
  };

  const handleUpdateCollege = async (item) => {
    Swal.fire({
      title: "Do you want to update ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await updateCollege(item);
          if (res.status === "done") {
            Swal.fire(
              "Updated!",
              "Your list has been updated successfully.",
              "success"
            );
            window.location.reload(false);
            setAddC(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved");
      }
    });
  };

  const getFranchiseSchoolsData = async () => {
    try {
      let res = await getSubMenu("Outside Sultanate");
      if (res) {
        setPagetitle(res.category);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleRefreshPage = async () => {
    history.push(`${process.env.PUBLIC_URL}/app/menus/Dubai`);
  };

  const handleDeleteOutsideCollege = async (item) => {
    let _id = item?._id?.$oid;
    Swal.fire({
      title: "Do you want to Delete ?",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Delete`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deleteCollege(_id);
          if (res) {
            getCollegeList();
            window.location.reload(false);
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleUploadFile = () => {
    setOpenModal(true);
  };

  const handleDownloadFile = async () => {
    const headerRow = [
      "COLLEGE NAME",
      "COLLEGE TYPE",
      "PROGRAM TYPE",
      "STREAM",
      "PROGRAM CODE",
      "PROGRAM DETAILS",
      "COUNTRY",
    ];
    var dataRows = [];
    dataE.forEach((item) => {
      var rowData = [
        item?.college_name,
        item?.college_type,
        item?.program_type,
        item?.relation,
        item?.gender,
        item?.relativeEmployeeNumber,
        item?.relativeGovtId,
        item?.firstDoseDate?.split("-").join("/"),
        item?.secondDoseDate?.split("-").join("/"),
      ];
      dataRows.push(rowData);
    });
    const delimiter = ",";
    const csvContent = [headerRow, ...dataRows]
      .map((e) => e.join(delimiter))
      .join("\n");
    const csvFileName = "Outside Sultanate";
    downloadCsv(csvContent, csvFileName);
    setTableLoader(false);
  };
  const downloadCsv = (data, fileName) => {
    const finalFileName = fileName.endsWith(".csv")
      ? fileName
      : `${fileName}.csv`;
    const a = document.createElement("a");
    a.href = URL.createObjectURL(new Blob([data], { type: "text/csv" }));
    a.setAttribute("download", finalFileName);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title={pagetitle && pagetitle} />
      <Container fluid={true}>
        <div className="edit-profile">
          <div
            style={{
              cursor: "pointer",
              position: "relative",
              zIndex: 2,
              marginBottom: 10,
              marginTop: -15,
            }}
          >
            <Button onClick={handleRefreshPage}>Main Menu</Button>
          </div>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <div className="row">
                    <div
                      className="col-md-12"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        marginBottom: 20,
                      }}
                    >
                      <Input
                        type="select"
                        name="select"
                        style={{ height: 50, padding: 10 }}
                        className="form-control digits"
                        value={sorting}
                        onChange={(e) => {
                          setSorting(e.target.value);
                        }}
                      >
                        <option value={"{}"}>Please select program type</option>
                        {uniqueData &&
                          uniqueData.map((item, index) => {
                            return (
                              <option key={index} value={item}>
                                {item}
                              </option>
                            );
                          })}
                      </Input>
                      {/* <Tooltip title="Download Excel">
                        <IconButton>
                          <CloudDownloadIcon
                          onClick={handleDownloadFile}
                          />
                        </IconButton>
                      </Tooltip> */}
                      <Tooltip title="Upload Excel">
                        <IconButton>
                          <CloudUploadIcon onClick={handleUploadFile} />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Add Country">
                        <IconButton>
                          {!addC ? (
                            <Add onClick={handleAddCountry} />
                          ) : (
                            <Clear onClick={handleAddCountry} />
                          )}
                        </IconButton>
                      </Tooltip>
                    </div>
                  </div>
                  {/* {addC && (
                    <div className="row">
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Country
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Add College"
                            value={addcountry}
                            onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div className="col-md-5">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Program Type
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Program Type"
                            value={addProgramType}
                            onChange={(e) => setAddProgramType(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddCountryData()}
                          style={{ height: 40, marginTop: 10 }}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )} */}
                  {addC && (
                    <div className="row">
                      <div className="col-md-12">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Country
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Add Country"
                            value={addcountry}
                            onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Program Type
                          </Label>
                          <Input
                            type="select"
                            name="select"
                            style={{ height: 38, padding: "0px 10px" }}
                            className="form-control digits"
                            value={addProgramType}
                            onChange={(e) => {
                              setAddProgramType(e.target.value);
                            }}
                          >
                            <option value={"{}"}>
                              Please select program type
                            </option>
                            {uniqueData &&
                              uniqueData.map((item, index) => {
                                return (
                                  <option key={index} value={item}>
                                    {item}
                                  </option>
                                );
                              })}
                          </Input>

                          {/* <Label className="col-form-label pt-0">
                            Add Program Type
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Program Type"
                            value={addProgramType}
                            onChange={(e) => setAddProgramType(e.target.value)}
                          /> */}
                        </FormGroup>
                      </div>
                      <div
                        className={
                          addProgramType === "برنامج القبول المباشر"
                            ? "col-md-6"
                            : "col-md-5"
                        }
                      >
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Unique Title
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Add Country"
                            value="Outside Sultanate"
                            readOnly={true}
                            // onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      {addProgramType === "برنامج القبول المباشر" && (
                        <div className="col-md-11">
                          <FormGroup>
                            <Label className="col-form-label pt-0">
                              Add College
                            </Label>
                            <Input
                              className="form-control"
                              type="textarea"
                              placeholder="College"
                              value={addCollege1}
                              onChange={(e) => setAddCollege1(e.target.value)}
                            />
                          </FormGroup>
                        </div>
                      )}
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddCountryData()}
                          style={{ height: 40, marginTop: 10 }}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )}
                  {dataA &&
                    dataA.map((item, index) => {
                      return (
                        index != 0 && (
                          <div
                            className="accordinContainer accordinContainerStyle"
                            key={index}
                          >
                            <Accordion style={{ marginTop: 10 }}>
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1c-content"
                                id="panel1c-header"
                              >
                                Country : {item.country}
                              </AccordionSummary>
                              <AccordionDetails
                                style={{ flexDirection: "column" }}
                              >
                                <div className="AccordionSummaryHeading">
                                  <div className="AccordionSummaryHeadingActionButtons">
                                    {!edit ? (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Edit Country and Program type
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                    {/* <Tooltip title="Edit College and details">
                                      <IconButton>
                                        {!edit ? (
                                          <Edit
                                            onClick={handleEditCountryName}
                                          />
                                        ) : (
                                          <Check
                                            onClick={handleEditCountryName}
                                          />
                                        )}
                                      </IconButton>
                                    </Tooltip> */}
                                  </div>
                                </div>
                                <div>
                                  <div className="row">
                                    {edit && (
                                      <div className="col-md-12">
                                        <FormGroup>
                                          <Label className="col-form-label pt-0">
                                            Edit Country
                                          </Label>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Edit Country"
                                            value={item.country}
                                            onChange={(e) =>
                                              handleEditCountry(e, index)
                                            }
                                          />
                                        </FormGroup>
                                      </div>
                                    )}
                                    <div className="col-md-6">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Program Type
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Enter Program Type"
                                          value={item.program_type}
                                          onChange={(e) =>
                                            handleEditProgramType(e, index)
                                          }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div className="col-md-6">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Unique Title
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Enter Program Type"
                                          value="Outside Sultanate"
                                          // onChange={(e) =>
                                          //   handleEditProgramType(e, index)
                                          // }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div
                                      className="col-md-12"
                                      style={{
                                        display: "flex",
                                        justifyContent: "flex-end",
                                      }}
                                    >
                                      {item.program_type ===
                                      "برنامج القبول المباشر" ? null : (
                                        <>
                                          {add ? (
                                            <Button
                                              onClick={handleAddS}
                                              style={{
                                                height: 40,
                                                marginTop: 10,
                                                marginBottom: 10,
                                              }}
                                            >
                                              Remove Stream
                                            </Button>
                                          ) : (
                                            <Button
                                              onClick={handleAddS}
                                              style={{
                                                height: 40,
                                                marginTop: 10,
                                                marginBottom: 10,
                                              }}
                                            >
                                              Add Stream
                                            </Button>
                                          )}
                                        </>
                                      )}
                                      {/* <Tooltip title="Add Stream">
                                        <IconButton>
                                          {!add ? (
                                            <Add onClick={handleAddS} />
                                          ) : (
                                            <Clear onClick={handleAddS} />
                                          )}
                                        </IconButton>
                                      </Tooltip> */}
                                    </div>
                                  </div>
                                </div>
                                {item.program_type === "برنامج القبول المباشر"
                                  ? add && (
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Add College
                                        </Label>
                                        <div style={{ display: "flex" }}>
                                          <Input
                                            className="form-control"
                                            type="textarea"
                                            placeholder="Enter College"
                                            value={addCollege1}
                                            onChange={(e) =>
                                              setAddCollege1(e.target.value)
                                            }
                                          />
                                          <Button
                                            // onClick={() =>
                                            //   handleAddCollege1(index)
                                            // }
                                            style={{ height: 38 }}
                                          >
                                            Add
                                          </Button>
                                        </div>
                                      </FormGroup>
                                    )
                                  : add && (
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Add Stream
                                        </Label>
                                        <div style={{ display: "flex" }}>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Enter Stream"
                                            value={addStream}
                                            onChange={(e) =>
                                              setAddStream(e.target.value)
                                            }
                                          />
                                          <Button
                                            onClick={() =>
                                              handleAddStreamData(index)
                                            }
                                            style={{ height: 38 }}
                                          >
                                            Add
                                          </Button>
                                          {/* <Tooltip title="Save">
                                          <IconButton>
                                            <Check
                                              onClick={() =>
                                                handleAddStreamData(index)
                                              }
                                            />
                                          </IconButton>
                                        </Tooltip> */}
                                        </div>
                                      </FormGroup>
                                    )}
                                {item.program_type ===
                                "برنامج القبول المباشر" ? (
                                  <>
                                    {item.college_name && (
                                      <Input
                                        readOnly={edit ? false : true}
                                        className="form-control"
                                        type="textarea"
                                        placeholder="Enter College"
                                        value={item.college_name}
                                        onChange={(e) =>
                                          handleEditCollegeA(e, index)
                                        }
                                      />
                                    )}
                                  </>
                                ) : (
                                  item.streams_available.map(
                                    (itemS, indexS) => {
                                      return (
                                        <div className="accordinContainer">
                                          <StreamsAdd
                                            itemS={itemS}
                                            indexS={indexS}
                                            index={index}
                                            handleEditProgramDetails={
                                              handleEditProgramDetails
                                            }
                                            handleEditStreamData={
                                              handleEditStreamData
                                            }
                                            handleDeleteProgram={
                                              handleDeleteProgram
                                            }
                                            handleAddProgramData={
                                              handleAddProgramData
                                            }
                                            handleEditProgramData={
                                              handleEditProgramData
                                            }
                                            setAddProgram={setAddProgram}
                                            addProgram={addProgram}
                                            handleAddS={handleAddS}
                                            setDetails={setDetails}
                                            details={details}
                                          />
                                          {/* <Tooltip title="Delete Stream">
                                            <IconButton>
                                              <DeleteOutline
                                                onClick={() =>
                                                  handleDeleteStream(
                                                    index,
                                                    indexS
                                                  )
                                                }
                                              />
                                            </IconButton>
                                          </Tooltip> */}
                                        </div>
                                      );
                                    }
                                  )
                                )}

                                <div className="row insideSubmitButton">
                                  <Button onClick={handleAddCollege}>
                                    Submit
                                  </Button>
                                </div>
                              </AccordionDetails>
                            </Accordion>
                            {/* <Tooltip title="Delete Country">
                            <IconButton>
                              <DeleteOutline
                                onClick={() => handleDeleteCountry(index)}
                              />
                            </IconButton>
                          </Tooltip> */}
                          </div>
                        )
                      );
                    })}
                  {/* sbksakcbksabcasbcbsakcbksakcksakcbksackakscbksabkcbsckaskcbksacksabkc */}
                  {newData1 &&
                    newData1.map((item, index) => {
                      return (
                        <div className="accordinContainer">
                          <Accordion key={index} style={{ marginTop: 10 }}>
                            <AccordionSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1c-content"
                              id="panel1c-header"
                            >
                              Country : {item.country}
                            </AccordionSummary>
                            <AccordionDetails
                              style={{ flexDirection: "column" }}
                            >
                              <div className="AccordionSummaryHeading">
                                <div className="AccordionSummaryHeadingActionButtons">
                                  <Tooltip title="Edit Country and details">
                                    <IconButton>
                                      {edit1 ? (
                                        <Button
                                          onClick={() =>
                                            handleEditCountryNameList(item)
                                          }
                                          style={{ height: 38 }}
                                        >
                                          Save
                                        </Button>
                                      ) : (
                                        <Button
                                          onClick={() =>
                                            handleEditCountryNameList(item)
                                          }
                                          style={{ height: 38 }}
                                        >
                                          Edit
                                        </Button>
                                      )}
                                    </IconButton>
                                  </Tooltip>
                                  <Tooltip title="Add Stream">
                                    {!add1 ? (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Add Stream
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Remove
                                      </Button>
                                    )}
                                  </Tooltip>
                                </div>
                              </div>
                              <div>
                                <div className="row">
                                  {edit1 && (
                                    <div className="col-md-12">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Edit Country
                                        </Label>
                                        <Input
                                          className="form-control"
                                          type="text"
                                          placeholder="Edit Country"
                                          value={item.country}
                                          onChange={(e) =>
                                            handleEditCountry1(e, index, item)
                                          }
                                        />
                                      </FormGroup>
                                    </div>
                                  )}
                                  <div className="col-md-6">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Program Type
                                      </Label>
                                      <Input
                                        readOnly={true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Enter Program Type"
                                        value={item.program_type}
                                        onChange={(e) =>
                                          handleEditProgramType1(e, index)
                                        }
                                      />
                                    </FormGroup>
                                  </div>
                                  <div className="col-md-6">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Unique Title
                                      </Label>
                                      <Input
                                        readOnly={true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Unique Title"
                                        value={item.unique_title}
                                        // onChange={(e) =>
                                        //   handleEditProgramType1(e, index)
                                        // }
                                      />
                                    </FormGroup>
                                  </div>
                                  <div className="col-md-12">
                                    <FormGroup>
                                      {item.college_name && edit1 ? (
                                        <>
                                          <Label className="col-form-label pt-0">
                                            Edit College
                                          </Label>
                                          <Input
                                            readOnly={edit1 ? false : true}
                                            className="form-control"
                                            type="textarea"
                                            placeholder="Enter College Name"
                                            value={item.college_name}
                                            onChange={(e) =>
                                              handleEditCollege1(e, index)
                                            }
                                          />
                                        </>
                                      ) : (
                                        item.college_name && (
                                          <>
                                            <Label className="col-form-label pt-0">
                                              College Name
                                            </Label>
                                            <p
                                              style={{
                                                backgroundColor: "#e9ecef",
                                                border: "1px solid #ced4da",
                                                padding: 10,
                                                borderRadius: 5,
                                              }}
                                            >
                                              {item?.college_name &&
                                                item.college_name
                                                  .split("\n")
                                                  .map((item, idx) => {
                                                    return (
                                                      <React.Fragment key={idx}>
                                                        {parse(item)}
                                                        <br />
                                                      </React.Fragment>
                                                    );
                                                  })}
                                            </p>
                                          </>
                                        )
                                      )}
                                    </FormGroup>
                                  </div>
                                </div>
                              </div>
                              {add && (
                                <FormGroup>
                                  <Label className="col-form-label pt-0">
                                    Add Stream
                                  </Label>
                                  <div style={{ display: "flex" }}>
                                    <Input
                                      className="form-control"
                                      type="text"
                                      placeholder="Enter Stream"
                                      value={newStream}
                                      onChange={(e) =>
                                        setNewStream(e.target.value)
                                      }
                                    />
                                    <Button
                                      onClick={() => handleNewStreamData(index)}
                                      style={{ height: 38 }}
                                    >
                                      Add
                                    </Button>
                                  </div>
                                </FormGroup>
                              )}
                              {item?.streams_available &&
                                item.streams_available.map((itemS, indexS) => {
                                  return (
                                    <div className="accordinContainer">
                                      <Streams
                                        itemS={itemS}
                                        indexS={indexS}
                                        index={index}
                                        handleEditStreamData1={
                                          handleEditStreamData1
                                        }
                                        handleDeleteProgram={
                                          handleDeleteProgram
                                        }
                                        handleEditProgramData1={
                                          handleEditProgramData1
                                        }
                                        handleEditProgramDetails1={
                                          handleEditProgramDetails1
                                        }
                                        setAddProgram={setAddProgram}
                                        addProgram={addProgram}
                                        handleAddS={handleAddS}
                                        setDetails={setDetails}
                                        details={details}
                                        sorting={sorting}
                                        handleNewProgramData={
                                          handleNewProgramData
                                        }
                                      />
                                      <Tooltip title="Delete Stream">
                                        <IconButton>
                                          <DeleteOutline
                                            onClick={() =>
                                              handleDeleteStream(index, indexS)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip>
                                    </div>
                                  );
                                })}
                              <div style={{ display: "flex" }}>
                                <Button
                                  onClick={() => handleSaveCollege(index, item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Save
                                </Button>
                                <Button
                                  disabled={saveData ? false : true}
                                  onClick={() => handleUpdateCollege(item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Submit
                                </Button>
                              </div>
                            </AccordionDetails>
                          </Accordion>
                          <div style={{ marginLeft: 10 }}>
                            <Tooltip title="Delete Country">
                              <IconButton>
                                <DeleteOutline
                                  onClick={() =>
                                    handleDeleteOutsideCollege(item)
                                  }
                                />
                              </IconButton>
                            </Tooltip>
                          </div>
                        </div>
                      );
                    })}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
      <NewModal open={openModal} setOpen={setOpenModal} />
    </Fragment>
  );
};

export default Programs;
