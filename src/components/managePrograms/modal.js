import React, { useEffect, Fragment, useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col,
  Card,
  Badge,
  CardHeader,
  CardBody,
  CardFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { alpha, makeStyles } from "@material-ui/core/styles";
import Dropzone from "react-dropzone-uploader";
import { X } from "react-feather";
import { uploadFileExcle, submitExcleOutside } from "../../services/menuService";

const NewModal = (props) => {
  const { open, setOpen, getCollegeData } = props;
  const [fileName, setFileName] = useState();
  const [upload, setUpload] = useState(false);
  const [result, setResult] = useState();

  const handleClose = () => {
    setOpen(false);
    setFileName(null);
    setUpload(false);
    setResult(null);
  };

  const getUploadParams = ({ meta }) => {
    return { url: "https://httpbin.org/post" };
  };

  const handleChangeStatus = ({ meta, file }, status) => {
    if (status === "done") {
      setFileName(file);
      setUpload(true);
    } else if (status === "removed") {
      setFileName();
      setUpload(false);
    }
  };

  const handleUpload = async () => {
    try {
      let res = await uploadFileExcle(fileName);
      if (res) {
        setResult(res?.mainUrl);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleSubmit = async () => {
    try {
      let res = await submitExcleOutside(result);
      if (res) {
        handleClose();
        window.location.reload(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  //   curl --location --request POST 'http://2.56.215.239:3010/api/upload/attachement/excel' \
  // --form 'file=@"/home/adutta/Downloads/AllPrograms.xlsx"'
  // curl -X 'POST' \
  //   'http://86.107.197.194:8000/addcolleges_by_excel?excel_url=fewfwefw&category=ewfewfw&unique_title=ewfwefew' \
  //   -H 'accept: application/json' \

  return (
    <Modal isOpen={open} centered className="addIntentStyle">
      <ModalHeader style={{ padding: "20px 30px" }}>Upload File</ModalHeader>
      <ModalBody>
        <Card style={{ marginBottom: 0 }}>
          <Form className="form theme-form fileUploadInside">
            <CardBody style={{ padding: "0px 30px" }}>
              <Dropzone
                getUploadParams={getUploadParams}
                onChangeStatus={handleChangeStatus}
                maxFiles={1}
                multiple={false}
                accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                inputContent="Drop files or click to upload."
                styles={{
                  // dropzone: { width: "100%", height: 25 },
                  dropzoneActive: { borderColor: "green" },
                }}
              />
              <Button
                color="primary"
                type="button"
                className="mr-1"
                disabled={upload ? false : true}
                onClick={handleUpload}
              >
                Upload
              </Button>
            </CardBody>
            <CardFooter style={{ padding: "20px 30px" }}>
              <p style={{ color: "#ff0000", fontSize: 10, fontWeight: 500, marginBottom:0,}}>File must be in XLSX format.</p>
              <p style={{ color: "#ff0000", fontSize: 10, fontWeight: 500}}>After upload file click submit to add on list.</p>
              {result && (
                <Button
                  color="primary"
                  type="button"
                  className="mr-1"
                  onClick={handleSubmit}
                >
                  Submit
                </Button>
              )}
              <Button color="light" type="button" onClick={handleClose}>
                Cancel
              </Button>
            </CardFooter>
          </Form>
        </Card>
      </ModalBody>
    </Modal>
  );
};
export default NewModal;
