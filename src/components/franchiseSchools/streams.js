import React, { useState, useEffect } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Chip from "@material-ui/core/Chip";
import { Row, Col, Button, FormGroup, Label, Input } from "reactstrap";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

export default function Streams(props) {
  const { itemS, indexS, index, handleEditStatesList, handleEditSchoolsList } =
    props;
  const [addS, setAddS] = useState(false);
  const [editS, setEditS] = useState(false);

  const handleEditStream = () => {
    setAddS(false);
    setEditS(!editS);
  };
  const handleAddP = () => {
    setEditS(false);
    setAddS(!addS);
  };

  return (
    <>
      {itemS.state && (
        <Accordion key={indexS} style={{ marginTop: 10 }}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1c-content"
            id="panel1c-header"
          >
            State :{itemS.state !== "Unspecified" ? itemS.state : ""}
          </AccordionSummary>
          <AccordionDetails style={{ flexDirection: "column" }}>
            <FormGroup
              style={{ display: "block", textAlign: "end" }}
            ></FormGroup>

            <div className="AccordionSummaryHeading">
              <div className="AccordionSummaryHeadingActionButtons">
                <Tooltip title="Edit Stream and Program">
                  {!editS ? (
                    <Button onClick={handleEditStream} style={{ height: 38 }}>
                      Edit / Add
                    </Button>
                  ) : (
                    <Button onClick={handleEditStream} style={{ height: 38 }}>
                      Save
                    </Button>
                  )}
                </Tooltip>
                {/* {addS ? (
            <Tooltip title="Add Stream">
              <IconButton>
                <Clear onClick={handleAddP} />
              </IconButton>
            </Tooltip>
          ) : (
            <Tooltip title="Add Stream">
              <IconButton>
                <Add onClick={handleAddP} />
              </IconButton>
            </Tooltip>
          )} */}
              </div>
            </div>
            {editS && (
              <FormGroup>
                <Label className="col-form-label pt-0">Edit State</Label>
                <Input
                  className="form-control"
                  type="text"
                  placeholder="Enter State"
                  value={itemS.state}
                  onChange={(e) =>
                    handleEditStatesList(e, itemS.stream_option, indexS, index)
                  }
                />
              </FormGroup>
            )}
            <div>
              <FormGroup>
                <Label className="col-form-label pt-0">Schools</Label>
                <Input
                  readOnly={editS ? false : true}
                  className="form-control"
                  type="textarea"
                  style={{ minHeight: 100 }}
                  placeholder="Enter Schools"
                  value={itemS.schools}
                  onChange={(e) =>
                    handleEditSchoolsList(e, itemS.stream_option, indexS, index)
                  }
                />
              </FormGroup>
            </div>
          </AccordionDetails>
        </Accordion>
      )}
    </>
  );
}
