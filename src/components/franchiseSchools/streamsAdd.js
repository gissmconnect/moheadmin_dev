import React, { useState, useEffect } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Chip from "@material-ui/core/Chip";
import { Row, Col, Button, FormGroup, Label, Input } from "reactstrap";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

export default function StreamsAdd(props) {
  const {
    itemS,
    indexS,
    index,
    handleAddEditState,
    handleAddEditSchool,
    handleAddProgramData,
    setSchools,
    schools,
  } = props;
  const [addS, setAddS] = useState(false);
  const [editS, setEditS] = useState(false);

  const handleEditStream = () => {
    setAddS(false);
    setEditS(!editS);
  };
  const handleAddP = () => {
    setEditS(false);
    setAddS(!addS);
    if (addS === false) {
      setSchools("");
    }
  };
  return (
    <>
      <Accordion key={indexS} style={{ marginTop: 10 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          State : {itemS.state}
        </AccordionSummary>
        <AccordionDetails style={{ flexDirection: "column" }}>
          <FormGroup style={{ display: "block", textAlign: "end" }}></FormGroup>

          <div className="AccordionSummaryHeading">
            <div className="AccordionSummaryHeadingActionButtons">
              {!editS ? (
                <Button
                  onClick={handleEditStream}
                  style={{ height: 40, margin: 10 }}
                >
                  Edit State and Schools
                </Button>
              ) : (
                <Button
                  onClick={handleEditStream}
                  style={{ height: 40, margin: 10 }}
                >
                  Save
                </Button>
              )}
            </div>
          </div>
          {editS && (
            <FormGroup>
              <Label className="col-form-label pt-0">state</Label>
              <Input
                className="form-control"
                type="text"
                placeholder="Enter Stream"
                value={itemS.state}
                onChange={(e) =>
                  handleAddEditState(e, itemS.stream_option, indexS, index)
                }
              />
            </FormGroup>
          )}
          <div className="row">
            <div className="col-md-12">
              <FormGroup>
                <Label className="col-form-label pt-0">Schools</Label>
                <Input
                  readOnly={editS ? false : true}
                  style={{ minHeight: 100 }}
                  className="form-control"
                  type="textarea"
                  placeholder="Enter Schools"
                  value={itemS.schools}
                  onChange={(e) =>
                    handleAddEditSchool(e, itemS.stream_option, indexS, index)
                  }
                />
              </FormGroup>
            </div>
          </div>
          {addS && (
            <FormGroup>
              <div className="row" style={{ display: "flex" }}>
                <div className="col-md-12">
                  <Label className="col-form-label pt-0 mt-2">
                    Add Schools
                  </Label>
                  <Input
                    className="form-control"
                    type="textarea"
                    placeholder="Enter Schools"
                    value={schools}
                    onChange={(e) => setSchools(e.target.value)}
                    style={{ minHeight: 150, marginTop: 10 }}
                  />
                </div>
                <div className="col-md-1">
                  <div style={{ display: "flex" }}>
                    <Button
                      onClick={() => handleAddProgramData(indexS, index)}
                      style={{ height: 40, marginTop: 10, marginBottom: 10 }}
                    >
                      Add
                    </Button>
                  </div>
                </div>
              </div>
            </FormGroup>
          )}
        </AccordionDetails>
      </Accordion>
    </>
  );
}
