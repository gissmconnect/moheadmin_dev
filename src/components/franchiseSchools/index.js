import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import Add from "@material-ui/icons/Add";
import { toast } from "react-toastify";
import { dataNew } from "./dataNew";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Streams from "./streams";
import StreamsAdd from "./streamsAdd";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import {
  getCollege,
  addCollege,
  updateCollege,
  getFranchiseSchools,
  updateFranchiseSchools,
  addFranchiseSchools,
  addSubmenusFranchiseSchools,
  getSubMenu,
  deleteFranchiseSubmenus,
  deletePrefecture,
} from "../../services/menuService";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";

const Programs = (props) => {
  const history = useHistory();
  const [dataA, setDataA] = useState(dataNew);
  const [dataE, setDataE] = useState([]);
  const [addPrefecture, setAddPrefecture] = useState("");
  const [addArea, setAddArea] = useState("");
  const [addState, setAddState] = useState("");
  const [schools, setSchools] = useState("");

  const [edit, setEdit] = useState(false);
  const [add, setAdd] = useState(false);
  const [addC, setAddC] = useState(false);
  const [addProgram, setAddProgram] = useState("");
  const [details, setDetails] = useState("");
  const [addCollegeType, setAddCollegeType] = useState("");
  const [addProgramType, setAddProgramType] = useState("");
  const [newStream, setNewStream] = useState("");

  const [edit1, setEdit1] = useState(false);
  const [add1, setAdd1] = useState(false);
  const [saveData, setSaveData] = useState(false);
  const [sorting, setSorting] = useState("");
  const [schoolTitle, setSchoolTitle] = useState("");
  const [submenuData, setSubmenuData] = useState("");
  const [submenuTitle, setSubmenuTitle] = useState(false);
  const [submenuTitleText, setSubmenuTitleText] = useState("");
  const [pagetitle, setPagetitle] = useState("");

  useEffect(() => {
    getCollegeList();
    getFranchiseSchoolsData();
  }, []);

  const getCollegeList = async () => {
    try {
      let res = await getFranchiseSchools();
      // console.log(res);
      setDataE(res);
    } catch (error) {
      console.log(error);
    }
  };

  const handleAddS = () => {
    setEdit(false);
    setAdd(!add);
    if (add === false) {
      setAddState("");
    }
  };

  const handleAddPrefecture = () => {
    setAddC(!addC);
    if (add === false) {
      setAddPrefecture("");
      setAddCollegeType("");
      setAddProgramType("");
    }
  };

  const handleAddPrefectureData = (index) => {
    setSchoolTitle(addPrefecture);
    var formData = [...dataA];
    var obj = dataA[index];
    var objC = {
      category: "مدارس التوطين / الامتياز",
      unique_title: "Franchise Schools",
      type: "sub menu",
      area: addArea,
      prefecture: addPrefecture,
      states_list: [],
    };
    obj = { ...objC };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    setAddC(!addC);
    setAddPrefecture("");
    setAddProgramType("");
    toast.success(
      "College added successfully. Proceed to stream and programs section"
    );
    // console.log(formData, "formData----");
  };

  const handleAddStatesList = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objS = {
        state: addState,
        schools: schools,
      },
      obj = { ...obj, states_list: [...obj.states_list, objS] };
    formData[index] = obj;
    setDataA(formData);
    setAddState("");
    setSchools("");
    toast.success("Stream added successfully. Proceed to programs section");
    // console.log(formData, "formData----");
  };

  const handleEditCountryName = () => {
    setAdd(false);
    setEdit(!edit);
  };

  const handleAddEditPrefecture = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, prefecture: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleAddEditPrefecture-----------");
  };

  const handleAddEditState = (e, stream_option, indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].states_list[indexS];
    obj = { ...obj, state: e.target.value };
    formData[index].states_list[indexS] = obj;
    setDataA(formData);
    // console.log(formData, "handleAddEditState-----------");
  };

  const handleAddEditSchool = (e, stream_option, indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].states_list[indexS];
    obj = { ...obj, schools: e.target.value };
    formData[index].states_list[indexS] = obj;
    setDataA(formData);
    // console.log(formData, "handleAddEditSchool-----------");
  };

  const handleAddFranchiseSchools = () => {
    Swal.fire({
      title: "Do you want to save ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await addFranchiseSchools(dataA);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been added successfully.",
              "success"
            );
            window.location.reload(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const getFranchiseSchoolsData = async () => {
    try {
      let res = await getSubMenu("Franchise Schools");
      if (res) {
        setSubmenuData(res);
        setPagetitle(res.category);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeleteCountry = (index) => {
    var formData = [...dataE];
    var array = dataE;
    array.splice(index, 1);
    formData = array;
    setDataE(formData);
  };

  const handleDeleteStream = (index, indexS) => {
    Swal.fire({
      title: "Do you want to Remove State ?",
      text: "Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...newData1];
        var array = newData1[index].states_list;
        array.splice(indexS, 1);
        formData[index].states_list = array;
        setNewData1(formData);
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleDeletePrefecture = async (item) => {
    let _id = item?._id?.$oid;
    Swal.fire({
      title: "Do you want to Delete Prefecture ?",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Delete`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deletePrefecture(_id);
          if (res) {
            window.location.reload(false);
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  var updatedData =
    submenuData?.data &&
    submenuData.data.map((item, index) => {
      return item.title;
    });

  const uniqueData = [...new Set(updatedData)];

  const [newData1, setNewData1] = useState([]);

  const newData =
    dataE && dataE.filter((item) => item.area === sorting).map((item) => item);

  useEffect(() => {
    setNewData1(newData);
  }, [sorting]);

  const handleEditCountryNameList = (item) => {
    setAdd1(false);
    setEdit1(!edit1);
  };

  const handleEditPrefecture = (e, index, item) => {
    var formData = [...newData];
    var obj = newData[index];
    obj = { ...obj, prefecture: e.target.value };
    formData[index] = obj;
    setNewData1(formData);
    // console.log(formData, "handleEditPrefecture-----------");
  };

  const handleEditStatesList = (e, stream_option, indexS, index) => {
    var formData = [...newData1];
    var obj = newData1[index].states_list[indexS];
    obj = { ...obj, state: e.target.value };
    formData[index].states_list[indexS] = obj;
    setNewData1(formData);
    // console.log(formData, "state-----------");
  };

  const handleEditSchoolsList = (e, stream_option, indexS, index) => {
    var formData = [...newData1];
    var obj = newData1[index].states_list[indexS];
    obj = { ...obj, schools: e.target.value };
    formData[index].states_list[indexS] = obj;
    setNewData1(formData);
    // console.log(formData, "schools-----------");
  };

  const handleSaveCollege = async (index, item) => {
    var formData = [...newData1];
    var obj = newData1[index];
    obj = { ...obj, _id: item._id.$oid };
    formData[index] = obj;
    setNewData1(formData);
    // console.log(formData, "save-----");
    setSaveData(true);
  };

  const handleUpdateupdateFranchiseSchools = async (item) => {
    Swal.fire({
      title: "Do you want to update ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await updateFranchiseSchools(item);
          if (res.status === "done") {
            Swal.fire(
              "Updated!",
              "Your list has been updated successfully.",
              "success"
            );
            window.location.reload(false);
            setAddC(false);
            getCollegeList();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved");
      }
    });
  };

  const addButton = async () => {
    setSubmenuTitle(!submenuTitle);
    if (submenuTitle === false) {
      setSubmenuTitleText("");
    }
  };

  const addNewButton = async () => {
    // console.log(submenuTitleText, submenuData._id, "schoolTitle, button_title");
    try {
      let res = await addSubmenusFranchiseSchools(
        submenuTitleText,
        submenuData._id
      );
      if (res) {
        setSchoolTitle("");
        getFranchiseSchoolsData();
        setSubmenuTitleText("");
        setSubmenuTitle(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleRefreshPage = async () => {
    history.push(`${process.env.PUBLIC_URL}/app/menus/Dubai`);
  };

  const handleDeleteData = async (item) => {
    let _id = submenuData._id;
    let uniqueTitle = item;
    let title = item;
    Swal.fire({
      title: "Are you sure you want to Delete ?",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: "Confirm",
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deleteFranchiseSubmenus(title, _id, uniqueTitle);
          if (res.status === "done") {
            getFranchiseSchoolsData();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleNewStreamData = (index) => {
    var formData = [...newData1];
    var obj = newData1[index];
    var objS = {
        state: newStream,
        schools: [],
      },
      obj = { ...obj, states_list: [...obj.states_list, objS] };
    formData[index] = obj;
    setNewData1(formData);
    setNewStream("");
    toast.success("Stream saved. Proceed to program section");
    // console.log(formData, "formData----stream");
  };

  const handleNewProgramData = (indexS, index) => {
    var formData = [...newData1];
    var obj = newData1[index].streams_available[indexS];
    // console.log(obj, "obj---");
    var objS = {
        program_code: addProgram,
        details: details,
      },
      obj = { ...obj, program_list: [...obj.program_list, objS] };
    formData[index].streams_available[indexS] = obj;
    setNewData1(formData);
    toast.success("Program saved. Click submit to update");
    // console.log(formData, "program_code---");
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title={pagetitle && pagetitle} />
      <Container fluid={true}>
        <div className="edit-profile">
          <div
            style={{
              cursor: "pointer",
              position: "relative",
              zIndex: 2,
              marginBottom: 10,
              marginTop: -15,
            }}
          >
            <Button onClick={handleRefreshPage}>Main Menu</Button>
          </div>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <div className="row">
                    <div
                      className="col-md-12"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        marginBottom: 20,
                        justifyContent: "flex-end",
                      }}
                    >
                      <Button onClick={addButton}>Add Area</Button>
                    </div>
                    <div className="col-md-12">
                      <Label className="col-form-label pt-0">Select area</Label>
                    </div>
                    <div className="col-md-12">
                      {uniqueData &&
                        uniqueData.map((item, index) => {
                          return (
                            <>
                              <div className="row">
                                <div
                                  className="col-md-11"
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    marginBottom: 20,
                                    flexWrap: "wrap",
                                  }}
                                >
                                  <Button
                                    className="franchiseSchoolUniqueData"
                                    key={index}
                                    value={item}
                                    onClick={(e) => {
                                      setSorting(e.target.value);
                                    }}
                                  >
                                    {item}
                                  </Button>
                                </div>
                                <div
                                  className="col-md-1"
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    marginBottom: 20,
                                    flexWrap: "wrap",
                                  }}
                                >
                                  <DeleteOutline
                                    color="secondary"
                                    onClick={() => handleDeleteData(item)}
                                  />
                                </div>
                              </div>
                            </>
                          );
                        })}
                    </div>
                    {submenuTitle && (
                      <>
                        <div
                          className="col-md-11"
                          style={{
                            display: "flex",
                            alignItems: "center",
                            marginBottom: 20,
                            flexWrap: "wrap",
                          }}
                        >
                          <FormGroup style={{ width: "100%" }}>
                            <Label className="col-form-label pt-0">
                              Add Area
                            </Label>
                            <Input
                              className="form-control"
                              type="text"
                              placeholder="Add Area"
                              value={submenuTitleText}
                              onChange={(e) =>
                                setSubmenuTitleText(e.target.value)
                              }
                            />
                          </FormGroup>
                        </div>
                        <div
                          className="col-md-1"
                          style={{
                            display: "flex",
                            alignItems: "center",
                            marginBottom: 20,
                            justifyContent: "flex-end",
                          }}
                        >
                          <Button onClick={addNewButton}>Add</Button>
                        </div>
                      </>
                    )}
                    <div
                      className="col-md-12"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        marginBottom: 20,
                        justifyContent: "flex-end",
                      }}
                    >
                      <Tooltip title="Add Prefecture">
                        <IconButton>
                          {!addC ? (
                            <Add onClick={handleAddPrefecture} />
                          ) : (
                            <Clear onClick={handleAddPrefecture} />
                          )}
                        </IconButton>
                      </Tooltip>
                    </div>
                  </div>
                  {addC && (
                    <div className="row">
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Prefecture
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Add Prefecture"
                            value={addPrefecture}
                            onChange={(e) => setAddPrefecture(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Area
                          </Label>
                          <Input
                            type="select"
                            name="select"
                            style={{ height: 38, padding: "0px 10px" }}
                            className="form-control digits"
                            value={addArea}
                            onChange={(e) => {
                              setAddArea(e.target.value);
                            }}
                          >
                            <option value={"{}"}>Please select area</option>
                            {uniqueData &&
                              uniqueData.map((item, index) => {
                                return (
                                  <option key={index} value={item}>
                                    {item}
                                  </option>
                                );
                              })}
                          </Input>
                        </FormGroup>
                      </div>
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddPrefectureData()}
                          style={{ height: 40, marginTop: 10 }}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )}
                  {dataA &&
                    dataA.map((item, index) => {
                      return (
                        index != 0 && (
                          <div
                            className="accordinContainer accordinContainerStyle"
                            key={index}
                          >
                            <Accordion style={{ marginTop: 10 }}>
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1c-content"
                                id="panel1c-header"
                              >
                                Prefecture : {item.prefecture}
                              </AccordionSummary>
                              <AccordionDetails
                                style={{ flexDirection: "column" }}
                              >
                                <div className="AccordionSummaryHeading">
                                  <div className="AccordionSummaryHeadingActionButtons">
                                    {!edit ? (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Edit Prefecture
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                  </div>
                                </div>
                                <div>
                                  <div className="row">
                                    {edit && (
                                      <div className="col-md-6">
                                        <FormGroup>
                                          <Label className="col-form-label pt-0">
                                            Edit Prefecture
                                          </Label>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Edit Prefecture"
                                            value={item.prefecture}
                                            onChange={(e) =>
                                              handleAddEditPrefecture(e, index)
                                            }
                                          />
                                        </FormGroup>
                                      </div>
                                    )}
                                    <div
                                      className={
                                        edit ? "col-md-6" : "col-md-12"
                                      }
                                    >
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Area
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Edit Area"
                                          value={item.area}
                                          // onChange={(e) =>
                                          //   handleAddEditArea(e, index)
                                          // }
                                        />
                                      </FormGroup>
                                    </div>
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-md-12">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Add State
                                      </Label>
                                      <div style={{ display: "flex" }}>
                                        <Input
                                          className="form-control"
                                          type="text"
                                          placeholder="Enter State"
                                          value={addState}
                                          onChange={(e) =>
                                            setAddState(e.target.value)
                                          }
                                        />
                                      </div>
                                    </FormGroup>
                                  </div>
                                  <div className="col-md-12">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Add Schools
                                      </Label>
                                      <div style={{ display: "flex" }}>
                                        <Input
                                          className="form-control"
                                          type="textarea"
                                          style={{ minHeight: 100 }}
                                          placeholder="Enter Schools"
                                          value={schools}
                                          onChange={(e) =>
                                            setSchools(e.target.value)
                                          }
                                        />
                                      </div>
                                    </FormGroup>
                                  </div>
                                  <div className="col-md-12">
                                    <Button
                                      disabled={
                                        schools && addState ? false : true
                                      }
                                      onClick={() => handleAddStatesList(index)}
                                      style={{ height: 38 }}
                                    >
                                      Add
                                    </Button>
                                  </div>
                                </div>

                                {item?.states_list &&
                                  item.states_list.map((itemS, indexS) => {
                                    return (
                                      <div className="accordinContainer">
                                        <StreamsAdd
                                          itemS={itemS}
                                          indexS={indexS}
                                          index={index}
                                          // handleDeleteProgram={
                                          //   handleDeleteProgram
                                          // }
                                          handleAddEditState={
                                            handleAddEditState
                                          }
                                          handleAddEditSchool={
                                            handleAddEditSchool
                                          }
                                          setAddProgram={setAddProgram}
                                          addProgram={addProgram}
                                          handleAddS={handleAddS}
                                          setSchools={setSchools}
                                          schools={schools}
                                        />
                                      </div>
                                    );
                                  })}

                                <div className="row insideSubmitButton">
                                  <Button onClick={handleAddFranchiseSchools}>
                                    Submit
                                  </Button>
                                </div>
                              </AccordionDetails>
                            </Accordion>
                            {/* <Tooltip title="Delete Country">
                            <IconButton>
                              <DeleteOutline
                                onClick={() => handleDeleteCountry(index)}
                              />
                            </IconButton>
                          </Tooltip> */}
                          </div>
                        )
                      );
                    })}
                  {/* sbksakcbksabcasbcbsakcbksakcksakcbksackakscbksabkcbsckaskcbksacksabkc */}
                  {newData1 &&
                    newData1.map((item, index) => {
                      return (
                        <div className="accordinContainer">
                          <Accordion key={index} style={{ marginTop: 10 }}>
                            <AccordionSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1c-content"
                              id="panel1c-header"
                            >
                              Prefecture : {item.prefecture}
                            </AccordionSummary>
                            <AccordionDetails
                              style={{ flexDirection: "column" }}
                            >
                              <div className="AccordionSummaryHeading">
                                <div className="AccordionSummaryHeadingActionButtons">
                                  <Tooltip title="Edit Country and details">
                                    <IconButton>
                                      {edit1 ? (
                                        <Button
                                          onClick={() =>
                                            handleEditCountryNameList(item)
                                          }
                                          style={{ height: 38 }}
                                        >
                                          Save
                                        </Button>
                                      ) : (
                                        <Button
                                          onClick={() =>
                                            handleEditCountryNameList(item)
                                          }
                                          style={{ height: 38 }}
                                        >
                                          Edit
                                        </Button>
                                      )}
                                    </IconButton>
                                  </Tooltip>
                                  <Tooltip title="Add State">
                                    {!add1 ? (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Add State
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Remove
                                      </Button>
                                    )}
                                  </Tooltip>
                                </div>
                              </div>
                              <div>
                                <div className="row">
                                  {edit1 && (
                                    <div className="col-md-6">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Edit Prefecture
                                        </Label>
                                        <Input
                                          className="form-control"
                                          type="text"
                                          placeholder="Edit Prefecture"
                                          value={item.prefecture}
                                          onChange={(e) =>
                                            handleEditPrefecture(e, index, item)
                                          }
                                        />
                                      </FormGroup>
                                    </div>
                                  )}
                                  <div
                                    className={edit1 ? "col-md-6" : "col-md-12"}
                                  >
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Area
                                      </Label>
                                      <Input
                                        readOnly={true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Edit Prefecture"
                                        value={item.area}
                                        onChange={(e) =>
                                          handleEditPrefecture(e, index, item)
                                        }
                                      />
                                    </FormGroup>
                                  </div>
                                </div>
                              </div>
                              {add && (
                                <FormGroup>
                                  <Label className="col-form-label pt-0">
                                    Add State
                                  </Label>
                                  <div style={{ display: "flex" }}>
                                    <Input
                                      className="form-control"
                                      type="text"
                                      placeholder="Enter State"
                                      value={newStream}
                                      onChange={(e) =>
                                        setNewStream(e.target.value)
                                      }
                                    />
                                    <Button
                                      onClick={() => handleNewStreamData(index)}
                                      style={{ height: 38 }}
                                    >
                                      Add
                                    </Button>
                                    {/* <Tooltip title="Save">
                                      <IconButton>
                                        <Check
                                          onClick={() =>
                                            handleAddStatesList(index)
                                          }
                                        />
                                      </IconButton>
                                    </Tooltip> */}
                                  </div>
                                </FormGroup>
                              )}
                              {item?.states_list &&
                                item.states_list.map((itemS, indexS) => {
                                  return (
                                    <div className="accordinContainer">
                                      <Streams
                                        itemS={itemS}
                                        indexS={indexS}
                                        index={index}
                                        handleEditStatesList={
                                          handleEditStatesList
                                        }
                                        handleEditSchoolsList={
                                          handleEditSchoolsList
                                        }
                                        // handleDeleteProgram={
                                        //   handleDeleteProgram
                                        // }
                                        setAddProgram={setAddProgram}
                                        addProgram={addProgram}
                                        handleAddS={handleAddS}
                                        setDetails={setDetails}
                                        details={details}
                                        sorting={sorting}
                                        handleNewProgramData={
                                          handleNewProgramData
                                        }
                                      />
                                      <Tooltip title="Delete State">
                                        <IconButton>
                                          <DeleteOutline
                                            onClick={() =>
                                              handleDeleteStream(index, indexS)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip>
                                    </div>
                                  );
                                })}
                              <div style={{ display: "flex" }}>
                                <Button
                                  onClick={() => handleSaveCollege(index, item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Save
                                </Button>
                                <Button
                                  disabled={saveData ? false : true}
                                  onClick={() =>
                                    handleUpdateupdateFranchiseSchools(item)
                                  }
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Submit
                                </Button>
                              </div>
                            </AccordionDetails>
                          </Accordion>
                          <div style={{ marginLeft: 10 }}>
                            <Tooltip title="Delete Prefecture">
                              <IconButton>
                                <DeleteOutline
                                  onClick={() => handleDeletePrefecture(item)}
                                />
                              </IconButton>
                            </Tooltip>
                          </div>
                        </div>
                      );
                    })}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
};

export default Programs;
