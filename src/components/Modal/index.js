import React, { useEffect, Fragment, useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  Row,
  Col,
  Card,
  CardBody,
  Form,
  FormGroup,
  Input,
  CardFooter,
  ModalBody,
  ModalFooter,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Label,
} from "reactstrap";
import InputLabel from "@material-ui/core/InputLabel";
import { Download } from "react-feather";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { uploadExl } from "../../services/uploadService";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import studentmaster from "../../../src/assets/files/StudentMaster.xlsx";
import studentoffer from "../../../src/assets/files/StudentOffer1.xlsx";
import studentoffer1 from "../../../src/assets/files/StudentOffer2.xlsx";
import regoffer from "../../../src/assets/files/RegistrationMaster1.xlsx";
import regoffer1 from "../../../src/assets/files/RegistrationMaster2.xlsx";
import cutOff from "../../../src/assets/files/Cutoffdetails.xlsx";
import CircularProgress from "@material-ui/core/CircularProgress";

const UploadModal = (props) => {
  const { open, setOpen } = props;
  const [offer, setOffer] = useState("1");
  const [file, setFile] = useState();
  const [fileName, setFileName] = useState("");
  const [loader, setloader] = useState(false);
  const [button, setbutton] = useState(false);

  const handleFile = (event) => {
    setFileName(event.target.files[0].name);
    setFile(event.target.files[0]);
  };

  const handleClose = () => {
    setOpen(false);
    setFileName("");
    setFile();
    setOffer("1");
  };

  const handleUpload = async () => {
    if (!offer) {
      toast.error("Select Type to upload file");
    } else if (!file) {
      toast.error("Select file");
    } else {
      setloader(true);
      try {
        let res = await uploadExl(file, offer);
        if (res === "timeout") {
          setbutton(true);
          setTimeout(() => {
            setbutton(false);
          }, 30000);
        }
        handleClose();
      } catch (error) {
        console.log(error);
      }
      setloader(false);
    }
  };

  return (
    <Modal isOpen={open} centered>
      <ModalHeader>Upload Offer</ModalHeader>
      <ModalBody>
        <Row>
          <Col md="12">
            <Card>
              <Form className="form theme-form">
                {loader ? (
                  <CardBody
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      padding: 40,
                      minHeight: 300,
                    }}
                  >
                    <Row>
                      <Col>
                        <CircularProgress />
                      </Col>
                    </Row>
                  </CardBody>
                ) : (
                  <CardBody>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Label>Select Type</Label>
                          <Input
                            type="select"
                            name="select"
                            className="form-control digits"
                            onChange={(e) => {
                              setOffer(e.target.value);
                            }}
                          >
                            <option value="1">Student Master</option>
                            <option value="2">Student Offer 1</option>
                            <option value="3">Student Offer 2</option>
                            <option value="4">Registration Offer 1</option>
                            <option value="5">Registration Offer 2</option>
                            <option value="6">Cut Off Details</option>
                          </Input>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <FormGroup>
                          <Button color="primary" className="mr-1">
                            {offer === "1" && (
                              <Link
                                to={studentmaster}
                                target="_blank"
                                style={{ color: "#fff" }}
                                download
                              >
                                Download Sample
                              </Link>
                            )}
                            {offer === "2" && (
                              <Link
                                to={studentoffer}
                                target="_blank"
                                style={{ color: "#fff" }}
                                download
                              >
                                Download Sample
                              </Link>
                            )}
                            {offer === "3" && (
                              <Link
                                to={studentoffer1}
                                target="_blank"
                                style={{ color: "#fff" }}
                                download
                              >
                                Download Sample
                              </Link>
                            )}
                            {offer === "4" && (
                              <Link
                                to={regoffer}
                                target="_blank"
                                style={{ color: "#fff" }}
                                download
                              >
                                Download Sample
                              </Link>
                            )}
                            {offer === "5" && (
                              <Link
                                to={regoffer1}
                                target="_blank"
                                style={{ color: "#fff" }}
                                download
                              >
                                Download Sample
                              </Link>
                            )}
                            {offer === "6" && (
                              <Link
                                to={cutOff}
                                target="_blank"
                                style={{ color: "#fff" }}
                                download
                              >
                                Download Sample
                              </Link>
                            )}
                          </Button>
                          <p
                            style={{
                              color: "red",
                              margin: "0 0",
                              width: "100%",
                              fontSize: 10,
                            }}
                          >
                            *Select type to download sample.
                          </p>
                        </FormGroup>
                      </Col>
                    </Row>

                    <Row>
                      <Col>
                        <div className="custom-file">
                          <Input
                            className="custom-file-input"
                            onChange={handleFile}
                            id="validatedCustomFile"
                            type="file"
                            accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                            required=""
                          />
                          {fileName === "" ? (
                            <Label
                              className="custom-file-label"
                              htmlFor="validatedCustomFile"
                            >
                              Choose file...
                            </Label>
                          ) : (
                            <Label
                              className="custom-file-label"
                              htmlFor="validatedCustomFile"
                            >
                              {fileName}
                            </Label>
                          )}
                        </div>
                        {/* <FormGroup>
                                                <input
                                                    style={{ padding: 10 }}
                                                    // accept="file"
                                                    id="icon-button-file"
                                                    type="file"
                                                    onChange={handleFile}
                                                    accept=" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                                                />
                                                
                                            </FormGroup> */}
                        <p
                          style={{
                            color: "red",
                            margin: "0 5px",
                            width: "100%",
                            fontSize: 10,
                          }}
                        >
                          **Only .XLSX files allowed.
                        </p>
                      </Col>
                    </Row>
                  </CardBody>
                )}
                <CardFooter>
                  <Button
                    color="primary"
                    disabled={loader ? true : button ? true : false}
                    className="mr-1"
                    onClick={handleUpload}
                  >
                    Upload
                  </Button>

                  <Button color="light" type="reset" onClick={handleClose}>
                    Cancel
                  </Button>
                  {button && (
                    <p
                      style={{
                        color: "red",
                        margin: "0 5px",
                        width: "100%",
                        fontSize: 10,
                      }}
                    >
                      *Uploading file in background. Please wait before
                      uploading new file.
                    </p>
                  )}
                </CardFooter>
              </Form>
            </Card>
          </Col>
        </Row>
      </ModalBody>
    </Modal>
  );
};
export default UploadModal;
