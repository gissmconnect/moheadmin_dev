import React, { Fragment, useEffect, useState } from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Button,
  ButtonGroup,
  DropdownToggle,
} from "reactstrap";
import DatePicker from "react-datepicker";
import ApexCharts from "react-apexcharts";
import ChartistChart from "react-chartist";
import Knob from "knob";
import { Currentlysale, Marketvalue } from "./chartsData/apex-charts-data";
import {
  smallchart1data,
  smallchart1option,
  smallchart2data,
  smallchart2option,
  smallchart3data,
  smallchart3option,
  smallchart4data,
  smallchart4option,
} from "./chartsData/chartist-charts-data";
import { Send, Clock } from "react-feather";

import ClusteredAmChart from "../amcharts/stackedColunAmChart";
import {
  getDashboardTotalCount,
  getDashboardTotalReport,
  getDayWiseTotalReport,
  getVisitors
} from "../../services/dashboardApi";

const Default = (props) => {
  const [daytimes, setDayTimes] = useState();
  const today = new Date();
  const curHr = today.getHours();
  const curMi = today.getMinutes();
  const [meridiem, setMeridiem] = useState("AM");

  const [totalData, setTotalData] = useState("");
  const [totalVisitors, setTotalVisitors] = useState("");
  const [topQueries, setTopQueries] = useState([]);
  const [recentlyAsked, setRecentlyAsked] = useState([]);
  const [dropdownValue, setDropdownValue] = useState("Monthly");
  const [chartData, setChartData] = useState();
  const [newDropdownOpen, setNewDropdownOpen] = useState(false);
  const [uniqueQuestion, setUniqueQuestion] = useState("")
  const [totalQuestionAsked, setTotalQuestionAsked] = useState("")
  const [totalUsers, setTotalUsers] = useState("")

  const toggle = () => setNewDropdownOpen((prevState) => !prevState);


  useEffect(() => {
    // getDashboardBoxCount();
    getDashboardBoxUniqueUsers();
    getDashboardBoxUniqueAsked();
    getDashboardBoxQuestionAsked();
    getDashboardBoxVisitorsToday();
    getDashboardReport();
    // getVisitorsToday();
  }, []);


  useEffect(() => {
    getDayWiseReport();
  }, [dropdownValue]);


  const getDashboardBoxUniqueUsers = async () => {
    var type = 1
    try {
      let result = await getDashboardTotalCount(type);
      console.log(result, "totalUsers---------")
      if (result) {
        setTotalUsers(result.totalUsers);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDashboardBoxUniqueAsked = async () => {
    var type = 2
    try {
      let result = await getDashboardTotalCount(type);
      if (result) {
        setUniqueQuestion(result.uniqueQuestion)
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDashboardBoxQuestionAsked = async () => {
    var type = 3
    try {
      let result = await getDashboardTotalCount(type);
      if (result) {
        setTotalQuestionAsked(result.totalQuestionAsked);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDashboardBoxVisitorsToday = async () => {
    var type = 4
    try {
      let result = await getDashboardTotalCount(type);
      if (result) {
        setTotalVisitors(result.totalDayUsers);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // const getVisitorsToday = async () => {
    // var sevenDays = new Date();
    // var date = sevenDays.getFullYear() + "-" + (sevenDays.getMonth() + 1) + "-" + sevenDays.getDate();
    // try {
    //   let result = await getVisitors(date);
    //   if (result) {
    //     setTotalVisitors(result);
    //   }
    // } catch (error) {
    //   console.log(error);
    // }
  // };

  const getDashboardReport = async () => {
    try {
      let result = await getDashboardTotalReport();
      if (result) {
        setTopQueries(result.topQueries);
        setRecentlyAsked(result.recentlyAsked);
        // console.log("result--------------", result, result.topQueries);
      }
    } catch (error) {
      console.log(error);
    }
  };

  // eslint-disable-next-line
  const [date, setDate] = useState({ date: new Date() });
  // eslint-disable-next-line
  const [startDate, setStartDate] = useState(new Date());

  const handleChange = (date) => {
    setDate(date);
  };

  const currentDate = startDate.getFullYear() + "-" + (startDate.getMonth() + 1) + "-" + startDate.getDate();
  const sevenDays = new Date(new Date().setDate(new Date().getDate() - 6));
  const currentSevenDays = sevenDays.getFullYear() + "-" + (sevenDays.getMonth() + 1) + "-" + sevenDays.getDate();
  const thirtyDays = new Date(new Date().setDate(new Date().getDate() - 29));
  const currentThirtyDays = thirtyDays.getFullYear() + "-" + (thirtyDays.getMonth() + 1) + "-" + thirtyDays.getDate();

  const getDayWiseReport = async () => {
    if(dropdownValue === "Monthly"){
      var selectedDateRange = currentThirtyDays;
    }else if (dropdownValue === "Weakly") {
      var selectedDateRange = currentSevenDays;
    } else {
      var selectedDateRange = currentDate;
    }
    try {
      let result = await getDayWiseTotalReport(selectedDateRange);
      if (result) {
        setChartData(result);
        
      }
    } catch (error) {
      console.log(error);
    }
  };

  const onDropdownItem_Click = (e) => {
    setDropdownValue(e.target.value);
  };

  useEffect(() => {
    if (curHr < 12) {
      setDayTimes("Good Morning");
    } else if (curHr < 18) {
      setDayTimes("Good Afternoon");
    } else {
      setDayTimes("Good Evening");
    }

    if (curHr >= 12) {
      setMeridiem("PM");
    } else {
      setMeridiem("AM");
    }
    // var ordervalue1 = Knob({
    //   value: 60,
    //   angleOffset: 0,
    //   thickness: 0.3,
    //   width: 65,
    //   fgColor: "#7366ff",
    //   readOnly: false,
    //   dynamicDraw: true,
    //   tickColorizeValues: true,
    //   bgColor: '#eef5fb',
    //   lineCap: 'round',
    //   displayPrevious: false
    // })
    // document.getElementById('ordervalue1').appendChild(ordervalue1);

    // var ordervalue2 = Knob({
    //   value: 60,
    //   angleOffset: 0,
    //   thickness: 0.3,
    //   fgColor: "#7366ff",
    //   readOnly: false,
    //   width: 65,
    //   dynamicDraw: true,
    //   lineCap: 'round',
    //   displayPrevious: false
    // })
    // document.getElementById('ordervalue2').appendChild(ordervalue2);

    // eslint-disable-next-line
  }, []);

  return (
    <Fragment>
      <Breadcrumb parent="Dashboard" title="Dashboard" />
      <Container fluid={true}>
        <Row className="second-chart-list third-news-update">
          <Col xl="4 xl-50" lg="12" className="morning-sec box-col-12">
            <Card className="o-hidden profile-greeting">
              <CardBody style={{maxHeight: 422}}>
                <div className="media">
                  <div className="badge-groups w-100">
                    <div className="badge f-12">
                      <Clock
                        style={{ width: "16px", height: "16px" }}
                        className="mr-1"
                      />
                      <span id="txt">
                        {curHr}:{curMi < 10 ? "0" + curMi : curMi} {meridiem}
                      </span>
                    </div>
                    <div className="badge f-12">
                      <i className="fa fa-spin fa-cog f-14"></i>
                    </div>
                  </div>
                </div>
                <div className="greeting-user text-center">
                  <div className="profile-vector">
                    <img
                      className="img-fluid"
                      src={require("../../assets/images/dashboard/welcome.png")}
                      alt=""
                    />
                  </div>
                  <h4 className="f-w-600">
                    <span id="greeting">{daytimes}</span>{" "}
                    <span className="right-circle">
                      <i className="fa fa-check-circle f-14 middle"></i>
                    </span>
                  </h4>
                  <p>
                    <span>
                      {" "}
                      {totalData.total_question} number of questions asked.
                    </span>
                  </p>
                  <div className="whatsnew-btn">
                    <a className="btn btn-primary" href="#javascript">
                      {"Whats New !"}
                    </a>
                  </div>
                  <div className="left-icon">
                    <i className="fa fa-bell"> </i>
                  </div>
                </div>
              </CardBody>
            </Card>
          </Col>
          {/* <Col xl="8 xl-100" className="dashboard-sec box-col-12"> */}
          {/* <Col xl="4 xl-50" className="notification box-col-6"> */}
          {/* <Col xl="4 xl-50" className="news box-col-6">
            <Card> */}
              {/* <CardHeader className="card-no-border">
                <div className="header-top">
                  <h3 className="m-0">Summary</h3>
                  <div className="card-header-right-icon">
                    <select className="button btn btn-primary">
                      <option>Today</option>
                      <option>Tomorrow</option>
                      <option>Yesterday</option>
                    </select>
                  </div>
                </div>
              </CardHeader> */}
              {/* <CardBody className="pt-0">
                <div className="media">
                  <div className="media-body">
                    <h4>Overview of Last Month</h4>
                    <h5>
                      {"100"}
                      <span className="dot-notification"></span>
                    </h5>
                    <h6>
                      {" "}
                      <span>{"Number of Questions asked."}</span>
                    </h6>
                  </div>
                </div>
                <div className="media">
                  <div className="media-body">
                    <h5>
                      {"95"}
                      <span className="dot-notification"></span>
                    </h5>
                    <h6>
                      <span>{"Number of Questions answered "}</span>
                    </h6>
                  </div>
                </div>
                <div className="media">
                  <div className="media-body">
                    <h5>
                      {"1000"}
                      <span className="dot-notification"></span>
                    </h5>
                    <h6>
                      <span>{"Students enrolled "}</span>
                    </h6>
                  </div>
                </div>
                <div className="media">
                  <div className="media-body">
                    <div className="d-flex mb-3">
                      <div className="inner-img"><img className="img-fluid" src={require("../../assets/images/notification/1.jpg")} alt="Product-1" /></div>
                      <div className="inner-img"><img className="img-fluid" src={require("../../assets/images/notification/2.jpg")} alt="Product-2" /></div>
                    </div><span className="mt-3">{"Quisque a consequat ante sit amet magna..."}</span>
                  </div>
                </div>
              </CardBody> */}
            {/* </Card>
          </Col> */}
          {/* <Col xl="4 xl-50" className="news box-col-6">
            <Card className="earning-card">
              <CardBody className="p-0">
                <Row className="m-0">
                  <Col xl="3" className="earning-content p-0"></Col>
                  <Col xl="9" className="p-0">
                    <div className="chart-right">
                      <Row className="m-0 p-tb">
                        <Col xl="8" md="8" sm="8" className="col-12 p-0">
                          <div className="inner-top-left">
                            <ul className="d-flex list-unstyled">
                              <li>{Daily}</li>
                              <li className="active">{Weekly}</li>
                              <li>{Monthly}</li>
                              <li>{Yearly}</li>
                            </ul>
                          </div>
                        </Col>
                      </Row>
                      <Row>
                        <Col xl="12">
                          <CardBody className="p-0">
                            <div className="current-sale-container">
                              <ApexCharts
                                id="chart-currently"
                                options={Currentlysale.options}
                                series={Currentlysale.series}
                                type="area"
                                height={240}
                              />
                            </div>
                          </CardBody>
                        </Col>
                      </Row>
                    </div>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col> */}
          <Col xl="9 xl-100" className="chart_data_left box-col-12">
            <Card>
              <CardBody className="p-0">
                <Row className="m-0 chart-main">
                  <Col xl="3" md="6" sm="6" className="p-0 box-col-6">
                    <div className="media align-items-center">
                      <div className="hospital-small-chart">
                        <div className="small-bar">
                          <ChartistChart
                            className="small-chart flot-chart-container"
                            data={smallchart1data}
                            options={smallchart1option}
                            type={"Bar"}
                            listener={{
                              draw: function (data) {
                                if (data.type === "bar") {
                                  data.element.attr({
                                    style: "stroke-width: 3px",
                                  });
                                }
                              },
                            }}
                          />
                        </div>
                      </div>
                      <div className="media-body">
                        <div className="right-chart-content">
                          <h4>{totalUsers}</h4>
                          <span>Total number of user </span>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col xl="3" md="6" sm="6" className="p-0 box-col-6">
                    <div className="media align-items-center">
                      <div className="hospital-small-chart">
                        <div className="small-bar">
                          <ChartistChart
                            className="small-chart1 flot-chart-container"
                            data={smallchart2data}
                            options={smallchart2option}
                            type={"Bar"}
                            listener={{
                              draw: function (data) {
                                if (data.type === "bar") {
                                  data.element.attr({
                                    style: "stroke-width: 3px",
                                  });
                                }
                              },
                            }}
                          />
                        </div>
                      </div>
                      <div className="media-body">
                        <div className="right-chart-content">
                          <h4>{totalQuestionAsked}</h4>
                          <span>Total number of questions</span>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col xl="3" md="6" sm="6" className="p-0 box-col-6">
                    <div className="media align-items-center">
                      <div className="hospital-small-chart">
                        <div className="small-bar">
                          <ChartistChart
                            className="small-chart2 flot-chart-container"
                            data={smallchart3data}
                            options={smallchart3option}
                            type={"Bar"}
                            listener={{
                              draw: function (data) {
                                if (data.type === "bar") {
                                  data.element.attr({
                                    style: "stroke-width: 3px",
                                  });
                                }
                              },
                            }}
                          />
                        </div>
                      </div>
                      <div className="media-body">
                        <div className="right-chart-content">
                          <h4>{uniqueQuestion}</h4>
                          <span>Total Number of unique questions</span>
                        </div>
                      </div>
                    </div>
                  </Col>
                  <Col xl="3" md="6" sm="6" className="p-0 box-col-6">
                    <div className="media align-items-center">
                    <div className="hospital-small-chart">
                        <div className="small-bar">
                          <ChartistChart
                            className="small-chart1 flot-chart-container"
                            data={smallchart2data}
                            options={smallchart2option}
                            type={"Bar"}
                            listener={{
                              draw: function (data) {
                                if (data.type === "bar") {
                                  data.element.attr({
                                    style: "stroke-width: 3px",
                                  });
                                }
                              },
                            }}
                          />
                        </div>
                      </div>
                      <div className="media-body">
                        <div className="right-chart-content">
                          <h4>{totalVisitors}</h4>
                          <span>Total Number of Visitors Today</span>
                        </div>
                      </div>
                    </div>
                  </Col>
                  {/* <Col xl="3" md="6" sm="6" className="p-0 box-col-6">
                    <div className="media align-items-center">
                      <div className="hospital-small-chart">
                        <div className="small-bar">
                          <ChartistChart
                            className="small-chart1 flot-chart-container"
                            data={smallchart3data}
                            options={smallchart3option}
                            type={"Bar"}
                            listener={{
                              draw: function (data) {
                                if (data.type === "bar") {
                                  data.element.attr({
                                    style: "stroke-width: 3px",
                                  });
                                }
                              },
                            }}
                          />
                        </div>
                      </div>
                      <div className="media-body">
                        <div className="right-chart-content">
                          <h4>{"1037"}</h4>
                          <span>Number of New Students Enrolled</span>
                        </div>
                      </div>
                    </div>
                  </Col> */}
                </Row>
              </CardBody>
            </Card>
          </Col>

          <Col xl="9 xl-100" className="chart_data_left box-col-12">
            <Card>
              <CardBody className="p-0">
                <Row className="m-0 chart-main">
                  <div className="filterClusteredAmChart">
                    <h5 class="m-0">Number of questions asked</h5>
                    <Dropdown isOpen={newDropdownOpen} toggle={toggle}>
                      <DropdownToggle caret>{dropdownValue}</DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem
                          onClick={(e) => onDropdownItem_Click(e)}
                          value="Daily"
                        >
                          Daily
                        </DropdownItem>
                        <DropdownItem
                          onClick={(e) => onDropdownItem_Click(e)}
                          value="Weakly"
                        >
                          Weekly
                        </DropdownItem>
                        <DropdownItem
                          onClick={(e) => onDropdownItem_Click(e)}
                          value="Monthly"
                        >
                          Monthly
                        </DropdownItem>
                        {/* <DropdownItem disabled>Action (disabled)</DropdownItem> */}
                      </DropdownMenu>
                    </Dropdown>
                  </div>
                  <ClusteredAmChart chartData={chartData}/>
                </Row>
              </CardBody>
            </Card>
          </Col>

          <Col xl="4 xl-50" className="appointment-sec box-col-6">
            <Row>
              <Col xl="12" className="appointment">
                <Card>
                  <CardHeader className="card-no-border">
                    <div className="header-top">
                      <h5 className="m-0">Most Popular</h5>
                    </div>
                  </CardHeader>
                  <CardBody className="pt-0">
                    <div className="appointment-table table-responsive">
                      <table className="table table-bordernone">
                        <tbody>
                          <tr>
                            <th className="img-content-box">
                              <span className="d-block">Questions</span>
                            </th>
                            <th className="text-right">
                              <div className="img-content-box">
                                No. of times
                              </div>
                            </th>
                          </tr>
                          {/* <tr>
                            <td className="img-content-box">
                              <span className="d-block"> &nbsp; </span>
                            </td>
                            <td className="text-right">
                              <div className="img-content-box"> &nbsp; </div>
                            </td>
                          </tr> */}
                          {topQueries &&
                            topQueries.map((item, index) => (
                              <>
                                <tr>
                                  <td className="img-content-box">
                                    <span className="d-block">
                                      {item.question}
                                    </span>
                                  </td>
                                  <td className="text-right">
                                    <div className="img-content-box">
                                      {item.count}
                                    </div>
                                  </td>
                                </tr>
                              </>
                            ))}
                        </tbody>
                      </table>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xl="4 xl-50" className="appointment-sec box-col-6">
            <Row>
              <Col xl="12" className="appointment">
                <Card>
                  <CardHeader className="card-no-border">
                    <div className="header-top">
                      <h5 className="m-0">Recently Asked</h5>
                    </div>
                  </CardHeader>
                  <CardBody className="pt-0">
                    <div className="appointment-table table-responsive">
                      <table className="table table-bordernone">
                        <tbody>
                          <tr>
                            <th className="img-content-box">
                              <span className="d-block">Questions</span>
                            </th>
                            {/* <td className="text-right">
                              <div className="img-content-box">
                                Question
                              </div>
                            </td> */}
                          </tr>
                          {/* <tr>
                            <td className="img-content-box">
                              <span className="d-block"> &nbsp; </span>
                            </td>
                            <td className="text-right">
                              <div className="img-content-box"> &nbsp; </div>
                            </td>
                          </tr> */}
                          {recentlyAsked &&
                            recentlyAsked.map((item, index) => (
                              <>
                                <tr>
                                  <td className="img-content-box">
                                    <span className="d-block">
                                      {item.question}
                                    </span>
                                  </td>
                                  {/* <td className="text-right">
                                    <div className="img-content-box">100</div>
                                  </td> */}
                                </tr>
                              </>
                            ))}
                        </tbody>
                      </table>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Col>
          <Col xl="4 xl-50" lg="12" className="calendar-sec box-col-6">
            <Card className="gradient-primary o-hidden">
              <CardBody>
                <div className="default-datepicker">
                  <DatePicker
                    selected={startDate}
                    onChange={handleChange}
                    inline
                  />
                </div>
                <span className="default-dots-stay overview-dots full-width-dots">
                  <span className="dots-group">
                    <span className="dots dots1"></span>
                    <span className="dots dots2 dot-small"></span>
                    <span className="dots dots3 dot-small"></span>
                    <span className="dots dots4 dot-medium"></span>
                    <span className="dots dots5 dot-small"></span>
                    <span className="dots dots6 dot-small"></span>
                    <span className="dots dots7 dot-small-semi"></span>
                    <span className="dots dots8 dot-small-semi"></span>
                    <span className="dots dots9 dot-small"> </span>
                  </span>
                </span>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Container>
    </Fragment>
  );
};

export default Default;
