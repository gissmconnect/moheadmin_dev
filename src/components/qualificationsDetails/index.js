import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import { AddBox, ArrowDownward } from "@material-ui/icons";
// import MaterialTable, { MTableEditField } from "material-table";
import MaterialTable from "@material-table/core";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import { toast } from "react-toastify";
import { data } from "./data";
import { dataNew } from "./dataNew";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Streams from "./streams";
import StreamsAdd from "./streamsAdd";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import {
  updateCompatibleType,
  getQualificationsDetails,
  addCompatibleType,
  deleteCompatible,
} from "../../services/menuService";
import Swal from "sweetalert2";
import { useHistory } from "react-router-dom";

const QualificationsDetails = (props) => {
  const history = useHistory();
  const [dataA, setDataA] = useState(dataNew);
  const [dataE, setDataE] = useState([]);
  const [addcountry, setAddCountry] = useState("");
  const [addStream, setAddStream] = useState("");
  const [edit, setEdit] = useState(false);
  const [add, setAdd] = useState(false);
  const [addC, setAddC] = useState(false);
  const [addProgram, setAddProgram] = useState("");
  const [details, setDetails] = useState("");
  const [newStream, setNewStream] = useState("");

  const [edit1, setEdit1] = useState(false);
  const [add1, setAdd1] = useState(false);
  const [saveData, setSaveData] = useState(false);
  const [pagetitle, setPagetitle] = useState("");

  const [majorType, setMajorType] = useState("");
  const [sorting, setSorting] = useState("");
  const [sorting2, setSorting2] = useState("");

  useEffect(() => {
    getQualifications();
  }, []);

  const getQualifications = async () => {
    try {
      let res = await getQualificationsDetails(
        "Qualifications corresponding to the required specialization"
      );
      setDataE(res);
    } catch (error) {
      console.log(error);
    }
  };

  var updatedData =
    dataE &&
    dataE.map((item, index) => {
      return item.compatible_type;
    });
  const uniqueData = [...new Set(updatedData)];

  const [newData1, setNewData1] = useState([]);

  const newData =
    dataE &&
    dataE
      .filter((item) => item.compatible_type === sorting)
      .map((item) => item);

  useEffect(() => {
    setNewData1(newData);
    setSorting2("");
    setNewData2([]);
  }, [sorting]);

  var updatedNewData =
    newData &&
    newData.map((item, index) => {
      return item.major_type;
    });
  const uniqueNewData = [...new Set(updatedNewData)];

  const [newData2, setNewData2] = useState([]);

  const newData12 =
    newData &&
    newData.filter((item) => item.major_type === sorting2).map((item) => item);

  useEffect(() => {
    setNewData2(newData12);
  }, [sorting2]);

  const handleAddS = () => {
    setEdit(false);
    setAdd(!add);
    if (add === false) {
      setAddStream("");
    }
  };

  const handleAddCountry = () => {
    setAddC(!addC);
    if (add === false) {
      setAddCountry("");
    }
  };

  const handleAddCountryData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objC = {
      category: "Qualifications corresponding to the required specialization",
      unique_title:
        "Qualifications corresponding to the required specialization",
      type: "sub menu",
      compatible_type: addcountry,
      major_type: majorType,
      major_list: [],
    };
    obj = { ...objC };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    setAddC(!addC);
    setAddCountry("");
    toast.success(
      "Compatible type added successfully. Proceed to Major section"
    );
    // console.log(formData, "formData----");
  };

  const handleAddStreamData = (index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    var objS = {
        major_name: addStream,
        university_available: [],
      },
      obj = { ...obj, major_list: [...obj.major_list, objS] };
    formData[index] = obj;
    setDataA(formData);
    setAddStream("");
    toast.success("Major added successfully. Proceed to University section");
    // console.log(formData, "formData----");
  };

  const handleAddProgramData = (indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].major_list[indexS];
    // console.log(obj, "obj---");
    var objS = {
        university_name: addProgram,
        subspeciality: details,
      },
      obj = {
        ...obj,
        university_available: [...obj.university_available, objS],
      };
    formData[index].major_list[indexS] = obj;
    setDataA(formData);
    setAddProgram("");
    setDetails("");
    toast.success("University added successfully. Click Submit to add on list");
    // console.log(formData, "program_code---");
  };

  const handleEditCountryName = () => {
    setAdd(false);
    setEdit(!edit);
  };

  const handleEditCompatibleType = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, compatible_type: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditCountry-----------");
  };

  const handleEditMajorType = (e, index) => {
    var formData = [...dataA];
    var obj = dataA[index];
    obj = { ...obj, major_type: e.target.value };
    formData[index] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditCountry-----------");
  };

  const handleEditStreamData = (e, stream_option, indexS, index) => {
    var formData = [...dataA];
    var obj = dataA[index].major_list[indexS];
    obj = { ...obj, major_name: e.target.value };
    formData[index].major_list[indexS] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditStreamData-----------");
  };

  const handleEditProgramData = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].major_list[indexS].university_available[indexP];
    obj = { ...obj, university_name: e.target.value };
    formData[index].major_list[indexS].university_available[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramData-----------");
  };

  const handleEditProgramDetails = (e, indexS, indexP, index) => {
    var formData = [...dataA];
    var obj = dataA[index].major_list[indexS].university_available[indexP];
    obj = { ...obj, subspeciality: e.target.value };
    formData[index].major_list[indexS].university_available[indexP] = obj;
    setDataA(formData);
    // console.log(formData, "handleEditProgramDetails-----------");
  };

  const handleAddCompatibleType = async () => {
    Swal.fire({
      title: "Do you want to submit ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await addCompatibleType(dataA);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been added successfully.",
              "success"
            );
            window.location.reload(false);
            getQualifications();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleEditCountryNameList = (item) => {
    // console.log(item, "e, index--------");
    setAdd1(false);
    setEdit1(!edit1);
  };

  const handleEditCountry1 = (e, index, item) => {
    var formData = [...newData2];
    var obj = newData2[index];
    obj = { ...obj, compatible_type: e.target.value };
    formData[index] = obj;
    setNewData2(formData);
    // console.log(formData, "handleEditCountry1-----------");
  };

  const handleEditMajor_type = (e, index, item) => {
    var formData = [...newData2];
    var obj = newData2[index];
    obj = { ...obj, major_type: e.target.value };
    formData[index] = obj;
    setNewData2(formData);
    // console.log(formData, "major_type-----------");
  };

  const handleEditStreamData1 = (e, stream_option, indexS, index) => {
    var formData = [...newData2];
    var obj = newData2[index].major_list[indexS];
    obj = { ...obj, major_name: e.target.value };
    formData[index].major_list[indexS] = obj;
    setNewData2(formData);
  };

  const handleEditProgramData1 = (e, indexS, indexP, index) => {
    var formData = [...newData2];
    var obj = newData2[index].major_list[indexS].university_available[indexP];
    obj = { ...obj, university_name: e.target.value };
    formData[index].major_list[indexS].university_available[indexP] = obj;
    setNewData2(formData);
    // console.log(formData);
  };

  const handleEditProgramDetails1 = (e, indexS, indexP, index) => {
    var formData = [...newData2];
    var obj = newData2[index].major_list[indexS].university_available[indexP];
    obj = { ...obj, subspeciality: e.target.value };
    formData[index].major_list[indexS].university_available[indexP] = obj;
    setNewData2(formData);
    // console.log(formData);
  };

  const handleSaveCollege = async (index, item) => {
    var formData = [...newData2];
    var obj = newData2[index];
    obj = { ...obj, _id: item._id.$oid };
    formData[index] = obj;
    setNewData2(formData);
    setSaveData(true);
    toast.success("all data saved. Click submit to update");
    // console.log(formData, "save-----");
  };

  const handleUpdateCompatibleType = async (item) => {
    Swal.fire({
      title: "Do you want to update ?",
      icon: "info",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Save`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await updateCompatibleType(item);
          if (res.status === "done") {
            Swal.fire(
              "Added!",
              "Your list has been updated successfully.",
              "success"
            );
            window.location.reload(false);
            getQualifications();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleNewStreamData = (index) => {
    var formData = [...newData2];
    var obj = newData2[index];
    var objS = {
        major_name: newStream,
        university_available: [],
      },
      obj = { ...obj, major_list: [...obj.major_list, objS] };
    formData[index] = obj;
    setNewData2(formData);
    setNewStream("");
    toast.success("Major Name saved. Proceed to university section");
    // console.log(formData, "formData----stream");
  };

  const handleNewProgramData = (indexS, index) => {
    var formData = [...newData2];
    var obj = newData2[index].major_list[indexS];
    // console.log(obj, "obj---");
    var objS = {
        university_name: addProgram,
        subspeciality: details,
      },
      obj = {
        ...obj,
        university_available: [...obj.university_available, objS],
      };
    formData[index].major_list[indexS] = obj;
    setNewData2(formData);
    toast.success("University saved. Click submit to update");
    // console.log(formData, "program_code---");
    setAddProgram("");
    setDetails("");
  };

  const handleDeleteCountry = (index) => {
    var formData = [...newData2];
    var array = newData2;
    array.splice(index, 1);
    formData = array;
    setNewData2(formData);
  };

  const handleDeleteMajor = (index, indexS) => {
    Swal.fire({
      title: "Do you want to Remove Major  ?",
      text:"Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...newData2];
        var array = newData2[index].major_list;
        array.splice(indexS, 1);
        formData[index].major_list = array;
        setNewData2(formData);
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleDeleteProgram = (indexS, indexP, index) => {
    Swal.fire({
      title: "Do you want to Remove University  ?",
      text:"Save & Submit after removing",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Remove`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        var formData = [...newData2];
        var array = newData2[index].major_list[indexS].university_available;
        array.splice(indexP, 1);
        formData[index].major_list[indexS].university_available = array;
        setNewData2(formData);
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleDeleteCompatible = async (item) => {
    let _id = item?._id?.$oid;
    Swal.fire({
      title: "Do you want to Delete Compatible ?",
      icon: "error",
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Delete`,
      denyButtonText: `Don't save`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          let res = await deleteCompatible(_id);
          if (res) {
            window.location.reload(false);
            getQualifications();
          }
        } catch (error) {
          console.log(error);
        }
      } else if (result.isDenied) {
        Swal.fire("Changes are not saved", "", "info");
      }
    });
  };

  const handleRefreshPage = async () => {
    history.push(`${process.env.PUBLIC_URL}/app/menus/Dubai`);
  };

  return (
    <Fragment>
      <Breadcrumb
        parent="Apps"
        title="Qualifications corresponding & specialization"
      />
      <Container fluid={true}>
        <div className="edit-profile">
          <div
            style={{
              cursor: "pointer",
              position: "relative",
              zIndex: 2,
              marginBottom: 10,
              marginTop: -15,
            }}
          >
            <Button onClick={handleRefreshPage}>Main Menu</Button>
          </div>
          <Row>
            <Col md="12">
              <Card>
                <CardBody>
                  <div className="row">
                    <div
                      className="col-md-5"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        marginBottom: 20,
                      }}
                    >
                      <Input
                        type="select"
                        name="select"
                        style={{ height: 50, padding: 10 }}
                        className="form-control digits"
                        value={sorting}
                        onChange={(e) => {
                          setSorting(e.target.value);
                        }}
                      >
                        <option value={"{}"}>
                          Please select Compatible Type
                        </option>
                        {uniqueData &&
                          uniqueData.map((item, index) => {
                            return (
                              <option key={index} value={item}>
                                {item}
                              </option>
                            );
                          })}
                      </Input>
                    </div>
                    <div
                      className="col-md-5"
                      style={{
                        display: "flex",
                        alignItems: "center",
                        marginBottom: 20,
                      }}
                    >
                      <Input
                        type="select"
                        name="select"
                        style={{ height: 50, padding: 10 }}
                        className="form-control digits"
                        value={sorting2}
                        onChange={(e) => {
                          setSorting2(e.target.value);
                        }}
                      >
                        <option value={"{}"}>Please select Major Type</option>
                        {uniqueNewData &&
                          uniqueNewData.map((item, index) => {
                            return (
                              <option key={index} value={item}>
                                {item}
                              </option>
                            );
                          })}
                      </Input>
                    </div>
                    <div className="col-md-2">
                      <FormGroup
                        style={{ display: "flex", justifyContent: "flex-end" }}
                      >
                        <Tooltip title="Add Country">
                          <IconButton>
                            {!addC ? (
                              <Add onClick={handleAddCountry} />
                            ) : (
                              <Clear onClick={handleAddCountry} />
                            )}
                          </IconButton>
                        </Tooltip>
                      </FormGroup>
                    </div>
                  </div>
                  {addC && (
                    <div className="row">
                      <div className="col-md-6">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Compatible Type
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Compatible Type"
                            value={addcountry}
                            onChange={(e) => setAddCountry(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div className="col-md-5">
                        <FormGroup>
                          <Label className="col-form-label pt-0">
                            Add Major Type
                          </Label>
                          <Input
                            className="form-control"
                            type="text"
                            placeholder="Major Type"
                            value={majorType}
                            onChange={(e) => setMajorType(e.target.value)}
                          />
                        </FormGroup>
                      </div>
                      <div
                        className="col-md-1"
                        style={{ display: "flex", alignItems: "center" }}
                      >
                        <Button
                          onClick={() => handleAddCountryData()}
                          style={{ height: 40, marginTop: 10 }}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )}
                  {dataA &&
                    dataA.map((item, index) => {
                      return (
                        index != 0 && (
                          <div
                            className="accordinContainer accordinContainerStyle"
                            key={index}
                          >
                            <Accordion style={{ marginTop: 10 }}>
                              <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1c-content"
                                id="panel1c-header"
                              >
                                Compatible Type : {item.compatible_type}
                              </AccordionSummary>
                              <AccordionDetails
                                style={{ flexDirection: "column" }}
                              >
                                <div className="AccordionSummaryHeading">
                                  <div className="AccordionSummaryHeadingActionButtons">
                                    {!edit ? (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Edit Compatible Type
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleEditCountryName}
                                        style={{ height: 38 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                  </div>
                                </div>
                                <div>
                                  <div className="row">
                                    {edit && (
                                      <div className="col-md-6">
                                        <FormGroup>
                                          <Label className="col-form-label pt-0">
                                            Edit Compatible Type
                                          </Label>
                                          <Input
                                            className="form-control"
                                            type="text"
                                            placeholder="Edit college"
                                            value={item.compatible_type}
                                            onChange={(e) =>
                                              handleEditCompatibleType(e, index)
                                            }
                                          />
                                        </FormGroup>
                                      </div>
                                    )}
                                    <div
                                      className={
                                        edit ? "col-md-6" : "col-md-12"
                                      }
                                    >
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          {edit
                                            ? "Edit Major Type"
                                            : "Major Type"}
                                        </Label>
                                        <Input
                                          readOnly={edit ? false : true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Edit Major Type"
                                          value={item.major_type}
                                          onChange={(e) =>
                                            handleEditMajorType(e, index)
                                          }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div className="col-md-12">
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Unique Title
                                        </Label>
                                        <Input
                                          readOnly={true}
                                          className="form-control"
                                          type="text"
                                          placeholder="Unique Title"
                                          value="Qualifications corresponding to the required specialization"
                                          // onChange={(e) =>
                                          //   handleEditCollegeType(e, index)
                                          // }
                                        />
                                      </FormGroup>
                                    </div>
                                    <div
                                      className="col-md-12"
                                      style={{
                                        display: "flex",
                                        justifyContent: "flex-end",
                                      }}
                                    >
                                      {add ? (
                                        <Button
                                          onClick={handleAddS}
                                          style={{
                                            height: 40,
                                            marginTop: 10,
                                            marginBottom: 10,
                                          }}
                                        >
                                          Remove
                                        </Button>
                                      ) : (
                                        <Button
                                          onClick={handleAddS}
                                          style={{
                                            height: 40,
                                            marginTop: 10,
                                            marginBottom: 10,
                                          }}
                                        >
                                          Add Major
                                        </Button>
                                      )}
                                    </div>
                                  </div>
                                </div>
                                {add && (
                                  <FormGroup>
                                    <Label className="col-form-label pt-0">
                                      Add Major Name
                                    </Label>
                                    <div style={{ display: "flex" }}>
                                      <Input
                                        className="form-control"
                                        type="text"
                                        placeholder="Add Major Name"
                                        value={addStream}
                                        onChange={(e) =>
                                          setAddStream(e.target.value)
                                        }
                                      />
                                      <Button
                                        onClick={() =>
                                          handleAddStreamData(index)
                                        }
                                        style={{ height: 38 }}
                                      >
                                        Add
                                      </Button>
                                    </div>
                                  </FormGroup>
                                )}
                                {item.major_list.map((itemS, indexS) => {
                                  return (
                                    <div className="accordinContainer">
                                      <StreamsAdd
                                        itemS={itemS}
                                        indexS={indexS}
                                        index={index}
                                        handleEditProgramDetails={
                                          handleEditProgramDetails
                                        }
                                        handleEditStreamData={
                                          handleEditStreamData
                                        }
                                        handleDeleteProgram={
                                          handleDeleteProgram
                                        }
                                        handleAddProgramData={
                                          handleAddProgramData
                                        }
                                        handleEditProgramData={
                                          handleEditProgramData
                                        }
                                        setAddProgram={setAddProgram}
                                        addProgram={addProgram}
                                        handleAddS={handleAddS}
                                        setDetails={setDetails}
                                        details={details}
                                      />
                                      {/* <Tooltip title="Delete Stream">
                                        <IconButton>
                                          <DeleteOutline
                                            onClick={() =>
                                              handleDeleteStream(index, indexS)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip> */}
                                    </div>
                                  );
                                })}
                                <div className="row insideSubmitButton">
                                  <Button onClick={handleAddCompatibleType}>
                                    Submit
                                  </Button>
                                </div>
                              </AccordionDetails>
                            </Accordion>
                          </div>
                        )
                      );
                    })}
                  {/* sbksakcbksabcasbcbsakcbksakcksakcbksackakscbksabkcbsckaskcbksacksabkc */}
                  {newData2 &&
                    newData2.map((item, index) => {
                      return (
                        <div className="accordinContainer">
                          <Accordion key={index} style={{ marginTop: 10 }}>
                            <AccordionSummary
                              expandIcon={<ExpandMoreIcon />}
                              aria-controls="panel1c-content"
                              id="panel1c-header"
                            >
                              Compatible Type : {item.compatible_type}
                            </AccordionSummary>
                            <AccordionDetails
                              style={{ flexDirection: "column" }}
                            >
                              <div className="AccordionSummaryHeading">
                                {/* <div> College : {item.college_name}</div> */}
                                <div className="AccordionSummaryHeadingActionButtons">
                                  <Tooltip title="Edit College and details">
                                    {!edit1 ? (
                                      <Button
                                        onClick={() =>
                                          handleEditCountryNameList(item)
                                        }
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Edit Compatible Type
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={() =>
                                          handleEditCountryNameList(item)
                                        }
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Save
                                      </Button>
                                    )}
                                  </Tooltip>
                                  <Tooltip title="Add Stream">
                                    {!add1 ? (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Add Major
                                      </Button>
                                    ) : (
                                      <Button
                                        onClick={handleAddS}
                                        style={{ height: 40, margin: 10 }}
                                      >
                                        Remove
                                      </Button>
                                    )}
                                  </Tooltip>
                                </div>
                              </div>
                              <div>
                                <div className="row">
                                  <div
                                    className={edit1 ? "col-md-6" : "col-md-0"}
                                  >
                                    {edit1 && (
                                      <FormGroup>
                                        <Label className="col-form-label pt-0">
                                          Edit Compatible Type
                                        </Label>
                                        <Input
                                          className="form-control"
                                          type="text"
                                          placeholder="Edit Compatible Type"
                                          value={item.compatible_type}
                                          onChange={(e) =>
                                            handleEditCountry1(e, index, item)
                                          }
                                        />
                                      </FormGroup>
                                    )}
                                  </div>
                                  <div
                                    className={edit1 ? "col-md-6" : "col-md-12"}
                                  >
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        {edit1
                                          ? "Edit Major Type"
                                          : "Major Type"}
                                      </Label>
                                      <Input
                                        readOnly={edit1 ? false : true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Edit Major Type"
                                        value={item.major_type}
                                        onChange={(e) =>
                                          handleEditMajor_type(e, index, item)
                                        }
                                      />
                                    </FormGroup>
                                  </div>
                                  <div className="col-md-12">
                                    <FormGroup>
                                      <Label className="col-form-label pt-0">
                                        Unique Title
                                      </Label>
                                      <Input
                                        readOnly={true}
                                        className="form-control"
                                        type="text"
                                        placeholder="Unique Title"
                                        value={item.unique_title}
                                        // onChange={(e) =>
                                        //   handleEditCollegeType1(e, index)
                                        // }
                                      />
                                    </FormGroup>
                                  </div>
                                </div>
                              </div>
                              {add && (
                                <FormGroup>
                                  <Label className="col-form-label pt-0">
                                    Add Major Name
                                  </Label>
                                  <div style={{ display: "flex" }}>
                                    <Input
                                      className="form-control"
                                      type="text"
                                      placeholder="Enter Major Name"
                                      value={newStream}
                                      onChange={(e) =>
                                        setNewStream(e.target.value)
                                      }
                                    />
                                    {/* <Tooltip title="Save">
                                      <IconButton>
                                        <Check
                                          onClick={() =>
                                            handleAddStreamData(index)
                                          }
                                        />
                                      </IconButton>
                                    </Tooltip> */}
                                    <Button
                                      onClick={() => handleNewStreamData(index)}
                                      style={{ height: 38 }}
                                    >
                                      Add
                                    </Button>
                                  </div>
                                </FormGroup>
                              )}
                              {item?.major_list &&
                                item.major_list.map((itemS, indexS) => {
                                  return (
                                    <div className="accordinContainer">
                                      <Streams
                                        itemS={itemS}
                                        indexS={indexS}
                                        index={index}
                                        handleEditStreamData1={
                                          handleEditStreamData1
                                        }
                                        handleDeleteProgram={
                                          handleDeleteProgram
                                        }
                                        handleEditProgramData1={
                                          handleEditProgramData1
                                        }
                                        handleEditProgramDetails1={
                                          handleEditProgramDetails1
                                        }
                                        setAddProgram={setAddProgram}
                                        addProgram={addProgram}
                                        handleAddS={handleAddS}
                                        setDetails={setDetails}
                                        details={details}
                                        handleNewProgramData={
                                          handleNewProgramData
                                        }
                                      />
                                      <Tooltip title="Delete Major">
                                        <IconButton>
                                          <DeleteOutline
                                            onClick={() =>
                                              handleDeleteMajor(index, indexS)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip>
                                    </div>
                                  );
                                })}
                              <div style={{ display: "flex" }}>
                                <Button
                                  onClick={() => handleSaveCollege(index, item)}
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Save
                                </Button>
                                <Button
                                  disabled={saveData ? false : true}
                                  onClick={() =>
                                    handleUpdateCompatibleType(item)
                                  }
                                  style={{
                                    width: 150,
                                    height: 40,
                                    margin: 10,
                                  }}
                                >
                                  Submit
                                </Button>
                              </div>
                            </AccordionDetails>
                          </Accordion>
                          <div style={{ marginLeft: 10 }}>
                            <Tooltip title="Delete Student Type">
                              <IconButton>
                                <DeleteOutline
                                  onClick={() => handleDeleteCompatible(item)}
                                />
                              </IconButton>
                            </Tooltip>
                          </div>
                        </div>
                      );
                    })}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </Container>
    </Fragment>
  );
};

export default QualificationsDetails;
