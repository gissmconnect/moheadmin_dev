import React, { useState, useEffect } from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionActions from "@material-ui/core/AccordionActions";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Chip from "@material-ui/core/Chip";
import { Row, Col, Button, FormGroup, Label, Input } from "reactstrap";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Edit from "@material-ui/icons/Edit";
import Add from "@material-ui/icons/Add";
import Check from "@material-ui/icons/Check";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";

export default function StreamsAdd(props) {
  const {
    itemS,
    indexS,
    index,
    handleEditStreamData,
    handleEditProgramData,
    handleEditProgramDetails,
    handleDeleteProgram,
    handleAddProgramData,
    setAddProgram,
    addProgram,
    setDetails,
    details,
    handleAddS,
  } = props;
  const [addS, setAddS] = useState(false);
  const [editS, setEditS] = useState(false);

  const handleEditStream = () => {
    setAddS(false);
    setEditS(!editS);
  };
  const handleAddP = () => {
    setEditS(false);
    setAddS(!addS);
    if (addS === false) {
      setAddProgram("");
      setDetails("");
    }
  };
  return (
    <>
      <Accordion key={indexS} style={{ marginTop: 10 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1c-content"
          id="panel1c-header"
        >
          Major Name : {itemS.major_name}
        </AccordionSummary>
        <AccordionDetails style={{ flexDirection: "column" }}>
          <FormGroup style={{ display: "block", textAlign: "end" }}></FormGroup>
          <div className="AccordionSummaryHeading">
            <div className="AccordionSummaryHeadingActionButtons">
              {!editS ? (
                <Button
                  onClick={handleEditStream}
                  style={{ height: 40, margin: 10 }}
                >
                  Edit Major
                </Button>
              ) : (
                <Button
                  onClick={handleEditStream}
                  style={{ height: 40, margin: 10 }}
                >
                  Save
                </Button>
              )}
              {addS ? (
                <Button
                  onClick={handleAddP}
                  style={{ height: 40, marginTop: 10, marginBottom: 10 }}
                >
                  Remove University
                </Button>
              ) : (
                <Button
                  onClick={handleAddP}
                  style={{ height: 40, marginTop: 10, marginBottom: 10 }}
                >
                  Add University
                </Button>
              )}
              {/* {addS ? (
                <Tooltip title="Add Stream">
                  <IconButton>
                    <Clear onClick={handleAddP} />
                  </IconButton>
                </Tooltip>
              ) : (
                <Tooltip title="Add Stream">
                  <IconButton>
                    <Add onClick={handleAddP} />
                  </IconButton>
                </Tooltip>
              )} */}
            </div>
          </div>
          {editS && (
            <FormGroup>
              <Label className="col-form-label pt-0">Edit Major Name</Label>
              <Input
                className="form-control"
                type="text"
                placeholder="Enter Major Name"
                value={itemS.major_name}
                onChange={(e) =>
                  handleEditStreamData(e, itemS.stream_option, indexS, index)
                }
              />
            </FormGroup>
          )}
          {addS && (
            <FormGroup>
              <div className="row" style={{ display: "flex" }}>
                <div className="col-md-12">
                  <Label className="col-form-label pt-0">
                    Add University Name
                  </Label>
                </div>
                <div className="col-md-12">
                  <Input
                    className="form-control"
                    type="text"
                    placeholder="Enter University Name"
                    value={addProgram}
                    onChange={(e) => setAddProgram(e.target.value)}
                  />
                </div>
                <div className="col-md-12">
                  <Label className="col-form-label pt-0 mt-2">
                    Add Subspeciality
                  </Label>
                  <Input
                    className="form-control"
                    type="textarea"
                    placeholder="Enter Subspeciality"
                    value={details}
                    onChange={(e) => setDetails(e.target.value)}
                    style={{ minHeight: 150, marginTop: 10 }}
                  />
                </div>
                <div className="col-md-1">
                  <div style={{ display: "flex" }}>
                    <Button
                      onClick={() => handleAddProgramData(indexS, index)}
                      style={{ height: 40, marginTop: 10, marginBottom: 10 }}
                    >
                      Add
                    </Button>
                    {/* <Tooltip title="Save">
                            <IconButton>
                              <Check
                                onClick={() =>
                                  handleAddProgramData(indexS, index)
                                }
                              />
                            </IconButton>
                          </Tooltip> */}
                  </div>
                </div>
              </div>
            </FormGroup>
          )}
          {itemS?.university_available &&
            itemS.university_available.map((itemP, indexP) => {
              return (
                <div className="row">
                  <div className="col-md-12">
                    <Label className="col-form-label pt-0">
                    University Name
                    </Label>
                    <FormGroup
                      style={{ display: "flex", flexDirection: "row" }}
                    >
                      <Input
                        readOnly={editS ? false : true}
                        className="form-control"
                        type="text"
                        placeholder="Enter University Name"
                        value={itemP.university_name}
                        onChange={(e) =>
                          handleEditProgramData(e, indexS, indexP, index)
                        }
                      />
                      {/* <Tooltip title="Delete Program">
                        <IconButton>
                          <DeleteOutline
                            onClick={() => handleDeleteProgram(indexS, indexP, index)}
                          />
                        </IconButton>
                      </Tooltip> */}
                    </FormGroup>
                  </div>
                  <div className="col-md-12">
                    <Label className="col-form-label pt-0">Subspeciality</Label>
                    <FormGroup
                      style={{ display: "flex", flexDirection: "row" }}
                    >
                      <Input
                        readOnly={editS ? false : true}
                        className="form-control"
                        type="textarea"
                        placeholder="Enter Subspeciality"
                        value={itemP.subspeciality}
                        onChange={(e) =>
                          handleEditProgramDetails(e, indexS, indexP, index)
                        }
                        style={{ minHeight: 150 }}
                      />
                      {/* <Tooltip title="Delete Program">
                        <IconButton>
                          <DeleteOutline
                            onClick={() =>
                              handleDeleteProgram(indexS, indexP, index)
                            }
                          />
                        </IconButton>
                      </Tooltip> */}
                    </FormGroup>
                  </div>
                </div>
              );
            })}
        </AccordionDetails>
      </Accordion>
    </>
  );
}
