export const dataNew = [
  {
    category: null,
    unique_title: null,
    type: "sub menu",
    compatible_type: null,
    major_type: null,
    major_list: [
      {
        major_name: null,
        university_available: [
          {
            university_name: null,
            subspeciality: null,
          },
        ],
      },
    ],
  },
];