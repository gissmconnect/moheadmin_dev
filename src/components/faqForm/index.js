import React, {
  Fragment,
  useEffect,
  useState,
  Component,
  forwardRef,
} from "react";
import Breadcrumb from "../../layout/breadcrumb";
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Media,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import { AddBox, ArrowDownward } from "@material-ui/icons";
// import MaterialTable, { MTableEditField } from "material-table";
import MaterialTable from '@material-table/core';
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import { toast } from "react-toastify";
import FaqModal from "./modalFaq";
import { getFaq, getRes, deleteFaq } from "../../services/faqService";

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const FaqForm = (props) => {
  const [open, setOpen] = useState();
  const [editrowData, seteditrowData] = useState([]);
  const [faqList, setFaqList] = useState([]);
  const [responses, setResponses] = useState({});
  const [loader, setloader] = useState(false);

  useEffect(() => {
    getFaqList();
  }, []);

  const getFaqList = async () => {
    try {
      let res = await getFaq();
      setFaqList(res.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDialog = async (rowData) => {
    try {
      let res = await getRes(rowData.intent);
      setResponses(res.data);
    } catch (error) {
      console.log(error);
    }
    setOpen(true);
    seteditrowData(rowData);
  };

  const handleRowDelete = async (oldData, resolve, reject) => {
    setloader(true);
    try {
      await deleteFaq(oldData.intent);
      getFaqList();
      resolve();
    } catch (error) {
      console.log(error);
      reject();
    }
    setloader(false);
  };

  return (
    <Fragment>
      <Breadcrumb parent="Apps" title="Faq Questions" />
      <Container fluid={true}>
        <div className="edit-profile">
          <Row>
            <Col md="12">
              <MaterialTable
                icons={tableIcons}
                isLoading={loader}
                columns={[
                  {
                    title: "Questions",
                    field: "intent",
                    headerStyle: {
                      zIndex: 0,
                    },
                    render: (rowData) => {
                      return (
                        <p
                          style={{
                            color: "blue",
                            textDecoration: "underline",
                            cursor: "pointer",
                            fontSize: 15,
                          }}
                          onClick={() => handleDialog(rowData)}
                        >
                          {rowData.intent}
                        </p>
                      );
                    },
                  },
                ]}
                data={
                  faqList &&
                  faqList.map((item) => {
                    return {
                      intent: item,
                    };
                  })
                }
                title=""
                options={{
                  actionsColumnIndex: -1,
                  addRowPosition: "first",
                  pageSize: 20,
                  pageSizeOptions: [20, 50, 100, 200, 500],
                }}
                actions={[
                  {
                    icon: tableIcons.Add,
                    onClick: (event, rowData) => {
                      handleDialog(rowData);
                    },
                    isFreeAction: true,
                    tooltip: "Create Intent",
                  },
                ]}
                editable={{
                  onRowDelete: (oldData) =>
                    new Promise((resolve, reject) => {
                      handleRowDelete(oldData, resolve, reject);
                    }),
                }}
              />
            </Col>
          </Row>
        </div>
      </Container>
      <FaqModal
        open={open}
        setOpen={setOpen}
        editrowData={editrowData}
        seteditrowData={seteditrowData}
        responses={responses}
        getFaqList={getFaqList}
        setloader={setloader}
      />
    </Fragment>
  );
};

export default FaqForm;
