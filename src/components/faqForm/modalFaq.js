import React, { useEffect, Fragment, useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Container,
  Row,
  Col,
  Card,
  Badge,
  CardHeader,
  CardBody,
  CardFooter,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { alpha, makeStyles } from "@material-ui/core/styles";
import Dropzone from "react-dropzone-uploader";
import { X } from "react-feather";
import {
  uploadFile,
  AddResponse,
  AddUpdate,
  uploadFileNew,
} from "../../services/faqService";
import { result } from "lodash";
import { toast } from "react-toastify";
import { dataNew } from "./dataNew";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: "100%",
  },
  table: {
    width: 1100,
    minHeight: 100,
    margin: "20px auto",
    height: 500,
    borderWidth: 2,
    borderColor: "#e4e4e4",
    backgroundColor: "#fff",
    borderRadius: 20,
    overflow: "hidden",
    boxShadow:
      "0px 2px 4px -1px #00000033, 0px 4px 5px 0px #00000024, 0px 1px 10px 0px #0000001f",
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchPanel: {
    borderBottom: "solid 1px rgb(236, 239, 247)",
    paddingTop: 10,
    paddingBottom: 10,
  },
}));

const FaqModal = (props) => {
  const {
    open,
    setOpen,
    editrowData,
    seteditrowData,
    responses,
    getFaqList,
    setloader,
  } = props;

  const classes = useStyles();
  const [dataA, setDataA] = useState(dataNew);
  const [lastClicked, setLastClicked] = useState(false);
  const [faqName, setFaqName] = useState("");
  const [spaceError, setSpaceError] = useState(false);
  const [spaceErrorSubmit, setSpaceErrorSubmit] = useState(false);
  const [fileName, setFileName] = useState();
  const [response, setResponse] = useState([]);
  const [keywords, setKeywords] = useState([]);
  const [upload, setUpload] = useState(false);
  const [pdf, setPdf] = useState(false);
  const [uploadedFileName, setUploadedFileName] = useState([]);
  const [keywordValue, setKeywordValue] = useState("");
  const [responseValue, setResponseValue] = useState("");
  const [responseType, setResponseType] = useState("");
  const [keywordEntered, setKeywordEntered] = useState("");
  const [category, setCategory] = useState("");
  const [responsesId, setResponsesId] = useState("");
  const [buttonStatus, setButtonStatus] = useState(true);

  const getUploadParams = ({ meta }) => {
    return { url: "https://httpbin.org/post" };
  };
  const handleChangeStatus = ({ meta, file }, status) => {
    if (status === "done") {
      setFileName(file);
      setUpload(true);
      setButtonStatus(!buttonStatus);
    } else if (status === "removed") {
      setFileName();
      setUpload(false);
    }
  };

  const handleType = (e) => {
    setResponseType(e.target.value);
    if (e.target.value === "pdf" || e.target.value === "video") {
      setLastClicked(true);
      if (e.target.value === "pdf") {
        setResponseType(e.target.value);
        setPdf(true);
      } else if (e.target.value === "video") {
        setResponseType(e.target.value);
        setPdf(false);
      }
    } else {
      setLastClicked(false);
    }
  };

  const handleUpload = async () => {
    try {
      let res = await uploadFileNew(fileName, pdf);
      let fileArray = [...uploadedFileName];
      fileArray.push(res.url);
      setUploadedFileName(fileArray);
      console.log(fileArray);
    } catch (error) {
      console.log(error);
    }
  };

  const handleResponseValue = (e) => {
    setResponseValue(e.target.value);
  };

  useEffect(() => {
    if (responses?._id) {
      setResponsesId(responses._id.$oid);
    }
    if (responses?.keyword) {
      setKeywordEntered(responses.keyword);
    }
    if (responses?.response_type) {
      setResponseType(responses.response_type);
    }
    if (responses?.response) {
      setResponseValue(responses.response);
    }
    if (responses?.variations) {
      setKeywords(responses?.variations && responses.variations);
    } else {
      setKeywords("");
    }
  }, [responses]);

  useEffect(() => {
    if (editrowData) {
      setFaqName(editrowData.intent);
    }
  }, [editrowData]);

  const handleClose = () => {
    setOpen(false);
    setKeywords([]);
    setKeywordValue("");
    setLastClicked(false);
    seteditrowData([]);
    setUploadedFileName([]);
    setResponseValue("");
    setResponse([]);
    setKeywordEntered("");
    setResponseType("");
    setCategory("");
  };

  const handleSubmit = async () => {
    if (lastClicked) {
      if (uploadedFileName) {
        var file = uploadedFileName[0];
      } else {
        var file = responseValue;
      }
    } else {
      var file = responseValue;
    }
    var formData = [...dataA];
    var obj = dataA[0];
    var objC = {
      question: faqName,
      keyword: keywordEntered,
      category: category,
      response_type: responseType,
      response: file,
      variations: keywords,
    };
    obj = { ...objC };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    if (formData.length === 2) {
      try {
        let res = await AddResponse(formData[1]);
        if (res.success === true) {
          getFaqList();
          setOpen(false);
          setFaqName("");
          setKeywordEntered("");
          setCategory("");
          setResponseType("");
          setResponseValue("");
          setKeywords([]);
          setDataA(dataNew);
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  const handleUpdate = async () => {
    setloader(true);
    if (lastClicked) {
      if (uploadedFileName) {
        var file = uploadedFileName[0];
      } else {
        var file = responseValue;
      }
    } else {
      var file = responseValue;
    }
    var formData = [...dataA];
    var obj = dataA[0];
    var objC = {
      question: faqName,
      keyword: keywordEntered,
      response_type: responseType,
      response: file,
      variations: keywords,
    };
    obj = { ...objC };
    formData[dataA?.length && dataA.length] = obj;
    setDataA(formData);
    if (formData.length === 2) {
      try {
        let res = await AddUpdate(formData[1], responsesId);
        if (res.success === true) {
          getFaqList();
          setOpen(false);
          setFaqName("");
          setKeywordEntered("");
          setCategory("");
          setResponseType("");
          setResponseValue("");
          setKeywords([]);
          setResponsesId("");
          setKeywordValue("");
          setDataA(dataNew);
          // window.location.reload(false);
          setloader(false);
        }
      } catch (error) {
        console.log(error);
      }
      setloader(false);
    }
  };

  const handleKeywordEntered = (e) => {
    setKeywordEntered(e.target.value);
    var key = e.target.value;
    if (/\s/.test(key)) {
      setSpaceError(true);
      setSpaceErrorSubmit(true);
    } else {
      setSpaceError(false);
      setSpaceErrorSubmit(false);
      // setFaqName(key);
    }
  };

  const handleRemoveKeyword = (index) => {
    const keyword = [...keywords];
    keyword.splice(index, 1);
    setKeywords(keyword);
  };
  const handleRemoveResponse = (index) => {
    const respo = [...response];
    respo.splice(index, 1);
    setResponse(respo);
  };

  return (
    <Modal isOpen={open} centered className="addIntentStyle">
      <ModalHeader style={{ padding: "20px 30px" }}>
        {editrowData.intent ? "Update Question" : "Add Question"}
      </ModalHeader>
      <ModalBody>
        <Card style={{ marginBottom: 0 }}>
          <Form className="form theme-form">
            <CardBody style={{ padding: "0px 30px" }}>
              <Row>
                <Col>
                  <FormGroup>
                    <Label>Add Question</Label>
                    <Input
                      className="form-control"
                      type="text"
                      onChange={(e) => setFaqName(e.target.value)}
                      disabled={editrowData.intent ? true : false}
                      placeholder="Add Questions"
                      value={faqName}
                    />
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col>
                  <FormGroup>
                    <Label for="validationCustom01">Query</Label>
                    <Input
                      className="form-control"
                      id="validationCustom01"
                      type="text"
                      // disabled={editrowData.intent ? true : false}
                      value={keywordValue}
                      onKeyPress={(e) => {
                        if (e.charCode === 13) {
                          let keyArray = [...keywords];
                          keyArray.push(keywordValue);
                          setKeywords(keyArray);
                          setKeywordValue("");
                          // console.log(keyArray, "keyArray---")
                        }
                      }}
                      onChange={(e) => setKeywordValue(e.target.value)}
                      placeholder="Please enter query"
                    />
                    <p
                      style={{
                        color: "red",
                        margin: "0 0",
                        width: "100%",
                        fontSize: 10,
                      }}
                    >
                      *Minimum 2 variations required.
                    </p>
                  </FormGroup>
                  <FormGroup>
                    <div
                      className="custom-scrollbar3"
                      style={{
                        display: "flex",
                        flexWrap: "wrap",
                        overflowX: "auto",
                      }}
                    >
                      {keywords &&
                        keywords.map((item, index) => {
                          return (
                            <Badge
                              key={index}
                              color="primary"
                              style={{
                                fontSize: 16,
                                display: "flex",
                                alignItems: "center",
                                marginBottom: 6,
                              }}
                            >
                              <span>{item}</span>
                              <X
                                size={"30px"}
                                style={{
                                  width: "20px",
                                  height: "20px",
                                  marginRight: 0,
                                  position: "static",
                                }}
                                onClick={() => handleRemoveKeyword(index)}
                              />
                            </Badge>
                          );
                        })}
                    </div>
                  </FormGroup>
                </Col>
              </Row>
              <Row>
                <Col md="6" xs="12">
                  <FormGroup>
                    <Label>Response Type</Label>
                    <Input
                      type="select"
                      name="select"
                      className="form-control digits"
                      value={responseType}
                      onChange={(e) => handleType(e)}
                    >
                      <option value="">Select</option>
                      <option value="text">Text</option>
                      <option value="pdf">PDF</option>
                      <option value="video">Video</option>
                    </Input>
                  </FormGroup>
                </Col>
                {!lastClicked ? (
                  <>
                    <Col md="6" xs="12">
                      <FormGroup>
                        <Label for="validationCustom16">
                          {editrowData.intent ? "Response" : "Add Response"}
                        </Label>
                        {/* <textarea>{responseValue}</textarea> */}
                        <Input
                          className="form-control AddandViewResponse"
                          id="validationCustom16"
                          type="textarea"
                          style={{height: 34, padding: "3px 10px"}}
                          value={responseValue}
                          // onKeyPress={(e) => {
                          //   if (e.charCode === 13) {
                          //     let keyArray = [...response];
                          //     keyArray.push(responseValue);
                          //     setResponse(keyArray);
                          //     setResponseValue("");
                          //   }
                          // }}
                          onChange={(e) => handleResponseValue(e)}
                          placeholder="Enter responses"
                        />
                      </FormGroup>
                    </Col>
                  </>
                ) : (
                  <Col md="6" xs="12" className="fileUploadStyleFaqForm">
                    <FormGroup className="m-b-20">
                      <Label for="validationCustom16">
                        {editrowData.intent ? "Response" : "Add Response"}
                      </Label>
                      <div className="m-0 dz-message needsclick">
                        <Dropzone
                          getUploadParams={getUploadParams}
                          onChangeStatus={handleChangeStatus}
                          maxFiles={1}
                          multiple={false}
                          accept={pdf ? "application/pdf" : "video/mp4"}
                          inputContent="Drop files or click to upload."
                          styles={{
                            // dropzone: { width: "100%", height: 25 },
                            dropzoneActive: { borderColor: "green" },
                          }}
                        />
                        <Button
                          color="primary"
                          type="button"
                          className="mr-1"
                          disabled={upload ? false : true}
                          onClick={handleUpload}
                        >
                          Upload
                        </Button>
                      </div>
                    </FormGroup>
                  </Col>
                )}
                <Col md="6" xs="12">
                  <FormGroup className="mb-0">
                    <Label>
                      {editrowData.intent ? "Keyword" : "Add Keyword"}
                    </Label>
                    <Input
                      type="text"
                      className="form-control"
                      rows="3"
                      disabled={editrowData.intent ? true : false}
                      value={keywordEntered}
                      onChange={(e) => handleKeywordEntered(e)}
                      placeholder="Enter Keyword"
                    />
                    {spaceError && (
                      <p
                        style={{
                          color: "red",
                          margin: "0 0",
                          width: "100%",
                          fontSize: 10,
                          marginTop: 5,
                        }}
                      >
                        * Use underscore(_) instead of space.
                      </p>
                    )}
                    <p
                      style={{
                        color: "blue",
                        margin: "0 0",
                        width: "100%",
                        fontSize: 10,
                      }}
                    >
                      Unique search keyword.
                    </p>
                  </FormGroup>
                </Col>
                {!editrowData.intent && (
                  <Col md="6" xs="12">
                    <FormGroup className="mb-0">
                      <Label>Add Category</Label>
                      <Input
                        type="text"
                        className="form-control"
                        rows="3"
                        placeholder="Enter Category"
                        value={category}
                        disabled={editrowData.intent ? true : false}
                        onChange={(e) => setCategory(e.target.value)}
                      />
                    </FormGroup>
                  </Col>
                )}
              </Row>
              {!lastClicked ? (
                <Row>
                  <Col>
                    {/* <FormGroup>
                          <div
                            style={{
                              display: "flex",
                              flexWrap: "wrap",
                              overflowX: "auto",
                            }}
                          >
                            {response &&
                              response.map((item, index) => {
                                return (
                                  <Badge
                                    key={index}
                                    color="primary"
                                    style={{
                                      fontSize: 16,
                                      display: "flex",
                                      alignItems: "center",
                                      marginBottom: 6,
                                    }}
                                  >
                                    <span>{item}</span>
                                    <X
                                      size={"30px"}
                                      style={{
                                        width: "20px",
                                        height: "20px",
                                        marginRight: 0,
                                        position: "static",
                                      }}
                                      onClick={() =>
                                        handleRemoveResponse(index)
                                      }
                                    />
                                  </Badge>
                                );
                              })}
                          </div>
                        </FormGroup> */}
                  </Col>
                </Row>
              ) : (
                ""
              )}
            </CardBody>
            <CardFooter style={{ padding: "20px 30px" }}>
              {!editrowData.intent ? (
                <Button
                  color="primary"
                  type="button"
                  className="mr-1"
                  onClick={handleSubmit}
                  // disabled={spaceErrorSubmit ? true : false}
                  disabled={
                    !spaceError &&
                    faqName &&
                    keywords.length != 0 &&
                    responseType &&
                    keywordEntered &&
                    category &&
                    (responseValue || uploadedFileName[0])
                      ? false
                      : true
                  }
                >
                  Submit
                </Button>
              ) : (
                <Button
                  color="primary"
                  type="button"
                  className="mr-1"
                  onClick={handleUpdate}
                  disabled={
                    keywords.length != 0 &&
                    responseType &&
                    (responseValue || uploadedFileName[0])
                      ? false
                      : true
                  }
                >
                  Update
                </Button>
              )}
              <Button color="light" type="button" onClick={handleClose}>
                Cancel
              </Button>
            </CardFooter>
          </Form>
        </Card>
      </ModalBody>
    </Modal>
  );
};
export default FaqModal;
