# nginx state for serving content
FROM nginx:alpine
# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html
#Remove default nginx file
RUN rm /etc/nginx/conf.d/default.conf
#copy custom nginx config file into nginx conf.d
COPY nginx.conf /etc/nginx/conf.d
# Remove default nginx static assets
RUN rm -rf ./*
# Copy static assets from builder stage
COPY ./build .
# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]
